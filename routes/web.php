<?php

use App\Library\Upload;
use Illuminate\Support\Facades\Artisan;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(
[
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localize' ]
],

function() {

    Route::get('/clear-cache', function() {
        $exitCode = Artisan::call('cache:clear');

        $exitCode = Artisan::call('optimize');

        $exitCode = Artisan::call('route:clear');
        $exitCode = Artisan::call('config:cache');
        $exitCode = Artisan::call('view:clear');
        // return what you want
    });
    Route::get('uploads/thumb/{catalog}/{size}/{tt}/{ta}/{tm}/{file}', function ($catalog, $size_str, $tt, $ta, $tm, $file) {
        $size = Config::get('app.image_sizes.' . $size_str);

        if ($size === null) abort(404);

        $original_path = public_path() . "/uploads/$catalog/$tt/$ta/$tm/$file";
        $new_path = public_path() . '/' . Request::path();
        $new_dir = dirname($new_path);

        $img = Image::cache(function ($image) use ($original_path, $size) {
            return $image->make($original_path)->fit($size[0], $size[1]);
        }, 10, true);
        @mkdir($new_dir, 0777, true);
        $catalog = 'thumb/' . $catalog . '/' . $size_str;
        $img->save($new_path);

        return $img->response();
    });

    /* ***********CART */
    Route::get("/get-cart", "CartController@getCart");

    Route::get("/get-cart-count", "CartController@getCount");

    Route::post("/add-cart", "CartController@addCart");

    Route::post("/change-cart", "CartController@changeCart");

    Route::post("/delete-cart-item", "CartController@deleteItem");

    Route::delete("/clear-cart", "CartController@clear");

    Route::get("/cart", "OrderController@getForm");

    Route::get("/full-cart-part", "OrderController@getPartCart");

    Route::get("/checkout-form", "OrderController@getCheckoutForm");

    Route::post("/checkout-add", "OrderController@postForm");

    Route::post("/checkpromo", "OrderController@checkPromo");

    Route::get("/places", "ShopController@getPlaces");

    Route::get("/map", "PageController@getMap");

    Route::post("/map-init", "PageController@postMapInit");

    Route::post("/payment-action", "OrderController@payment");

    Route::get("/menu-shops", "PageController@getShops");

    Route::post("/online-payment", "OrderController@onlinePayment");

    Route::get("/test-file", function(){
        print_r(upload::getThumbMainProductFile("product", 4, "213x157"));
    });

    /* ***********CART */
    // Route::get('/test', function(){
    //     $expiresAt = Carbon\Carbon::now()->addMinutes(10);

    //     Cache::put('key', 'value', $expiresAt);


    //     $value = Illuminate\Support\Facades\Cache::get('key');
    //     var_dump($value);
    // });

    Route::get('/', 'PageController@getIndex')->name('home');

    // Static pages

    Route::get('/dir/{alias}', 'PageController@getDir');
    Route::get('/page/{alias}', 'PageController@show');

    // All Category
    Route::get('/category/all', 'PageController@getAllCategory');
    Route::get('/category/{alias}', 'PageController@getCategory');

//    Route::get('/brand-shops', 'ShopController@getBrandShops')->name('brand-shops');
//    Route::get('/private-shops', 'ShopController@getPrivateShops')->name('private-shops');
    Route::get('/recommended-shops', 'ShopController@getRecommendedShops')->name('recommended_shops');

    Route::get('/feedback', 'PageController@getFeedback');
    Route::post('/feedback', 'PageController@postFeedback');

    // Product
    Route::get('/products', 'ProductController@getIndex');
    Route::get('/product/category/{alias}','ProductController@getCategory');
    Route::get('/product/category2/{alias}','ProductController@getCategory2');
    Route::get('/product/subcategory/{alias}', 'ProductController@SubCategory');
    Route::get('/product/recommended/','ProductController@getRecommended');
    Route::get('/product/discount/','ProductController@getDiscountProducts');
    Route::get('/product/popular', 'ProductController@getPopular');
    Route::get('/product/change-mode/','ProductController@getChangeMode');
    Route::get('/product/form/subcategory', 'ProductController@getSubCategory');

    Route::get('/product/view/{alias}','ProductController@getView');
    Route::get('/product/all/{alias}','ProductController@getAll');
    Route::get('/shop/get/select', 'ShopController@select2Shop');
    Route::get('/shop/number/{id}','ShopController@getPhone');
    Route::get('/shop/view/{alias}','ShopController@show');
    Route::get('/shop/view/change-mode', 'ShopController@getChangeMode');
    Route::get('/shop/product', 'ShopController@product');
    Route::post('/product/{what}/{id}', 'ProductController@likeOrDislike');
    Route::get('/product/add-favourite/{product_id}','ProductController@addFavourite');
    Route::get('/product/like/{product_id}', 'ProductController@postLike');
    Route::post('/comment/insert', 'CommentController@postComment');



    // Product

	Route::group(['middleware' => 'guest'], function(){
		Route::get('/register', 'UserController@getRegister');
		Route::post('/user-register', 'UserController@postUserRegister');
		Route::post('/seller-register', 'UserController@postSellerRegister');
		Route::get('/login', 'UserController@getLogin');
		Route::post('/ajax-login', 'UserController@postAjaxLogin');
		Route::get('/form-confirm-code', 'UserController@postFormConfirmCode');
		Route::post('/resend-code', 'UserController@postResendCode');
		Route::post('/check-code', 'UserController@postVarificate');
	});

	Route::group(['middleware' => ['auth']], function(){
		Route::get('/cabinet', 'UserController@getProfile');
		Route::get('/logout', 'UserController@getLogout');

		// Cabinet
		Route::get('/cabinet/product', 'CabinetController@getIndex');
		Route::get('/cabinet/favorite', 'CabinetController@getFavorite');
		Route::get('/cabinet/shop', 'CabinetController@getShops');
		Route::get('/cabinet/reviews', 'CabinetController@getReviews');
		Route::get('/cabinet/user/reviews', 'CabinetController@getUserReviews');
		Route::get('/cabinet/profile', 'CabinetController@getProfile');
		Route::post('/cabinet/profile', 'CabinetController@postProfile');
		Route::post('/cabinet/password', 'CabinetController@postChangePassword');

		// delete favorite product
		Route::post('cabinet/favorite/delete', 'CabinetController@postDelete');

        // Cabinet Shop
		Route::get('/cabinet/shop', 'ShopController@getForm');
		Route::post('/cabinet/shop', 'ShopController@postForm');

		// Cabinet Product
		Route::get('/cabinet/product/form/{id?}', 'ProductController@getForm');
		Route::post('/cabinet/product/form/{id?}', 'ProductController@postForm');
		Route::post('/cabinet/index/delete', 'ProductController@postDelete');

        Route::post('/cabinet/product/send-file', 'ProductController@uploadFile');
        Route::post('/cabinet/product/remove-file', 'ProductController@removeFile');


		/* ORDER*/
		Route::get("/cabinet/orders",'CabinetController@getOrders');
		Route::get("/cabinet/my-orders",'CabinetController@getMyOrders');
		Route::get("/cabinet/my-order",'CabinetController@getMyOrder');
		Route::post("/cabinet/part-orders",'CabinetController@getOrdersPart');
		Route::post("/cabinet/chage-status",'CabinetController@postStatus');
		Route::get('/cabinet/my-balans', 'CabinetController@getMyBalans');


		Route::get('/click-api', 'ClickController@action');
	});






    // Admin
    Route::group(['prefix' => 'mypanel', 'middleware' => ['admin','role:admin'], 'namespace' => 'Backend'], function() {
        Route::get('/', 'PageController@getDashboard');
        Route::get('/page', 'PageController@getIndex');
        Route::get('/page/form/{id?}', 'PageController@getForm');
        Route::post('/page/form/{id?}', 'PageController@postForm');
        Route::post('/page','PageController@postDelete');


		/*
		|--------------------------------------------------------------------------
		| User Routes
		|--------------------------------------------------------------------------
		*/
        Route::get('/user', 'UserController@getIndex');
        Route::get('/user/form/{id?}', 'UserController@getForm');
        Route::post('/user/form/{id?}', 'UserController@postForm');
        Route::post('/user', 'UserController@postDelete');
        Route::get('/user/data', 'UserController@getData');

		/*
		|--------------------------------------------------------------------------
		| Salesman Routes
		|--------------------------------------------------------------------------
		*/
        Route::get('/salesman', 'SalesmanController@getIndex');
        Route::get('/salesman/form/{id?}', 'SalesmanController@getForm');
        Route::post('/salesman/form/{id?}', 'SalesmanController@postForm');
        Route::post('/salesman', 'SalesmanController@postDelete');
        Route::get('/salesman/data', 'SalesmanController@getData');


		/*
		|--------------------------------------------------------------------------
		| Shop Routes
		|--------------------------------------------------------------------------
		*/
        Route::get('/shops','ShopController@getIndex');
        Route::get('/shop/{id}','ShopController@getShow');
        Route::get('/shops/form/{id?}', 'ShopController@getForm');
        Route::post('/shops/form/{id?}', 'ShopController@postForm');
        Route::post('/shops', 'ShopController@postDelete');
        Route::get('/shops/data', 'ShopController@getData');
        Route::get('/shops/getFind', 'ShopController@getFind');
        Route::post('/shop/status', 'ShopController@postStatus');

			/*
		|--------------------------------------------------------------------------
		| Product Routes
		|--------------------------------------------------------------------------
		*/
        Route::get('/product', 'ProductController@getIndex');
        Route::get('/product/form/{id?}', 'ProductController@getForm');
        Route::post('/product/form/{id?}', 'ProductController@postForm');
        Route::post('/product', 'ProductController@postDelete');
        Route::get('/product/data', 'ProductController@getData');
        Route::get('/product/change/{id?}/{whatToChange}',
										'ProductController@getChange');

		/*
		|--------------------------------------------------------------------------
		| Product Categories routes
		|--------------------------------------------------------------------------
		*/
        Route::get('/category', 'CategoryController@getIndex');
        Route::get('/category/form/{id?}', 'CategoryController@getForm');
        Route::post('/category/form/{id?}', 'CategoryController@postForm');
        Route::post('/category', 'CategoryController@postDelete');
        Route::get('/category/data', 'CategoryController@getData');

        Route::get('/category/parents','CategoryController@parentId');
        Route::get('/category/parent/{id}', 'CategoryController@parentIndex');
        Route::get('/category/parent-add/{id}', 'CategoryController@addParent');
        Route::post('/category/parent-add/{id}', 'CategoryController@addParent');




		/*
		|--------------------------------------------------------------------------
		| Comment Routes
		|--------------------------------------------------------------------------
		*/
        Route::get('/comments', 'CommentController@getIndex');
        Route::get('/comments/form/{id?}', 'CommentController@getForm');
        Route::post('/comments/form/{id?}', 'CommentController@postForm');
        Route::post('/comments/delete', 'CommentController@postDelete');
        Route::get('/comments/data', 'CommentController@getData');

		/*
		|--------------------------------------------------------------------------
		| Comment Routes
		|--------------------------------------------------------------------------
		*/

		Route::get('/feedback', 'FeedbackController@getIndex');
		Route::get('/feedback/data','FeedbackController@getData');
		Route::post('/feedback', 'FeedbackController@postDelete');

		// Filemanager
		Route::get('filemanager', 'FileManagerController@getIndex');

		Route::get('/orders', 'OrderController@getIndex');
		Route::get('/order/show/{id}', 'OrderController@getShow');
		Route::post('/order/status', 'OrderController@postStatus');
		Route::post('/order/delete', 'OrderController@postDelete');
		Route::get('/order/infomap', 'OrderController@getInfoOrderForMap');
		Route::post('/order/paystatus', 'OrderController@postPaystatus');

		Route::get("/promo", "PromoCodeController@getIndex");
		Route::get("/promo/form/{id?}", "PromoCodeController@getForm");
		Route::post("/promo/form/{id?}", "PromoCodeController@postForm");
		Route::post("/promo/delete", "PromoCodeController@postDelete");

		Route::get('/service/show', 'SettingController@getShow');
		Route::get('/service/form', 'SettingController@getForm');
		Route::post('/service/form', 'SettingController@postForm');

		Route::get('/size-product', 'SizeProductController@getShow');
		Route::get('/size-product/form', 'SizeProductController@getForm');
		Route::post('/size-product/form', 'SizeProductController@postForm');
		Route::post("/size-product/delete", "SizeProductController@postDelete");

		Route::get('/icon', 'IconController@getShow');
		Route::get('/icon/form', 'IconController@getForm');
		Route::post('/icon/form', 'IconController@postForm');
		Route::post("/icon/delete", "IconController@postDelete");

        Route::get('/key', 'KeyController@getShow');
        Route::get('/key/form/{id?}', 'KeyController@getForm');
        Route::post('/key/form/{id?}', 'KeyController@postForm');

        Route::get('/products/sort-setting', 'ProductController@getSort');
        Route::post('/products/sort-setting', 'ProductController@getSort');


        Route::get("/banners", "BannerController@getIndex");
        Route::get("/banners/form/{id?}", "BannerController@getForm");
        Route::post("/banners/form/{id?}", "BannerController@postForm");
        Route::post("/banners/delete", "BannerController@postDelete");

        Route::get("/sliders", "SliderController@getIndex");
        Route::get("/sliders/form/{id?}", "SliderController@getForm");
        Route::post("/sliders/form/{id?}", "SliderController@postForm");
        Route::post("/sliders/delete", "SliderController@postDelete");

        Route::get("/information", "InformationController@getIndex");
        Route::get("/information/form/{id?}", "InformationController@getForm");
        Route::post("/information/form/{id?}", "InformationController@postForm");
        Route::post("/information/delete", "InformationController@postDelete");

	});
});
