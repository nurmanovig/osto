<?php

function getSortItems(){

    if(!is_file(SORT_ITEMS)){
        return [];
    }
    $data = file_get_contents(SORT_ITEMS);
    return json_decode($data, true);
}

function setSortItems($data){
    foreach($data as $key => $item){
        if(empty($item)){
            unset($data[$key]);
        }
    }

    $dir = pathinfo(SORT_ITEMS, PATHINFO_DIRNAME);

    if(!is_dir($dir)){
        mkdir($dir);
    }
    file_put_contents(SORT_ITEMS, json_encode($data));
}

function salePrice($actual_price, $sale){

    $price =   ceil($actual_price * (100 - $sale)/100);
    return number_format($price, 0, ".", " ");
}

function substrName($str, $length = 8){
    $current_length = mb_strlen($str);
    
    
    if($current_length > $length){
        $str = mb_substr($str, 0, $length)."..";
    }
    return $str;
}

function workingHours(){
    $arr = [];
    for($i=0; $i<24; $i++){
        $arr[] = $i>9 ? $i : "0".$i;
    }

    return $arr;
}

?>