<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    const STATUS_NOTACTIVE = 0;
    const STATUS_ACTIVE = 1;


    /**
     * Many-to-many relationships between users and roles; 
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }

    // Likes relation

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'lastname', 'phone', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relation with Shop
     *
    */
    
    public function shops(){
        return $this->hasMany('App\Models\Shop');
    }

    public function product(){
        return $this->hasOne('App\Models\Product', 'user_id', 'id');
    }
    
    // public function products(){
    //     return $this->hasMany('App\Models\Product', 'user_id', 'id');
    // }
    
    public function productsId(){
        return Product::select('id')->where('user_id', $this->id)->get()->toArray();
    }

    public function orders(){
        return $this->hasMany('App\Models\Order', 'user_id', 'id');
    }

    public function stoppedOrders(){
        return $this->hasMany('App\Models\Order', 'user_id', 'id')->where('status', Order::STATUS_STOPPED);
    }

    public function paidOrders(){
        return $this->hasMany('App\Models\Order', 'user_id', 'id')->where('pay_status', Order::PAY_STATUS_PAID);
    }

    public function finishedOrders(){
        return $this->hasMany('App\Models\Order', 'user_id', 'id')->where('status', Order::STATUS_FINISHED);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function productComments()
    {
        return $this->hasManyThrough(
            'App\Models\Comments', 'App\Models\Product',
            'user_id', 'product_id',
            'id', 'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function productOrders()
    {
        return $this->hasManyThrough(
            'App\Models\OrderProduct', 'App\Models\Product',
            'user_id', 'product_id',
            'id', 'id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function productFavourites()
    {
        return $this->hasManyThrough(
            'App\Models\Favourite', 'App\Models\Product',
            'user_id', 'product_id',
            'id', 'id'
        );
    }


    public function countOrderByType(){

        // if(Cache::has("count_order_".$this->id)){
        //     return Cache::get("count_order_".$this->id);
        // }else{

        
    // const STATUS_STOPPED = -2;
    // const STATUS_FAIL = -1;
    // const STATUS_INACTIVE = 0;
    // const STATUS_PAID = 1;
    // const STATUS_FINISHED = 3;
        $statusArr = [
                    [0, 'Отменённые', [Order::STATUS_FAIL, Order::STATUS_INACTIVE, Order::STATUS_STOPPED]], 
                    [0, 'В ожидании', [Order::STATUS_STARTED]],
                    [0,'Успешные', [Order::STATUS_FINISHED]]
                ];

        $result =  Order::select(DB::raw('count(*) as count, status'))->where('user_id', $this->id)->groupBy('status')->get();

        foreach($result as $item){
            foreach($statusArr as $key=> $status){
                if(array_search($item->status, $status) !== false){
                    $statusArr[$key][0]  += $item->count;
                }
            }

        }
        
        // Cache::put("count_order_".$this->id, $statusArr, 100);
        
        return $statusArr;
        // }
    }


    public function countSolds(){
        $result = OrderProduct::select(DB::raw("sum(order_product.count) count"))
            ->join("orders", "order_id", "orders.id")
            ->join("products", "product_id", "products.id")
            ->where("products.user_id", $this->id)
            ->where("orders.status", Order::STATUS_FINISHED)
            ->where("orders.pay_status", Order::PAY_STATUS_PAID)
            ->where("order_product.status", OrderProduct::STATUS_READY)
            ->first();

        return $result->count ? : 0;
    }
}
