<?php
namespace App\Library;
use Faker\Provider\Image;
use ImageIn;
use Imagine;

class Upload
{
	private static
		$alphabet=['T','o','m','u','z','j', 'p', '_', 'B', 'f', 'S', 'c', 'O', '6', '-', '3', 'R', 'w', 'l', 'A', 'M',  'I', 'J', 'k', 'y', 'E', 'v', '4', '5', '2',  'C',  'H',  'K', 'b', 'W', '7', 'q', 'P', 'U', 's', 'h', 'V', 'Q', '0', 'G', 'n', 'F', 'x', 'e', 't',  'Y', '1', 'd', '8', 'L', 'i', 'a', 'D', 'Z', 'g', 'X', 'N', '9', 'r'],
		$fliped_alphabet= ['T' => 0, 'o' => 1, 'm' => 2, 'u' => 3, 'z' => 4, 'j' => 5, 'p' => 6, '_' => 7, 'B' => 8, 'f' => 9, 'S' => 10, 'c' => 11, 'O' => 12, 6 => 13, '-' => 14, 3 => 15, 'R' => 16, 'w' => 17, 'l' => 18, 'A' => 19, 'M' => 20, 'I' => 21, 'J' => 22, 'k' => 23, 'y' => 24, 'E' => 25, 'v' => 26, 4 => 27, 5 => 28, 2 => 29, 'C' => 30, 'H' => 31, 'K' => 32, 'b' => 33, 'W' => 34, 7 => 35, 'q' => 36, 'P' => 37, 'U' => 38, 's' => 39, 'h' => 40, 'V' => 41, 'Q' => 42, 0 => 43, 'G' => 44, 'n' => 45, 'F' => 46, 'x' => 47, 'e' => 48, 't' => 49, 'Y' => 50, 1 => 51, 'd' => 52, 8 => 53, 'L' => 54, 'i' => 55, 'a' => 56, 'D' => 57, 'Z' => 58, 'g' => 59, 'X' => 60, 'N' => 61, 9 => 62, 'r' => 63, ];
	private static $index=[];
	
	private static function getPathArr($catalog,$id)
	{
		$encoded = self::encode($id);
		$path = str_split(str_repeat(self::$alphabet[0], 6-strlen($encoded)).$encoded,2);
		array_unshift($path, 'uploads', $catalog);
		return $path;
	}
	
	public static function getPath($catalog,$id)
	{
		static::createDir("uploads/".$catalog."/".$id);
		return "/uploads/".$catalog."/".$id;
		return '/'.implode('/', self::getPathArr($catalog,$id));
	}

	private static function encode($int)
	{
		$val='';
		$len=64;
		$mod=0;
		while($int>0)
		{
			// $mod=$int%$len;
			$mod=$int-floor($int/$len)*$len;
			$int=($int-$mod)/$len;
			$val=self::$alphabet[$mod].$val;
		}
		return $val;
	}
	private static function decode($str)
	{
		$val=0;
		foreach(array_reverse(str_split($str)) as $ind=>$char)
		{
			if(isset(self::$fliped_alphabet[$char]))
			{
				$val+=self::$fliped_alphabet[$char]*pow(64, $ind);
			}
		}
		return $val;
	}
	public function postTest()
	{

		// self::saveFiles('images',10,Input::file('files'));
		foreach(Input::file('files') as $ind=>$file)
		{
			self::saveFile('profile',11+$ind,$file);
			var_dump(self::getFile('profile',11+$ind));
		}
			var_dump(self::getFile('profile',1));
		//
		 // print_r(self::getFiles('images',10));

	}
	
	public static function getFile($catalog,$id,$filter=null)
	{
		$files=self::getFiles($catalog,$id,$filter);
		if($files)
			return $files[0];
		return null;
	}

	public static function getProductMainFile($catalog, $id, $name = null){
		$files=self::getFiles($catalog,$id);
		print_r($files);		
	}


	public static function getFiles($catalog,$id,$filter=null)
	{
		if(isset(self::$index[$catalog.$id]))
			return self::$index[$catalog.$id];

		if(is_array($filter))
			$filter='*.{'.implode(',', $filter).'}';
		else
			$filter='*';

		//TMP FOR TRANS
		$files=[];
		// if($catalog=='elon')
		// {
		//   $ad=Advert::select('images')->find($id);
		//   if($ad && $ad->images!='')
		//     $files=json_decode($ad->images);
		//   foreach($files as $ind=>$file)
		//     $files[$ind]='http://tom.uz/'.$file;
		// }
		// if(!$files)
		{
			// Log::info($files);
			$files=glob(public_path().self::getPath($catalog,$id).'/'.$filter,GLOB_BRACE);

			foreach($files as $ind=>$val)
				$files[$ind]=str_replace(public_path(), '', $val);
		}

		self::$index[$catalog.$id]=$files;

		// file_put_contents("file.text", public_path());
		return $files;
	}

	public static function saveFile($catalog,$id,$file, $size = [])
	{
		//Перед сохранением одного файла удаляю предыдущие версии файлов
		self::removeFiles($catalog,$id);
		$path=public_path().self::getPath($catalog,$id).'/';
		$name=$id."_".time();
		$ex=$file->getClientOriginalExtension();
		$file->move($path,$name.'.'.$ex);
		$src = $path.$name.'.'.$ex;
//		 if(!empty($size)){
//		 	foreach($size as $item){
//		 		$width = $item[0];
//		 		$height = $item[1];
//		 		$file_path = preg_replace('/.*uploads\/'.$catalog.'/', '', $path);
//		 		$file_path = 'uploads/thumb/'.$catalog.'/'.$width."x".$height.$file_path;
//
//		 		static::clearDir($file_path);
//		 		$dest = $file_path .$name.'.'.$ex;
//
//		 		static::img_resize($src, $dest, $width, $height,  0xFFFFFF,100);
//		 		return $dest;
//		 	}
//		 }
		
		return $src;
	}

	public static function saveFiles($catalog,$id,$orig_files, $size = [])
	{

		$path=public_path().self::getPath($catalog,$id).'/';
		$name=$id."_".time();
		$i = 0;
		$files = [];
		foreach($orig_files as $ind=>$file)
		{
			$ex=$file->getClientOriginalExtension();
			$file->move($path,$name.$ind.'.'.$ex);
			$files[] = $name.$ind.'.'.$ex;

			sleep(1);
		}

		return $files;
	}

	public static function saveFilesForProduct($catalog,$id,$orig_files, $size = []){
		$path=public_path().self::getPath($catalog,$id).'/';
		$name=$id;
		$i = 0;	
		$size_image_doctype = true;

		foreach ($size as $key => $item) {
			$width = $item[0];
			$height = $item[1];
			if($fmain_files = static::getThumbMainProductFile($catalog, $id, $width."x".$height)){

				$size_image_doctype = false;
				break;
			}
		}

		foreach($orig_files as $ind=>$file)
		{
			$ex=$file->getClientOriginalExtension();
			$file->move($path,$name."_".$ind.'.'.$ex);
			if($size_image_doctype){
				if($ind == 0){
				$src = $path."/".$name."_".$ind.'.'.$ex;
				if($size_image_doctype){
					foreach($size as $item){
						$width = $item[0];
						$height = $item[1];


						$file_path = preg_replace('/.*uploads\/'.$catalog.'/', '', $path);
						$file_path = 'uploads/thumb/'.$catalog.'/'.$width."x".$height.$file_path;
						file_put_contents("path.text", $width."x".$height);
						// static::clearDir($file_path);
						$dest = $file_path .$name."_".$ind.'.'.$ex;

						static::img_resize($src, $dest, $width, $height,  0xFFFFFF,100);
					}
				}

				
			}
			}
			

			static::img_resize($path."/".$name."_".$ind.'.'.$ex, $path."/".$name."_".$ind.'.'.$ex, 532, 532,  0xFFFFFF,100);
			
		}
	}



	public static function saveFilesW($catalog,$id,$orig_files)
	{

		$path=public_path().self::getPath($catalog,$id).'/';
		$name=time();

		foreach($orig_files as $ind=>$file)
		{
			$ex=$file->getClientOriginalExtension();
			$file->move($path,$name.$ind.'.'.$ex);
			$img = Image::make($path.$name.$ind.'.'.$ex);

			$watermark = Image::make(public_path('assets/img/watermark.png'));
			$watermark->resize($img->width()*0.5, null, function ($constraint) {
					$constraint->aspectRatio();
			});

			$img->insert($watermark, 'center');
			$img->save($path.$name.$ind.'.'.$ex);
		}
	}

	public static function getImage($catalog,$id,$class='')
	{
		$file = self::getFile($catalog,$id);
		if($file)
			return '<img src="'.$file.'" class="'.$class.'"/>';
		return'<div class="no-image"></div>';
	}

	public static function getThumbImage($catalog, $id, $size,$class='')
	{
		$file = self::getFile($catalog,$id);
		$file =str_replace('uploads/'.$catalog.'/', '', $file);
		
		if($file){
			// $ext = preg_replace('|^.*(\.\w+)$|is', '$1', $file);
			return '<img src="/uploads/thumb/'.$catalog.'/'.$size.$file.'" alt="" class="'.$class.'"/>';
		}
		return'';
	}

	public static function getThumbFiles($catalog, $id, $size){
			$files = self::getFiles($catalog,$id);
			$new_files = [];
			foreach($files as $ind=>$file){
				$file = str_replace('uploads/'.$catalog.'/', '', $file);
				$filename = '/uploads/thumb/'.$catalog.'/'.$size.$file;
				if( file_exists(public_path().$filename))
					$new_files[] = $filename;
			}
			return $new_files;
	}

	public static function getThumbMainProductFile($catalog, $id, $size){
        $files = self::getFiles($catalog, $id);
        $new_files = [];

        foreach($files as $ind => $file){
            $file = str_replace('uploads/'.$catalog.'/', '', $file);
            $filename = '/uploads/thumb/'.$catalog.'/'.$size.$file;
            if( file_exists(public_path().$filename)){
                return [$filename];
            }
        }

        return null;
	}

	public static function getThumbFilesNew($catalog, $id, $size){
			$files = self::getFiles($catalog,$id);
			$new_files = [];
			foreach($files as $ind=>$file){
				$file = str_replace('uploads/'.$catalog.'/', '', $file);
				$new_files[] = '/uploads/thumb/'.$catalog.'/'.$size.$file;
			}
			return $new_files;
	}

 	public static function getThumb($catalog, $id, $size){
			$files = self::getFiles($catalog,$id);
			if($files){
				return '/uploads/thumb/'.$catalog.'/'.$size.$files[0];
			}else{
				return null;
			}
	}

	public static function getImages($catalog,$id,$class='')
	{
		$files = self::getFiles($catalog,$id);
		$res='';
		foreach($files as $ind=>$file)
				$res.= '<img src="'.$file.'" class="'.$class.'"/>';

		return $res;
	}

	public static function hasFiles($catalog,$id)
	{
		return count(self::getFiles($catalog,$id))>0;
	}
	public static function hasFile($catalog,$id)
	{
		return self::getFile($catalog,$id)!==null;
	}
	public static function removeFile($catalog,$id){
		self::removeFiles($catalog,$id);
	}
	public static function removeFiles($catalog,$id,$file_names=null)
	{
		$files=[];
		if($file_names===null)
			$files=self::getFiles($catalog,$id);
		else
		{
			$path=self::getPath($catalog,$id);
			foreach($file_names as $ind=>$file)
				$files[]=$path.'/'.basename($file);
		}
		foreach($files as $file){
			unlink(public_path().$file);
			
			$width = 208;
			$height = 200;
			$path = preg_replace('/.*uploads\/'.$catalog.'/', '', $file);
			$path = 'uploads/thumb/'.$catalog.'/'.$width."x".$height.$path;
			if(file_exists($path)){
				chmod(pathinfo($path, PATHINFO_DIRNAME)."/", 0777);
				unlink($path);
			}
			
		}



	}

	public static function removeFilesBySize($catalog,$id)
	{
		$files=[];
		$files=self::getFiles($catalog,$id);
		

		$sizes = require public_path()."/../config/image-size.php";
        
        if(!isset($sizes[$catalog]))
        	return;

		foreach($files as $file){
			unlink(public_path().$file);
			
			foreach ($sizes[$catalog] as  $item) {
				$path = preg_replace('/.*uploads\/'.$catalog.'/', '', $file);
				$path = 'uploads/thumb/'.$catalog.'/'.$item[0]."x".$item[1].$path;
				if(file_exists($path)){
					chmod(pathinfo($path, PATHINFO_DIRNAME)."/", 0777);
					chmod($path, 0777);
					try{
						unlink($path);
					}catch(\Exception $e){
						continue;
					}
				}
			}
			
			
		}
				

	}


	public static function getImageSize($link,$width,$height)
	{
		$path=public_path().$link;
		list($real_width, $real_height) = getimagesize($path);
		$w_d=$width/$real_width;
		$h_d=$height/$real_height;
		$str='<img src="'.$link.'" ';
		if($w_d<$h_d)
			$str.="height=\"$height\"";
		else
			$str.="width=\"$width\"";
		return $str." />";
	}

	public static function getBlogThumb($catalog, $id, $size)
	{
		$file = self::getFile($catalog,$id);
		$file =str_replace('uploads/'.$catalog.'/', '', $file);

		if($file){
			// $ext = preg_replace('|^.*(\.\w+)$|is', '$1', $file);
			return "/".$catalog."/".$size."".$file;
		}
		return '';
	}


	public static function clearDir($dir){
		if (is_dir($dir)) {
		    if ($dh = opendir($dir)) {
		        while (($file = readdir($dh)) !== false) {
		            if($file == "." || $file == "..")
		            	continue;
		            chmod($dir."/", 0777);
		            unlink($dir."/".$file);
		        }
		        closedir($dh);
		    }
		}
	}	

	public static function createDir($dir){
		$arr = explode("/", $dir);
		$str = public_path();
		foreach ($arr as $key => $item) {
			$str .= "/".$item;
			if(!is_dir($str)){
			    try{
				mkdir($str);
			   
				chmod($str, 0777);
			    }catch(\Exception $e){
			       
			    }
			}
		}

	}

	public static function deleteImage($path){
        $files = static::readDirFile($path);

        if(!$files)
            return;

        foreach($files as $file){
            unlink($file);
        }
    }
    public static function readDirFile($path){
        $files = [];
        if(is_dir($path)){
            if ($handle = opendir($path)) {
                while (false !== ($file = readdir($handle))) {
                    if($file == "." || $file == "..")
                        continue;

                    $files[] = ($path."/".$file);
                }
            }
        }
        sort($files);
        return $files;
    }

    public static function hasImage($fullname){
    	
    	if(is_file(public_path().$fullname)){
    		return $fullname;
    	}

    	return false;
    }

    public static function hasImageBySize($dir, $id, $size){

    }



	public static function reSizeNew($filename,  $path = null, $w = null, $h = null, $procent = null){
		$type = exif_imagetype($filename);

		switch ($type) {
			case IMAGETYPE_JPEG:
				$im_res = imagecreatefromjpeg($filename);
				break;
			case IMAGETYPE_GIF:
				$im_res = imagecreatefromgif($filename);
				break;
			case IMAGETYPE_PNG:
				$im_res = imagecreatefrompng($filename);
				break;

			default:
				return false;
				break;
		}
		
		
		$old_width = imagesx($im_res);
		$old_height = imagesy($im_res);

		if($w == null){
			if($procent){
				$w = $old_width * $procent / 100;
			}else{
				$w = $old_width;

			}

		}else{
			$w = (int)$w;	
		}

		if($h == null){
			if($procent){
				$h = $old_height * $procent / 100;
			}else{
				$h = $old_height;
			}
		}else{
			$h = (int)$h;	
		}
		
		
		$propor = static::getProportion($old_width, $old_height, $w, $h);
		$new_res = imagecreatetruecolor($w,  $h);
		imagecopyresampled($new_res, $im_res, 0,0,(($old_width - $propor['width'])/2),(($old_height - $propor['height'])/2),$w, $h, $propor['width'], $propor['height'] );
		
		
		
		
		switch ($type) {
			case IMAGETYPE_JPEG:
				imagejpeg($new_res, $path, 100);
				
				break;
			case IMAGETYPE_GIF:
				imagegif($new_res, $path);
				
				break;
			case IMAGETYPE_PNG:
				imagepng($new_res, $path);
				break;

			default:
				return false;
				break;
		}
		imagedestroy($new_res);

		if($path != null )
			return $path;
		
	}

	public static function getProportion($old_width, $old_height, $new_width, $new_height){
		$old_diagonal = $old_width / $old_height;
		$new_diagonal = $new_width / $new_height;

		if($old_diagonal > $new_diagonal){
			$width = $old_height * $new_diagonal;
			$height = $old_height;
		}else{
			$height = $old_width / $new_diagonal;
			$width = $old_width;
		}

		return ["width" => $width, "height" => $height];
	}

    public static function resize($catalog, $id, $name){
        $sizes = require public_path()."/../config/image-size.php";
        
        if(!isset($sizes[$catalog]))
        	return;
        foreach($sizes[$catalog] as $size){
            
            Upload::deleteImage(public_path()."/uploads/thumb/".$catalog."/".$size[0]."x".$size[1]."/".$id."/");
            
            $fullname = public_path()."/uploads/".$catalog."/".$id."/".$name;
            $resize_path = "uploads/thumb/".$catalog."/".$size[0]."x".$size[1]."/".$id;

            Upload::createDir($resize_path);

            
            try{
            	static::reSizeNew($fullname, public_path().'/'.$resize_path."/".$name, $size[0], $size[1]);

            }catch(\Exception $e){
                
            }
        }
    }

    public static function hasMainImage($category, $id, $name = null){
        $files = Upload::readDirFile(public_path()."/uploads/".$category."/".$id);
        // $files = Upload::getFiles("product", $id);
        
        if(!$name)
            return [
                    "error" => false, 
                    "name"=>isset($files[0]) ? $files[0]: null
                    ];
        
        foreach($files as $file){
            $file_name = pathinfo($file, PATHINFO_BASENAME);
            if($file_name == $name)
                return true;
        }

        return [
                    "error" => false, 
                    "name"=>isset($files[0]) ? $files[0]: null,
                    "files" => $files
                    ];
    }

}
