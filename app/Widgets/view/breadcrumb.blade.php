<ol class="breadcrumb">
	{!! $i = count(pages)!!}
	@foreach($pages as $key => $page)
		{!! $i --;!!}
		@if($i != 0)
			<li><a href="{{$page['link']}}">{{$page['name']}}</a></li>		
		@else
  			<li class="active">{{$page['name']}}</li>

		@endif
	@endforeach
  
</ol>