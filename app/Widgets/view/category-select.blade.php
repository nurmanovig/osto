@widget("category-select", [["category" => $parent_id, 'first' => false]])
<div class="col-md-6 category_list" data-id="{{ $parent_id}}">
	<div class="form-group" id="woops">
	<label class="input-label">{{$parent_id == 0 ? 'Категория' : 'Выберите подкатегорию'}}</label>
		<div class="select-container">
			<select class="input input--gray child" {{ $first ? 'name=category_id' : null}}>
				<option value=0>{{$parent_id == 0 ? 'Выберите категорию' : 'Выберите подкатегорию'}}</option>
				@foreach($category as $item)

					<option value="{{$item->id}}" {{ $selected == $item->id ? "selected" : null}}>{{ $item->title}}</option>

				@endforeach
			</select>

			@if($first && $errors->has('category_id'))
			<span class="form-group__label form-group__label--error">
				{{ ($errors->get('category_id')[0]) }}
			</span>
			@endif
		</div>
	</div>
</div>

