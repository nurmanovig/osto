<div class="form-group opened">
    <div class="form-group-title">Категории <i class="dropdown__icon icon-angle-down"></i></div>
    <div class="form-group-collapse">
        <nav class="categories-list">
            <ul>
                <?php
                foreach($categories as $category):
                    $class = "";
                     if (count($category->childsShortInfo)) {
                         $class .= " has-child";
                     }

                     if ($category->id == $active) {
                         $class .= " active";
                     } else {
                         foreach($category->childsShortInfo as $subcategory) {
                             if ($subcategory->id == $active) {
                                 $class .= " active";
                             }
                         }
                     }

                    ?>
                    <li class="<?=$class?>">
                        <a href="/product/category/<?=$category->alias . ($sale ? "?sale=1" : null)?>"> <?=$category->title ?> </a>
                        <ul>
                            <?php foreach($category->childsShortInfo as $subcategory):?>
                                <li>
                                    <a href="/product/category/<?=$subcategory->alias. ($sale ? "?sale=1" : null)?>" class="sk-checkbox">
                                        <span class="sk-checkbox-text"><?=$subcategory->title?></span>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                <?php endforeach ?>

            </ul>
        </nav>
    </div>
</div>