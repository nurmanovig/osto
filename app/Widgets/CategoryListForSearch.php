<?php 
namespace App\Widgets;

use App\Widgets\Contract\ContractWidget;
use App\Menu;

use App\Models\Category;

class CategoryListForSearch implements ContractWidget{

    protected $category_id = 0;

	private $ids = [];

    private $all_category = false;
    
    private $sale = null;
    /**
     * CategoryListForSearch constructor.
     * @param array $data
     */
	public function __construct($data = []) {
	    if (isset($data['category_id'])) {
	        $this->category_id = $data['category_id'];
	    }
	    
	    if (isset($data['sale'])) {
	        $this->sale = $data['sale'];
	    }

        if (isset($data['all_category'])) {
            $this->all_category = $data['all_category'];
        }
 	}

	public function execute() {
	    $params  = [];
	    if($this->sale){
	        $params['sale_status'] = $this->sale;
	    }
	    
	    if( $this->all_category){
	        $categories = $this->getAllCategory($params);
        }else{
            $category = $this->getCategory($this->category_id, $params);
            $categories = isset($category->childsShortInfo) ? $category->childsShortInfo : null;
        }


        if (!$categories) {
            return null;
        }

	    return view('Widgets::category-list-for-search', [
            "categories" => $categories,
            'active' => $this->category_id,
            'sale' => $this->sale
        ]);
	}

    /**
     * @return mixed|null|static
     */
	public function getCategory($id, $params = null) {
        $category = Category::select("categories.*")->where('categories.id',$id);
        // $category = $category->join('products', 'categories.id', '=', 'category_id');
        // if($params){
            
        //     foreach($params as $key => $value){
        //         $category = $category->where($key, $value);
        //     }
            
        // }
        $category = $category->first();
        if (!$category) {
            return null;
        }

        if ($category->parent_id) {
            $category = $this->getCategory($category->parent_id, $params);
        }

        return $category;
    }

    public function getAllCategory($params = null){
        $query = Category::select("categories.*")->where('parent_id', '=', 0);
        // if($params){
         
        //     $query->join('products', 'categories.id', '=', 'category_id');
        //     foreach($params as $key => $value){
                 
        //         $query->where($key, $value);
        //     }
            
        // }
        
        return $query->get();
        
    }
    
    private function getShopIdsByProducts($params){
        
    }

}

?>