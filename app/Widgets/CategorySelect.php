<?php 
namespace App\Widgets;

use App\Widgets\Contract\ContractWidget;
use App\Menu;

use App\Models\Category;

class CategorySelect implements ContractWidget{
	protected $category = null;
	protected $first = false;
	public function __construct($data = []){
	    if (isset($data['category'])){
	        $this->category = $data['category'];
	    }

	    if(isset($data['first'])){
	    	$this->first = $data['first'];
	    }
 	} 

	public function execute(){
		if($this->category == 0)
			return;
		
		if(!($array = $this->getList())){
			return false;
		}
		$selected = $this->category;
		list($category, $parent_id) = $array;

	    return view('Widgets::category-select', 
	    					[
	    						"category" => $category, 
	    						"parent_id" => $parent_id, 
	    						"selected" => $this->category, 
	    						'first' => $this->first
	    					]);
	} 

	public function getList(){
		$category = Category::where("id", $this->category)->with("parent")->first();

		// if(empty($category) || $category->parent_id == 0)
		// 	return false;

		return [Category::where("parent_id", $category->parent_id)->get(), $category->parent_id];

	}
}

?>