<?php
namespace App\Widgets;

use App\Models\Banners;
use App\Widgets\Contract\ContractWidget;
use App\Menu;

class BannerWidget implements ContractWidget{


    public function execute(){
        $banner = Banners::inRandomOrder()->where('actived_at', '>=', date("Y-m-d h:i:s"))->first();

        if(!$banner){
            return;
        }


        return view('Widgets::banner', [
            'banner' => $banner
        ]);
    }
}

?>