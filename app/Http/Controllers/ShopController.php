<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Library\Uploads;

use App\Models\MallCategory;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Mall;

use Validator;
use Response;
use Session;
use Upload;
use Input;
use Lang;
use Auth;
use Log;

class ShopController extends Controller
{


	public function getRecommendedShops(Request $request)
	{
		$page = [
			'title'	=> 'РЕКОМЕНДОВАННЫЕ МАГАЗИНЫ',
			'cover' => asset('assets/img/recommended-shops-bg.jpg')
		];

		return $this->recommendedShops($request, $page);
	}


	/**
	 * Get Recommended shops page
	 * @param  Request  $request
	 * @param  array 	$page
	 * @param  array  	$filter
	 * @return view
	 */
	public function recommendedShops($request, $page, $filter = [])
	{
		$shops = Shop::orderBy('view_amount','desc')->where('is_recommended','1');

		// Search by keyword
        if($request->has('q')){
            $keyword = preg_split('/\s+/', $request->get('q'), -1, PREG_SPLIT_NO_EMPTY);
            $shops->where(function ($q) use ($keyword) {
                foreach ($keyword as $value) {
                    $q->orWhere('title', 'like', "%{$value}%");
                }
            });
        }

		$shops = $shops->paginate(16);
		return view('frontend.shop.index', compact('page', 'shops'));
	}


	/**
	 * show Shop by alias
	 * @param  string $alias
	 * @return view
	 */
	public function show(Request $request,$alias){

		$shop = Shop::where('alias', $alias)->where("status", 1)->first();

		if(!$shop){
			abort(404);
		}
		// Low secured View Count protection
        if( !in_array($shop->id, Session::get('viewed_shops', [])) ){
            $shop->increment('view_amount');
            Session::push('viewed_shops', $shop->id);
        }
        
        $products = Product::where('shop_id',$shop->id)->where("status", 1)->orderBy('created_at','desc')->paginate(60);	

        /* breadcrumbs*/
        $dir_page = PageController::dirBreadCrumbs([]);
        $dir_page[] = ["link" =>null, "name" => $shop->title];
        $pages = $this->breadcrumbs($dir_page);
		/* breadcrumbs*/
		return view('frontend.shop.show_new', compact('shop', 'products', 'pages'));
	}


	public function select2Shop(Request $request){

		$shop = Shop::where('title_ru','like','%'.$request->get('term').'%')
					->where("status", 1)
					->orWhere('title_uz','like','%'.$request->get('term').'%')
					->get()->toArray();
		return response()->json(['params' => $shop]);
	}

	public function getPhone($id){

        $phone = Shop::find($id);
		if(!$phone)	return 0;	

        $phone->phone_view_amount =  $phone->phone_view_amount + 1;
        $phone->save();


		return response()->json(['phone' => $phone->phone]);

	}


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
	public function getForm(Request $request){

		$shop = Shop::where('user_id', $request->user()->id)->first();

		$pages[] = ["link" => "/cabinet/product", "name" => "Мои товары"];
		$pages = $this->breadcrumbs($pages);

		if(!empty($shop)){
			if($shop->status == 0){
				Session::flash('success', "Ваш магазин заблокирован");
				return redirect()->back();
			}else{
				return view('frontend.cabinet.shop-form',compact('shop', 'directions'));
			}
		}

		if(Auth::user()->hasRole('salesman') || Auth::user()->hasRole('admin')){
		    return view('frontend.cabinet.shop-form', compact('directions','shop'));
		}
		abort(404);
	}

	public function postForm(Request $request){

		$data = $request->only(
			'title','address','phone',
			'working_hour_start',
			'working_hour_end',
			'image','lng','lat',
            'day1','day2','day3',
			'day4','day5','day6',
			'day7','image','merchant'
		);

		$rules = [
			'title' 		 => 'required|max:255',
			'address' 	 	 => 'required|max:255',
			'phone'		     => 'required|max:255',
            'merchant'       => 'required|max:255',
 			'day1' 			 => 'boolean',
			'day2' 			 => 'boolean',
			'day3' 			 => 'boolean',
			'day4' 			 => 'boolean',
			'day5' 			 => 'boolean',
			'day6' 			 => 'boolean',
			'day7' 			 => 'boolean',
		];

		if (empty($data['lat'])) {
            $data['lat'] = 0;
        }

        if (empty($data['lng'])) {
            $data['lng'] = 0;
        }

		$data = static::daysOperations($request, $data);

		$data['phone'] = preg_replace('/[\+\-\(\)\ ]/is', '', $data['phone']);

		$validator = Validator::make($data, $rules);

		if($validator->fails()){
			$errors = $validator->errors();
            $directions = MallCategory::orderBy('id','asc')->where('parent_id', '0')->get();
            return view('frontend.cabinet.shop-form',compact('shop', 'directions', 'errors'));
		} 
		
        unset($data['image']);

        $this->shopEdit($request, $data);

        Session::flash('success', Lang::get('alert.shop.'.(!isset($id) || $id == NULL ? 'insert' : 'update'))); 

        return redirect()->action('CabinetController@getIndex');
	}


    /**
     * @param $value
     * @return string
     */
	private function aliasCreate($value){
        $slug = str_slug($value);
        $model = new Shop;
        $slugCount = count($model->whereRaw("alias REGEXP '^{$slug}(-[0-9]+)?$' and id != '{$model->id}'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

	private static function daysOperations($request, $data){
		for($i=1; $i<=7; $i++){
			if($request->has('day'.$i)){
				$data['day'.$i] = 1;
			}else{
				$data['day'.$i] = 0;
			}
		}

		return $data;
	}

	private function shopEdit($request, $data){
		$shop = static::shopSave($request, $data);

		if($request->hasFile('image')){
            $str = Upload::saveFile('shop', $shop->id, $request->file('image'));
        }

        $name = isset($shop) ? $shop->main_image : null;
        $res = Upload::hasMainImage("shop", $shop->id, $name);
        
        if($res !== true){
            Shop::addMainImage($shop->id, $res['name']);
        }
        Shop::editShopPaymeConfig($shop->id, ['merchant' => $data['merchant']]);
	}

	private function shopSave($request, $data){
		$shop = Shop::where('user_id', $request->user()->id)->first();
		
        // $shops = Shop::where('user_id', $request->user()->id)->get();

        // $mall = MallCategory::where("id", $data['mall_id'])->first();
        
        if(empty($shop)){
            $data['user_id'] = $request->user()->id;
            $data['alias'] = $this->aliasCreate($data['title']);
            $create = Shop::create($data);

        } else {
            // $shop = Shop::find($shop->id);
            if($shop){
                $shop->update($data);

            }
        }


        return !empty($shop) ? $shop : $create;

	}




}
