<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Role;
use App\Models\Shop;

use Validator;
use App\User;
use Redirect;
use Auth;

class UserController extends Controller
 {   
    public function getProfile(Request $request){
        if($request->user()->hasRole('salesman') || $request->user()->hasRole('admin')){
            $product=Product::where('user_id',$request->user()->id)
            ->orderBy('id','desc')
                ->paginate(15);
            return redirect()->action('CabinetController@getIndex');
        }
        else{
            return redirect()->action('CabinetController@getFavorite');
        }
            
    }

    public function getRegister(){
    	return view('frontend.user.register');
    }

    /**
     * Регистрация пользователя
     * @param  Request $request
     * @return json
    */
    public function postUserRegister(Request $request)
    {
        $data = $request->only('name','lastname','email','phone','password','password_confirmation');

        $rules = [
            'name'                  => 'required',
            'lastname'              => 'required',
            'email'                 => 'email|required|unique:users',
            'phone'                 => 'required',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ];

        $data['phone'] = preg_replace('/[\+\-\(\)]/is', '', $data['phone']);

        $validator = Validator::make($data,$rules);
        
        if($validator->fails()){
            return response()->json([
                'status' => 0,
                'errors' => $validator->errors()
                ]);
        }
        else{
            $user = new User;
            $user->name = $data['name'];
            $user->lastname = $data['lastname'];
            $user->email = $data['email'];
            $user->phone = $data['phone'];
            $user->password = bcrypt($data['password']);
            $user->save();
                    
            $role = Role::where('name', '=', 'user')->first();
            $user->roles()->attach($role->id);
            // /Auth::login($user);
            $this->sendMessageAndSave($user->id);
            return response()->json([
                'status' => 1,
                //'url' => action('UserController@getProfile')
            ]);
        }
    }

    /**
     * Регистрация продавца
     * @param  Request $request
     * @return json
    */
   
    public function postSellerRegister(Request $request){

        $data = $request->only('name','lastname','phone','email','password','password_confirmation');

        $rules = [
            'name'                  => 'required',
            'lastname'              => 'required',
            'phone'                 => 'required',
            'email'                 => 'email|required|unique:users',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',    
        ];

        $data['phone'] = preg_replace('/[\+\-\(\)]/is', '', $data['phone']);

        $validator = Validator::make($data,$rules);

        if($validator->fails()){
            return response()->json([
                'status' => 0,
                'errors' => $validator->errors()
            ]);
        }

        else{
            $seller = new User;
            $seller->name = $data['name'];
            $seller->lastname = $data['lastname'];
            $seller->email = $data['email'];
            $seller->phone = $data['phone'];
            $seller->password = bcrypt($data['password']);
            $seller->save();
            
            $role = Role::where('name', '=', 'salesman')->first();
            $seller->roles()->attach($role->id);
            // Auth::login($seller);

            $this->sendMessageAndSave($seller->id);
            
            return response()->json([
                'status' => 1,
                //'url' => action('UserController@getProfile')
            ]);

        }
    }

    public function getLogin(){
        
        if(Auth::check()){
            return redirect()->to('/');
        }
        
        return Redirect::to('/?login=show');
    }

    /**
     * Проба авторизации пользователя по данной форме
     * @param  Request $request
     * @return json
    */
   
    public static function postAjaxLogin(Request $request)
    {
        $data = $request->only('login', 'password');

        $rules = [
            'login'     => 'required',
            'password'  => 'required',
        ];

        $data['login'] = str_replace('+', '', $data['login']);

        $validator = Validator::make($data, $rules);

        if($validator->fails()){
            return response()->json([
                'status' => 0,
                'msg' => 'Неверный логин или пароль'
            ]);    
        }else{
            $attempt = Auth::attempt(['phone' => $data['login'], 'password' => $data['password'], 'status' => User::STATUS_ACTIVE]);

            if($attempt){
                return response()->json([
                 'status' => 1,
                 'msg' => 'Success',
                 'url' => action('UserController@getProfile')
                ]);
            }else{
                $attempt = Auth::attempt(['email' => $data['login'], 'password' => $data['password'], 'status' => User::STATUS_ACTIVE]); 
            }
            
            if($attempt){
                return response()->json([
                    'status' => 1,
                    'msg' => 'Success',
                    'url' => action('UserController@getProfile')
                ]);
            }else{
                return response()->json([
                    'status' => 0,
                    'msg' => 'Неверный логин или пароль'
                ]);  
            }
        }
    }



    public function getLogout(){
        Auth::logout();
        return Redirect::to('/');
    }

    public function postFormConfirmCode(){
        return view("frontend.user.code");
    }


    public function postResendCode(){
        if(Session::has("varificate.id")){
            $status = $this->sendMessageAndSave(Session::get("varificate.id"));
            return response()->json([
                            "status" => $status, 
                            "msg" => "Код отправлен",
                            "code" => Session::get("varificate.code")
                        ]);
        }else{
            return response()->json([
                "status" => 0,
                'code' => Session::has("varificate"),
                'msg' => "Ошибка при отпраление",
            ]);
        }

    }

    /**
     * Sms potverjdeniya
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postVarificate(Request $request){

        if( 
            $request->has("code") && 
            Session::has("varificate.code") && Session::has("varificate.id") &&
            trim(Session::get("varificate.code")) == trim($request->code) &&
            time() <= Session::get("varificate.endtime")
        ) {
            
            $user = User::findOrFail(Session::get("varificate.id"));
            $user->status = User::STATUS_ACTIVE;
            $user->save();
            Auth::login($user);
            Session::forget("varificate");

            return response()->json([
                'status' => 1,
                'url' => action('UserController@getProfile')
            ]);

        } else {
            return response()->json([
                'status' => 0,
                'code' => Session::get('varificate.code'),
                'msg' => "Неверный код подтверждения",
            ]);
        }
    }


    /**
     * Отправка смс
     * @param  Request $request
     * @return boolean
    */


    public function sendMessageAndSave($user_id){
        $code = rand(11111, 99999);

        Session::put("varificate.code", $code);
        Session::put("varificate.id", $user_id);
        Session::put("varificate.endtime", time()+60);

        $user = User::find($user_id);
        if(!$user)
            return false;

        return static::sms($user->phone, $code);
    }

    public static function sms($phone, $code){


        $username = "ostouz";
        $password = '0$t##u^';

        $json = '{
          "messages": [{
            "recipient":"'.$phone.'",
            "message-id":"10126",
             "sms": {
                "originator": "3700",
                "content": {
                    "text": "OSTO: '. $code.'"
                }
              }
          }]
        }';
        
        $url = "http://91.204.239.42:8083/broker-api/send";
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, 
            array('Content-Type: application/json;charset=UTF-8'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $info = curl_getinfo($ch); 

        curl_close($ch);
        return true;

    }
    
    
    public function getSmsRequest(){
        
    }
    
    public function smsRequest(Request $request){
        

        $username = "ostouz";
        $password = '0$t##u^';

        $json = '{
          "messages": [{
            "recipient":"'.$phone.'",
            "message-id":"10126",
             "sms": {
                "originator": "3700",
                "content": {
                    "text": "'. $code.'"
                }
              }
          }]
        }';
        
        $url = "http://91.204.239.42:8083/broker-api/send";
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, 
            array('Content-Type: application/json;charset=UTF-8'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        $info = curl_getinfo($ch); 

        curl_close($ch);
        return true;
        
    }


}
