<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\URL;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $pages = [];
    
    public function __construct(){
        
        
    }

    protected function breadcrumbs($pages){

    	if(empty($this->pages)){
    		$this->pages[] = ["link" => "/", "name" => "Главная страница"];
    	}
    	foreach($pages as $page){
    		$this->pages[] = $page;
    	}
    	return $this->pages;
    }
}