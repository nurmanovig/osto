<?php

// S.Mirakhmedov v.1.1

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Icon;

use Datatables;
use Validator;
use Session;
use Upload;
use Lang;

class CategoryController extends Controller
{
    public function getIndex(){
       	return view('backend.category.index');
    }

    public function getData(){
        return Datatables::eloquent(Category::where('parent_id','0'))->make(true);
    }

    public function postDelete(Request $request){
    	if($request->has('delete') && Category::find($request->input('delete'))->delete()){
    		Session::flash('success', Lang::get('alert.success.delete'));
    	}

    	return redirect()->back();
    }

    public function parentId(Request $request){
        $categories = Category::where('title_ru','like','%'.$request->get('term').'%')
          ->orWhere('title_ru','like','%'.$request->get('term').'%')
            ->get()->toArray();
        return response()->json(['params' => $categories]);
    }


    public function getForm($id = NULL){
        $category = Category::find($id);
        $icons = Icon::all();
        if(!$category) $category = [];
        return view('backend.category.form', compact('category', 'icons'));
    }

    public function postForm(Request $request, $id = NULL){
        
        $data = $request->only('title_ru', 'title_uz','description_ru','description_uz','parent_id', 'icon');

        $rules = [
            'title_ru'       => 'required',
            'title_uz'       => 'required',
        ];

        $validator = Validator::make($data,$rules);

        if($validator->fails()){
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        } else {
              if($id==NULL){
                 $data['parent_id'] = '0';
                 $data['alias'] = $this->aliasCreate($data['title_ru']);
                 $create = Category::create($data);
              } else {
                 unset($data['parent_id']);
                 $category = Category::find($id);
                 if($category) {
                     $category->update($data);
                 }
              }

              if($request->hasFile('image')){
                Upload::saveFile('category', !isset($id) || $id==null ? $create->id : $category->id, $request->file('image'));
              }

              $name = isset($category) ? $category->main_image : null;
                $id = !isset($create) ? $category->id : $create->id;
                $res = Upload::hasMainImage("category", $id, $name);
                
                if($res !== true){
                    Category::addMainImage($id, $res['name']);
                }

              Session::flash('success', Lang::get('alert.success.'.($id==NULL ? 'insert' : 'update')));
              return redirect()->action('Backend\CategoryController@getIndex');  
        }
    }

    public function parentIndex($id){
        $parent = Category::where('parent_id', $id)->get();
        if(!$parent) $parent = [];
        return view('backend.parent.index',compact('parent', 'id'));
    }

    public function addParent(Request $request, $id=null){

        if( $request->isMethod('get') ){
            $category = Category::find($id);;
            $icons = Icon::all();

            return view('backend.category.form', compact('category', 'id', 'icons'));
        }

        // If Request method post ---> do -->

        if($request->isMethod('post')){

            $data=$request->only('title_ru','title_uz','description_ru','description_uz');

            $rules=['title_ru' => 'required'];
            $validate = Validator::make($data,$rules);

            if($validate->fails()){
                return redirect()->back()->withErrors($validate)->withInput();
            }
            // Current ID as a parentId
            $data['parent_id'] = $id;
            $data['alias'] = $this->aliasCreate($data['title_ru']);

            $create = Category::create($data);
                if($create){
                    Session::flash('success', Lang::get('alert.success.'.($id==NULL ? 'insert' : 'update')));
                    return redirect()->action('Backend\CategoryController@parentIndex',[$id]);
                }
        }

    }
    

    private function aliasCreate($value)
    {
        $slug = str_slug($value);
        $model = new Category;
        $slugCount = count($model->whereRaw("alias REGEXP '^{$slug}(-[0-9]+)?$' and id != '{$model->id}'")->get());

        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

 
}
