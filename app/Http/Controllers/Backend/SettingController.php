<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Setting;

use Illuminate\Http\Request;
use Validator;

class SettingController extends Controller{

	public  function getShow(){
		$setting = Setting::first();

		if(empty($setting)){
			return redirect(action("Backend\SettingController@getForm"));
		}

		return view("backend.settings.show", compact("setting"));

	}

	public function getForm(){
		$setting = Setting::first();

		return view("backend.settings.form", compact("setting"));
	}

	public function postForm(Request $request){
		$data = $request->only("price_delivery", "course");

		$rules = [
				'price_delivery'   => 'integer',
				'course'           => 'numeric|min:0',
				// 'auto_course'      => 'boolean',
				];
		$validator = Validator::make($data,$rules);
		$error = null;

    	if(!($validator->fails())){


    		$setting = Setting::first();

    		if(!empty($setting)){
            	Setting::where("id", $setting->id)->update($data);
    		}else{
    			Setting::create($data);
    		}

    		return redirect(action("Backend\SettingController@getShow"));

    	}else{
    		$error = $validator->errors();
    	}



    	return view("backend.settings.form", compact("error", "settings"));
	}

	


}
?>