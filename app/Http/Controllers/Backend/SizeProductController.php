<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\SizeProduct;

use Validator;
class SizeProductController extends Controller{


	public function getShow(){
		$sizes = SizeProduct::all();
		return view("backend.size.show", compact("sizes"));
	}

	public function postForm(Request $request){
		$data = $request->only("key", "value");

		$rules = [
			'key' => 'required|string|max:50',
			'value' => 'required|string|max:300',
		];

		$validator = Validator::make($data,$rules);

		
    	if(!$validator->fails()){

    		SizeProduct::updateOrcreate(['key' => $data['key']], $data);
    	}

		return redirect()->back();
	}

	public function postDelete(Request $request){


		if($request->has('delete') && SizeProduct::find($request->input('delete'))
				->delete()){
			Session::flash('success', Lang::get('alert.success.delete'));
		}
		return redirect()->back();
	}
}

?>