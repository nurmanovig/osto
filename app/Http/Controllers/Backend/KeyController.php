<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\PromoCode;
use App\Models\KeyWord;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Lang;

class KeyController extends Controller
{
	public function getShow(){
		$keys = KeyWord::all();
		return view("backend.key_words.show", compact("keys"));
	}

	public function getForm(Request $request, $id = null){
		$key = $id ? KeyWord::find($id) : [];
		return view("backend.key_words.form", compact("key"));
	}

	public function postForm(Request $request, $id = null){
		$data = $request->only('word', 'class');

		$rules = [
			'word' => 'required|string|max:256',
			'class' => 'required|string|max:256',
		];

		$validator = Validator::make($data,$rules);
		
    	if(!$validator->fails()){
    		$key = KeyWord::find($id);
            if($key) {
                $key->update($data);
            }
            return redirect('/mypanel/key');
    	}else{
    		return redirect()->back()
                ->withErrors($validator)
                ->withInput();
    	}

    	return view("backend.key_words.form", compact("key"));

		// return redirect()->back();
	}

	public function postDelete(Request $request){

		// if($request->has('delete') && Icon::find($request->input('delete'))
		// 		->delete()){
		// 	Session::flash('success', Lang::get('alert.success.delete'));
		// }
		// return redirect()->back();
	}
}
