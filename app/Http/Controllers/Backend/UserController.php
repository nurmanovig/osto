<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Role;
use App\User;

use Datatables;
use Validator;
use Session;
use Auth;
use Lang;

class UserController extends Controller
{
    public function getIndex(){
    	$role = Role::where('name', 'user')->first();
        $users = $role->users()->paginate(10);

    	return view('backend.user.index', compact('users'));
    }

    public function getData(){
        $role = Role::where('name', 'user')->first();
        $users = $role->users()->get();
        return Datatables::of($users)->make(true);
    }

    public function getForm($id = null){
    	$user = User::find($id);
    	if(!$user) $user = [];
        
    	return view('backend.user.form', compact('user'));
    }

    public function postDelete(Request $request){
    	if($request->has('delete') && User::find($request->input('delete'))->delete()){
    		Session::flash('success', Lang::get('alert.success.delete'));
    	}

    	return redirect()->back();
    }

    public function postForm(Request $request, $id = null){
    	$user = User::find($id);
    	$data = $request->only('name', 'lastname', 'phone', 'email','password');
    	$rules = [
    		'name' => 'required',
            'lastname' => 'required',
            'phone' => 'required',
    		'email' => 'email|unique:users,email,'.$user->id.'',
    		'password' => 'min:6'
    	];

    	$validator = Validator::make($data,$rules);

    	if($validator->fails()){
    		return redirect()->back()->withErrors($validator)->withInput();
    	}
    	else {
    		if($request->has('password')){
    			$data['password'] = bcrypt($data['password']);
    		}
    		else{
    			unset($data['password']);
    		}

    		if($user->update($data)){
                Session::flash('success', Lang::get('alert.success.update'));
            }
        
            return redirect()->action('Backend\UserController@getIndex');
    	}
    }

}
