<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\PromoCode;
use App\Models\OrderProduct;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
	public function getIndex(Request $request = null){


		$status = null;
		$query = Order::with("products");
		$query_summ = Order::select(DB::raw("sum(summ) summ, count(id) count"));
		if(!empty($request->id)){
			$query_summ->where("id", $request->id);
			$query->where("id", $request->id);
		}

		if(isset($request->status)){
			$status = $request->status;
			$query->where("status", $request->status);
			$query_summ->where("status", $request->status);
		}


		$info = $query_summ->first();
		$orders =  $query->orderBy("id", "desc")->paginate(10);

		$currency = "UZS";
		return view("backend.order.index", compact("orders", "status", "info", "currency"));
	}

	public function getShow($id){

		$order = Order::where("id", $id)->with("products")->firstOrFail();


		if(!$order->products)
			abort(404);

		return view("backend.order.show", compact("order"));
	}

	public function getInfoOrderForMap(Request $request){
		$order = Order::where("id", $request->id)->with("products")->firstOrFail();
		// $order = OrderProduct::where("order_id", $request->id)->with("products")->firstOrFail();
		$days = [
			1 => "Пн",
			2 => "Вт",
			3 => "Ср",
			4 => "Чт",
			5 => "Пт",
			6 => "Сб",
			7 => "Вс",
		];

		$ids = [];

		$data = [
			[
				"type" => "user",
				"coords" => [
					'lat' => $order->lat,
					'lng' => $order->lng,
				],
				'address' => "Адрес: ". $order->address,
				'title' => "Имя: ". $order->username,
				'phone' => "Телефон: ". $order->phone,
			]
		];

		foreach($order->products as $product){
			$shop = $product->product->shop;
			if(array_search($shop->id, $ids) !== false)
				continue;
			$info = [
						"type" => "shop",
						"coords" => [
							'lat' => $shop->lat,
							'lng' => $shop->lng,
						],
						'address' => "Адрес: ". $shop->address,
						'title' => "Название: ". $shop->title,
						'phone' => "Телефон: ". $shop->phone,
						'work_time' => "Время работы: " . $shop->working_hour_start ." - ". $shop->working_hour_end,


					];
			$work_days = [];
			for($i=1; $i<=7; $i++){
				$key = "day".$i;
				if($shop->$key){
					$work_days[] = $days[$i];
				}
			}
			$info["work_days"] = "Рабочие дни: ". implode(", ", $work_days);
			$data[] = $info;
			$ids[] = $product->product->shop->id;
		}

		return ($data);
	}

	public function postStatus(Request $request){
		 Order::changeStatus($request->id, $request->status);
		 return [];
	}

	public function postPaystatus(Request $request){

		  Order::changePaystatus($request->id);
		  return [];
	}

	public function postDelete(Request $request){
		 Order::deleteOrder($request->delete);
		 return redirect()->back();
	}


}
?>