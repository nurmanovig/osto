<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PromoCode;

use Validator;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;
class PromoCodeController extends Controller
{
    //

    public function getIndex(){
    	$promo_codes = 	PromoCode::orderBy("start_at")->paginate(10);
    	return view("backend.promo.list", compact("promo_codes"));
    }

    public function getForm($id = null){

    	$promo = PromoCode::find($id);
        if(!$promo) $promo = [];
        
    	return view("backend.promo.form", compact("promo"));
    }

    public function postForm(Request $request, $id = null){
    	$data = $request->only("code", "percent", "start_at", "finished_at");
		$rules = [
				'code'             => 'required|string',
				'percent'          => 'required|integer|min:0|max:100',
				'start_at'         => 'required|date',
				'finished_at'      => 'required|date',

				];
    	$validator = Validator::make($data,$rules);

    	if($validator->fails()){
			return redirect()->back()->withErrors($validator)->withInput();
    	}else{
            $promo = PromoCode::firstOrNew(["id" => $id]);

            foreach($data as $key => $item){
                $promo->$key = $item;
            }
            $promo->save();

    		return redirect()->action('Backend\PromoCodeController@getIndex');
    	}
    }

    public function postDelete(Request $request){
        if($request->has('delete') && $promo = PromoCode::find($request->input('delete'))){
            if($promo->delete()){
                Session::flash('success', Lang::get('alert.success.delete'));
            }
           
        } 

        return redirect()->back();
    }
}
