<?php

namespace App\Http\Controllers\Backend;

use App\Library\Upload;
use App\Models\Banners;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\PromoCode;

use Validator;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;
class BannerController extends Controller
{
    //

    public function getIndex(){
        $items = 	Banners::orderBy("id", "desc")->paginate(10);
        return view("backend.banners.index", compact("items"));
    }

    public function getForm($id = null){

        $item = Banners::find($id);

        if(!$item) $item = [];

        return view("backend.banners.form", compact("item"));
    }

    public function postForm(Request $request, $id = null){
        $data = $request->only(['url', 'actived_at']);

        $rules = [
            'url'             => 'required|string',
            'actived_at'          => 'required|date',
        ];

        $validator = Validator::make($data,$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            if($request->file('img')) {
                $image = $request->file('img');
                $name = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/banners');
                $image->move($destinationPath, $name);
                $data['img'] = time() . '.' . $image->getClientOriginalExtension();

                if($banner = Banners::find($id)){
                    @unlink($destinationPath."/".$banner->img);
                }
            }

            Banners::updateOrCreate(
                ['id' => $id],
                $data
            );

            return redirect()->action('Backend\BannerController@getIndex');
        }
    }

    public function postDelete(Request $request){
        if($request->has('delete') && $promo = Banners::find($request->input('delete'))){
            if($promo->delete()){
                Session::flash('success', Lang::get('alert.success.delete'));
            }

        }

        return redirect()->back();
    }
}
