<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Models\MallCategory;
use App\Models\Shop;
use App\Models\Mall;
use App\Models\Role;

use Illuminate\Http\Request;
use Datatables;
use Validator;
use Response;
use App\User;
// use Request;
use Session;
use Upload;
use Input;
use Lang;

class ShopController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(){
    	$shops = Shop::orderBy("id", "desc")->get();
        return view('backend.shop.index', compact('shops'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getShow($id){
    	$shop = Shop::where("id", $id)->firstOrFail();
    	
    	return view("backend.shop.show", compact("shop"));
    }

    /**
     * @return mixed
     */
    public function getData(){
        return Datatables::of(Shop::with('direction'))->make(true);
    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getForm($id = null){
    	$shop = Shop::find($id);
    	if(!$shop) $shop = [];
        

        $role = Role::where('name', 'salesman')->first();
        $seller = $role->users()->orderBy('name', 'asc')->pluck('name', 'id');

    	return view('backend.shop.form',compact('shop','seller'));
    }

    /**
     * @return mixed
     */
    public function getFind(){
        $direction_id = Input::get('id');
        $data = MallCategory::select('title_ru', 'id')->where('parent_id', $direction_id)->get();
        return Response::json($data);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDelete(Request $request){

        if($request->has('delete') && Shop::find($request->input('delete'))
            ->delete())
        {
    		Session::flash('success', Lang::get('alert.success.delete'));
    	}
    	
        return redirect()->back();
    }

    /**
     * @param null $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function postForm(Request $request, $id = NULL){
        $data = $request->only(
            'title',
            'address',
            'user_id',
            'phone', 
            'lat', 
            'lng', 
            'working_hour_start', 
            'working_hour_end',
            'merchant'
            );

        $rules = [
            'title'          => 'required|min:2|max:255',
            'address'        => 'required',     
            'phone'          => 'required|min:5|max:15',
            'lat'            => 'between:0,99.99',
            'lng'            => 'between:0,99.99',
            'day1'           => 'boolean',
            'day2'           => 'boolean',
            'day3'           => 'boolean',
            'day4'           => 'boolean',
            'day5'           => 'boolean',
            'day6'           => 'boolean',
            'day7'           => 'boolean',
        ];

        // if($request->input('mall_type') == 3 || $request->input('mall_type') == 4){
        //     $data['mall_id'] = 0;
        // }


        for($i=1; $i<=7; $i++){
            if($request->has('day'.$i)){
                $data['day'.$i] = 1;
            }else{
                $data['day'.$i] = 0;
            }
        }

        $validator = Validator::make($data,$rules);

        if(!$validator->fails()){
            return redirect()->back()->withErrors($validator);
        }

        else{
            if(empty($data['lat']) && empty($data['lng'])){
                unset($data['lat']);
                unset($data['lng']);
            }

            if($id==NULL){
                $data['alias'] = $this->aliasCreate($data['title']);
                $insert = Shop::create($data);
            }
            else{
                $shop = Shop::find($id);
                if($shop)
                    $shop->update($data);
            }

            if($request->hasFile('image')){
                // $size = [[400,370]];
                $str = Upload::saveFile('shop', $id==NULL ? $insert->id : $shop->id, $request->file('image'));
            }

            $name = isset($shop) ? $shop->main_image : null;
            $res = Upload::hasMainImage("shop", $shop->id, $name);
              
            if($res !== true){

                Shop::addMainImage($shop->id, $res['name']);
            }
            Shop::editShopPaymeConfig($shop->id, ['merchant' => $data['merchant']]);

            Session::flash('success', isset($str) ? $str : "");
              // Session::flash('success', Lang::get('alert.success.'.($id==NULL ? 'insert' : 'update')));
            return redirect()->action('Backend\ShopController@getIndex');
        }
    }

    /**
     * @param Request $request
     */
    public function postStatus(Request $request){
    	Shop::changeStatus($request->id, $request->type);
    }


    /**
     * @param $value
     * @return string
     */
    private function aliasCreate($value){
        $slug = str_slug($value);
        $model = new Shop;
        $slugCount = count($model->whereRaw("alias REGEXP '^{$slug}(-[0-9]+)?$' and id != '{$model->id}'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }
    
}
