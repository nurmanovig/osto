<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comments;
use Datatables;
use Session;
use Lang;

class CommentController extends Controller
{
 
    public function getIndex(){
       	return view('backend.comment.index');
    }

    public function getData(){
        return Datatables::of(Comments::with('user'))->make(true);
    }


    public function postDelete(Request $request){
    	if($request->has('delete') && Comments::find($request->input('delete'))->delete()){
    	   Session::flash('success', Lang::get('alert.success.delete'));
    	}  return redirect()->back();
    }



}