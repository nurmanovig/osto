<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Page;
use App\Models\Product;
use App\Models\Shop;
use App\User;
use App\Models\Role;
use App\Models\Comments;
use App\Models\Feedback;
use App\Models\Category;
use App\Models\MallCategory;
use Validator;
use Session;
use Lang;

class PageController extends Controller
{
    public function getDashboard(){
        $count['product'] = Product::count();
        $count['shop']  = Shop::count();
        $count['category'] = Category::count();
        $count['comment'] = Comments::count();
        $count['feedback'] = Feedback::count();
        $user = Role::where('name', 'user')->first();

        $count['user'] = $user ? $user->users()->count() : 0;
        $seller = Role::where('name', 'salesman')->first();
        $count['seller'] = $seller->users()->count();
//        $count['direction'] = MallCategory::where('parent_id', '0')->count();
//        $count['mall'] = MallCategory::where('parent_id', '2')->count();
//        $count['bazar'] = MallCategory::where('parent_id', '1')->count();
//        $count['brand'] = MallCategory::where('parent_id', '3')->count();
//        $count['private'] = MallCategory::where('parent_id', '4')->count();
    	return view('backend.page.dashboard', compact('count'));
    }

    public function getIndex(){
    	$pages = Page::orderBy('id','asc')->paginate(10);
    	return view('backend.page.index', compact('pages'));	
    }

    public function postDelete(Request $request){
    	if($request->has('delete') && Page::find($request->input('delete'))->delete()){
    		Session::flash('success', Lang::get('alert.success.delete'));
    	}

    	return redirect()->back();
    }

    public function getForm($id = NULL){
    	$page = Page::find($id);
    	if(!$page) $page = [];
    	return view('backend.page.form-new', compact('page'));
    }

    public function postForm(Request $request, $id=NULL){

    	$data = $request->only('title_ru','title_uz','description_ru','description_uz','meta_description_ru','meta_description_uz','meta_keywords_ru','meta_keywords_uz');

    	$rules = [
    		'title_ru' => 'required',
    		'description_ru' => 'required',
    	 ];

    	 $validator = Validator::make($data,$rules);

    	 if($validator->fails()){
    	 	return redirect()->back()->withErrors($validator)->withInput();
    	 }
    	 else{
    	 	if($id==NULL){
    	 		$data['alias'] = $this->aliasCreate($data['title_ru']);
    	 		$create = Page::create($data);	
    	 	}
    	 	else{
    	 		$edit = Page::find($id);
    	 		if($edit)
    	 			$edit->update($data);
    	 	}

    	 	Session::flash('success', Lang::get('alert.success.'.($id == NULL ? 'insert' : 'update')));
    	 	return redirect()->action('Backend\PageController@getIndex');
    	 }

    }
    
    /**
    * Alias creator for Page
    * @param  string $value 
    * @return string        
    */
    private function aliasCreate($value)
    {  
        $slug = str_slug($value);
        $model = new Page;
        $slugCount = count($model->whereRaw("alias REGEXP '^{$slug}(-[0-9]+)?$' and id != '{$model->id}'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }

}
