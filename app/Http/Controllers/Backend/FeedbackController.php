<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Feedback;

use Datatables;
use Session;
use Lang;

class FeedbackController extends Controller
{
    public function getIndex()
    {
    	return view('backend.feedback.index');
    }

    public function getData()
    {
    	return Datatables::of(Feedback::with('type'))->make(true);
    }

    public function postDelete(Request $request)
    {
    	if($request->has('delete') && Feedback::find($request->input('delete'))->delete())
    	{
    		Session::flash('success', Lang::get('alert.success.delete'));
    	}

    	return redirect()->back();
    }
}
