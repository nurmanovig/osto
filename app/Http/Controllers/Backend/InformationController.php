<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Library\Upload;
use App\Models\Information;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;

class InformationController extends Controller {

    public function getIndex(){
        $items = 	Information::orderBy("id", "desc")->paginate(10);
        return view("backend.information.index", compact("items"));
    }

    public function getForm($id = null){

        $item = Information::find($id);

        if(!$item) $item = [];

        return view("backend.information.form", compact("item"));
    }

    public function postForm(Request $request, $id = null){
        $data = $request->only(['url', 'title', 'text', 'status']);

        $rules = [
            'url'           => 'required|string|max:250',
            'title'         => 'required|string|max:250',
            'status'        => 'required|integer',
        ];

        $validator = Validator::make($data,$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $item = Information::find($id);

            if(!$item && !$request->file('img')){
                return redirect()->back();
            }

            if($item && $request->file('img')){
                @unlink(public_path()."/uploads/information/".$item->img);
            }

            if($request->file('img')) {
                $image = $request->file('img');
                $name = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/information');
                $image->move($destinationPath, $name);
                $data['img'] = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path()."/uploads/information/".$data['img'];

//                Upload::reSizeNew($path, $path, 180, 300);

                if($item = Information::find($id)){
                    @unlink($destinationPath."/".$item->img);
                }
            }

            Information::updateOrCreate(
                ['id' => $id],
                $data
            );

            return redirect()->action('Backend\InformationController@getIndex');
        }
    }

    public function postDelete(Request $request){
        if($request->has('delete') && $item = Information::find($request->input('delete'))){
            @unlink(public_path()."/uploads/information/".$item->img);
            if($item->delete()){
                Session::flash('success', Lang::get('alert.success.delete'));
            }

        }

        return redirect()->back();
    }
}