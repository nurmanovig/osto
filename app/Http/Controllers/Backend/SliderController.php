<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\SliderItems;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;

class SliderController extends Controller {


    public function getIndex(){
        $items = 	SliderItems::orderBy("id", "desc")->paginate(10);
        return view("backend.slider.index", compact("items"));
    }

    public function getForm($id = null){

        $item = SliderItems::find($id);

        if(!$item) $item = [];

        return view("backend.slider.form", compact("item"));
    }

    public function postForm(Request $request, $id = null){
        $data = $request->only(['url', 'title', 'text']);

        $rules = [
            'url'             => 'required|string|max:250',
            'title'          => 'required|string|max:250',
            'text'          => 'required|string|max:400',
            'status'        => 'default:1',
        ];

        $validator = Validator::make($data,$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $slider = SliderItems::find($id);

            if(!$slider && !$request->file('img')){
                return redirect()->back();
            }

            if($slider && $request->file('img')){
                @unlink(public_path()."/uploads/slider/".$slider->img);
            }

            if($request->file('img')) {
                $image = $request->file('img');
                $name = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/slider');
                $image->move($destinationPath, $name);
                $data['img'] = time() . '.' . $image->getClientOriginalExtension();

                if($item = SliderItems::find($id)){
                    @unlink($destinationPath."/".$item->img);
                }
            }

            SliderItems::updateOrCreate(
                ['id' => $id],
                $data
            );

            return redirect()->action('Backend\SliderController@getIndex');
        }
    }

    public function postDelete(Request $request){
        if($request->has('delete') && $item = SliderItems::find($request->input('delete'))){
            @unlink(public_path()."/uploads/slider/".$item->img);
            if($item->delete()){
                Session::flash('success', Lang::get('alert.success.delete'));
            }

        }

        return redirect()->back();
    }
}