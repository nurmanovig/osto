<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\PromoCode;
use App\Models\Icon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Lang;

class IconController extends Controller
{
	public function getShow(){
		$icons = Icon::all();
		return view("backend.icon.show", compact("icons"));
	}

	public function postForm(Request $request){
		$data = $request->only("icon");

		$rules = [
			'icon' => 'required|string|max:256',
		];

		$validator = Validator::make($data,$rules);
		
    	if(!$validator->fails()){
    		
    		Icon::create(['icon' => $data['icon']], $data);
    	}

		return redirect()->back();
	}

	public function postDelete(Request $request){

		if($request->has('delete') && Icon::find($request->input('delete'))
				->delete()){
			Session::flash('success', Lang::get('alert.success.delete'));
		}
		return redirect()->back();
	}
}
