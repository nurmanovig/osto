<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Category;
use App\Models\Shop;
use Datatables;
use Validator;
use Session;
use Upload;
use Lang;

class ProductController extends Controller
{
	public function getIndex(Request $request){
		$products = new Product;

        if($request->has('query')){
            $query = $request->input('query');
            $products = $products->where('title', 'like', '%'.$query.'%');
        }

        $products = $products->paginate(15);

    	return view('backend.product.index', compact('products'));
    }

    public function getForm($id = NULL){
    	$product = Product::find($id);
    	if(!$product) $product = [];
    	$categories = Category::orderBy('title_ru', 'asc')->where('parent_id','0')
                                ->pluck('title_ru','id');
        $shop = Shop::orderBy('id','asc')->pluck('title', 'id');
    	return view('backend.product.form', compact('product','categories','shop'));
    }

    public function postForm(Request $request, $id = null){
    	$data = $request->only('title', 'description', 'category_id','price', 'recommended', 'popular', 'currency','status','shop_id');

    	$rules = [
    		'title' => 'required',
    		'description' => 'required|min:2',
    		'price' => 'required',
    	];
    	
        // dd($data);
        $data['currency'] = 'UZS';
    	$validator = Validator::make($data,$rules);

    	if($validator->fails()){
    		return redirect()->back()->withErrors($validator)->withInput();
    	} else {

    		if($id==NULL){
                $data['alias'] = $this->aliasCreate($data['title']);
    			$insert = Product::create($data);
    		}
    		else{
    			$product = Product::find($id);
    			if($product)
    				$product->update($data);

                //Delete images
                $images_for_delete = explode(',', $request->input('images_for_delete'));
                $images = Upload::getFiles("product", $id);

                if (strlen($images_for_delete[0]) > 0) {

                    foreach ($images_for_delete as $del) {
                        $files[] = $images[$del];
                    }

                    Upload::removeFiles('product', $id, $files);
                }
    		}

            $files = [];
            if($request->hasFile('images')){
                
                $files = Upload::saveFiles("product", $id == NULL ? $create->id : $id, 
                    $request->file('images'));
            }

            $id = $id == NULL ? $insert->id : $id;
            $name = isset($product) ? $product->main_image : null;
            $res = Upload::hasMainImage("product", $id, $name);
            
            if($res !== true){

                Product::addMainImage($id, $res['name']);
            }

            // if ($request->hasFile("images")) {
            //     Upload::saveFiles(
            //         "product",
            //         $id == NULL ? $insert->id : $id,
            //         $request->file('images'),
            //         [[250, 375], [285, 190]]
            //     );
            // }
			
            Session::flash('success', Lang::get('alert.success.'.($id == NULL ? 'insert': 'update')));
    		
    			return redirect()->action('Backend\ProductController@getIndex');
    	 }
    }

    public function postDelete(Request $request){
    	if($request->has('delete') && Product::find($request->input('delete'))->delete()){

    	   Session::flash('success', Lang::get('alert.success.delete'));
    	}  return redirect()->back();
    }

    public function getSort(Request $request = null){
        $sort_keys = ['date', 'price', 'view_amount', 'recommended'];

        if ($request->isMethod('post')) {
            $data = $request->only($sort_keys);

            setSortItems($data);
            return redirect()->to('/mypanel/products/sort-setting');
        }

        $sort_items = getSortItems();
        return view("backend.product.sort", compact('sort_items', 'sort_keys'));
    }


   /**
   * aliasCreator for Product
   * @param  array $value 
   */
     private function aliasCreate($value){
        $slug = str_slug($value);
        $model = new Product;
        $slugCount = count($model->whereRaw("alias REGEXP '^{$slug}(-[0-9]+)?$' and id != '{$model->id}'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }


   /**
   * Change product status
   * @param  array $value 
   */ 
    public function getChange($id, $whatToChange = null)
    {
        $change = [];
        $getProduct = Product::find($id);

        if (!is_null($getProduct)) {
            if ($whatToChange == 'recommended') {
                if ($getProduct->recommended == '1')
                    $change['recommended'] = '0';
                else
                    $change['recommended'] = '1';
              }

            if($whatToChange == 'popular'){
                if($getProduct->popular == '1')
                    $change['popular'] = '0';
                else{
                    $change['popular'] = '1';
                }
            } 

            if($whatToChange == 'status'){
                if($getProduct->status == '1')
                    $change['status'] = '0';
                else{
                    $change['status'] = '1';
                }
            }  

            $change = Product::find($id)->update($change);
            if (!$change)
                return 'false';
            else
                return 'true';

        }

    }

}
