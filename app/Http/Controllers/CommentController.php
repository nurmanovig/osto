<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Comments;
use Validator;
use Lang;

class CommentController extends Controller
{
    public function postComment(Request $request)
    {
    	$data = $request->only('text','product_id');
    	$validate = Validator::make($data, ['text'=>'required']);
    	 	if($validate->fails()){
    	 		return response()->json(['errors' => $validate->errors(), 'input' => $request->all()]);
    	 	}else{
    	 		$data['user_id'] = $request->user()->id;
    	 		$insert = Comments::create($data);
    	 			if($insert){
    	 				return response()->json(['success' => 'Ваш комментарий успешно добавлен']);
    	 			}
    	 	}
    }




    
}
