<?php

namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use App\Models\MallCategory;
use Illuminate\Http\Request;


use App\Models\SizeProduct;
use App\Models\Favourite;
use App\Models\Category;
use App\Models\Product;
use App\Models\Likes;
use App\Models\Shop;

use Validator;
use Session;
use Config;
use Upload;
use Auth;
use Lang;
use Log;
use App;

class ProductController extends Controller
{
    protected $pages = [];

    public function __construct() {
        $this->middleware('auth')->only(
            'likeOrDislike',
            'addFavourite',
            'getForm',
            'postForm'
        );
    }

    public function getIndex(Request $request) {
        $title = 'Поиск "'.$request->input('keyword').'"';
        $this->breadcrumbs([["link" => null, "name" => $title]]);

        return $this->getShowProducts($request, $title);
    }


    /**
     * @param Request $request
     * @param $alias
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCategory(Request $request, $alias) {
        $category = Category::where('alias', $alias)->first();

        if(!$category) {
            abort(404);
        }

        $ids = [$category->id];
        Category::getAllChildsId($ids, $ids);


        $meta = [
            'title'         => Lang::get('main.seo.product_page.title'),
            'description'   => Lang::get('main.seo.product_page.description')
        ];

        $productsQuery = Product::whereIn('category_id', $ids)->where("status", 1);

        if ($request->get('price')) {
            $productsQuery->whereBetween('price', explode('-', $request->get('price')));
        }

        if ($request->get('colors')) {
            $productsQuery->whereIn('color', $request->get('colors'));
        }

        if (($sale = $request->get('sale')) == 1) {
            $productsQuery->where('sale_status', $sale );
        }

        $products = $productsQuery->orderBy('created_at','desc')->paginate(60);
        $category_id = $category->id;

        $title = 'Категория “'.$category->title.'”';

        $pages = Category::getBreadCrumbsForCategory($category_id);
        $pages = $this->breadcrumbs($pages);
        $sale = $request->has("sale") ? $request->get("sale") : null;
        return view('frontend.product.new_index', compact('products','meta', 'title', 'alias', 'category_id', 'pages', "sale"));
    }

    public function getCategory2(Request $request, $alias) {
        $category = Category::where('alias', $alias)->first();

        if(!$category) {
            abort(404);
        }

        $microtime = microtime(true);
        $ids = [$category->id];
        Category::getAllChildsId($ids, $ids);


        $meta = [
            'title'         => Lang::get('main.seo.product_page.title'),
            'description'   => Lang::get('main.seo.product_page.description')
        ];

        $productsQuery = Product::whereIn('category_id', $ids)->where("status", 1);

        if ($request->get('price')) {
            $productsQuery->whereBetween('price', explode('-', $request->get('price')));
        }

        if ($request->get('colors')) {
            $productsQuery->whereIn('color', $request->get('colors'));
        }

        if (($sale = $request->get('sale')) == 1) {
            $productsQuery->where('sale_status', $sale );
        }

        $products = $productsQuery->orderBy('created_at','desc')->paginate(60);
        $category_id = $category->id;

        $title = 'Категория “'.$category->title.'”';

        $pages = Category::getBreadCrumbsForCategory($category_id);
        $pages = $this->breadcrumbs($pages);
        $sale = $request->has("sale") ? $request->get("sale") : null;
        return view('frontend.product.new_index', compact('products','meta', 'title', 'alias', 'category_id', 'pages', "sale"));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRecommended(Request $request)
    {
        $title = 'Рекомендованные товары';
        $this->breadcrumbs([["link" => null, "name" => $title]]);

        return $this->getShowProducts($request, $title, ["key" => "recommended", "value" => 1]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPopular(Request $request)
    {
        $title = 'Популярные товары';

        $this->breadcrumbs([["link" => null, "name" => $title]]);
        // $page = ["link" => null, "name" => $title];

        return $this->getShowProducts($request, $title);
    }

    /**
     * @param $request
     * @param string $title
     * @param array $other_column
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getShowProducts($request, $title = 'Продукты', $other_column = [], $all_category = false, $sale = null)
    {
        $info = $this->products($request, null,  $other_column);
        $meta = [
            'title'         => Lang::get('main.seo.product_page.title'),
            'description'   => Lang::get('main.seo.product_page.description')
        ];

        if(!Session::has('product-view-mode')){
                Session::put('product-view-mode','grid');
        }

        $alias = null;


        $info['pages'] = Category::getBreadCrumbsForCategory($info['category_id']);
        $info['pages'][] = ["link" => null, "name" => $title];

        $info['pages'] = $this->breadcrumbs($info['pages']);
        $info['title'] = $title;
        $info['alias'] = $alias;
        $info['meta'] = $meta;
        $info['all_category'] = $all_category;
        $info['sale'] = $sale;
        return view('frontend.product.new_index',$info);
    }

    /**
     * Products page with filter and search
     * @param  Request $request
     * @param  string $title
     * @return view
     */
    private function products(Request $request, $category = null, $other_column = []){
            $category_id = null;
            $category_alias = null;
            $product = Product::where("status", 1);
            $pages = [];
            if($request->has('keyword') && !empty($request->get('keyword'))){
                $keyword = preg_split('/\s+/', $request->get('keyword'), -1, PREG_SPLIT_NO_EMPTY);

                $product = $product->where(function ($q) use ($keyword) {
                        foreach ($keyword as $value) {
                                $q->orWhere('title', 'like', "%{$value}%");
                        }
                });
            }

            if($request->has('category')){
                $category = Category::where("alias", $request->get("category"))->first();
                $category_id = $category->id;
                $ids = [$category->id];
                Category::getAllChildsId($ids, $ids);

                $product = $product->whereIn("category_id",$ids);
                $category_alias = $request->get("category");
            }

            if($request->has('direction')){
                    $shop = Shop::select('id')->where('mall_type',$request->get('direction'))->get()->toArray();
                    $product = $product->whereIn('shop_id',$shop);
                }

            if($request->has('mall')){
                    $shop = Shop::select('id')->where('mall_id', $request->get('mall'))->get()->toArray();
                    $product = $product->whereIn('shop_id',$shop);
                }

            if($request->get('price')){
                $prices = explode("-", $request->get('price'));

                if(!empty($prices))
                    $product = $product->where('price','>=',intval($prices[0]));

                if(count($prices) == 2){
                    $product = $product->where('price','<=',intval($prices[1]));
                }

            }

            if($request->has('min_price') && is_numeric($request->get('min_price'))){
                    $product = $product->where('price','>=',intval($request->get('min_price')));
            }
            if($request->has('max_price') && is_numeric($request->get('max_price')) && $request->get('max_price') != "unlim"){
                 $product = $product->where('price','<=',intval($request->get('max_price')));
            }

            if (($sale = $request->get('sale')) == 1) {
                $product->where('sale_status', $sale );
            }

            if ($request->get('colors')) {
                $product->whereIn('color', $request->get('colors'));
            }

            if(!empty($other_column)){
                $product = $product->where($other_column['key'], $other_column['value']);
            }

            // Сортировка при переключениии
            $sort = ['date' => 'created_at', 'price' => 'price', 'view_amount' => 'view_amount', 'recommended' => 'recommended'];
            if($request->has('sort_by') && isset($sort[$request->get('sort_by') ])){

                $product = $product->orderBy($sort[$request->get('sort_by')], 'desc');

            }else{
                    $product->orderBy('created_at','desc');
            }
//        print_r($product->toSql());
            $products = $product->paginate(60);




            return compact("products", "category_id", "pages", "category_alias");
    }


    /**
    * Product view page
    * @param  array  $product
    * @param  array  $meta
    * @return view
    */
    public function getView(Request $request, $alias){

            $product = Product::where('alias', $alias)->where("status", 1)->firstOrFail();


            if(!in_array($product->id, Session::get('viewed_products', [])) ){
                    $product->increment('view_amount');
                    Session::push('viewed_products', $product->id);
            }

            if(!$product){
                    abort(404);
            }

            $products=Product::where('id', '!=', $product->id)
                    ->where('category_id',$product->category_id)
                    ->where("status", 1)
                    ->orderBy('recommended')
                    ->orderBy('id','desc')
                    ->take(20)
                    ->get();

            $meta = [
                'title'  =>  $product->title,
                'description'  => $product->description,
            ];

            $favourite = null;
            $user_like = null;
            if(Auth::check()){
                $user_like = Likes::where('product_id',$product->id)
                                ->where('user_id',$request->user()->id)
                                ->first();
                $favourite = Favourite::where('product_id', $product->id)
                                ->where('user_id', $request->user()->id)
                                ->first();
            }


            $pages = Category::getBreadCrumbsForCategory($product->category_id);
            $pages[] = ['link' => null, 'name' => $product->title];
            $pages = $this->breadcrumbs($pages);

            $product->size();
            $model = $product;

            return view('frontend.product.show_new',compact('model', 'product', 'products', 'meta', 'favourite','user_like', 'pages'));
    }



    public function getAll($alias=null){
            if(!$alias) abort('404');

            if($alias=='popular'){

                    $title=Lang::get('site.titles.recommended');
                    $popular_product = Product::where('popular','1')->where("status", 1)
                            ->orderBy('popular')
                                ->orderBy('view_amount','desc')
                                    ->paginate(20);
            }

            if($alias=='recommended'){

                    $title=Lang::get('site.titles.recommended');
                    $recommended_product = Product::where('recommended','1')->where("status", 1)
                                ->orderBy('recommended')
                                    ->orderBy('view_amount','desc')
                                        ->paginate(20);
            }
    }

    public function getDiscountProducts(Request $request){
        $title = 'Товары со скидкой';
        $this->breadcrumbs([["link" => null, "name" => $title]]);


        return $this->getShowProducts($request, $title, ["key" => "sale_status", "value" => 1], true, $sale = 1);
    }


	 /**
		* Like system
		* @param Request $request
		* @return json
		*/

    public function postLike(Request $request,$product_id = null)
    {
        $user = $request->user();
            if($user && !is_null($product_id)){

                    $like_product = Likes::where('product_id', $product_id)
                            ->where('user_id', $user->id)->first();

                        if(!$like_product){

                                $create_like = Likes::create([
                                    'user_id' => $user->id,
                                    'product_id' => $product_id
                                ]);

                                if($create_like) {
                                    $json = ['error' => 0];
                                }
                                else {
                                    $json = ['error' => 1];
                                }
                        }

                        else{
                            $delete_like = $like_product->delete();

                                if($delete_like){
                                        $json = ['error' => 1];
                                }else  {
                                        $json = ['error' => 0];
                                }

                        }

                    return response()->json($json);
            }
    }

    /**
    * Add to Favourites
    * @param Request $request
    * @return json
    */
    public function addFavourite(Request $request, $product_id = null)
    {
            if ( Auth::check() && !is_null($product_id) ) {

                    $f_product = Favourite::where('product_id', $product_id)
                            ->where('user_id', $request->user()->id)
                            ->first();

                    if (!$f_product) {
                            $add_f = Favourite::create([
                                    'user_id' => $request->user()->id,
                                    'product_id' => $product_id
                            ]);

                            if($add_f) {
                                    $json = ['error' => 0, 'text' => 'Убрать'];
                            } else {
                                    $json = ['error' => 1];
                            }
                    } else {
                            $delete_f = $f_product->delete();

                            if($delete_f) {
                                    $json = ['error' => 0, 'text' => 'Добавить в избранное'];
                            }else {
                                    $json = ['error' => 1];
                            }

                    }

                    return response()->json($json);
            }
    }


   /**
     * @param Request $request
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getForm(Request $request,$id=null){
        if(!$request->user()->hasRole('admin') && !$request->user()->hasRole('salesman')) {
            abort(404);
        }

        if($id==null){
            $meta = ['title' => Lang::get('main.cabinet.product_add')];
        }else{
            $meta = ['title' => Lang::get('main.cabinet.product_edit')];
        }

        $product = Product::find($id);

        if(!$product){
            $shop = Shop::where("user_id", Auth::user()->id)->first();

            if(!$shop){
                return redirect("/cabinet/shop");
            }

            if($shop->status == 0){
                Session::flash('success', "Ваш магазин заблокирован");
                return redirect()->back();
            }

            $product = [];
        } else if($product->status == 0){
            Session::flash('success', "Товар заблокирован");
            return redirect()->back();
        } else {
            $product->size_product = json_decode($product->size_product, true);
        }


        $category = Category::orderBy('title_ru','desc')
            ->where('parent_id', '0')
            ->pluck('title_ru', 'id');

        $shop = Shop::orderBy('id', 'asc')
            ->where('user_id', $request->user()->id)
            ->first();

        if(!$shop){
            return redirect()->action('ShopController@getForm');
        }

        $categories = Category::orderBy('title_ru', 'asc')->where('parent_id','0')->get();

        $pages[] = ["link" => "/cabinet/products", "name" => "Мои товары"];
        $pages[] = ["link" => null, "name" => $product ? $product->title : "Добавление товара"];


        $size_product = SizeProduct::all();

        return view('frontend.cabinet.product-form', compact('meta','shop','categories','product', 'pages', 'size_product'));
    }


    public function postForm(Request $request, $id = NULL){
        $data = $request->only(
            'title',
            'shop_id',
            'category_id',
            'price',
            'color',
            'currency',
            'count_product',
            'description',
            'price',
            'currency',
            'images_data',
            'size_product',
            'sale_status',
            'sale_value'
        );
        $rules = [
            'title'            => 'required|min:2|max:255',
            'shop_id'          => 'required',
            'category_id'      => 'required|integer',
            'price'            => 'required|integer|min:0|max:10000000',
            'description'      => 'required|min:2',
            'size_product' 	   => 'required',
            'sale_status'      => 'boolean',
            'sale_value'	   => 'integer|min:0|max:100'
        ];
        $images_data = $data['images_data'];

        $data['count_product'] = 100000000;

        if(!$request->has("sale_status")) {
            $data['sale_value'] = 0;
            $data['sale_status'] = 0;
        } else {
            $data['sale_status'] = $data['sale_value'] > 0 ? 1 : 0;
        }

        $data['currency'] = 'UZS';

        $validator = Validator::make($data,$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            $data['size_product'] = json_encode($data['size_product']);

            if($id==NULL){
                $data['status'] = 1;
                $data['user_id'] = $request->user()->id;
                $data['alias'] = $this->aliasCreate($data['title']);
                unset($data['images_data']);
                $product = Product::create($data);
            }else{
                $product = Product::find($id);
                if($product)
                    unset($data['images_data']);
                $product->update($data);

                $images_for_delete = explode(',', $request->input('images_for_delete'));
                $images = Upload::getFiles("product", $id);

                if (strlen($images_for_delete[0]) > 0 && !empty($images_for_delete)) {

                    foreach ($images_for_delete as $del) {
                        if(isset($images[$del]))
                            $files[] =  $images[$del];
                    }
                    if(isset($files))
                        Upload::removeFiles('product', $id, $files);
                }

            }

            $files = [];
//			if($request->hasFile('images')){

            $id = $id == NULL ? $product->id : $id;
            if($images_data){
                $images_data = json_decode($images_data, true);
                $product->uploadFile($images_data);
//				$files = Upload::saveFiles("product", $id == NULL ? $create->id : $id,
//					$request->file('images'));
            }


            $product->addMainImage();

            // self::removeTempFiles();
            Session::flash('success', Lang::get('alert.product.'.($id==NULL ? 'insert' : 'update')));
            return redirect()->action('CabinetController@getIndex');
        }
    }

    /**
     * Product delete
     * @param  $request
     */
    public function postDelete(Request $request)
    {
        if($request->has('delete') && Product::find($request->input('delete'))
                ->delete()){
            $size = [[213, 157], [208, 200]];
            Upload::removeFilesBySize("product", $request->input('delete'), $size);
            Session::flash('success', Lang::get('alert.success.delete'));
        }
        return redirect()->back();
    }

    public function removeFile(Request $request){
        $data = $request->only("url");

         $name = public_path()."/".$data['url'];

         if(is_file($name))
             unlink($name);
         header("Content-Type: application/json");
         return $name;
    }


	 /**
		* Used for getting view type grid || list
		* @param  $request
		*/
    public function getChangeMode(Request $request)
	 {
         $data = $request->all();

         $info = $this->products($request);
         $products = $info['products'];
         $category_id = $info['category_id'];

         if ($data['mode'] == 'list')
         {
                Session::put('product-view-mode', $data['mode']);
                return view('partials.product-list-mode', compact('products', "alias", 'category_info'));
         }

         Session::put('product-view-mode', $data['mode']);
         return view('partials.product-grid-mode', compact('products', 'alias', 'category_id'));
	 }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
	public function getMallCategory()
	{
		$direction_id = Input::get('id');
		$locale = App::getlocale();
		$mall = MallCategory::select('title_'.$locale, 'id')->where('parent_id', $direction_id)->get();
		return response()->json($mall);
	}

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function getSubCategory() {
		$category_id = (int)Input::get('id');
		if(!$category_id)
			return;
		$locale = App::getlocale();
		$category = Category::select('title_'.$locale, 'id')->where('parent_id', $category_id)->get();
		if(count($category) == 0)
			return;
		return view("frontend.product.subcategory", compact("category", "category_id"));
	}


    /**
     * @param $value
     * @return string
     */
	private function aliasCreate($value){
        $slug = str_slug($value);
        $model = new Product;
        $slugCount = count($model->whereRaw("alias REGEXP '^{$slug}(-[0-9]+)?$' and id != '{$model->id}'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
	}


	private function getCategoryExtendsForPage(){

	}

	private function getChildsIdCategory($id, &$ids){
		$category = Category::select("id", "parent_id")->where("parent_id", $id)->get();

		if($category == null)
			return;
		foreach ($category as  $item) {
			$ids[] = $item['id'];
			$this->getChildsIdCategory($item['id'], $ids);
		}

	}


}
