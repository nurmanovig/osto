<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\Click;

class ClickController extends Controller
{

	public function action(Request $request){
		$click = new Click();
		$click->params = $_REQUEST;
		$click->listening();
	}
}