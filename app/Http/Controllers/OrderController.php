<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\PromoCode;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Models\Click;

class OrderController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getForm(){
        
        
      
        Session::forget("promo");
    	$user = Auth::user();

        $array = $this->getSummInner();
        
        $array['user'] = $user;
        $array['summ_str'] = $array['summ'] . " UZS";


    	return view("frontend.order.form", $array);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPartCart(){
        
        $user = Auth::user();

        $array = $this->getSummInner();
        $array['user'] = $user;
        $array['summ_str'] = $array['summ'] . " UZS";
        return view("frontend.order.part-form", $array);
    }

    /**
     * @return bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCheckoutForm(){
        if(!Session::has("cart")){
            return false;
        }

        $user = Auth::user();
        $order = new Order;

        if($user){
            $order->username = $user->name . " ". $user->lastname;
            $order->phone = $user->phone;
        }

        return view("frontend.order.form-checkout", compact("order"));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkPromo(Request $request){
        $promo = PromoCode::get();
        if(!$request->code){
            return response()->json(['errors' => "Неверный код"]); 
        }

        $res = PromoCode::checkCode($request->code);
        if($res !== false){
            Session::put("promo", $request->code);
            return response()->json(['success' => "Ваш промо код сущесвует, скидка на покупку: " . $res. "%"]);
        }

        return response()->json(['errors' => "Неверный код"]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function postForm(Request $request){
        $response = static::checkoutValidate($request);

        if($response['status'] == 0){
            return $response['data'];
        }else{
            $order_data = $response['data'];
        }

        $all_info = $this->getSummInner();

        $order_data['summ'] = $product_summ = $all_info['summ'];
        $sale_summ = 0;

        if(Session::has("promo")){
            $sale = PromoCode::checkCode(Session::get("promo"));
            $sale_summ = $order_data['summ'] * ($sale)/100;
            $order_data['summ'] -= $sale_summ;
        }

        $order_data['sale'] = $sale_summ;
        $delivery_price =  $this->settings("price_delivery");
        $order_data['summ'] +=  $delivery_price;
        /* Put sum*/
        $sale_summ += $all_info['sale_summ'];

        Session::put("order_data", $order_data);


        return view("frontend.order.payment-form", [
            "summ" => $order_data['summ'],
            "sale_summ" => $sale_summ,
            "product_summ" => $product_summ,
            "delivery_price" => $delivery_price,
            'order_data' => $order_data,
            'products' => $all_info['products'],
            "currency" => "UZS",
        ]);
    }

    public static function checkoutValidate(Request $request){
        $data = $request->only('username', 'phone');

        $rules = [
            "username" => "required|max:255",
            "phone" => "required|max:15",
        ];

        $data['phone'] = preg_replace('/[^\d]/is', '', $data['phone']);


        $validator = Validator::make($data, $rules);
        
        if($validator->fails()){
            return ['status' => 0, 'data' => response()->json(["notice" => "Заполнить все необходимые поля"])];
        }

        foreach(["address", "lat", "lng"] as $item){
            if(!Session::has("location.".$item)){
                return ['status' => 0, 'data' => response()->json(["notice" => "Укажите адрес доставки"])];
            }else{
                $order_data[$item] =  Session::get("location.".$item)[0];
            }
        }

        foreach($data as $key => $item){
            $order_data[$key] =  htmlspecialchars($item);
        }

        return ["status" => 1, "data" => $order_data];
    }

    /**
     * @return bool|\Illuminate\Http\JsonResponse|mixed
     * @throws \Illuminate\Support\Facades\Exception
     */
    public function checkout(){
        $order_data = Session::get("order_data");
        $order = new Order;

        $user = Auth::user();
        if($user)
            $order->user_id =  $user->id; 
        else
            $order->user_id = 0;
           
        $order->with_delivery =  1;
        foreach ($order_data as $key => $value) {
            $order->$key = $value;
        }
        
        /* Put sum*/

        DB::beginTransaction();
        // try{
            if(!$order->save()){
                return ['notice' => "Неизвестная ошибка"];
            }
            
            
            $cart = json_decode(Session::get("cart"), true);
           
            $data = $this->changeCartToData($cart, $order->id);
            $error_str = $this->checkCountProduct($data);
            
            if($error_str){
                return ['notice' => $error_str]; 
            }
             
            $res = DB::table("order_product")->insert($data);
            
            if($res){
              Session::forget("cart");
                Session::forget("promo");
               
    
                DB::commit();
                return $order->id;
            }  
        // }catch(\Exception $e){
             DB::rollback();
        
            return ['notice' => "Неизвестная ошибка"];
        // }
       
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Support\Facades\Exception
     */
    public function payment(Request $request){
        $data = $request->only("pay_type", 'comments');
        $types = [1 , 2 ];

        
        if(array_search($data['pay_type'], $types) === false ){
            return response()->json(["notice" => "Выберите способ оплаты"]);
        }
        
        $data['comments'] = htmlspecialchars($data['comments']);

        $order_data = Session::get("order_data");
        $order_data['comments'] = $data['comments'];
        $order_data['pay_type'] = $data['pay_type'];
        $order_data['status'] = $data['pay_type'] == 1 ? Order::STATUS_INACTIVE : Order::STATUS_FAIL;
        Session::put("order_data", $order_data);

        $id = $this->checkout();
        if(is_numeric($id)){
            if($data['pay_type'] == 1){
                $order = Order::find($id);

                $order->sendInfo();
                $response = ["success" => "Заказ отправлен"];

            }else{
                Session::put("order", $id);
                
                return self::getPaymentForm($id);
            }
           
        }
   
        return response()->json($response);

    }

    public function onlinePayment(Request $request){
        $data = $request->only("type");
        $types = [
                "click" => [2, "Перенаправление для оплаты через пару секунд .."],
                "payme" => [3, "Перенаправление для оплаты через пару секунд .."],
                "card" => [2, 'Заказ отправлен' ]
                ];

        if(!isset($types[$data['type']])){
            return response()->json(["notice" => "Выберите способ оплаты"]);
        }

        $id = Session::get("order");
        if(!$id)
            return false;

        
        Order::where("id", $id)->update(["pay_type" => $types[$data['type']][0], 'status' => Order::STATUS_INACTIVE]);

        $response = [];
       
           
        if($types[$data['type']][0] != 2){
            $response = static::getUrl($id, $data['type']);
        }

        $response["success"] = $types[$data['type']][1];
        
        return $response;
    }

    private static function getUrl($id, $type){
        
        $order = Order::find($id);
        if(!$order)
            return false;
        
      
        $urls = [
                    'payme' => [
                            'url' => 'https://checkout.paycom.uz/' . base64_encode("m=5b921bef8773480c529a0ee8;ac.order_id=".$order->id.";a=".$order->summ * 100)]
                ];

        return isset($urls[$type]) ?$urls[$type] : null;
    }
    
    private static function getPaymentForm($id){
        $order = Order::find($id);
        $result = Click::postForm(['transId'=>$order->id, 'phone_number' => $order->phone, 'amount' => $order->summ]);
        return view("frontend.order.payment-view", $result);
    }
 

    private function checkCountProduct($data){
        $error_str = null;
        foreach($data as $item){
            $res = Order::changeCount($item['product_id'], $item['count'], $item['size_product']);
            if($res !== true){
                $error_str .= $res."\n";
            }
        }

        return $error_str;
    }

    private function changeCartToData($cart, $order_id){
        foreach($cart as $product_id => $info){
            $product = Product::select('price', 'currency', 'sale_status', 'sale_value')->where("id", $product_id)->where("status", 1)->first();

            foreach($info as $size => $count){
                $sale = $product->price;
                if($product->sale_status){
                    $product->price *= (100 - $product->sale_value)/100;
                }
                
                $product->price = ceil($product->price);
                $sale -= $product->price;

                if( $product->currency == "usd"){
                    $product->price *= $this->settings("course");
                    $sale *= $this->settings("course");
                }

                $data[] = ["order_id" => $order_id, "product_id" => $product_id, "count" => $count, 'price' => $product->price, 'size_product' => $size, "sale" => $sale];
            }
            
        }

        return $data;
    }

    private function settings($key = null){
        try{
            if(Cache::has('settings')){
                $settings = Cache::get('settings');
            }else{
                
                $settings = [];
                $result = DB::table("settings")->first();
                
                foreach ($result as $name => $value) {
                    $settings[$name] = $value;
                }

                Cache::put('settings', $settings, 100);
            }

            if($key){
                return $settings[$key];
            }


            return $settings;
        }catch(\Exception $e){
            return null;
        }
    }

    public function getSumm(){

        $array = $this->getSummInner();
        return $array['summ'];   
    }

    private function getSummInner(){
        $cart = null;
        $summ = 0;
        $sale_summ = 0;
        $products = null;

        if(Session::has("cart")){
            $cart = Session::get("cart");
            $cart = json_decode($cart, true);
            $course = $this->settings("course");
            foreach($cart as $id => $count){
                $ids[] = $id;
            }


            $products = Product::whereIn('id', $ids)->where("status", 1)->get();

            $info = Product::getByIdsForCartArray($cart);
            $products = $info['products'];
            $summ = $info['summ'];
            $sale_summ = $info['sale_summ'];


        }
        return compact("cart", "summ", "products", "sale_summ");
    }

    public function cartProducts(){
        $ids = Session::get("cart");

        $products = [];
        foreach ($ids as $id => $info) {
            Product::where("id", $id)->one();
        }
    }

    private function getAllSale(){

    }

}
/* 122133*/