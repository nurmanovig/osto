<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

use App\Models\MallCategory;
use App\Models\Favourite;
use App\Models\Category;
use App\Models\Feedback;
use App\Models\Product;
use App\Models\Role;
use App\Models\Shop;
use App\Models\Page;
use App\Models\Bazar;
use App\User;

use Validator;
use Session;
use Upload;
use Auth;
use DB;

class PageController extends Controller
{
    public function getIndex(){

    	$recommended_product = Product::where('recommended','1')
        ->where("status", 1)
    		->orderBy('recommended')
    			->orderBy('view_amount','desc')
    				->take(20)->get();

    	$recommended_shops = Shop::where("is_recommended", "1")
                        ->where("status", 1)
                            ->orderBy('view_amount','desc')->take(20)
    			                 ->get();




        $categories = Category::where('parent_id', 0)->limit(5)->get();


        $product_count = number_format(Product::count(), 0, '.', ' ');
    	return view('frontend.page.index',compact(
            'recommended_product',
            'recommended_shops',
            'directions',
            'categories',
            'urls',
            'product_count'
        ));
    }

    /**
     * Show page by alias
     * @param  string $alias
     * @return view
    */
    public function show($alias)
    {
        $page = Page::where('alias', $alias)->first();

        if (empty($page)){
            abort(404);
        }
        // $pages = $this->breadcrumbs(["name" => $page->title]);
        return view('frontend.page.show', compact('page', 'pages'));
    }

    /**
    * Get all Bazars
    * @return view
    */
    public function getBazars()
    {
        $dir = MallCategory::find(1);
        return $this->dirs($dir);
    }

    /**
     * Get all Malls
     * @return view
    */
    public function getMalls()
    {
        $dir = MallCategory::find(2);
        return $this->dirs($dir);
    }

    /**
     * get Direction
     * @param  string $alias
     * @return view
    */
    public function getDir(Request $request, $alias)
    {
        $dir = MallCategory::where('alias', $alias)->first();
        $shops = Shop::latest()
            ->where("status", 1)
            ->where('mall_type', $dir->parent_id)
            ->where('mall_id', $dir->id);

        // Search
        if($request->has('q')) {
            $keyword = preg_split('/\s+/', $request->get('q'), -1, PREG_SPLIT_NO_EMPTY);
            $shops->where(function ($q) use ($keyword) {
                foreach ($keyword as $value) {
                    $q->orWhere('title', 'like', "%{$value}%");
                }
            });
        }

        $shops = $shops->paginate(16);
        if($dir->parent_id != 0){
            $dir_parent = MallCategory::where("id", $dir);
            $pages = [['link' => false, "name" => $dir->title]];
        }
        $pages = static::dirBreadCrumbs($dir);
        $pages = $this->breadcrumbs($pages);

        return view('frontend.page.dir', compact('dir', 'shops', 'pages'));
    }

    /**
     * Get Mall Categories by ID
     * @param  int $id
     * @return view
    */
    public function dirs($dir, $page = null)
    {
        $dirs = MallCategory::orderBy('id','desc')
            ->where('parent_id', $dir->id)
            ->paginate(16);

        // $pages = $this->breadcrumbs(["name"]);
        return view('frontend.page.dirs', compact('dir', 'dirs'));
    }

    /**
     * User feedback page
     * @return [type] [description]
    */
    public function getFeedback()
    {
        $types = DB::table('feedback_type')->get();
        return view('frontend.page.feedback', compact('types'));
    }

    public function postFeedback(Request $request)
    {
        $data = $request->only('g-recaptcha-response','feedback_type','text','email','phone');

        $rules = [
            'text'  => 'required',
            'email' => 'required',
            'phone' => 'required',
            'feedback_type' => 'required',
            'g-recaptcha-response'  => 'required|recaptcha',
        ];

        // dd($data);
        $validator = Validator::make($data,$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            unset($data['g-recaptcha-response']);
            Feedback::create($data);
        }

        Session::flash('success','Success');
        return redirect()->back();
    }


   /**
   * get all cateogry
   * @return view
   */
   public function getAllCategory()
   {
    $categories = Category::orderBy('id', 'asc')->where('parent_id','0')->get();
       //dd($categories);
    $title = "Все категории";
    $pages = [["link" => null, "name" => $title]];
    $pages = $this->breadcrumbs($pages);
    return view('frontend.category.index', compact('categories', 'pages', 'title'));
   }

   public function getCategory(Request $request){

        $category = Category::select("id", "alias", "parent_id", "title_ru", "title_uz")->where("alias", $request->alias)->with("childs")->first();
        if(!$category)
            abort(404);

        if(count($category->childs) == 0)
            return redirect("/product/category/".$category->alias);

        $categories = Category::orderBy('id', 'asc')->where('parent_id',$category->id)->get();
        // $pages = $this->getBreadCrumbsForCategory($category->id);
        $pages = [];
        $pages = Category::getBreadCrumbsForCategory($category->id);
        $pages = $this->breadcrumbs($pages);
        $title = $category->title;
        return view('frontend.category.index', compact('categories', 'pages', 'title'));
   }


   public static function dirBreadCrumbs($dir, $pages = []){
        try{
            if($dir->parent_id != 0){
                $parent_dir = MallCategory::where("id", $dir->parent_id)->first();
                $pages = static::dirBreadCrumbs($parent_dir, $pages);
                $pages[] = ["link" => "/dir/".$dir->alias, "name" => $dir->title];
            }else{
                switch($dir->id){
                    case 1:
                        $pages[] = ["link" => "/bazars", "name" => "Базары"];

                        break;
                    case 2:
                        $pages[] = ["link" => "/malls", "name" => "Торговые центры"];
                        break;
                    case 3:
                        $pages[] = ["link" => "/brand-shops", "name" => "Брендовые магазины"];
                        break;
                    case 4:
                        $pages[] = ["link" => "/private-shops", "name" => "Чатсные торговцы"];
                        break;
                }
            }
        }catch(\Exception $e){
            return  [];
        }
        return $pages;
   }

    public function getMap(){
        return view("frontend.page.map");
    }

    public function postMapInit(Request $request){
        $data = $request->only('address','lat','lng');

        $rules = [
            'address'     => 'required',
            'lat'         => 'required|max:255',
            'lng'         => 'required|max:255',
        ];

        $validator = Validator::make($data,$rules);

        if($validator->fails()){
            return response()->json(['errors' => "errors"]);
        }else{
            Session::push("location.address", $request->address);
            Session::push("location.lat", $request->lat);
            Session::push("location.lng", $request->lng);
            return response()->json(['success' => "success"]);
        }
    }

    public function getShops(){

        $shops = Shop::select(
                'title', 'id', 'main_image', 'address',
                'working_hour_start', 'working_hour_end',
                'day1', 'day2', 'day3', 'day3', 'day4', 'day5', 'day6', 'day7'
                )
                ->where('status', '!=', 0)->get();

        return view("partials.menu-shop", compact("shops"));

    }


    // public function getCategor
}
