<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\OrderProduct;
use App\Models\Favourite;
use App\Models\Comments;
use App\Models\Product;
use App\Models\Order;
use App\Models\Shop;


use Validator;
use App\User;
use Session;
use Auth;
use Hash;
use Lang;
use App;


class CabinetController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request) {
        $product = Product::withCount('favourite')->where('user_id', $request->user()->id)
            ->orderBy('id','desc')
            ->paginate(15);

        $statistics = [];
        $statistics['products'] = $request->user()->product()->count();
        $statistics['orders'] = $request->user()->productOrders()->count();
        $statistics['favourite'] = $request->user()->productFavourites()->count();
        $statistics['comments'] = $request->user()->productComments()->count();
        $statistics['phones'] = $request->user()->shops()->sum('phone_view_amount');


        return view('frontend.cabinet.index', [
            'product' => $product,
            'statistics' => $statistics
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getShops(Request $request) {
        $shop = Shop::where('user_id',$request->user()->id)->paginate(10);
    	return view('frontend.cabinet.shops',compact('shop'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getReviews(Request $request) {
        $reviews = Comments::where('user_id', $request->user()->id)
                    ->orderBy('id','asc')
                    ->paginate(10);

    	return view('frontend.cabinet.reviews', compact('reviews'));
    }

    public function getUserReviews(Request $request)
    {
        $user = $request->user();
        $ids = $user->productsId();

        if($ids){
            $reviews = Comments::whereIn('product_id', $ids)
                                    ->orderBy('id','desc')
                                        ->paginate(10);
        return view('frontend.cabinet.reviews', compact('reviews'));                             
        }
        $reviews = 0;
        return view('frontend.cabinet.reviews', compact('reviews'));                
    }

    /**
     * get Users favorite products
     * @param  array $value 
    */
    public function getFavorite(Request $request)
    {
        $favorites = Favourite::where('user_id', $request->user()->id)->get();
        return view('frontend.cabinet.favorite', compact('favorites'));
    }

    /**
     * delete favorite product
     * @param  array $value 
    */
   
   public function postDelete(Request $request)
   {
        if($request->has('delete') && Favourite::find($request->input('delete'))->delete()){
            Session::flash('success', Lang::get('alert.success.delete'));
        }

        return redirect()->back();
   }


    /*
    |--------------------------------------------------------
    | USER PROFILE SETTINGS
    |--------------------------------------------------------
    */

    public function getProfile()
    {
        $user = Auth::user();
    	return view('frontend.cabinet.profile', compact('user'));
    }

    public function postProfile(Request $request)
    {
        $user = Auth::user();
        $data = $request->only('name','lastname','email','phone');

        $rules = [
            'name'     => 'required|max:255',
            'lastname' => 'required|max:255',
            'email'    => 'email|unique:users,email,'.$user->id.'',
            'phone'    => 'required|max:255',
        ];

        $validator = Validator::make($data,$rules);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
            $user = User::find($request->user()->id);
            if($user){
                $user->update($data);
            }
        }

        Session::flash('success', Lang::get('alert.profile.update'));
        return redirect()->back();
    }

    public function postChangePassword(Request $request)
    {
        $user = User::find($request->user()->id);
        $data = $request->only('password','password_confirmation');

        $rules = [
            'password'              => 'required|min:6',
            'new_password'          => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ];

        // dd($data);
        $validator = Validator::make($data,$rules);

        if(!Hash::check($data['password'], $user->password)){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else{
             $data['password'] = Hash::make($request->new_password);    
            if($user->update($data)){
                Session::flash('success', 'Ваш пароль успешно изменен');
            }
            return redirect()->back();
        }
    }


    public function getOrders(Request $request, $o_status = null){
        $link = [];
        // $op_status = $o_status = null;
        if(!Auth::user()->hasRole("admin") && !Auth::user()->hasRole("salesman")){
            App::abort(404);
        }

        $query_summ = OrderProduct::select(
            DB::raw("sum(count*order_product.price) as summ"

                    ))
                    ->join("orders", "orders.id", "order_id")
                    ->join("products", "products.id", "product_id")
                    ->join("shops", "shops.user_id", "products.user_id");

        $query_items = OrderProduct::select(
            DB::raw("orders.created_at created_at,
                    orders.status o_status, 
                    order_product.*, 
                    shops.title as sh_title, 
                    shops.alias sh_alias, 
                    products.title p_title,
                    products.alias p_alias,
                    order_product.size_product as size_product"

                    ))
                    ->join("orders", "orders.id", "order_id")
                    ->join("products", "products.id", "product_id")
                    ->join("shops", "shops.user_id", "products.user_id");

        if($request->o_status !== null){
            $query_summ->where("orders.status", $request->o_status); 
            $query_items->where("orders.status", $request->o_status);
            $link['o_status'] = $request->o_status;
        }


        if($request->op_status){
            $query_summ->where("order_product.status", $request->op_status); 
            $query_items->where("order_product.status", $request->op_status);
            $link['op_status'] = $request->op_status;
        }
        $summ = $query_summ->first()->summ;


        $order_products = $query_items->orderBy("created_at", "desc")
                                ->paginate(10);

        $product_model = new App\Models\Product;

        $pages = [["link" => "/cabinet/product", "name" => "Персональный кабинет"], ["link" => null, "name" =>"Заказы"]];
        $pages = $this->breadcrumbs($pages);

        return view("frontend.cabinet.order", compact("order_products", "link", "summ", "pages", "product_model"));
    }

    public function getMyOrders(Request $request){
        $orders = Order::where("user_id", Auth::user()->id)->orderBy("id", "desc")->paginate(10);

        $pages = [["link" => "/cabinet/product", "name" => "Персональный кабинет"], ["link" => null, "name" => "Мои заказы"]];
        $pages = $this->breadcrumbs($pages);

        return view("frontend.cabinet.my-orders", compact("orders", "pages"));
    }

    public function getMyOrder(Request $request){
        try{
            $products = OrderProduct::select("order_product.*")->join("orders", "id", "order_id")->where("user_id", Auth::user()->id)->where("order_id", $request->id)->with("product")->get();
            //if(isset($product) && $product->products){
                return view("frontend.cabinet.my-order", compact("products"));
            //}else{
              ///  return "Неизвестная ошибка";
            //}
            
        }catch(\Exception $e){
            return "Неизвестная ошибка";
        }
    }

    public function getMyBalans(){
        $balans = 0 . " сум";
        return view("frontend.cabinet.my-balans", compact("balans"));
    }

    public function postStatus(Request $request){
        $result =  OrderProduct::changeStatus($request->order_id, $request->product_id, $request->size_product, $request->status);

        return  response()->json(['success' => 'success']);
    }
}
