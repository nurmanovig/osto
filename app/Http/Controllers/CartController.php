<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Comments;
use Validator;
use Lang;
use App\Models\Product;
use App\Models\Order;
use Session;

class CartController extends Controller
{

    public function getCart(){
        if(Session::has("cart")){
            $cart = Session::get("cart");
            $cart = json_decode($cart, true);


            $info = Product::getByIdsForCartArray($cart);

            $products = $info['products'];
            $summ = $info['summ'];
           

        }
        return view("frontend.cart.view", compact("products", "cart", "summ"));
    }
    
    public function getCount(){
        $count = 0;
        if(Session::has("cart")){
            $cart = Session::get("cart");
            $cart = json_decode($cart, true);
            $count = $this->cartItemCouter($cart);
        }

        return $count;
        
    }

    public function addCart(Request $request = null)
    {
        $cart = [];
        if(Session::has("cart")){
            $cart = Session::get("cart");
            $cart = json_decode($cart, true);
        }

        // if(!empty($cart))
        //     return response()->json(["notice" => serialize($cart)]);

        $count = $request->count > 0 ? $request->count : 0;

        if(empty($cart) || isset($cart[$request->id][$request->size])=== false){

            $cart[$request->id][$request->size] = $count;
           
        }else{
            $cart[$request->id][$request->size] += $count;
        }

        $res = Order::checkCount($request->id,$cart[$request->id][$request->size], $request->size);

        if($res !== true){
            return response()->json(['notice' => $res]);
        }

        Session::put("cart", json_encode($cart));

        return $this->cartItemCouter($cart);
    }

    public function changeCart(Request $request){
        if(!Session::has("cart")){
            return false;
        }
       
        $cart = Session::get("cart");
        $cart = json_decode($cart, true);
        $count = $request->count > 0 ? $request->count : 1;

        
        if(isset($cart[$request->id][$request->size])){
            $res = Order::checkCount($request->id, $count, $request->size);

            if($res !== true){
                return response()->json(['notice' => $res]);
            }else{
                $cart[$request->id][$request->size] = $count;
            }
        }


        Session::put("cart", json_encode($cart));
        return response()->json(['success' => 'success']);
    }

    public function deleteItem(Request $request){


        if(!Session::has("cart")){
            return false;
        }
        $cart = Session::get("cart");

        $cart = json_decode($cart, true);

        if(isset($cart[$request->id][$request->size])){
                  
          
            unset($cart[$request->id][$request->size]);
            if(empty($cart[$request->id]))
                unset($cart[$request->id]);
                
                 
       
            if(!empty($cart)){
                
                Session::put("cart", json_encode($cart));
            }else{
               
              Session::forget('cart');
              Session::save();
            }
            
        } 

     
        return "true";

            
    }

    public function clear(){
        Session::forget('cart');
    }

    public function cartItemCouter($cart){
        $count = 0;
        foreach ($cart as $key => $value) {
            $count += array_sum($value);
        }
        return $count;
    }

    public function cartProducts(){

    }
    

}
