<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Favourite;
use Upload;

class Product extends Model
{
    protected $guarded = [];
    protected $table = 'products';
    public $_id = 0;
    public $_count = 0;
    public $_price = 0;
    public $_countprice = 0;
    public $_size = [];
    public $_saleprice = 0;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category() {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subcategory() {
        return $this->hasOne('App\Models\Category', 'id', 'subcategory_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories() {
        return $this->belongsToMany(
            'App\Models\Category',
            'product_category',
            'product_id', 'category_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shop() {
        return $this->hasOne('App\Models\Shop', 'id', 'shop_id');
    }

    /**
     * @return string
     */
    public function getPriceFormatAttribute() {
        return number_format($this->price,0,'.',' ');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() {
        return $this->hasMany('App\Models\Comments');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes() {
        return $this->hasMany('App\Models\Likes');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function favourite() {
        return $this->hasOne('App\Models\Favourite', 'product_id', 'id');
    }

    public static function arrayAllKeys($array){
        $ids = [];
        foreach($array as $key => $item){
            $ids[] = $key;
        }

        return $ids;

    }

    public static function getByIdsForCart($cart){
        $_products = [];
        $ids = static::arrayAllKeys($cart);
        $sale_summ = 0;
        $summ = 0;
        $products = self::select()->whereIn('id', $ids)->get();

        foreach($products as $key => $product){
            foreach($cart[$product->id] as $size => $count){

                $price = $product->price * (100 - $product->sale_value) / 100;

                $product->_price = ceil($price);
                $product->_size = serialize($cart);
                $product->_count = $count;
                $product->_countprice = $count * $product->_price;
                $product->_saleprice =  $count * ($product->price - $product->_price);

                $_products[] = $product;
                $sale_summ += $product->_saleprice;
                $summ += $product->_countprice;
            }

        }

        return ['products' => $_products, "summ" => $summ, 'sale_summ' => $sale_summ];
    }

    public static function getByIdsForCartArray($cart){
        $_products = [];
        $ids = static::arrayAllKeys($cart);
        $sale_summ = 0;
        $summ = 0;
        $products = self::select()->whereIn('id', $ids)
            ->with("shop")
            ->get()
            ->toArray();

        foreach($products as $key => $_product){
            foreach($cart[$_product['id']] as $size => $count){


                $price = $_product['price'] * (100 - $_product['sale_value']) / 100;

                $_product['_price'] = ceil($price);
                $_product['_size'] = $size;
                $_product['_count'] = $count;
                $_product['_countprice'] = $count * $_product['_price'];
                $_product['_saleprice'] =  $count * ($_product['price'] - $_product['_price']);
                $_product['_id'] = $_product['id'];
                $_products[] = $_product;
                $sale_summ += $_product['_saleprice'];
                $summ += $_product['_countprice'];
            }

        }

        return ['products' => $_products, "summ" => $summ, 'sale_summ' => $sale_summ];
    }


    /**
     * @return array|mixed
     */
    public function size() {
        if(!isset($this->size_product) || empty($this->size_product))
            return [];
        $this->size_product = json_decode($this->size_product, true);
        $this->size_product = SizeProduct::whereIn("id", $this->size_product)->get();
        return $this->product;
    }

    /**
     * @param $size
     * @return mixed|null
     */
    public function getSizeInfo($size = null) {
        $size = SizeProduct::firstOrNew(["id" => $size ? : $this->_size]);
        return !empty($size) ? $size->key : null;
    }

    public static function getSizeInfoStatic($size) {
        $size = SizeProduct::firstOrNew(["id" => $size ]);
        return !empty($size) ? $size->key : null;
    }

    public static function hasMainImage($category, $id, $name = null){
        $files = Upload::readDirFile(public_path()."/uploads/".$category."/".$id);
        // $files = Upload::getFiles("product", $id);

        if(!$name)
            return [
                "error" => false,
                "name"=>isset($files[0]) ? $files[0]: null
            ];

        foreach($files as $file){
            $file_name = pathinfo($file, PATHINFO_BASENAME);
            if($file_name == $name)
                return true;
        }

        return [
            "error" => false,
            "name"=>isset($files[0]) ? $files[0]: null,
            "files" => $files
        ];
    }

    // public static function addMainImage( $id, $name){

    //     $name = pathinfo($name, PATHINFO_BASENAME);

    //     $product = Product::find($id);
    //     $data["main_image"] = $name;
    //     $product->update($data);

    //     Upload::resize("product", $id, $name);

    // }

    // public static function moveFilesToDir($id, $files){
    //     $toDir = "uploads/product/".$id;
    //     if(!is_dir($toDir)){
    //         mkdir($toDir);
    //     }
    //     foreach($files as $file){
    //         if(!is_file($file))
    //             continue;
    //         $name = basename($file);
    //         rename($file, $toDir."/".$name);
    //     }
    // }

    public function addMainImage( ){
        $res = Upload::hasMainImage("product", $this->id, $this->main_image);

        if($res === true){

            return;
        }

        $name = pathinfo($res['name'], PATHINFO_BASENAME);

        $data["main_image"] = $name;
        $this->update($data);

        Upload::resize("product", $this->id, $name);

    }

    /*    */
    public function uploadFile($images_data){

        foreach($images_data as $data){
            $this->saveImgData($data);
        }
    }

    private function saveImgData( $data){
        $dir = "uploads/product/{$this->id}/";
        if(!is_dir($dir)){
            mkdir($dir);
        }
        $unique = str_replace(".", "_", microtime(true));
        $name =  $dir. "/".$this->id."_".$unique.".jpeg";

        file_put_contents($name, self::cutFile($data['url']));
        usleep(10);
    }

    private static function cutFile($data){
        $data = preg_replace("/data:image\/\w*;base64,/", "", $data);
        return base64_decode($data);
    }


}
