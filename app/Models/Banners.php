<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model {

    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $table = 'banners';

    protected $dates = ['created_at'];

    protected $fillable = ['url', 'actived_at','img'];
}