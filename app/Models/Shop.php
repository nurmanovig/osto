<?php

namespace App\Models;
use Log;
use Lang;
use App;
use Upload;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $guarded = [];
    protected $table = 'shops';
    protected $fillable = [
        'main_image',
        // 'phone_view_amount',
        'title', 'address',
        'user_id', 'alias',
        'mall_id', 'lat', "lng",
        'working_hour_start', 'working_hour_end',
        'mall_type','phone',
        'day1',
        'day2',
        'day3',
        'day4',
        'day5',
        'day6',
        'day7',
    ];

    public function direction(){
        return $this->hasOne('App\Models\MallCategory', 'id', 'mall_type');
    }

    public function mall(){
        return $this->hasOne('App\Models\MallCategory', 'id', 'mall_id');
    }

    public function products(){
        if($this->status == 1){
            return $this->hasMany('App\Models\Product')->where('status', 1);
        }else{
            return $this->hasMany('App\Models\Product');
        }
    }

    public function shopPaymeConfig(){
        return $this->hasOne(ShopPaymeConfig::class, 'shop_id' ,'id');
    }

    public function activeProducts(){
        // return $this->hasMany('App\Models\Product')->where()
    }

    public function getPhoneFormatAttribute(){
        $part1 = substr($this->phone, 0,3);
        $part2 = substr($this->phone, 3,2);
        return '+ '.$part1.' '.$part2. " XXX";
    }

    public function workingDays(){

        $days=['rest' => [], 'work' => []];
        for($i=1; $i<8; $i++){
            if($this->{'day'.$i} == 0){
                $days['rest'][] = Lang::get('main.working_days.day'.$i);
            }else{
                $days['work'][] = Lang::get('main.working_days.day'.$i);
            }
        }

        // Log::info($days);
        return $days;
    }

    public function getStatuses(){
        return [
            [
                'class' => $this->status ? 'btn btn-success status_shop' :'btn btn-danger status_shop',
                "type" => "status",
                'text' => 'Активный'
            ],
            [
                'class' => $this->is_recommended ? 'btn btn-success status_shop' :'btn btn-danger status_shop',
                'type' => 'recommend',
                'text' => 'Рекомендованный'
            ],
        ];
    }


    public static function changeStatus($id, $type){
        $data = ["status" => "status", "recommend" => "is_recommended"];
        if(!($item = $data[$type]))
            return false;

        $shop = Shop::find($id);

        if(!$shop)
            return false;

        $shop->$item = $shop->$item ? "0" : "1";
        $shop->save();

        if($item == "status"){
            static::changeStatusProducts($shop->id, $shop->$item);
        }

        return true;
    }

    public static function changeStatusProducts($shop_id, $status){
        Product::where("shop_id", $shop_id)->update(['status' => $status]);
    }

    public static function addMainImage( $id, $name){

        $name = pathinfo($name, PATHINFO_BASENAME);
        $data["main_image"] = $name;

        $shop = Shop::where("id", $id)->update($data);

        $res = Upload::resize("shop", $id, $name);


    }

    public static function editShopPaymeConfig($shop_id, $config = []){
        $paymeConfig = ShopPaymeConfig::find($shop_id);
        if(!$paymeConfig){
            $paymeConfig = new ShopPaymeConfig;
            $paymeConfig->shop_id = $shop_id;
        }
        $paymeConfig->merchant = $config['merchant'];
        $paymeConfig->save();

    }

}
