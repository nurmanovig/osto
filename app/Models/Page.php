<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;

class Page extends Model
{
    protected $guarded = [];
    protected $table = 'pages';

    public function getTitleAttribute()
    {
    	return $this->{'title_'.App::getLocale()};
    }

    public function getDescriptionAttribute()
    {
    	return $this->{'description_'.App::getLocale()};
    }
}
