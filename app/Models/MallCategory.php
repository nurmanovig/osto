<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
use Upload;

class MallCategory extends Model
{
    protected $guarded = [];
    protected $table = 'mall_categories';

    public function parent()
    {
        return $this->belongsTo('App\Models\MallCategory', 'parent_id');
    }

    public function getTitleAttribute()
    {
    	return $this->{'title_'.App::getLocale()};
    }

    public function getDescriptionAttribute()
    {
        return $this->{'description_'.App::getLocale()};
    }

    public function getAddressAttribute()
    {
        return $this->{'address'.App::getLocale()};
    }

    public function shops()
    {
        return $this->hasMany('App\Models\Shop', 'mall_id', 'id')->where("status", 1);
    }

    public static function addMainImage( $id, $name){

        $name = pathinfo($name, PATHINFO_BASENAME);

        $mall = static::find($id);
        $data["main_image"] = $name;
        $mall->update($data);
        Upload::resize("malls", $id, $name);

    }
}
