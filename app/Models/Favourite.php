<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
	protected $guarded = [];
    protected $table = 'favourite_products';

    public function product(){
    	return $this->hasOne('App\Models\Product', 'id', 'product_id');
    }
}


