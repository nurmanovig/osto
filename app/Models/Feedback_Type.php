<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback_Type extends Model
{
    protected $guarded = [];
    protected $table = 'feedback_type';
}
