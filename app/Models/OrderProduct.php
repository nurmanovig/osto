<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderProduct extends Model
{
	const STATUS_READY = 3;
	const STATUS_WAIT = 2;
	const STATUS_INACTION = 1;
	const STATUS_STOPPED = 0;

	const CREATED_AT = null;
  	const UPDATED_AT = null;

	protected $table = 'order_product';

	public function product(){
		return $this->hasOne("App\Models\Product", "id", "product_id");
	}

	public static function countOrderProduct($user_id){
		$sql = "select count(product_id) count from order_product op join orders o join products p 
				where order_id = o.id 
				and product_id = p.id 
				and o.status = :o_status 
				and op.status = :op_status 
				and p.user_id = :user_id";
		$results = DB::select($sql, [':user_id' => $user_id, ":o_status" => Order::STATUS_INACTIVE, ':op_status' => self::STATUS_INACTION]);

		return $results[0]->count;
	}

	public static function status($status = null){

		if($status !== null)
			return static::getStatus($status);
		return static::getAllStatus();

	}

	public static function getStatus($status){
		$result = DB::table("key_words")
				->where('key', 'ORDER_PRODUCT')
				->where('status', $status)
                ->first();
        if(empty($result))
        	return false;
		return ["class" => $result->class, "text" => $result->word];
	}

	public static function getAllStatus(){

            
	    $statuses = [];
	    $result = DB::table("key_words")->where("key", "ORDER_PRODUCT")->get();
	    
	    foreach($result as $item){
	    	$statuses[$item->status] = ["class" => $item->class, "text" => $item->word];
	    }

        return $statuses;
	}

	public function getStatusAction(){
		switch($this->status){
			case static::STATUS_STOPPED:
				$key = "ACTION_STOPPED";
			break;
			case static::STATUS_INACTION:
				$key = "ACTION_INACTION";
			break;
			case static::STATUS_WAIT:
				$key = "ACTION_WAIT";
			break;
			case static::STATUS_READY:
				$key = "ACTION_READY";
			break;
		}
		$result = DB::table("key_words")
				->where('key', $key)
                ->get();
        $statuses = [];
        foreach ($result as $key => $value) {
        	$statuses[] = ["class" => $value->class, "text" => $value->word, 'status' => $value->status];
        }

        return $statuses;

	}

	// public static function p

	public static function changeStatus($order_id, $product_id, $size_product, $status){
		$changedStatus = [static::STATUS_INACTION => [-1,1], static::STATUS_WAIT => [1]];

		$result = static::select(DB::raw("orders.status o_status,  order_product.*"))
				->join("orders", "id", "order_id")
				->where("order_id", $order_id)
				->where("product_id", $product_id)
				->where("size_product", $size_product)
				->first();
		if(empty($result))
			return false;
		if($result->o_status != Order::STATUS_STARTED)
			return false;

		if(!isset($changedStatus[$result->status])){
			return false;
		}
		if(array_search($status, $changedStatus[$result->status]) === false)
			return false;

			// throw new \Exception($status. " = " . $result->status . " | ". $size_product. " = " .$result->size_product);

		$result->status += $status;

		static::where("order_id", $order_id)
				->where("product_id", $product_id)
				->where("size_product", $size_product)
            	->update(['status' => $result->status]);

		return $result;
	}	

}