<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $guarded = [];
    protected $table = 'feedback';

    public function type()
    {
    	return $this->hasOne('App\Models\Feedback_Type', 'id', 'feedback_type');
    }
}
