<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{

	protected $guarded=[];
	protected $dates = ['created_at'];

    public function user(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
