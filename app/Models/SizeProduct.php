<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SizeProduct extends Model{
	
	const CREATED_AT = null;
	const UPDATED_AT = null;
	protected $table = 'size_products';

	protected $fillable = ['key', 'value'];

}

?>