<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $guarded = [];
    protected $table = 'features';

    public function categories(){
        return $this->belongToMany('App\Models\Feature');
    }
}
