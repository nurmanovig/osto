<?php

namespace App\Models;

use App\Library\SmsService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
	//

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	const STATUS_STOPPED = -1;
	const STATUS_FAIL = 0;
	const STATUS_INACTIVE = 1;
	const STATUS_STARTED = 2;
	const STATUS_FINISHED = 3;

	const PAY_STATUS_PAID = 2;
	const PAY_STATUS_CASH = 1;
	const PAY_STATUS_NOPAID = 0;



	public function users(){
		return $this->hasMany('App\User', 'id', 'user_id');
	}

	public function products(){
		return $this->hasMany("App\Models\OrderProduct", "order_id", "id");
	}

	public static function deleteOrder($id){
		$order = static::where("id", $id)->firstOrFail();
		DB::table("order_product")->where("order_id", $order->id)->delete();
		$order->delete();
	}

	public static function countPaid(){
		// if(Cache::has("countPaid")){
		// 	return Cache::get("countPaid");
		// }else{
			$count =  static::select(DB::raw("count(*) count"))->where("pay_status", self::PAY_STATUS_PAID)->first()->count;
			// Cache::put("countPaid", $count, 100);
			return $count;
		// }
		
	}

	public static function ordersBySalesman($user_id){
		$sql = "select o.created_at, p.*, op.*, sh.title shop_title, sh.alias shop_alias from order_product op join orders o join products p join shops sh
			where sh.user_id = p.user_id
			and order_id = o.id 
			and product_id = p.id 
			and p.user_id = :user_id
			order by o.id desc";
		$results = DB::select($sql, [':user_id' => $user_id]);

		return $results;
	}

	

	public static function changeStatus($order_id, $status){
		$changedStatus = [
						static::STATUS_FAIL => [-1,1], 
						static::STATUS_INACTIVE => [-1, 2],
						static::STATUS_STARTED => [-1, 3],
						];

		$result = static::where("id", $order_id)->first();
		if(array_search($status, $changedStatus[$result->status]) === false)
			return false;
		static::where("id", $order_id)
            ->update(['status' => $status]);

        if(($status) == static::STATUS_STOPPED){
        	OrderProduct::where("order_id", $order_id)->update(["status" => OrderProduct::STATUS_STOPPED]);

        	$products = OrderProduct::select("product_id", "count")->where("order_id", $order_id)->get();

        	foreach($products as $product){
        		static::crement($product->product_id, $product->count);
        	}

        }
		return false;				
	}

	public static function changePaystatus($order_id){
		$order = static::find($order_id);
		$order->pay_status = static::PAY_STATUS_PAID;
		$order->save();
	}

	public static function changeCount($product_id, $count, $size){
		$res = static::checkCount($product_id, $count, $size);
		if($res !== true)
			return $res;
		static::crement($product_id, $count, "-");

		return true;
	}

	public static function checkCount($product_id, $count, $size){
		$product = Product::select(DB::raw("title, count_product, size_product"))->where("id", $product_id)->first();
		if(!$product){
			return "Неизвестный товар";
		}
		
		$doctype = false;
		$product->size();
		foreach ($product->size_product as $k => $value) {
			if($value['id']== $size){
				$doctype = true;
				continue;
			}
		}

		if(!$doctype){
			return "Неизвестный размер";
		}

		if(($product->count_product - $count) < 0){

			return "Не достаточно количество товара \"".$product->title."\"";
		}

		return true;

	}

	public static function crement($product_id, $count, $type = "+"){
		DB::select("update products set count_product = count_product ".$type." :count where id = :product_id", [":count" => $count, ":product_id" => $product_id]);
	}

	public static function status($status = null){
		if($status !== null)
			return static::getStatus($status);
		return static::getAllStatus();

	}

	public static function getStatus($status){
		$result = DB::table("key_words")
				->where('key', 'ORDER')
				->where('status', $status)
                ->first();

        if(empty($result))
        	return false;
		return ["class" => $result->class, "text" => $result->word];
	}

	public static function getAllStatus(){

        $statuses = [];
        $result = DB::table("key_words")->where("key", "ORDER")->get();
        
        foreach($result as $item){
        	$statuses[$item->status] = ["class" => $item->class, "text" => $item->word];
        }


        return $statuses;
	}

	public static function paystatus($status = null){
		$status_list = [
			static::PAY_STATUS_NOPAID => ["class" => "default", "text" => "Не оплачен"],
			static::PAY_STATUS_CASH => ["class" => "primary", "text" => "Не оплачен"],
			static::PAY_STATUS_PAID => ["class" => "success", "text" => "Оплачен"],
		];

		if($status === null)
			$status = 0;
		return isset($status_list[$status]) ? $status_list[$status] : null;
	}

	public static function keys(){
		KeyWord::all();
	}

	public static function paytype($status){

		$status_list = [
			1 => ["class" => "warning", "text" => "Оплата наличными"],
			2 => ["class" => "primary", "text" => "Оплата картой"],
			3 => ["class" => "success", "text" => "Оплата онлайн"],
		];

		if($status === null)
			$status = 0;
		return isset($status_list[$status]) ? $status_list[$status] : null;
	}

	public static function payEvents($status){
		$status_events = [
			static::PAY_STATUS_NOPAID => [
										],
			static::PAY_STATUS_CASH => [
					["class" => "btn btn-success pay_status", "text" => "Оплатить", "status" => 2],
				],
			

		];

		return isset($status_events[$status]) ? 
			$status_events[$status] : 
			[];
		
	}

    public function sendInfo(){
        $this->sendToSmsUser();

        $this->sendMsgToAdmin();
    }

    public function sendMsgToAdmin(){
        $message = "<b>Новый заказ</b>\n";
        $message = "Новый заказ\n\n";
        $message .= "#ID: ".$this->id."\n";
        $message .= "Сумма: ".$this->summ." UZS"."\n";
        $message .= "Скидка: ".$this->sale."\n";
        $message .= "Имя: ".$this->username."\n";
        $message .= "Телефон: ".$this->phone."\n";
        $message .= "Адрес: ".$this->address."\n";
        $message .= "Комментария: ".$this->comments."\n";
        $message .= "Время заказа: ".$this->created_at."\n";
//
//        mail("nurmanovig@gmail.com", , $message);

        \App\Library\Telegram::sendMessage("@ostoorders", $message);
    }

    public function sendToSmsUser(){
        $setting = Setting::first();

        $message = "OSTO: ВАШ ЗАКАЗ".'\r\n';
        $message .= "Сумма: ".$this->summ." UZS".'\r\n';

        if($setting && $setting->price_delivery){
            $message .= "Цена доставки: ".$setting->price_delivery." UZS".'\r\n';
        }

        $products = $this->products;
        foreach($products as $key => $product){
            $productInfo = Product::find($product->product_id);

            if(!$productInfo){
                continue;
            }

            $message .= ($key+1).") ".$productInfo->title . "( ".$product->size_product." )" . " x ".$product->count ." = ". $product->price." UZS".'\r\n';
        }

        $message .= "Спасибо!!! Ваш заказ передан на обработку.";
        SmsService::send($this->phone, $message);

    }

}
