<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KeyWord extends Model
{
    //

	const CREATED_AT = null;
	const UPDATED_AT = null;
	protected $fillable = ['word', 'class'];
	protected $table = 'key_words';
    
}
