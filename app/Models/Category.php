<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
use Upload;

class Category extends Model
{
	protected $guarded = [];
    protected $table = 'categories';

    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id');
    }

    public function features(){
        return $this->belongsToMany('App\Models\Feature');
    }

    public function products(){
        return $this->belongsToMany('App\Models\Product', 'product_category','product_id', 'category_id');
    }

    public function parents()
    {
        return $this->hasMany('App\Models\Category', 'parent_id');
    }

    public function childs(){
        return $this->hasMany('App\Models\Category', 'parent_id');
    }

    public function childsShortInfo(){
        return $this->hasMany('App\Models\Category', 'parent_id')->select("id", "parent_id", "alias", "title_ru", "title_uz");
    }

    public function parentShortInfo()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id')->select("id", "parent_id", "alias", "title_ru", "title_uz");
    }

    public function getTitleAttribute()
    {
        return $this->{'title_'.App::getLocale()};
    }


    public static function boot()
    {
        parent::boot();
      
        Category::deleting(function($category)
        {   
             $category->parents()->delete();
             $category->products()->delete();
        });
    }



    public static function getBreadCrumbsForCategory($category_id){
        $pages = [];
         Category::getCategoryBreadCrumbs($category_id, $pages);

         //$page = ["link" => "/product/category/all", "name" => "Все категории"];

        //array_unshift($pages, $page);
        return $pages;
    }

    public static function getCategoryBreadCrumbs($category_id, &$pages = [], $prefix = "category"){
        $category = Category::select('id', 'alias', 'title_ru', 'parent_id')->where("id", $category_id)->first();

        if(empty($category))
            return;

        $page = ["link" => "/product/{$prefix}/".$category->alias, 'name' => $category->title];
        array_unshift($pages, $page);


        static::getCategoryBreadCrumbs($category->parent_id, $pages);
        if($category->parent_id == 0)
            return;
    }

    public static function getCategoryIdsWithAllChilds($item = []){
        $category = static::select("id", "parent_id");

        foreach($item as $key => $value){
            $category->where($key, $value);
        }

        $category = $category->first();

        if(empty($category))
            abort(404);


        $ids = [$category->id];

        return $ids;
    }

    public static function getAllChildsId($parent, &$ids = []){
        $categories = static::select("id", "parent_id")->whereIn("parent_id", $parent)->get();
        $childs_id = [];
        if(count($categories) == 0)
            return $ids;
        
        foreach($categories as $category){
            $ids[] = $childs_id[] = $category->id;
        }

        static::getAllChildsId($childs_id, $ids);

        return $ids;

    }

    public static function addMainImage( $id, $name){

        $name = pathinfo($name, PATHINFO_BASENAME);

        $category = static::find($id);
        $data["main_image"] = $name;
        $category->update($data);

        Upload::resize("category", $id, $name);

    }
   
}
