<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SliderItems extends Model
{
    //
    protected $table = "slider_items";

    public $fillable = ['img', 'title', 'text', 'status', 'url'];

    public $timestamps = null;
}
