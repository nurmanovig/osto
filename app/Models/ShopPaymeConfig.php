<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopPaymeConfig extends Model
{
    const UPDATED_AT = null;
    const CREATED_AT = null;
    protected $table = "shop_payme_config";
    protected $primaryKey = "shop_id";

    protected $fillable = ['merchant', 'key'];

}