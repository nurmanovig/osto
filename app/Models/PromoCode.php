<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    //
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $table = 'promo_code';
    protected $fillable = ['code', 'percent', 'start_at', 'finished_at'];

    public static function checkCode($code){
    	$promo = static::get();
    	$date = time();
    	foreach ($promo as $key => $value) {
            if($value['code'] == $code){
                $start = strtotime($value['start_at']);
                $finish = strtotime($value['finished_at']);
                if($date >= $start && $date <= $finish)
                    return $value['percent'];
            }
        }

        return false;
    }
}
