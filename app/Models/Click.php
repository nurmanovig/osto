<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
use Upload;

class Click extends Model
{
	const MERCHANT_ID = 8343;
    const SERVICE_ID = 12306;
    // const SECRET_KEY = "Z#QzxqaHPD0NH6apWWI";
    const SECRET_KEY = "evHowbxhNbvk";
    const MERCHANT_USER_ID = 10676;

	protected $table = 'click_uz';

	public $params = [];
    
    
    public static function postForm(array $paramsTr){
        $amount = number_format($paramsTr['amount'], 2, '.', '');
        $sign_time = date("Y-m-d h:i:s");
        
        $sign_string = md5($sign_time.self::SECRET_KEY.self::SERVICE_ID.$paramsTr['transId'].$amount);
        
        $params = [
                'MERCHANT_TRANS_AMOUNT' => $amount,
                'MERCHANT_ID' => self::MERCHANT_ID,
                'MERCHANT_USER_ID' => self::MERCHANT_USER_ID,
                'MERCHANT_SERVICE_ID' => self::SERVICE_ID,
                'MERCHANT_TRANS_ID' => $paramsTr['transId'],
                'MERCHANT_TRANS_NOTE' => 'Оплата OOO MERCHANT',
                'MERCHANT_USER_PHONE' => $paramsTr['phone_number'],
                'MERCHANT_USER_EMAIL' => 'test@email.com',
                'SIGN_TIME' =>  date("Y-m-d h:i:s"),
                'RETURN_URL' => 'https://osto.uz/cart',
                'SIGN_STRING' => $sign_string
            ];
        
        return ['url' => "https://my.click.uz/pay/", "params" => $params];    
        // $ch = curl_init("https://my.click.uz/pay/");
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);   
        // curl_setopt($ch, CURLOPT_HEADER, true);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        
        // $result = curl_exec($ch);
        // curl_close($ch);
        
        // return $result;
    } 
    
	public function listening(){
        // check sign key
        if(!isset($this->params['sign_string']) || $this->params['sign_string'] !== $this->getSing()) {
            exit(json_encode(self::clickMessages()[1]));
        }

        // check action
        if(!in_array($this->params['action'], [0, 1])) {
            exit(json_encode(self::clickMessages()[3]));
        }

        // check user
        $order = Order::find($this->params['merchant_trans_id']);
        if(!$order) {
            exit(json_encode(self::clickMessages()[5]));
        }

        if ($order->summ != $this->params['amount']) {
            exit(json_encode(self::clickMessages()[2]));
        }

        if($this->params['action'] == 0) {
            $this->prepare();
        } elseif($this->params['action'] == 1) {
            $this->confirm();
        }
    }

	private function prepare(){
        $transaction = self::select()->where("status", 0)
                                    ->where("click_trans_id", $this->params['click_trans_id'])
                                    ->first();


        if(!$transaction){
            $transaction = new self;
            $transaction->status = "0";
            $transaction->amount = $this->params['amount'];
            $transaction->account = $this->params['merchant_trans_id'];
            $transaction->date = time();
            $transaction->click_trans_id = $this->params['click_trans_id'];
            $transaction->error = $this->params['error'];
            $transaction->save();
        }

        $return = array(
            'click_trans_id' => $transaction->click_trans_id,
            'merchant_trans_id' => $transaction->account,
            'merchant_prepare_id' => $transaction->id,
        );

        exit(json_encode(array_merge(self::clickMessages()[0], $return)));
    }

	private function confirm(){
        $transaction = self::find($this->params['merchant_prepare_id']);

        if($transaction) {
            $order = Order::find($transaction->account);

            if($this->params['error'] == "-1") {
                exit(json_encode(self::clickMessages()[4]));
            }

            if($transaction->status) {
                exit(json_encode(self::clickMessages()[4]));
            }

            if($transaction->amount != $this->params['amount']) {
                exit(json_encode(self::clickMessages()[2]));
            }

            if($this->params['error'] == "-5017") {
                $transaction->error = "-5017";
                $transaction->save();
                exit(json_encode(self::clickMessages()[9]));
            }

            if($transaction->error == "-5017") {
                exit(json_encode(self::clickMessages()[9]));
            }


            $transaction->status = "1";
            if($transaction->save()) {
                $order->pay_status = 2;

                if($order->save()){
                    $return = array(
                        'click_trans_id'=> $transaction->click_trans_id,
                        'merchant_trans_id' => $transaction->account,
                        'merchant_confirm_id' => $transaction->id
                    );
                    exit(json_encode(array_merge(self::clickMessages()[0], $return)));
                }else{
                    exit(json_encode(self::clickMessages()[7]));
                }
            }

            exit(json_encode(self::clickMessages()[9]));
        } else {
            exit(json_encode(self::clickMessages()[6]));
        }
    }

	private function getSing(){
        return md5(
            $this->params['click_trans_id'] .
            $this->params["service_id"] .
            self::SECRET_KEY .
            $this->params['merchant_trans_id'] .
            ($this->params['action'] == 1 ? $this->params['merchant_prepare_id'] : '').
            $this->params['amount'] .
            $this->params['action'] .
            $this->params['sign_time']
        );
    }

	public static function clickMessages(){
        return [
            ["error" => "0", "error_note" => "Success"],
            ["error" => "-1", "error_note" => "SIGN CHECK FAILED"],
            ["error" => "-2", "error_note" => "Amount not correct"],
            ["error" => "-3", "error_note" => "Action not found"],
            ["error" => "-4", "error_note" => "Already paid"],
            ["error" => "-5", "error_note" => "User does not exist"],
            ["error" => "-6", "error_note" => "Transaction does not exist"],
            ["error" => "-7", "error_note" => "Failed to update user"],
            ["error" => "-8", "error_note" => "Error in request from click"],
            ["error" => "-9", "error_note" => "Transaction cancelled"]
        ];
    }
}