<?php
return [
	'category-select' => 'App\Widgets\CategorySelect',
	'category-list-search' => 'App\Widgets\CategoryListForSearch',
    'banner-widget' => 'App\Widgets\BannerWidget',
];
?>