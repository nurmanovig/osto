(function($, window){

	window.filePreview = function(options){
		this.input = options.input;
		this.trigger  = options.trigger;
		this.onChange = options.onChange;
		this.onRead   = options.onRead;
		this.removeBtnClass = options.removeBtnClass;

		this.files    = [];

		this.init();
	}

	window.filePreview.prototype = {
		init: function(){
			this.setEvents();
		},

		setEvents: function(){
			var _ = this;

			if (_.trigger) {
				_.trigger.click(function(e){
					e.preventDefault();

					_.input.trigger('click');
				});
			}

			if (_.input) {
				_.input.change(function(event){
					_.onInputChange(event);
				});
			}
		},

		onInputChange: function(event){
			var _ = this;
			var file_list = _.input[0].files;

			_.readFiles(file_list, function(url, file, id){
				_.files.push({
					url: url,
					id: id,
				});

				if (_.onRead) {
					_.onRead(url, _.files, file_list);
				}
			});
		},

		removeFile: function(index){
			this.files.splice(index, 1);
		},

		readFiles: function(file_list, callback){
			var _ = this;

			if (typeof window.FileReader !== undefined) {
                $.each(file_list, function(id, file){
					var reader = new FileReader();

					reader.onload = function (e) {
						if (callback) {
							callback(reader.result, file);
						}
	                }

	                reader.readAsDataURL(file);
                });
			}
		},
	}

})(jQuery, window);