var _body = $('body');
var _window = $(window);

var window_width = _window.width();
var window_height = _window.height();
var scrolled = _window.scrollTop();
var lastScrollPos = scrolled;
var isScrolling = false;

var screen = {
	md: 992
}

var VHSupport;

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var App = {
	wrapper: $('.wrapper'),
	overlay: $('.overlay'),
	overlaySpeed: 600,
	header: $('.header'),
	navbar: $('.navbar'),

	isOverlayAnimating: false,

	// Preloader
	preloader: $('.preloader'),

	// Login
	login: $('.login'),

	// User btn
	userBtn: $('.js-user-btn'),
	authForm: $('.js-auth-form'),
	authMessage: $('.js-auth-message'),

	// Nav
	navbar: $('.navbar'),
	navbarStickedHeight: 90,

	// Menu 
	hamburger: $('.js-menu-hamburger'),
	menu: $('.menu'),
	menuItems: $('.menu__item'),
	menuItemsDelay: 100,
	menuTotal: null,

	// All Item Carousels settings
	item_carousel_settings: {
		slidesToShow: 5,
		autoplay: true,
		speed: 300,
		autoplaySpeed:3000,
		arrows: true,
		cssEase: 'ease-in-out',
		prevArrow: '<button class="carousel-arrow carousel-arrow--prev"><i class="icon icon-prev"></i></button>',
		nextArrow: '<button class="carousel-arrow carousel-arrow--next"><i class="icon icon-next"></i></button>',

		responsive: [
			{
				breakpoint: 1600,
				settings: {
					slidesToShow: 4, 
				}
			},

			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3
				}
			},

			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
					arrows: false
				}
			},
		]
	},

	showPreloader: function(){
		this.preloader.show();
		_body.addClass('o-hidden');
	},

	hidePreloader: function(){
		this.preloader.hide();
		_body.removeClass('o-hidden');
	},

	init: function(){
		if (App.header.height() != window_height) {
			VHSupport = false;
		}else{
			VHSupport = true;
		}

		App.initMenu();
		App.navSticky();
		App.initFilter();
		App.initItemCarousels();
		App.initLogin();
		App.initSelect();
		App.initAlerts();
		App.initTips();
	},

	initTips: function(){
		var buttons = $('.tip__btn');

		buttons.click(function(e){
			e.preventDefault();

			var _parent = $(this).parent();

			if (_parent.hasClass('is-active')) {
				_parent.removeClass('is-active');
			}else{
				_parent.addClass('is-active');

				window.setTimeout(function(){
					_body.one('click', function(){
						_parent.removeClass('is-active');
					});
				}, 10);
			}
		});
	},

	initAlerts: function(){
		_body.on('click', '.alert__dismisser', function(){
			var _parent = $(this).parent();

			_parent.slideUp(200, function(){
				_parent.remove();
			});
		});
	},

	initSelect: function(){
		var sel = $('.js-select');

		if (sel.length != 0) {
			sel.each(function(){
				var _ = $(this);

				_.select2({
					minimumResultsForSearch: Infinity,
					theme: _.data('theme'),
					width: '100%'
				});
			});
		}
	},

	getOverlayOpenedSize: function(){
		return 3 * (window_height > window_width ? window_height : window_width);
	},

	navSticky: function(){
		var isPageIndex = true;

		// Check if current page is index
		if (_body.hasClass('nav-page')) {
			isPageIndex = false;

			_body.addClass('nav-sticked');
		}
			
		var scrollDelta = 20;

		if (Math.abs(lastScrollPos - scrolled) < scrollDelta) 
			return;

		if (scrolled > lastScrollPos && scrolled > App.navbarStickedHeight){
	        // Scroll Down
	        _body.addClass('nav-hidden');
	    } else {
	    	if (scrolled + _window.height() < $(document).height()) {
	    		_body.removeClass('nav-hidden').addClass('nav-sticked');
	    	}

	    	if (isPageIndex) {
	    		if (scrolled <= App.navbarStickedHeight) {
					_body.removeClass('nav-sticked');
				}
	    	}	
	    }

	    lastScrollPos = scrolled;
	},

	initItemCarousels: function(){
		$('.item-carousel').each(function(index, item){
			var _this = $(item);

			_this.on('init', function(){
				_this.css('visibility', 'visible');
			});

			_this.slick(App.item_carousel_settings)
		});
	},

	initFilter: function(){
		var filter = this.header.find('.header__filter');
		var isAnimating = false;

		$('.header__filter-btn').click(function(){
			if (!isAnimating) {
				isAnimating = true;

				if (_body.hasClass('filter-extended')) {
					_body.removeClass('filter-extended');

					filter.fadeOut(200, function(){ isAnimating = false });
				}else{
					_body.addClass('filter-extended');

					filter.fadeIn(200, function(){ isAnimating = false });
				}
			}
		});
	},

	animateMenuItems: function(props, callback){
		var delay = 0;
		var count = 0;

		this.menuItems.each(function(index, item){
			window.setTimeout(function(){
				$(item).animate(props, function(){
					count++;

					if (count >= App.menuTotal) {
						if (callback) callback();
					}
				});
			}, delay);

			delay += App.menuItemsDelay;
		});
	},

	initMenu: function(){
		var isAnimating = false;

		this.menuTotal = this.menuItems.length;

		this.hamburger.click(function(){
			if (!isAnimating) {

				isAnimating = true;

				var hamburgerSize = App.hamburger.width();
				var hamburgerPos = App.hamburger.position();

				// Hamburgers pos and size
				var hamburgerProps = {
					width: 	hamburgerSize + 'px',
					height: hamburgerSize + 'px',
					left: 	hamburgerPos.left + 'px',
					top: 	hamburgerPos.top + 'px'
				}

				if (_body.hasClass('menu-opened')) {
					_body.removeClass('menu-opened');

					App.animateMenuItems({ opacity: 0, left: '-50px'}, function(){

						// Hide menu itself after items animate finished
						App.menu.hide();

						// Animate overlay back to hams pos
						App.overlay.animate(hamburgerProps, App.overlaySpeed * 0.7, function(){
							// Hide overlay
							App.overlay.hide().attr('style', null);

							// Remove body o-hidden
							_body.removeClass('o-hidden');

							// Set animation finish
							isAnimating = false;
						});
					});
				}else{
					_body.addClass('menu-opened o-hidden');

					var overlaySize = App.getOverlayOpenedSize();
					var overlayPos = (overlaySize * 0.5);

					// Show overlay
					App.overlay.show();

					App.overlay.css(hamburgerProps);

					// Animate overlay to pos
					App.overlay.animate({
						width: overlaySize + 'px',
						height: overlaySize + 'px',
						left: -overlayPos + 'px',
						top: -overlayPos + 'px'
					}, App.overlaySpeed, function(){
						App.menu.show();

						App.animateMenuItems({ opacity: 1, left: '0' }, function(){
							isAnimating = false;
						});
					});
				}
			}
		});
	},

	openLogin: function(e){
		e.preventDefault();
		var loginContent = App.login.find('.login__content');
		var loginClose = App.login.find('.js-login-close');

		_body.addClass('login-opened o-hidden');

		var userBtnPos = App.userBtn.position();
		var userBtnOffset = App.userBtn.offset();

		var userBtnProps = {
			right: _window.width() - (userBtnOffset.left + App.userBtn.width() / 2) + 'px',
			top: (userBtnPos.top + App.userBtn.height() / 2) + 'px',
			width: '0px',
			height: '0px'
		}

		var overlaySize = App.getOverlayOpenedSize();
		var overlayPos = (overlaySize * 0.5);

		App.overlay.show().css(userBtnProps);

		App.hamburger.fadeOut(250);

		App.overlay.animate({
			right: -overlayPos + 'px',
			top: -overlayPos + 'px',
			width: overlaySize + 'px',
			height: overlaySize + 'px'
	 	}, App.overlaySpeed, function(){
	 		App.login.fadeIn(250);
	 		App.isOverlayAnimating = false;
	 	});

	 	loginClose.one('click', function(){
			if (!App.isOverlayAnimating) { 
				App.login.fadeOut(250, function(){
					App.overlay.animate(userBtnProps, App.overlaySpeed * 0.7, function(){
						App.hamburger.fadeIn(250);
						_body.removeClass('login-opened o-hidden');

						App.isOverlayAnimating = false;
					});
				});
			}
		});	
	},


	auth: function(e){
		e.preventDefault();

		var _this = $(this),
			btn = _this.find('[type=submit]');
			
		App.authMessage.hide(100);
		btn.attr('disabled', true).addClass('is-waiting');

		$.ajax({
            type: 'POST',
            url: _this.attr('action'),
            data: _this.serialize(),
            dataType: "JSON",
            success: function(data){

            	btn.attr('disabled', false).removeClass('is-waiting')
            	if (data.status == 1) {
            		window.location.href = data.url;
            	} else {
	            	App.authMessage
	            		.addClass('m-b-20')
	            		.fadeIn(500)
	            		.html(data.msg);
            	}
            	
            }
        });
	},

	initLogin: function(){
		App.userBtn.click(App.openLogin);
		App.authForm.submit(App.auth);
	},
}

App.Tab = function(options) {
	this.tabs = options.tabs;
	this.tabContents = options.tabContents;
	this.onChange = options.onChange;

	this.init();
}

App.Tab.prototype.init = function() {
	var _ = this;

	_.tabs.click(function(e){
		e.preventDefault();

		var _this = $(this);

		if (!_this.hasClass('is-active')) {
			var index = parseInt(_this.data('index'));

			_.tabs.removeClass('is-active');
			_.tabContents.removeClass('is-active');

			_.tabContents.eq(index).addClass('is-active');

			_this.addClass('is-active');

			if (_.onChange) {	
				_.onChange(index);
			}
		}
	});
}

$(document).ready(App.init);

var Modal = (function(){
	var obj = function(options)
	{
		var _ = this;

		_.modal = options.modal;
		_.openBtn = options.openBtn;
		_.closeBtn = options.closeBtn;
		_.modal_content = _.modal.find('.modal__content');
		_.isOpen = false;
		_.isAnimating = false;
		_.fadeDuration = options.fadeDuration || 400;

		_.init();
	}

	var _proto = obj.prototype; 

	_proto.init = function()
	{
		var _ = this;

		_.openBtn.click(function(e){
			e.preventDefault();

			if (!_.isOpen && !_.isAnimating) {
				_.openModal();
			}
		});

		_.closeBtn.click(function(e){
			e.preventDefault();

			if (_.isOpen && !_.isAnimating) {
				_.closeModal();
			}
		});

		_.modal.click(function(e){
			if (_.isOpen && !_.isAnimating) {
				if($(e.target).is(_.modal))
					_.closeModal();
			}
		});
	}

	_proto.openModal = function()
	{
		var _ = this;

		_.isAnimating = true;

		_body.addClass('o-hidden');

		_.modal.fadeIn(_.fadeDuration);

		window.setTimeout(function(){
			_.modal_content.addClass('modal__content--in').one(transitionEnd, function(){
				_.isAnimating = false;
				_.isOpen = true;
			});
		}, _.fadeDuration / 1.5);
	}

	_proto.closeModal = function()
	{
		var _ = this;

		_.isAnimating = true;		

		_.modal_content.removeClass('modal__content--in').one(transitionEnd, function(){
			_.modal.fadeOut(_.fadeDuration, function(){
				_.isAnimating = false;
				_.isOpen = false;
				_body.removeClass('o-hidden');
			});
		});
	}

	return obj;
})();

Phone number mask
var phoneMask = $('.js-phone-mask');

if (phoneMask.length > 0) {
	phoneMask.mask('+ (999) 99 999-9999', {'translation': {0: {pattern: /[0-9*]/}}});
}


// Dropdowns
(function(){
	var is_collapsed = false, 
		is_busy = false,
		slide_speed = 200;

	$('.dropdown__label').click(function(){
		var _this = $(this),
			_parent = _this.parent(),
			_content = _parent.find('.dropdown__content');

		if (is_collapsed) {
			if (!is_busy) {
				_parent.removeClass('is-active');

				_content.slideUp(slide_speed, function(){
					is_busy = false;
					is_collapsed = false;
				});
			}
		}else{
			if (!is_busy) {
				_parent.addClass('is-active');

				_content.slideDown(slide_speed, function(){
					is_busy = false;
					is_collapsed = true;
				});
			}
		}
	});
})();

// GLOBAL RESIZE EVENT
_window.resize(function(){
	// Get new window sizes
	window_width = _window.width();
});

_window.scroll(function(){
	scrolled = _window.scrollTop();

	isScrolling = true;

	window.setInterval(function() {
	    if (isScrolling) {
	        App.navSticky();
	        isScrolling = false;
	    }
	}, 250);

	// lastScrollPos = scrolled;
});