var ProductModeChanger = (function(){
	var ModeChanger = function(config)
	{
		var _ = this;

		/* Mode changer buttons */
		_.btnGrid = config.btnGrid || null;
		_.btnList = config.btnList || null;

		/* Default current mode */
		_.currentMode = config.currentMode || 'grid';

		/* Ajax */
		_.url = config.url || null;
		_.data = config.data || null;

		/* Result render container */
		_.resultContainer = config.resultContainer || null;

		/* Init */
		_.init();
	}

	ModeChanger.prototype = {
		init: function()
		{
			var _ = this;

			_.changeActiveButton();
			_.listenEvents();
		},

		listenEvents: function()
		{
			var _ = this;

			_.btnGrid.click(function(e){
				e.preventDefault();

				if (_.currentMode != 'grid') {
					_.changeMode('grid');
				}
			});

			_.btnList.click(function(e){
				e.preventDefault();

				if (_.currentMode != 'list') {
					_.changeMode('list');
				}
			});
		},

		changeActiveButton: function()
		{
			var _ = this;

			if (_.currentMode == 'grid') {
				_.btnGrid.removeClass('btn--gray').addClass('btn--purple');
				_.btnList.removeClass('btn--purple').addClass('btn--gray');
			}else if(_.currentMode == 'list'){
				_.btnList.removeClass('btn--gray').addClass('btn--purple');
				_.btnGrid.removeClass('btn--purple').addClass('btn--gray');
			}else{
				console.log('Undefined mode choose between "grid" and "list".')
			}
		},

		changeMode: function(mode){
			var _ = this;

			_.data += '&mode=' + mode;

			_.resultContainer.css('opacity', '0.5');

			$.ajax({
				type: 'GET',
				data: _.data,
				url: _.url,

				success: function(data)
				{
					_.resultContainer.css('opacity', '1');
					_.currentMode = mode;
					_.resultContainer.html(data);
					_.changeActiveButton();
				},
			});
		}
	}

	return ModeChanger;
})();
