var transitionEnd = (function(){
    var t,
        e = document.createElement('div');

    var transitions = {
        "transition"      : "transitionend",
        "OTransition"     : "oTransitionEnd",
        "MozTransition"   : "transitionend",
        "WebkitTransition": "webkitTransitionEnd"
    }

    for (t in transitions)
    {
        if (e.style[t] !== undefined)
        {
          return transitions[t];
        }
    }
})();

var transition = (function(){
    var t,
        e = document.createElement('div');

    var transitions = {
        "transition"      : "transition",
        "OTransition"     : "OTransition",
        "MozTransition"   : "MozTransition",
        "WebkitTransition": "WebkitTransition"
    }

    for (t in transitions)
    {
        if (e.style[t] !== undefined)
        {
            return transitions[t];
        }
    }
})();

var transform = (function(){
    var t,
        e = document.createElement('div');

    var vendorPrefixes = [
        'transform', 
        'WebkitTransform', 
        'msTransform', 
        'MozTransform', 
        'OTransform'
    ];

    for(var i = 0, l = vendorPrefixes.length; i < l; i++){
        if (typeof e.style[vendorPrefixes[i]] !== undefined) {
            return vendorPrefixes[i];
        }
    }
})();