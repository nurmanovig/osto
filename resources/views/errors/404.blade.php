@extends('layouts.frontend')

@section('styles')
<style>
	.page-404__clouds{
		position: relative;
		height: 180px;
	}

	.page-404__clouds-item {
		position: absolute;
		background-size: 100%;
		background-repeat: no-repeat;
	}

	.page-404__clouds-item:nth-child(1) {
		left:0;
		top:80px;
		width: 18%;
		padding-top: 5%;
		background-image: url('{{ asset('assets/img/404-cloud-1.svg') }}')
	}

	.page-404__clouds-item:nth-child(2) {
		left:20%;
		top:150px;
		width: 18%;
		padding-top: 5%;
		background-image: url('{{ asset('assets/img/404-cloud-2.svg') }}')
	}

	.page-404__clouds-item:nth-child(3) {
		left:50%;
		top:0px;
		width: 18%;
		padding-top: 8%;
		background-image: url('{{ asset('assets/img/404-cloud-3.svg') }}')
	}

	.page-404__clouds-item:nth-child(4) {
		right:11%;
		top:100px;
		width: 18%;
		padding-top: 7%;
		background-image: url('{{ asset('assets/img/404-cloud-4.svg') }}')
	}
	.page-404__content{
		padding-bottom:6%;
		width: 100%;
		background-size:80%;
		background-position: center 95%;
		background-repeat: no-repeat;
		background-image: url('{{ asset('assets/img/404-plant.svg') }}');
	}

	.page-404__oops{
		font-size: 22px;
	}

	.page-404__oops, .page-404__btn{
		margin-top: 120px;
	}

	.page-404__cart{
		width: 200px;
		height: 340px;
		position: relative;
	}

	.page-404__cart:after{
		content: '';
		width: 100%;
		height: 100%;
		position: absolute;
		bottom: 0;
		background-position: 50% 100%;
		background-repeat: no-repeat;
		background-image: url('{{ asset('assets/img/404-cart.svg') }}');
	}

	
	.page-404__quote {
		top:-40%;
		right:-100%;
		position: absolute;
		width: 250px;
		height: 250px;
		text-align: center;
		background-position: center;
		background-repeat: no-repeat;
		background-image: url('{{ asset('assets/img/404-quote.svg') }}');
	}

		.page-404__quote-text {
			top: 50%;
			color: #fff;
			position: absolute;
			width: 100%;
			-webkit-transform: translateY(-50%);
			-moz-transform: translateY(-50%);
			-o-transform: translateY(-50%);
			transform: translateY(-50%);
		}

		.page-404__quote-text h1{
			font-size: 70px;
			font-weight: bold;
			line-height: 1;
		}

		.page-404__quote-text span{
			margin-left: -50%;
		}

	@media(max-width: 768px) {
		.page-404__clouds{
			position: relative;
			height: 100px;
		}
		.page-404__content{
			background-position: 50% 85%;
			background-size: 100%;
		}

		.page-404__oops{
			margin-top: -60px;
			margin-bottom: 70px;
		}

		.page-404__btn{
			margin-top:50px;
			margin-bottom: -30px;
		}

		.page-404__cart{
			width: 100%;
			height: 300px;
		}

		.page-404__quote {
			z-index: 100;
			width: 150px;
			height: 150px;
			top:-20%;
			right:10%;
		}	

		.page-404__quote-text h1{
			font-size: 35px;
		}

		.page-404__quote-text span{
			font-size: 12px;
			margin-left: -30%;
		}	
	}

</style>
@stop

@section('content')

<section class="page-404 p-t-nav-sticked">
	<div class="p-y-layout">
		<div class="container">
			<div class="page-404__clouds">
				<div class="page-404__clouds-item"></div>
				<div class="page-404__clouds-item"></div>
				<div class="page-404__clouds-item"></div>
				<div class="page-404__clouds-item"></div>
			</div>
			<div class="page-404__content">
				<div class="row">
					<div class="col-sm-4 page-404__oops text-center">
						Ой, что то<br/>пошло не так
					</div>
					<div class="col-sm-4">
						<div class="page-404__cart">
							<div class="page-404__quote">
								<div class="page-404__quote-text">
									<span>ОШИБКА</span>
									<h1>404</h1>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4 text-center page-404__btn">
						<a href="{{ route('home') }}" class="btn btn--purple">Вернуться на главную</a>
					</div>
				</div>
				
			</div>
		</div>
	</div><!--/.p-layout-->
</section>

@stop

@section('scripts')

@stop