@if($pages)
<ol class="breadcrumb">
	
	@foreach($pages as $key => $page)
		@if($key == (count($pages) - 1) )
			<li class="active_bread">{{$page['name']}}</li>		
		@elseif(isset($page['link']) && !empty($page['link']))
  			
  			<li><a href="{{$page['link']. ($key != 0 && Request::get('sale') == 1 ?  '?sale=1' : null)}}">{{$page['name']}}</a></li>
		@endif
	@endforeach
  
</ol>

@endif