<?php
use App\Models\Product;
?>
<form>
	<button type="button" class="sbmincart-closer">×</button>    

	<?php $sum = $usd = 0; ?>


	@if(!empty($products))
	<div class="products-count"> 
		В ВАШЕЙ КОРЗИНЕ <small>{{count($products)}} шт. товаров</small>
	</div>
	<ul>
		@foreach($products as $product)
			
			<li class="sbmincart-item" data-id={{$product['id']}}>

				<div class="sk-cart-photo">
                    <img style="width: 62px;" src="{{ (Upload::hasFiles('product',$product['id'])) ? Upload::getThumbMainProductFile('product',$product['id'],'208x200')[0] : '/assets/img/noimage.png'}}" alt="Shop image">
				</div>

				<div class="sk-cart-text">
					<p>
						<a class="sbmincart-name" href="">{{$product['title']}}</a>
					</p>
					<ul class="sbmincart-attributes">
						<li class="size" data-size="{{$product['_size']}}">{{Product::getSizeInfoStatic($product['_size'])}}</li>
					</ul>
				</div>

				<div class="sk-cart-price text-right">
					<p>
						{{$product['_price']}}
						<span class="sbmincart-price"> {{$product['currency']}}</span>
					</p>
					<p>
						<button type="button" class="sbmincart-minus " data-sbmincart-idx="0"><span>-</span></button>
						<input class="sbmincart-quantity" data-sbmincart-idx="0" name="quantity_1" type="text" pattern="[0-9]*" value="{{$product['_count']}}" autocomplete="off" disabled="disabled">
						<button type="button" class="sbmincart-plus " data-sbmincart-idx="0"><span>+</span></button>    
						<!-- <button type="button" class="sbmincart-remove" data-sbmincart-idx="0">×</button>               -->
					</p>
				</div>
			</li>

			
		@endforeach

	</ul>    
	<div class="sbmincart-footer">                    
		<div class="sbmincart-subtotal text-right">Итого: {{$summ}} UZS</div> 

		<button type="button" class="sbmincart-closer">Продолжить покупку</button>           
		<a href="/cart" class="sbmincart-submit">Оформить заказ</a>            
	</div>
	@else
	<h3 style="text-align: center; margin:0; font-weight: 400; font-size: 18px;"> В вашей корзине еще нет товаров. </h3>
	@endif  
</form>