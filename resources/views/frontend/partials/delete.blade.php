<div class="modal js-delete-modal" style="display: none;">
	<div class="modal__content">
		<header class="modal__header">
			Удаление
			<button class="modal__dismisser js-delete-modal-close">&times;</button>
		</header>

		<section class="modal__body">
			Вы действительно хотите удалить?
		</section>

		<footer class="modal__footer text-right">
		{!! Form::open(['action' => $action, 'method' => 'POST']) !!}
			<input type="hidden" name="delete" id="delete">
			<button class="btn-outline btn-outline--purple m-r-6 js-delete-modal-close">Отмена</button>
			<button class="btn btn--purple">Да</button>
		{!! Form::close() !!}
		</footer>
	</div>
</div>

<script>
$(document).ready(function(){
    $('body').on('click', '[name=delete_item]', function(){
        $('#delete').val(this.id);
    });
  });
</script>