@extends('layouts.cabinet')

@section('content')
<style>
	.balance-section .my_balance{
		margin: 20px;
	}

	.balance-section .form-group label{
		margin: 10px;
		font-size: 20px;
	}
</style>
<div class="balance-section">
	<h2 class="my_balance">Ваш баланс: {{$balans}}</h2>



	<div class="form-group col-sm-6 ">
		<label for="pay_form">Пополнить баланс</label>
		<input type="text" id="pay_form" class="input input--gray" placeholder="Введите сумму">
	</div>
	<div class="clearfix"></div>
	<div class="payments col-sm-12">

		<a href="#" id="payment_chk_paynet" data-type="paynet" class="pay_method fleft tdnone box br5 marginbott20 marginleft20 tcenter small lheight14  selected">
			<span class="icon block paysystem" style="background-image: url(/assets/img/click.jpg)">></span>
			<span class="block title large flexline">
				<span class="vmiddle" style="padding-left:0px">CLICK</span>
			</span>
		</a>

		<a href="#" id="payment_chk_paynet" data-type="paynet" class="pay_method fleft tdnone box br5 marginbott20 marginleft20 tcenter small lheight14  selected">
			<span class="icon block paysystem" style="background-image: url(/assets/img/payme.png)">></span>
			<span class="block title large flexline">
				<span class="vmiddle" style="padding-left:0px">PAYME</span>
			</span>
		</a>

		<a href="#" id="payment_chk_paynet" data-type="paynet" class="pay_method fleft tdnone box br5 marginbott20 marginleft20 tcenter small lheight14 selected">
			<span class="icon block paysystem" style="background-image: url(/assets/img/paynet.jpg)"></span>
			<span class="block title large flexline">
				<span class="vmiddle" style="padding-left:0px">PAYNET</span>
			</span>
		</a>
	</div>
</div>
@stop