<?php
use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Support\Facades\URL;

?>

@extends('layouts.frontend')

@section('content')
<section class="p-t-nav-sticked container order-section" data-current="{{ URL::full()}}">
    <h1 >Мои заказы</h1>
    @include('widgets.breadcrumbs', ['pages' => $pages])
    
    @if(empty($orders))
        <h2>Ещё нет заказа</h2>
    @else

    <div class="table-responsive cart_info">
        <table class="table table-condensed">
            <thead>
                <tr class="cart_menu">
                    <td class="image">Общий статус</td>
                    <td class="image">Номер заказа</td>
                    <td class="image">Время</td>
                    <td class="image">Скидка (сум)</td>
                    <td class="price">Сумма (сум)</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                <tr>
                	<td class="cart_description">
                		<?php $status = Order::status($order->status);?>
                		<span class="btn btn-{{$status['class']}}">{{$status['text']}}</span>
                    </td>
                    <td class="cart_description">
                        <p>№ {{$order->id}}</p>
                    </td>
                    <td class="cart_description">
                        <p>{{$order->created_at}}</p>
                    </td>
                    
                    
                    <td class="cart_description">
                        <p>{{$order->sale}}</p>
                    </td>
                    <td class="cart_description">
                        <p>{{$order->summ}}</p>
                    </td>
					<td class="cart_description">
                        <button class="btn_new btn-info list_product" data-id="{{$order->id}}"> Посмотреть список</button>
                    </td>

                    
                </tr>

                @endforeach
                
                
            </tbody>
        </table>
    </div>
   </div>
   @endif
  </section>
@stop
