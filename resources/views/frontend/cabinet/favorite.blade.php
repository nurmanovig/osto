@extends('layouts.cabinet')

@section('content')

	@if(Session::has('success'))
		<div class="alert alert--success m-b-25">
			{{ Session::get('success') }}
		    <button class="alert__dismisser"></button>
		</div>			
	@endif 

	<div class="row">
		<div class="col-md-6">
			<h3 class="cabinet__panel-content-title">
				Избранные товары
			</h3>
		</div>
	</div>

	<div class="row row-eq-height p-t-20">
		@forelse($favorites as $favorite)
			@if(!is_null($favorite->product))
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
				<div class="cabinet__card">
						<div class="cabinet__card-img-container">
							<div class="cabinet__card-cover">
								<div class="cabinet__card-cover-content centered">
									<a href="/product/view/{{ $favorite->product->alias }}" class="btn btn--white btn-rounded icon-eye"></a>
									<a href="#" name="delete_item" id="{{$favorite->id}}" class="btn btn--white btn-rounded icon-rubbish-can js-delete-modal-open"></a>
								</div>	
							</div>

							<img src="{{ (Upload::hasFile('product',$favorite->product->id)) ? Upload::getThumbFiles('product',$favorite->product->id,'250x375')[0] : '/assets/img/noimage2.png' }}" alt="Product image" class="cabinet__card-img">
						</div>

						<div class="cabinet__card-content">
							<a class="cabinet__card-title" href="{{ action('ProductController@getForm',[$favorite->product->id]) }}">{{ str_limit($favorite->product->title, 15) }}</a>
							<div class="cabinet__card-texts">{{ $favorite->product->price_format }} {{ Config::get('site.currency')[$favorite->product->currency] }}</div>
						</div>
				</div>
			</div>
			@endif
		@empty
		
		<div class="cabinet__info-block m-b-20">
			<div class="row">
				<div class="col-md-7">
					<p class="cabinet__info-block-text">У вас нет избранных товаров</p>
				</div>
			</div>
		</div>	
			
		@endforelse
	</div>		
@endsection

@section('scripts')
@include('frontend.partials.delete', ['action' => 'CabinetController@postDelete'])
	<script type="text/javascript">			
		var delete_modal = new Modal({
			openBtn: $('.js-delete-modal-open'),
			closeBtn: $('.js-delete-modal-close'),
			modal: $('.js-delete-modal')
		});
	</script>
@stop
