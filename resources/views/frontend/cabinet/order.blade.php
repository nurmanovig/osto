<?php
use App\Models\Order;
use App\Models\OrderProduct;
use Illuminate\Support\Facades\URL;

?>

@extends('layouts.frontend')

@section('content')
<section class="p-t-nav-sticked container order-section" data-current="{{ URL::full()}}">
    <h1 >Заказы</h1>
    @include('widgets.breadcrumbs', ['pages' => $pages])
    
    @if(empty($order_products))
        <h2>Ещё нет заказа</h2>
    @else


    <div class="container p_10_10">
        <a class="btn bnt-default" href="{{ URL::action('CabinetController@getOrders') }}">Все</a> 
        
        @foreach(Order::status() as $key => $item)
            <a class="btn btn-{{$item['class']}}" href="{{ URL::action('CabinetController@getOrders',['o_status' => $key]) }}">{{$item['text']}}</a> 
        @endforeach
       
    </div>
    <h3 style="margin-bottom: 20px;">Общая сумма: {{$summ ?:0}} сум</h3>
    <div class="table-responsive cart_info">
        <table class="table table-condensed">
            <thead>
                <tr class="cart_menu">
                    <td class="image">Общий статус</td>
                    <td class="image">Номер заказа</td>
                    <td class="image">Время</td>
                    <td class="image">Изображение</td>
                    <td class="image">Товар</td>
                    <td class="description">Количество</td>
                    <td class="description">Скидка (сум)</td>
                    <td class="price">Сумма (сум)</td>
                    <td class="quantity">Статус</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                @foreach($order_products as $op)
                <tr>
                	<td class="cart_description">
                		<?php $status = Order::status($op->o_status);?>
                		<span class="btn btn-{{$status['class']}}">{{$status['text']}}</span>
                    </td>
                    <td class="cart_description">
                        <p>№ {{$op->order_id}}</p>
                    </td>
                    <td class="cart_description">
                        <p>{{$op->created_at}}</p>
                    </td>
                    <td class="cart_description">
                        <?php $product = $op->product; ?>
                        <?php $fullname = "/uploads/thumb/product/208x200/".$product->id."/".$product->main_image;?>
						<img src="{{ Upload::hasImage($fullname) ? : '/assets/img/noimage.png'}}" alt="Shop image" width="100px">
                    </td>
                    <td class="cart_description">
                        <h4><a href="/product/view/{{$op->p_alias}}">{{substr($op->p_title, 0, 70)}}</a> <small></small></h4>
                        <p  class="size" data-size="{{ $op->size_product }}" >{{$product_model->getSizeInfo($op->size_product)}}</p>
                    </td>
                   	<td class="cart_description">
                        <p>{{$op->count}}</p>
                    </td>
                    <td class="cart_description">
                        <p>{{$op->count * $op->sale}}</p>
                    </td>
                    <td class="cart_description">
                        <p>{{$op->count * $op->price}}</p>
                    </td>

                   	<td class="cart_description" data-product_id="{{ $op->product_id }} " data-order_id="{{ $op->order_id }}">
                    <?php $statuses = $op->getStatusAction();?>
                    @foreach($statuses as $status)
                        @if($status['status'] == 0)
                            <span class="btn btn-{{$status['class']}}"  
                            >{{$status['text']}}</span>
                        @else
                            <span class="btn_new btn-{{$status['class']}} status" data-status="{{$status['status']}}">{{$status['text']}}</span>
                        @endif
                    @endforeach
                    </td>
                    
                </tr>

                @endforeach
                
                
            </tbody>
        </table>
    </div>
     {!! $order_products->appends($link)->links() !!}
   </div>

   @endif;
  </section>
@stop
