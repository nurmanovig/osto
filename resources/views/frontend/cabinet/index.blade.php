@extends('layouts.cabinet')

@section('content')

	@if(Session::has('success'))
		<div class="alert alert--success m-b-25">
			{{ Session::get('success') }}
		    <button class="alert__dismisser"></button>
		</div>			
	@endif 

	<div class="row">
		<div class="col-md-6">
			<h3 class="cabinet__panel-content-title">
				Мои товары
			</h3>
		</div>
	</div>

	@if($product->isEmpty())
		<!-- Add product panel -->
		<div class="cabinet__info-block m-b-20">
			<div class="row">
				<div class="col-md-7">
					<p class="cabinet__info-block-text">Добавьте свой первый товар, чтобы начать торговать</p>
				</div>
				<div class="col-md-5">
					<a href="{{ action('ProductController@getForm') }}" class="btn btn--purple btn--block">Добавить товар</a>
				</div>
			</div>
		</div>

	@else

	<div class="row row-eq-height p-t-20">
		@foreach($product as $p)
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			<div class="cabinet__card">
				<div class="cabinet__card-img-container">

					<div class="cabinet__card-cover">
						<div class="cabinet__card-cover-content centered">
							<a href="{{ action('ProductController@getView',[$p->alias]) }}" class="btn btn--white btn-rounded icon-eye"></a>
							<a href="{{ action('ProductController@getForm',[$p->id]) }}" class="btn btn--white btn-rounded icon-edit-pen"></a>
							<a href="#" id="{{ $p->id }}" name="delete_item" class="btn btn--white btn-rounded icon-rubbish-can js-delete-modal-open"></a>
						</div>	
					</div>

					<img src="{{ (Upload::hasFile('product', $p->id)) ? Upload::getThumbMainProductFile('product', $p->id, '250x375')[0] : '/assets/img/noimage2.png' }}" alt="Product image" class="cabinet__card-img">
				</div>

				<div class="cabinet__card-content">
					<a class="cabinet__card-title" href="#">{{ str_limit($p->title,15) }}</a>
					<div class="cabinet__card-text">{{ $p->price_format }} {{ Config::get('site.currency')[$p->currency] }}</div>
				</div>
			</div>
		</div>
		@endforeach
		
		<div class="col-xs-12 text-right">
			<a href="{{ action('ProductController@getForm') }}" class="btn btn--purple">Добавить товар</a>
		</div>
		
	</div>

	<div class="text-right">
		{{ $product->links() }}
	</div>	

	@endif
	
@stop
@section('scripts')
@include('frontend.partials.delete', ['action' => 'ProductController@postDelete'])
	<script type="text/javascript">
	
		var delete_modal = new Modal({
			openBtn: $('.js-delete-modal-open'),
			closeBtn: $('.js-delete-modal-close'),
			modal: $('.js-delete-modal')
		});

	</script>
@stop