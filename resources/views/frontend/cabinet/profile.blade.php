@extends('layouts.cabinet')

@section('content')
	@if(Session::has('success'))
		<div class="alert alert--success m-b-25">
			{{ Session::get('success') }}
		    <button class="alert__dismisser"></button>
		</div>
	@endif	

	<div class="row">
		<div class="col-xs-12">
			<h3 class="cabinet__panel-content-title">
				Основные информации
			</h3>
		</div>
	</div>				
	
	{!! Form::open(['action' => 'CabinetController@postProfile','method' => 'POST']) !!}
	<div class="row">
		<div class="col-md-6">
			<div class="form-group p-t-20 {{ ($errors->has('name')) ? 'form-group--error' : '' }}">
			<label class="input-label">Имя </label>
			{!! Form::text('name', $user->name, ['class' => 'input input--gray']) !!}
			@if($errors->has('name'))
			<span class="form-group__label form-group__label--error">{{ $errors->get('name')[0] }}</span>
			@endif
			</div>	
		</div>
		
		<div class="col-md-6">
			<div class="form-group p-t-20 {{ ($errors->has('lastname')) ? 'form-group--error' : '' }}">
			<label class="input-label">Фамилия</label>
			{!! Form::text('lastname',$user->lastname, ['class' => 'input input--gray']) !!}
			@if($errors->has('lastname'))
			<span class="form-group__label form-group__label--error">{{ $errors->get('lastname')[0] }}</span>
			@endif
			</div>	
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group p-t-20 {{ ($errors->has('email')) ? 'form-group--error' : '' }}">
				<label class="input-label">E-mail</label>
				{!! Form::text('email',$user->email, ['class' => 'input input--gray']) !!}
				@if($errors->has('email'))
				<span class="form-group__label form-group__label--error">{{ $errors->get('email')[0] }}</span>
				@endif
			</div>		
		</div>

		<div class="col-md-6">
			<div class="form-group p-t-20 {{ ($errors->has('phone')) ? 'form-group--error' : '' }}">
				<label class="input-label">Телефон</label>
				{!! Form::text('phone',$user->phone, ['class' => 'input input--gray js-phone-mask']) !!}
				@if($errors->has('phone'))
				<span class="form-group__label form-group__label--error">{{ $errors->get('phone')[0] }}</span>
				@endif
			</div>		
		</div>
	</div>	

	<div class="text-right">
		<a href="{{ action('CabinetController@getIndex') }}" class="btn btn--white m-r-5">Отменить</a>
		{!! Form::submit('Сохранить', array('class' => 'btn btn--purple')) !!}
	</div>
{!! Form::close() !!}
	

{!! Form::open(['action' => 'CabinetController@postChangePassword','method' => 'POST']) !!}
	<div class="row">
		<div class="col-xs-12">
			<h3 class="cabinet__panel-content-title">
				Изменить пароль
			</h3>
		</div>
	</div>
	
	<div class="form-group p-t-20 {{ ($errors->has('password')) ? 'form-group--error' : '' }}">
		<label class="input-label">Текущий пароль</label>
		<input type="password" name="password" class="input input--gray">
		@if($errors->has('password'))
		<span class="form-group__label form-group__label--error">{{ $errors->get('password')[0] }}</span>
		@endif
	</div>	
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group p-t-20 {{ ($errors->has('new_password')) ? 'form-group--error' : '' }}">
				<label class="input-label">Придумайте новый пароль</label>
				<input type="password" name="new_password" class="input input--gray">
				@if($errors->has('password'))
				<span class="form-group__label form-group__label--error">{{ $errors->get('new_password')[0] }}</span>
				@endif
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group p-t-20 {{ ($errors->has('password_confirmation')) ? 'form-group--error' : '' }}">
				<label class="input-label">Повторите новый пароль</label>
				<input type="password" name="password_confirmation" class="input input--gray">
				@if($errors->has('password_confirmation'))
				<span class="form-group__label form-group__label--error">{{ $errors->get('password_confirmation')[0] }}</span>
				@endif
			</div>
		</div>
	</div>
	
	<div class="text-right">
		<a href="{{ action('CabinetController@getIndex') }}" class="btn btn--white m-r-5">Отменить</a>
		{!! Form::submit('Сохранить', array('class' => 'btn btn--purple')) !!}
	</div>
{!! Form::close() !!}

@stop
-