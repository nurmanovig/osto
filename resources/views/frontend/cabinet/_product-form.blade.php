@extends('layouts.cabinet')

@section('content')

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<div class="row">
		<div class="col-xs-12">
			<h3 class="cabinet__panel-content-title">
			{{ (empty($product) ? 'Добавить продукт' : 'Редактировать продукт') }}
			</h3>
		</div>
	</div>
	
  {!! Form::open(['files' => true, 'id' => 'add_form']) !!}
	 
	<div class="nameless p-t-20">
		<label class="input-label">
			Изображение товара
<!--			<div class="tip tip--right m-l-5">-->
<!--				<button class="tip__btn">?</button>-->
<!--				<div class="tip__content">This is a tip</div>-->
<!--			</div>-->

		</label>
		
		<div class="row ln_cabinet_card_item">				
				@if(!empty($product) && Upload::hasFile('product',$product->id))
					@foreach(Upload::getFiles('product',$product->id) as $key=>$image)
					<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 p-t-20 image_product " id="img_{{$key}}">
						<div class="cabinet__card  cabinet__card--add cabinet__card--small has-preview" style="background-image: url({{$image}})">
						<button data-key="{{$key}}" class="cabinet__card-remove-btn js-card-remove">&times;</button>
						</div>
					</div>
					@endforeach
					{!! Form::hidden('images_for_delete', null) !!}
				@endif

				<div class="js-cards-container">
				</div>
				
				<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 js-preview-card p-t-20">
					<div class="cabinet__card cabinet__card--add cabinet__card--small">
						<a href="#" class="cabinet__card-cover-link js-preview-card-toggle"></a>
						<span class="cabinet__card-plus-icon">+</span>
					</div>
				</div>

				<input type="text" name="images_to_upload" class="js-images-to-upload" value="" style="display: none;">
				<input type="file" name="images[]" style="display:none;" class="js-file-input" accept="image/jpeg,image/png,image/gif" multiple="multiple">
			
				@if($errors->has('images'))
					<div class="form-group__label form-group__label--error">{{ $errors->get('images')[0] }}</div>
				@endif	
		</div>
	</div>


	<div class="form-group {{ ($errors->has('title')) ? 'form-group--error' : '' }}">
        <label class="input-label">
            Название
<!--            <div class="tip tip--right m-l-5">-->
<!--                <button class="tip__btn">?</button>-->
<!--                <div class="tip__content">This is a tip</div>-->
<!--            </div>-->
        </label>

		{!! Form::text('title', empty($product) ? null : $product->title, ['class'=>'input input--gray']) !!}
		@if($errors->has('title'))
			<div class="form-group__label form-group__label--error">{{ $errors->get('title')[0] }}</div>
		@endif
	</div>

	<input type="hidden" name="shop_id" value="{{ $shop->id }}">

	<div class="row category_part">
		
		@if(!empty($product))
			@widget("category-select", [["category" => $product->category_id, 'first' => true]])
		@else
			<div class="col-md-6 category_list" data-id="0">
				<div class="form-group{{ ($errors->has('category_id')) ? 'form-group--error' : '' }}">
					<label class="input-label">Категория</label>
					<div class="select-container">
						
						<select class="input input--gray" name="category_id" id="category">
							<option value="0" disabled="true" selected="true">Выберите</option>
							@foreach($categories as $category)
								<option value="{{ $category->id }}">{{ $category->{'title_'.App::getLocale()} }}</option>
							@endforeach	
						</select>	
								
			
						@if($errors->has('category_id'))
						<span class="form-group__label form-group__label--error">
							{{ ($errors->get('category_id')[0]) }}
						</span>
						@endif
					</div>
				</div>		
			</div>
		@endif

	</div>

	<div class="row">
		<div class="col-md-6">
			<div class="form-group p-t-20 {{ ($errors->has('description')) ? 'form-group--error' : '' }}">
				<label class="input-label">
					Описание
<!--					<div class="tip tip--right m-l-5">-->
<!--						<button class="tip__btn">?</button>-->
<!--						<div class="tip__content">This is a tip</div>-->
<!--					</div>-->
				</label>
				{!! Form::textarea('description', empty($product) ? null : $product->description, ['class' => 'input input--gray input--textarea prodtc_description', 'cols' => 6]) !!}
					@if($errors->has('description'))
						<div class="form-group__label form-group__label--error">{{ $errors->get('description')[0] }}</div>
					@endif
			</div>

            <div class="form-group">
                <label class="input-label">Размеры</label>
                
						
						<div>
						    
						    <select multiple name="size_product[]" id="product_size">
							@foreach($size_product as $key => $size)
							     <?php $checked = false;?>
                                @if(!empty($product->size_product))
                                @foreach($product->size_product as $p_size)
                                <?php
                                    if($size['id'] == $p_size){
                                        $checked = "selected";
                                        continue;
                                    }
                                ?>
                                @endforeach
                                @endif
								<option value="{{ $size->id }}" {{$checked}}>{{$size['key']}}</option>
							@endforeach	
						</select>	
						    
						</div>
								
                

                @if($errors->has('size_product'))
                <div class="form-group__label form-group__label--error">{{ $errors->get('size_product')[0] }}</div>
                @endif
            </div>
		</div>



		<div class="col-md-6">
            <div class="nameless p-t-20">
                <div class="form-group">
                    <label class="input-label">Цвет</label>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {{ ($errors->has('color')) ? 'form-group--error' : '' }}">

                                <?php echo Form::select('color', config('app.colors'), null, [
                                    'class' => 'input input--gray child'
                                ]); ?>

                                @if($errors->has('count_product'))
                                <div class="form-group__label form-group__label--error">{{ $errors->get('count_product')[0] }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="nameless">
                <div class="form-group ">
                    <label class="input-label">Цена</label>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group {{ ($errors->has('price')) ? 'form-group--error' : '' }}">
                                {!! Form::text('price', empty($product) ? null : $product->price,['class' => 'input input--gray']) !!}


                                @if($errors->has('price'))
                                <div class="form-group__label form-group__label--error">{{ $errors->get('price')[0] }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" value="UZS" class="input input--gray" disabled="disabled">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ ($errors->has('sale_value')) ? 'form-group--error' : '' }}">
                        <label class="input-label ">Скидка на товар</label>
                        <div class="btn-group btn-group--purple status_checkbox">
                            <div class="btn-group__item btn-group__item--first">
                                <input type="checkbox" value="1" name="sale_status" id="sale_status" {{!empty($product) && $product->sale_status ? "checked" : null }} >
                                <label class="btn-group__btn btn" for="sale_status">{{!empty($product) && $product->sale_status ? "Вкл" : "Выкл"}}</label>
                            </div>
                        </div>

                        {!! Form::text('sale_value', empty($product) ? null : $product->sale_value,['class' => 'input input--gray status_input '. (empty($product) || !$product->sale_status ? "active" : ""), 'disabled' => !(!empty($product) && $product->sale_status) ]) !!}

                        @if($errors->has('sale_value'))
                            <div class="form-group__label form-group__label--error">
                                {{ $errors->get('sale_value')[0] }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

		</div>

	</div>

	<input type="hidden" name="main_image_position" id="main_image_position" value="">

	<div class="text-right">
		<a href="{{ action('CabinetController@getIndex') }}" class="btn btn--white m-r-5">Отменить</a>
		<button class="btn btn--purple" type="submit">Сохранить</button>
	</div>
	{{ Form::close() }}
	

@stop

@section('scripts')

<script type="text/javascript" src="{{ asset('assets/js/file-preview.js') }}"></script>
<script type="text/javascript">

	var file_input = $('.js-file-input');
	var file_cards_container = $('.js-cards-container');
	var image_to_upload_input = $('.js-images-to-upload');


		
	var fp = new filePreview({
		input: file_input,
		trigger: $('.js-preview-card-toggle'),
		removeBtnClass: '.js-card-remove',

		onRead: function(url, files){
			var result = "";
			var images_to_upload = "";

			$.each(files, function(index, file){

				var cardTemplate = '<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3  p-t-20" data-index="' + index + '">' +
										'<div class="cabinet__card cabinet__card--add cabinet__card--small has-preview" style="background-image: url(' + file.url + ')">' +
											'<a href="#" class="cabinet__card-cover-link js-preview-card-toggle"></a>' +
											'<span class="cabinet__card-plus-icon">+</span>' +
											'<button class="cabinet__card-remove-btn js-card-remove">&times;</button>' +
										'</div>' +
									'</div>';

				result += cardTemplate;

				file_cards_container.html(result);
				return;
			});
		}
	});	
	

	var trash = [];	

	$('body').on('click', '.js-card-remove', function(e){
		e.preventDefault();
	
		$(this).parent().parent().remove();
		@if(empty($product))
			fp.removeFile(parseInt($(this).data('index')))
		@else
			trash.push($(this).data('key'));
	        $('input[name=images_for_delete]').val(trash.join(','));
	        		console.log(11);
	        return false;
        @endif

	});



	$(document).ready(function(){
	    
	   $('#product_size').select2();
	    
        $(document).on('change', '#category, .child', function(e){
             
            var direction_id = parseInt($(this).val());
            var parents = $(this).parent().parent().parent().parent().parent();
            var parent = parents;
            var option = " ";
            var parent = $(this).parent().parent().parent().data("id");
            console.log(direction_id);
            

            deleteCategoryList(parent);
            $.ajax({
                type:'GET',
                url:'{!! action("ProductController@getSubCategory") !!}', 
                data: {'id':direction_id},
                success:function(data){
                	// console.log(data);

                	if(data){
                		reName(parent);
                	}

                	$(".category_part").append(data);


                	return;


                	if(data.length <= 0){
                		$("#woops").hide();
                	}else{
                		$("#woops").show();
                	}

                    for(var i=0; i < data.length; i++){
                        option+='<option value="'+data[i].id+'">'+data[i].title_ru+'</option>'; 
                    }

                    parent.find('#child').html(" ");
                    parent.find('#child').append(option);

                },
                error:function(data){
                    console.log('error');
                }      
            });
            
        });
    });
	
	// $("button[type='submit']").click(function(e){
	// 	e.preventDefault();
	// 	$("#main_image_position").val($(".image_product:first").attr("id"));
	// })
    function deleteCategoryList(parent){
    	$(".category_list").each(function(index, value){
    		if($(value).data("id") > parent){
    			$(value).remove();
    		}
    	})
    }

    function reName(parent){
    	$("select[name='category_id']").each(function(index, value){
    		console.log($(value).attr("name"));
    		if($(value).attr("name") == "category_id"){
    			$(value).removeAttr("name");
    		}
    	})
    }

</script>
@stop


<!-- 
	$(document).ready(function(){
        $(document).on('change', '#category, .child', function(e){
             
            var direction_id = $(this).val();
            var parents = $(this).parent().parent().parent().parent().parent();
            var parent = parents;
            var option = " ";
            
            $.ajax({
                type:'GET',
                url:'{!! action("ProductController@getSubCategory") !!}', 
                data: {'id':direction_id},
                success:function(data){
                	console.log(data);
                	return;
                if(data.length <= 0){
                		$("#woops").hide();
                	}else{
                		$("#woops").show();
                	}
                    for(var i=0; i < data.length; i++){
                        option+='<option value="'+data[i].id+'">'+data[i].title_ru+'</option>'; 
                    }

                    parent.find('#child').html(" ");
                    parent.find('#child').append(option);

                },
                error:function(data){
                    console.log('error');
                }      
            });
            
        });
    }); -->


