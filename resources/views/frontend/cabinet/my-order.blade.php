    <div class="table-responsive cart_info">
        <table class="table table-condensed">
            <thead>
                <tr class="cart_menu">
                    <td class="image">Название</td>
                    <td class="image">Количество</td>
                    
                </tr>
            </thead>
            <tbody>
                @foreach($products as $o_product)
                <?php $product = $o_product->product;?>
                @if(!empty($product))
                <tr>

                    <td class="cart_description">
                        <p>{{$product->title}}</p>
                        <small>{{$product->getSizeInfo($o_product->size_product)}}</small>
                    </td>  
                     <td class="cart_description">
                        <p>{{$o_product->count}}</p>
                    </td>                   
                </tr>
                @endif
                @endforeach
                
                
            </tbody>
        </table>
    </div>