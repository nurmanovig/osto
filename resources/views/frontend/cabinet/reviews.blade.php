@extends('layouts.cabinet')

@section('content')
	
	<div class="row">
		<div class="col-md-6">
			<h3 class="cabinet__panel-content-title">
				Отзывы
			</h3>
		</div>
	</div>

	@if(!empty($reviews))
		@foreach($reviews as $review)
			<div class="comment">
				<div class="comment__info">
					<a href="#" class="comment__title title cl-purple">{{ $review->user->name }}</a>
					<div class="comment__date">{{ $review->created_at }}</div>
					<div class="comment__text">{{ $review->text }}</div>
				</div>
			</div>
		@endforeach
		<div class="text-right">
		{{ $reviews->links() }}
		</div>
	@else
		<div class="comment">
			<div class="comment__info">
				
			</div>
		</div>
	@endif		
		
@stop