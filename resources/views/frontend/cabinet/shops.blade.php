@extends('layouts.cabinet')

@section('content')
	
	<div class="row">
		<div class="col-md-6">
			<h3 class="cabinet__panel-content-title">
				Мой магазин
			</h3>
		</div>
	</div>

	<div class="row row-eq-height p-t-20">
		@foreach($shop as $s)
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
				<div class="cabinet__card">
					<a href="{{ action('ShopController@getEdit',$s->id) }}" class="cabinet__card-cover-link"></a>

					<div class="cabinet__card-img-container">
						<img src="{{ Upload::hasFile('shop',$s->id) ? Upload::getFiles('shop',$s->id,'203x139')[0] : asset('assets/img/temp/cabinet-product-image.png') }}" alt="Product image" class="cabinet__card-img">
					</div>

					<div class="cabinet__card-content">
						<div class="cabinet__card-title">{{ $s->title }}</div>
						<div class="cabinet__card-text">{{ $s->products->count() }} товаров</div>
					</div>
				</div>
			</div>
		@endforeach
		
		@if($shop->count() < 1)	
			<div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
				<div class="cabinet__card cabinet__card--add">
					<a href="{{ action('ShopController@getAdd') }}" class="cabinet__card-cover-link"></a>
					<span class="cabinet__card-plus-icon">+</span>
				</div>
			</div>
		@else

		@endif
	</div>

	<div class="text-right">
			{{ $shop->links() }}
	</div>	

@stop