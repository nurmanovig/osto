@extends('layouts.cabinet')

@section('content')
    <link rel="stylesheet" href="/assets/leaflet/leaflet.css" />
<script src="/assets/leaflet/leaflet-src.js"></script>
<script src="/assets/leaflet/esri-leaflet.js"></script>
<script src="/assets/leaflet/esri-leaflet-geocoder.js"></script>
    
    @if(Session::has('success'))
        <div class="alert alert--success m-b-25">
            {{ Session::get('success') }}
            <button class="alert__dismisser"></button>
        </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <h3 class="cabinet__panel-content-title">
            {{ (empty($shop) ? 'Добавить магазин' : 'Редактировать  магазин') }} 
            </h3>
        </div>
    </div>  

    {!! Form::open(['files'=>true, 'class' => 'shop-form', 'id' => 'add_form']) !!}
    
    <div class="nameless p-t-20">
        <label class="input-label">
            Изображение магазина
<!--            <div class="tip tip--right m-l-5">-->
<!--                <button class="tip__btn">?</button>-->
<!--                <div class="tip__content">This is a tip</div>-->
<!--            </div>-->

        </label>
        
        <div class="row">
            <input type="text" name="images_to_upload" class="js-images-to-upload" value="" style="display: none;">
            <input type="file" name="image" class="js-file-input" style="display: none">
            <div class="js-cards-container">
            <?php $is_img = false;?>
            @if(!empty($shop))
                
                    @if(Upload::hasFile('shop',$shop->id))
                    <?php $is_img = true;?>
                    <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3" data-index="">
                        <div class="cabinet__card cabinet__card--add cabinet__card--small has-preview" style="background-image: url({!! Upload::getFile('shop', $shop->id) !!})">
                            <a href="#" class="cabinet__card-cover-link js-preview-card-toggle click_image"></a>
                            <span class="cabinet__card-plus-icon"></span>
                            <button class="cabinet__card-remove-btn js-card-remove">&times;</button>
                        </div>
                    </div>
                    @endif
                
            @endif
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 js-preview-card add_image {{$is_img ? 'none' : null}}">
                <div class="cabinet__card cabinet__card--add cabinet__card--small">
                    <a href="#" class="cabinet__card-cover-link js-preview-card-toggle"></a>
                    <span class="cabinet__card-plus-icon">
                      @if(!empty($shop))        
                        &#9998
                      @else
                        &#10133
                      @endif
                    </span>
                </div>
            </div>

    
            @if(!empty($errors) && $errors->has('image'))
                <div class="form-group__label form-group__label--error">{{ $errors->get('image')[0] }}</div>
            @endif
        </div>
    </div>  


    <div class="form-group">
        <label class="input-label js-shop-name">Название магазина</label>
        {!! Form::text('title', empty($shop) ? null : $shop->title, ['class' => 'input input--gray']) !!}
        @if(!empty($errors) && $errors->has('title'))
        <span class="form-group__label form-group__label--error">{{ $errors->get('title')[0] }}</span>
        @endif
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="input-label">Адрес</label>
                {!! Form::text('address', empty($shop) ? null : $shop->address,['class' => 'input input--gray']) !!}
                @if(!empty($errors) && $errors->has('address'))
                <span class="form-group__label form-group__label--error">
                    {{ $errors->get('address')[0] }}
                </span>
                @endif
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="input-label">Контактный номер магазина</label>
                {!! Form::text('phone', empty($shop) ? null : $shop->phone, ['class' => 'input input--gray', 'id' => "phone"]) !!}
                 @if(!empty($errors) && $errors->has('phone'))
                <span class="form-group__label form-group__label--error">
                    {{ $errors->get('phone')[0] }}
                </span>
                @endif
            </div>
        </div>
    </div>

 
 <!--41.311081 | 69.240562-->
    <input type="hidden" name="lat" class="js-lat-input" value="<?=isset($shop->lat) ? $shop->lat : null?>" <?=isset($shop->lat) ? $shop->lat : null?>>
    <input type="hidden" name="lng" class="js-lng-input" value="<?=isset($shop->lng) ? $shop->lng : null?>" <?=isset($shop->lat) ? $shop->lat : null?>>
    <div id="map_shop" class="js-shop-map w-100" style="height: 300px; background-color: #ccc"></div>
    <!--<span class="btn btn-default my_location shop_location" title="Мое местоположение"><img src="/assets/img/marker-general.png" width="18px"></span>-->
    
    <div class="row">
        <div class="col-md-8">
            <div class="form-group m-t-20">
                <label class="input-label">Рабочие дни</label>
                
                <br>

                <div class="btn-group btn-group--purple">

                    <div class="btn-group__item btn-group__item--first">
                        @if(!empty($shop))
                            <input type="checkbox" value="1" name="day1" @if($shop->day1==1) checked="checked" @endif id="item1">
                        @else
                            <input type="checkbox" value="1" name="day1" id="item1">
                        @endif      
                        <label class="btn-group__btn btn" for="item1">ПН.</label>
                    </div>

                    <div class="btn-group__item">
                        @if(!empty($shop))
                            <input type="checkbox" value="1" name="day2" @if($shop->day2==1) checked="checked" @endif id="item2">
                        @else
                            <input type="checkbox" value="1" name="day2"  id="item2">
                        @endif      
                        <label class="btn-group__btn btn" for="item2">ВТ.</label>
                    </div>

                    <div class="btn-group__item">
                        @if(!empty($shop))
                            <input type="checkbox" value="1" name="day3" @if($shop->day3==1) checked="checked" @endif id="item3">
                        @else
                            <input type="checkbox" value="1" name="day3"  id="item3">
                        @endif      
                        <label class="btn-group__btn btn" for="item3">СР.</label>
                    </div>

                    <div class="btn-group__item">
                        @if(!empty($shop))
                            <input type="checkbox" value="1" name="day4" @if($shop->day4==1) checked="checked" @endif id="item4">
                        @else
                            <input type="checkbox" value="1" name="day4"  id="item4">
                        @endif          
                        <label class="btn-group__btn btn" for="item4">ЧТ.</label>
                    </div>

                    <div class="btn-group__item">
                        @if(!empty($shop))
                            <input type="checkbox" value="1" name="day5" @if($shop->day5==1) checked="checked" @endif id="item5">
                        @else
                            <input type="checkbox" value="1" name="day5"  id="item5">
                        @endif  
                        <label class="btn-group__btn btn" for="item5">ПТ.</label>
                    </div>

                    <div class="btn-group__item">
                        @if(!empty($shop))
                            <input type="checkbox" value="1" name="day6" @if($shop->day6==1) checked="checked" @endif id="item6">
                        @else
                            <input type="checkbox" value="1" name="day6"  id="item6">
                        @endif      
                        <label class="btn-group__btn btn" for="item6">СБ.</label>
                    </div>

                    <div class="btn-group__item btn-group__item--last">
                        @if(!empty($shop))
                            <input type="checkbox" value="1" name="day7" @if($shop->day7==1) checked="checked" @endif id="item7">
                        @else
                            <input type="checkbox" value="1" name="day7"  id="item7">
                        @endif      
                        <label class="btn-group__btn btn" for="item7">ВС.</label>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-2">
            <div class="form-group m-t-20">
                <label class="input-label">Открытие в</label>
                <div class="select-container">
                    <select class="input input--gray" id="workstart" name="working_hour_start">
                    @if(!empty($shop))
                        @if($shop->working_hour_start)
                            <option value="{{ $shop->working_hour_start }}" selected="">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }}</option>
                        @endif  
                    @endif
                    <?php $workingHours = workingHours();?>
                        @foreach($workingHours as $hour)
                            <option value="{!! $hour !!}:00">{!! $hour !!}:00</option>
                        @endforeach


                    </select>
                </div>
            </div>
        </div>
        

        <div class="col-md-2">
            <div class="form-group m-t-20">
                <label class="input-label">Закрытие в</label>
                <div class="select-container">
                    <select class="input input--gray" id="workend" name="working_hour_end">
                    @if(!empty($shop))
                        @if($shop->working_hour_end)
                            <option value="{{ $shop->working_hour_end }}" selected="">{{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i') }}</option>
                        @endif  
                    @endif
                        @foreach($workingHours as $hour)
                            <option value="{!! $hour !!}:00">{!! $hour !!}:00</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-10">

        <div class="form-group">
            <label class="input-label js-shop-name">Транзитный счёт (Payme конфигурация) <span class="ln_payme-info">!</span></label>
            {!! Form::text('merchant', (empty($shop) || !isset($shop->shopPaymeConfig->merchant)) ? null : $shop->shopPaymeConfig->merchant, ['class' => 'input input--gray', 'placeholder' => 'merchant ID']) !!}
            @if(!empty($errors) && $errors->has('merchant'))
                <span class="form-group__label form-group__label--error">{{ $errors->get('merchant')[0] }}</span>
            @endif
            <div class="clearfix"></div>
            <span class="ln_payme-info-view">
                Для подключение через Payme, введите в своем web-кассе след. данные:<br>
                    Endpoint URL : {{ "https://".$_SERVER['HTTP_HOST']."/payme-webhook.php" }}<br>
                    Имя : order_id<br>

                <span class="ln_close_payme">x</span>
            </span>
        </div>

    </div>
    <div class="clearfix"></div>
    <div class="text-right">
        <a href="{{ action('CabinetController@getIndex') }}" class="btn btn--white m-r-5">Отменить</a>
        {!! Form::submit('Сохранить', array('class' => 'btn btn--purple shop-btn')) !!}
    </div>
    {!! Form::close() !!}
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('assets/js/file-preview.js') }}"></script>
<script>
$('#phone').mask('+ (999) 99 999-9999', {'translation': {0: {pattern: /[0-9*]/}}});
    // IMAGE UPLOAD
    var file_input = $('.js-file-input');
    var file_cards_container = $('.js-cards-container');
    var image_to_upload_input = $('.js-images-to-upload');
        var changed = false;
    
    var fp = new filePreview({
        input: file_input,
        trigger: $('.js-preview-card-toggle'),
        removeBtnClass: '.js-card-remove',

        onRead: function(url, files){
            var result = "";
            var images_to_upload = "";

            $.each(files, function(index, file){
                var cardTemplate = '<div class="col-xs-6 col-sm-3 col-md-3 col-lg-3" data-index="' + index + '">' +
                                        '<div class="cabinet__card cabinet__card--add cabinet__card--small has-preview" style="background-image: url(' + file.url + ')">' +
                                            '<a href="#" class="cabinet__card-cover-link js-preview-card-toggle click_image"></a>' +
                                            '<span class="cabinet__card-plus-icon">+</span>' +
                                            '<button class="cabinet__card-remove-btn js-card-remove">&times;</button>' +
                                        '</div>' +
                                    '</div>';

                result = cardTemplate;
                file_cards_container.html(result);
               
                // $(".add_image").css({"dissplay" : "none"});
                 $(".add_image").addClass("none");
            });
        }
    });

    $("body").on("click", ".click_image", function(e){
        e.preventDefault();
        $("body").on("click", ".add_image");
    });
    
    $('body').on('click', '.js-card-remove', function(e){
        e.preventDefault();

        var _ = $(this);

        fp.removeFile(parseInt(_.data('index')))
        _.parent().parent().remove();
        $(".add_image").removeClass("none");
    });

    $(document).ready(function(){
        $(document).on('change', '#direction', function(e){
            var direction_id = parseInt($(this).val());
            var parents = $(this).parent().parent().parent().parent().parent();
            var parent = parents;
            var option = " ";

            if(direction_id != 2){
                $('#floor').hide();
            }else{
                $('#floor').show();
            }

            // HandleShowHideForms(direction_id);
            getPlaces(direction_id);


        });
    });

    // AJAX POST FORM  
    $(document).ready(function(){
         var submit = $('.shop-btn');
        var form = $('.shop-form');
        var input = form.find('input');
        var input_group = input.parent();

        submit.on('click', function(e){
            // e.preventDefault();
            var data = form.serialize();
             // data = new FormData(jQuery('form')[0]);
            submit.attr('disabled', true);


            var file_data = $(".js-file-input").prop("files")[0];
            // var data = new FormData(this);                  // Creating object of FormData class

            $.ajax({
                method: "POST",
                data: data,
                async: false,
                url: '{{ action('ShopController@postForm') }}',
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');

                    if (token) {
                          return xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    }
                },

                // data: data,
                success:function(data, textStatus){

                    submit.attr('disabled', false);
                    if(data.success == 1){
                        window.location.href = data.url;
                    }else {
                        if(!input_group.hasClass('form-group--error')){
                            $.each(data.errors, function(key, value){
                                $.each(value, function(j, error) {
                                    var input = $('[name='+key+']');

                                    input.after('<span class="form-group__label form-group__label--error">'
                                                    + error +
                                                '</span>');

                                    input.parent().addClass('form-group--error');
                                });
                            });
                        }
                    }
                }
            });
        });
    });

    $(".ln_payme-info, .ln_close_payme").click(function(){
        $(".ln_payme-info-view").toggleClass("ln_show_payme_info");
    })

    function roundToTwo(num) {    
        return (Math.round(num * 100) / 100);
    }

    var map_shop;

    var position = undefined;
    var markers = [];
    
    var elem = {coords: {lat: $(".lat"), lng:  $(".lng")}, address: $(".address-info")};
    getPlaces(true);


    
    function mapInit(options = {}){
        if(options.hasOwnProperty("position") ){
            position = options.position;
            createMap();
            if(options.addMarker){
                addMarker({coords: position});
            }   

            if(options.geocode){
                geocodeByLatLng(options.position, options.elem);
                eventListener(map_shop, options.elem);

            }else{
                map_shop.off("click");
            }

        }else{
            navigator.geolocation.getCurrentPosition(function(pos) 
            {
                position = [
                    41.311081,
                    69.240562
                ];

                createMap();
                
                if(options.addMarker){
                    addMarker({coords: position});
                }   

                if(options.geocode){
                    geocodeByLatLng({lat: position[0], lng: position[1]}, options.elem);
                    eventListener(map_shop, options.elem);

                }else{
                    map_shop.off("click");
                }

            });
        }
        
    }


    function createMap(){
        if(!map_shop){
            accessToken = "pk.eyJ1IjoibnVybWFub3ZpZyIsImEiOiJjampoemg3YW4ycjVrM3ZudmQ1bWk0bjV5In0.ov5Y46Myp4-nugnYyHzHmw";

            map_shop = L.map('map_shop',{
                center: position,
                zoom: 15
            });

            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                 accessToken: accessToken
            }).addTo(map_shop);    
        }
        
    }


    function eventListener(map_shop, elem){
        map_shop.on("click", function(event){
            //console.log("event.latlng");
            //console.log(event.latlng);

            var info = {coords : [event.latlng.lat, event.latlng.lng]};
            addMarker(info);
            geocodeByLatLng(event.latlng, elem);
        })

    }



    function addMarker(info, clear = true){

        var options = {};

        if(clear){
            clearMarkers();
        }
        
        if(info.options){
            if(info.icon){
                var icon = L.icon({
                    iconUrl: info.icon,
                    iconSize: [38, 95], // size of the icon
                });
                options.icon = icon;
            }
        }

        var marker = L.marker(info.coords, options);
        
        if(info.popup){
            marker.bindPopup(info.popup);
        }
        
        marker.addTo(map_shop);
        try {
             map_shop.addLayer(marker);    

        } catch (err) {

        }
       
        markers.push(marker);
    }

    function clearMarkers(){
        //console.log("markers");
        //console.log(markers);
        for(x in markers){
            map_shop.removeLayer(markers[x]);

        }
        markers = [];
    }


    function geocodeByLatLng(coords, elem){

        if(coords.lat && coords.lng){
           
            elem.coords.lat.val(coords.lat);
            elem.coords.lng.val(coords.lng);
        }

        if(elem.address){
            var geocodeService = L.esri.Geocoding.geocodeService();
            geocodeService.reverse().latlng(coords).run(function(error, result)  {
                elem.address.val(result.address.Match_addr);    
            });
        }
    }

    function getPlaces(first = false){

        var elem = {coords: {lat: $(".js-lat-input"), lng: $(".js-lng-input")}, address: $("input[name='address']")};
        var options = {addMarker: true, geocode: true, elem: elem};
        if(first && $(".js-lat-input").val() && $(".js-lng-input").val()){


            options.position = [$(".js-lat-input").val()*1, $(".js-lng-input").val()*1];

        }

        mapInit(options);

    }

    </script>
@stop

