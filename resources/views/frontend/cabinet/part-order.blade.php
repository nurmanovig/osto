<?php
use App\Models\Order;
use App\Models\OrderProduct;
?>
<div class="container">
    <div class="table-responsive cart_info">
        <table class="table table-condensed">
            <thead>
                <tr class="cart_menu">
                    <td class="image">Общий статус</td>
                    <td class="image">Время</td>
                    <td class="image">Товар</td>
                    <td class="description">Количество</td>
                    <td class="price">Сумма (сум)</td>
                    <td class="quantity">Статус</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                @foreach($order_products as $op)
                <tr>
                    <td class="cart_description">
                        <?php $status = Order::status($op->o_status);?>
                        <span class="btn btn-{{$status['class']}}">{{$status['text']}}</span>
                    </td>
                    <td class="cart_description">
                        <p>{{$op->created_at}}</p>
                    </td>
                    <td class="cart_description">
                        <h4><a href="/order/view/{{$op->p_alias}}">{{substr($op->p_title, 0, 70)}}</a></h4>
                        <p><a href="/shop/view/{{$op->sh_alias}}">{{ $op->sh_title }}</a></p>
                    </td>
                    <td class="cart_description">
                        <p>{{$op->count}}</p>
                    </td>
                    <td class="cart_description">
                        <p>{{$op->count * $op->price}}</p>
                    </td>

                    <td class="cart_description" data-id="{{ $op->product_id }}">
                        @if($op->status == OrderProduct::STATUS_STOPPED)
                            <span class="btn btn-danger">Остановлен</span>
                        @elseif($op->status == OrderProduct::STATUS_INACTION)
                            <span class="btn_new btn-warning status" data-status="-1">Получено заявка</span>
                            <span class="btn_new btn-danger status" data-status="1">Остановить</span>
                        @elseif($op->status == OrderProduct::STATUS_WAIT)
                            <span class="btn_new btn-info status" data-status="1">Готово</span>
                        @elseif($op->status == OrderProduct::STATUS_FINISHED)
                            <span class="btn btn-success">Готово</span>
                        @endif
                        <span></span>
                    </td>
                    
                </tr>

                @endforeach
                
                
            </tbody>
        </table>
    </div>
         {!! $order_products->appends($status !== null ? ['status' => $status] : [])->links() !!}
   </div>