@extends('layouts.frontend')

@section('content')

<div class="register min-height">
	<div class="container p-t-nav-sticked">
		<div class="col-md-5" data-mh="mh1">
			<div class="register__panel big-shade">
				<ul class="register__tab clearfix">
					<li class="register__tab-item js-register-tab is-active" data-index="0">Я ПОКУПАТЕЛЬ</li>
					<li class="register__tab-item js-register-tab" data-index="1">Я ПРОДАВЕЦ</li>
				</ul>

				<div class="register__panel-body">
	
					<!-- Buyer -->
					<div class="register__tab-content js-register-tab-content is-active">			
						{!! Form::open(['action' =>'UserController@postUserRegister','method'=>'POST','class' => 'user-register']) !!}
							<div class="form-group">	
								<input type="text" name="name" class="input input--gray" placeholder="Имя">
							</div> 

							<div class="form-group">	
								<input type="text" name="lastname" class="input input--gray" placeholder="Фамилия">
							</div>

							<div class="form-group">
								<input type="email" name="email" class="input input--gray" placeholder="E-mail">
							</div>

							<div class="form-group">
								<input type="password" name="password" class="input input--gray" placeholder="Придумайте пароль">
							</div>

							<div class="form-group">
								<input type="password" name="password_confirmation" class="input input--gray" placeholder="Повторите пароль">
							</div>

							<div class="form-group">
								 <input type="text" name="phone" class="input input--gray" id="phone" placeholder="Номер телефона"> 
							</div>

							<button type="submit" class="btn btn--purple btn--block">ЗАРЕГИСТРИРОВАТЬСЯ</button>

							<p class="register__agreement-note text-center">
								Регистрируясь на портале Osto.uz,<br>
								я соглашаюсь <a href="{{ action('PageController@show', 'user-agreement') }}" class="font-medium">с пользовательским соглашением<br>
								и политикой конфиденциальности</a>
							</p>
						{!! Form::close() !!}
					</div>
					
					<!-- Seller -->
					<div class="register__tab-content js-register-tab-content">
						{!! Form::open(['action' => 'UserController@postSellerRegister','method'=>'POST','class'=>'seller-register']) !!}
							<div class="form-group">	
								<input type="text" name="name" class="input input--gray" placeholder="Имя">
							</div>

							<div class="form-group">	
								<input type="text" name="lastname" class="input input--gray" placeholder="Фамилия">
							</div>

							<div class="form-group">
								<input type="email" name="email" class="input input--gray" placeholder="E-mail">
							</div>

							<div class="form-group">
								<input type="password" name="password" class="input input--gray" placeholder="Придумайте пароль">
							</div>

							<div class="form-group">
								<input type="password" name="password_confirmation" class="input input--gray" placeholder="Повторите пароль">
							</div>

							<div class="form-group">
								<input type="text" name="phone" class="input input--gray" id="phone2" placeholder="Номер телефона">
							</div>
							<input type="hidden" name="role" value="5">
							<button type="submit"  class="btn btn--purple btn--block">ЗАРЕГИСТРИРОВАТЬСЯ</button>
							{{ csrf_field() }}

							<p class="register__agreement-note text-center">
								Регистрируясь на портале Osto.uz,<br>
								я соглашаюсь <span class="font-medium">с пользовательским соглашением<br>
								и политикой конфиденциальности</span>
							</p>
						{!! Form::close() !!}
					</div><!--/.register__tab-content-->
				</div><!--/.panel__body-->
			</div><!--/.register__panel-->
		</div>

		<div class="col-md-7" data-mh="mh1">
			<div class="register__content js-register-content is-active">
				<h2 class="register__title">
					Присоединяйтесь <br> к миллионам покупателей
				</h2>
				
				<div class="register__info-item">
					<div class="register__info-checkbox">
						<span class="icon-checkbox-tick"></span>
						Получите доступ ко всем товарам Узбекистана и <br>сравнивайте цены по всем направлениям
					</div>
				</div>

				<div class="register__info-item">
					<div class="register__info-checkbox">
						<span class="icon-checkbox-tick"></span>
						Удобный интерфейс и большое количество функций дадут <br> вам возможность с комфортом прогуляться по торговым точкам
					</div>
				</div>

				<div class="register__info-item">
					<div class="register__info-checkbox">
						<span class="icon-checkbox-tick"></span>
						Продуманная система рейтингов и модерации <br> сайта поможет Вам выбрать самый лучший товар который Вы ищете
					</div>
				</div>
			</div>

			<div class="register__content js-register-content ">
				<h2 class="register__title">
					Начните продавать <br> прямо сейчас
				</h2>
				
				<div class="register__info-item">
					<div class="register__info-checkbox">
						<span class="icon-checkbox-tick"></span>
						Создайте свой магазин,добавляйте неограниченное <br> количество товаров и продавайте
					</div>
				</div>

				<div class="register__info-item">
					<div class="register__info-checkbox">
						<span class="icon-checkbox-tick"></span>
						У нас можно продавать одежду, обувь и аксессуары <br> для всей семьи, а также текстиль и прочие товары для дома.
					</div>
				</div>

				<div class="register__info-item">
					<div class="register__info-checkbox">
						<span class="icon-checkbox-tick"></span>
						Покажите коллекцию своих товаров, привлекая <br> покупателей, увеличивайте объем продаж.
					</div>
				</div>

			</div>

		</div>

	</div>
</div>
<div class="modal modal-styled checkout fade" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
           
        </div>
    </div>
</div>
@stop

@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>
	<script src="{{ asset('assets/js/app.js') }}"></script>
	<script src="{{ asset('assets/js/jquery.mask.js') }}"></script>

	<script type="text/javascript">
	$('#phone').mask('+ (999) 99 999-9999', {'translation': {0: {pattern: /[0-9*]/}}});
	$('#phone2').mask('+ (999) 99 999-9999', {'translation': {0: {pattern: /[0-9*]/}}});





		// REGISTER TAB
		var register_content = $('.js-register-content');

		var register_tab = new App.Tab({
			tabs: $('.js-register-tab'),
			tabContents: $('.js-register-tab-content'),

			onChange: function(index){
				register_content.removeClass('is-active');
				register_content.eq(index).addClass('is-active');
			}
		});

		// AJAX USER REGISTER

		var user_form = $('.user-register');
		var user_btn = user_form.find('[type="submit"]');
		var user_input = user_form.find('input');
		var user_form_group = user_input.parent();

		user_form.submit(function(e){
			e.preventDefault();

			user_btn.attr('disabled', true);
			var user_data = $(this).serialize();

			$.ajax({

				type: 'POST',
				url: '{{ action('UserController@postUserRegister') }}',
				data: user_data,
				dataType: 'JSON',

				success:function(data){
					user_btn.attr('disabled', false);		 
					console.log(data);
					if(data.status == 1){
						getConfirmCode();

						// window.location.href = data.url;		
					}
					else{
						if(!user_form_group.hasClass('form-group--error')){
							$.each(data.errors, function(key, value){
			            	    $.each(value, function(j, error) {
			            			var input = $('[name='+key+']');

			            			input.after('<p class="form-group__label form-group__label--error">' 
				            						+ error + 
				            			   		'</p>');

			            			input.parent().addClass('form-group--error');
				            	});
				            });
						}
						
					}
				},
			});
		});


		// AJAX SELLER REGISTER
		var seller_form = $('.seller-register');
		var seller_btn = seller_form.find('[type="submit"]');
		var seller_input = seller_form.find('input');
		var seller_form_group = seller_input.parent();

		seller_form.submit(function(e){
			e.preventDefault();

			seller_btn.attr('disabled', true);
			var seller_data = $(this).serialize();

			$.ajax({

				type: 'POST',
				url: '{{ action('UserController@postSellerRegister') }}',
				data: seller_data,
				dataType: 'JSON',

				success:function(data){
					seller_btn.attr('disabled', false);
					console.log(data);
					if(data.status==1){
						getConfirmCode();
						
						// window.location.href = data.url;		
					}
					else{
						if(!seller_form_group.hasClass('form-group--error')){
							$.each(data.errors, function(key, value){
			            		$.each(value, function(i, error) {
			            			var input = $('[name='+key+']');

			            			input.after('<p class="form-group__label form-group__label--error">' 
				            						+ error + 
				            			   		'</p>');

			            			input.parent().addClass('form-group--error');
				            	});
				            });
						}
						    
					}
				}
			});
		});

		$("body").on("click","#confirm_code", function(){
			checkCode($("#sms_code").val());
		});

		$("body").on("click","#resend_code", function(){
			resendCode();
		});

		function getConfirmCode(){
			$.ajax({
				url: "/form-confirm-code",

				success: function(response){
					console.log(response);
					$(".modal.checkout  .modal-content").html(response);
							$(".modal.checkout").addClass("active");

				}
			})
		}

		function resendCode(){
			$.ajax({
				url: "/resend-code",
				method: "POST",
				beforeSend: function (request) {
		        	return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
		    	},
				success: function(response){
					console.log(response);
					var type = "success";
					if(response.status == 0){
						type =  "warning";
					}

					swal({
						type: type,
						text: response.msg
					});
				}
			})
		}

		function checkCode(code){
			if(!code){
				swal("Введите код");
			}else{
				$.ajax({
					url: "/check-code",
					method: "POST",
					data: {code: code},
					beforeSend: function (request) {
			        	return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
			    	},
					success: function(response){
						console.log(response);
						if(response.status == 1){

							$(location).attr("href", response.url);
						}else{
							swal(response.msg);
						}
					}

				})
			}
			
		}
	</script>

@stop
