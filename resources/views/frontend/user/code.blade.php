<div class="modal-body">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<div class="form-group">
		<h2 class="text-center">
			Подтвердить<br/> номер телефона
		</h2>
		<div class="modal-text">
			<p class="text-center">На ваш телефонный номер отправлен смс-код, подтвердить.</p>
		</div>
		<label for="sms_code">Введите код подтверждения</label>
		<input type="text" id="sms_code" class="input input--gray">
	</div>	
	<button id="confirm_code"  class="btn_new btn--purple btn-lg btn--block">Подтвердить</button>
	<button id="resend_code"  class="btn_new btn--default btn-link btn--block">Отправить код еще раз</button>
</div>	


