@extends('layouts.frontend')

@section('content')

<section class="bazars p-t-nav-sticked">
	<div class="cover" style="background-image: url({{ asset(Upload::getFile('malls', $dir->id)) }})">
		<div class="cover__text text-center">
			<h1>{{ $dir->title }}</h1>
			<p>Всего в списке - {{ $dirs->count() }}</p>
		</div>
	</div>
	<div class="p-y-layout">
		<div class="container">
			<div class="row">
				@foreach($dirs as $dir)
					<div class="col-xs-6 col-sm-6 col-lg-3">
						<article class="dir dir--purple dir--border">
							@if(Upload::hasFile('malls', $dir->id))
							<img src="{{ Upload::getThumbFiles('malls', $dir->id, 
							'400x370')[0] }}" alt="Direction img" class="dir__img">
							@else
							<img src="{{ asset('assets/img/noimage-mall.png') }}" alt="no-image" class="dir__img">
							@endif
							<a href="{{ action('PageController@getDir', $dir->alias) }}" class="dir__title">
								<span>
								  {{ $dir->title }}
								</span>
							</a>
						</article>
					</div>
				@endforeach
			</div>
			<div class="text-right">
				{!! $dirs->render() !!}
			</div>
		</div>
	</div><!--/.p-layout-->
</section>

@stop

@section('scripts')

@stop