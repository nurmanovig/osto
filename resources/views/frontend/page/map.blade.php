<style >
    #myMap {
        width: 100%;
        height: 100%;
    }
</style>
<link rel="stylesheet" href="/assets/leaflet/leaflet.css" />
<script src="/assets/leaflet/leaflet-src.js"></script>
<script src="/assets/leaflet/esri-leaflet.js"></script>
<script src="/assets/leaflet/esri-leaflet-geocoder.js"></script>
<!-- <script src="/assets/lefleat/leaflet.js"></script> -->

    <button type="button" class="close close_map" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

<div id="myMap"></div>
<div class="buttons">
    <input type="hidden" class="lat" value="">
    <input type="hidden" class="lng" value="">
    <button class="btn btn-default my_location" title="Мое местоположение"><img src="/assets/img/marker-general.png" width="18px"></button>
    <input class="btn btn-default address-info" value=""  placeholder="Укажите адрес доставки" disabled>
    <button class="btn btn-success address_save">Сохранить</button>
</div>
<script>

    var position = undefined;
    if(myMap){
        myMap.remove();
    }   
    var myMap;
    var markers = [];
    
    $(function(){

        var elem = {coords: {lat: $(".lat"), lng:  $(".lng")}, address: $(".address-info")};
        mapInit({addMarker: true, elem: elem, geocode: true});        

        $("body").on("click", ".my_location", function(e){
            var elem = { address: $(".address-info")};
            myLocation({addMarker: true, elem: elem, geocode: true });
        });

    });


    
    function mapInit(options = {}){
        navigator.geolocation.getCurrentPosition(function(pos) 
        {
            
            position = [
                pos.coords.latitude,
                 pos.coords.longitude
            ];
            accessToken = "pk.eyJ1IjoibnVybWFub3ZpZyIsImEiOiJjampoemg3YW4ycjVrM3ZudmQ1bWk0bjV5In0.ov5Y46Myp4-nugnYyHzHmw";

            myMap = L.map('myMap',{
                center: position,
                zoom: 15
            });

            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            // attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
             accessToken: accessToken
            }).addTo(myMap);

            if(options.addMarker){
                addMarker({coords: position});
            }   

            if(options.geocode){
                geocodeByLatLng({lat: position[0], lng: position[1]}, options.elem);
                eventListener(options.elem);

            }

            // addMarker();
        });
    }

    function myLocation(options){
        navigator.geolocation.getCurrentPosition(function(pos) 
        {
            
            position = [
                pos.coords.latitude,
                 pos.coords.longitude
            ];

            console.log(position);
            accessToken = "pk.eyJ1IjoibnVybWFub3ZpZyIsImEiOiJjampoemg3YW4ycjVrM3ZudmQ1bWk0bjV5In0.ov5Y46Myp4-nugnYyHzHmw";

            
            if(options.addMarker){
                addMarker({coords: position});
            }   

            if(options.geocode){
                geocodeByLatLng({lat: position[0], lng: position[1]}, options.elem);
                eventListener(options.elem);

            }

            // addMarker();
        });
    }


    function eventListener(elem = null){
        myMap.on("click", function(event){
            console.log(event.latlng);

            var info = {coords : [event.latlng.lat, event.latlng.lng]};
            addMarker(info);
            geocodeByLatLng(event.latlng, elem);
        })

    }



    function addMarker(info, clear = true){

        var options = {};

        if(clear){
            clearMarkers();
        }
        
        if(info.options){
            if(info.icon){
                var icon = L.icon({
                    iconUrl: info.icon,
                    iconSize: [38, 95], // size of the icon
                });
                options.icon = icon;
            }
        }

        var marker = L.marker(info.coords, options);
        
        if(info.popup){
            marker.bindPopup(info.popup);
        }
        
        marker.addTo(myMap);
        
        myMap.addLayer(marker);
        markers.push(marker);
    }

    function addMultiMarker(informations){
        for(x in informations){
            addMarker(informations[x]);
        }   
    }

    function clearMarkers(){
        console.log(markers);
        for(x in markers){
            myMap.removeLayer(markers[x]);

        }
        markers = [];
    }


    function geocodeByLatLng(coords, elem){

        if(elem.coords){
            elem.coords.lat.val(coords.lat);
            elem.coords.lng.val(coords.lng);
        }

        if(elem.address){
            var geocodeService = L.esri.Geocoding.geocodeService();
            geocodeService.reverse().latlng(coords).run(function(error, result)  {
                elem.address.val(result.address.Match_addr);    
            });
        }
    }


</script>