@extends('layouts.frontend')

@section('content')

<section class="p-t-nav-sticked ln_cover_sec">
	<div class="p-y-layout redactor-content">
		<div class="container">
			<h1 class="">{{ $page->title }}</h1>
			{!! $page->description !!}
		</div>
	</div><!--/.p-layout-->
</section>

@stop

@section('scripts')

@stop