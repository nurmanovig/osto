@extends('layouts.frontend')

@section('content')
	<!-- HEADER -->
	<header class="header">
		{{ Form::open(['action' => 'ProductController@getIndex', 'id' => 'search_form', 'method'=>'get']) }}
		{{ Form::close() }}
		<div class="container h-100">
			<div class="header__content centered-y text-center">
				<h1 class="header__title">ВСЕ ТОВАРЫ УЗБЕКИСТАНА В ОДНОМ МЕСТЕ</h1>
				<p class="p-y-15">Ищите среди более чем {{ $product_count }} товаров</p>
				<!-- Search & Button -->
				<div class="header__search big-o-search big-shade">
					<input type="text" name="keyword" placeholder="Введите название товара" class="big-o-search__input input input--white  js-header-autocomplete" form="search_form">
					<button href="#" class="big-o-search__btn header__search-btn icon-search-main" type="submit" form="search_form"></button>
				</div>
			</div>
		</div><!--/.container-->
	</header>

	<div class="container">

		<div class="cats text-center">
			<div class="row">
				@foreach($categories as $cat)
				<div class="cats__col">
					<a href="{{ action('ProductController@getCategory', $cat->alias) }}" class="cats__item" data-mh="cat_mh">
						<?= strip_tags($cat->icon, "<i><img></i>")?>
						<div class="cats__item-name \">
							{{ $cat->title }}
						</div>
					</a>
				</div>
				@endforeach

				<div class="cats__col">
					<a href="/product/discount" class="cats__item " data-mh="cat_mh">
						<svg viewBox="0 0 512 512" width="72" height="72">
								<path d="m418.12 118.82c-0.193-8.938-7.624-16.209-16.564-16.209h-77.679v-34.739c1e-3 -37.427-30.448-67.875-67.874-67.875s-67.875 30.448-67.875 67.875v34.739h-77.679c-8.939 0-16.37 7.271-16.564 16.208l-8.179 376.25c-0.098 4.493 1.579 8.735 4.721 11.947 3.143 3.212 7.349 4.981 11.842 4.981h307.46c4.494 0 8.7-1.769 11.842-4.981 3.143-3.212 4.82-7.456 4.721-11.947l-8.177-376.25zm-102.26 34.029c5.01 0 9.086 4.076 9.086 9.086s-4.076 9.086-9.086 9.086-9.086-4.076-9.086-9.086 4.076-9.086 9.086-9.086zm-119.72 0c5.01 0 9.086 4.076 9.086 9.086s-4.076 9.086-9.086 9.086-9.086-4.076-9.086-9.086 4.076-9.086 9.086-9.086zm213.97 342.95c-0.157 0.16-0.326 0.16-0.382 0.16h-307.47c-0.056 0-0.224 0-0.382-0.16s-0.154-0.329-0.153-0.385l8.179-376.25c6e-3 -0.289 0.246-0.523 0.534-0.523h77.679v19.49c-9.93 3.354-17.102 12.752-17.102 23.8 0 13.851 11.268 25.119 25.119 25.119s25.119-11.268 25.119-25.119c0-11.048-7.172-20.446-17.102-23.8v-19.491h77.494c4.427 0 8.017-3.589 8.017-8.017s-3.589-8.017-8.017-8.017h-77.494v-34.737c0-28.585 23.256-51.841 51.841-51.841s51.841 23.256 51.841 51.841v70.263c-9.93 3.354-17.102 12.752-17.102 23.8 0 13.851 11.268 25.119 25.119 25.119s25.119-11.268 25.119-25.119c0-11.048-7.172-20.446-17.102-23.8v-19.49h77.679c0.289 0 0.528 0.234 0.534 0.523l8.179 376.25c2e-3 0.055 6e-3 0.224-0.151 0.384z"/>
								<path d="m384.27 461.76h-17.105c-4.427 0-8.017 3.589-8.017 8.017 0 4.427 3.589 8.017 8.017 8.017h17.105c4.427 0 8.017-3.589 8.017-8.017s-3.589-8.017-8.017-8.017z"/>
								<path d="m332.96 461.76h-205.23c-4.427 0-8.017 3.589-8.017 8.017 0 4.427 3.589 8.017 8.017 8.017h205.23c4.427 0 8.017-3.589 8.017-8.017s-3.588-8.017-8.017-8.017z"/>
								<path d="m338.63 241.78c-3.131-3.131-8.207-3.131-11.337 0l-153.92 153.92c-3.131 3.13-3.131 8.207 0 11.337 1.565 1.565 3.617 2.348 5.668 2.348s4.103-0.782 5.668-2.348l153.92-153.92c3.131-3.131 3.131-8.207 1e-3 -11.336z"/>
								<path d="m204.69 230.88c-23.281 0-42.221 18.941-42.221 42.221s18.941 42.221 42.221 42.221 42.221-18.941 42.221-42.221-18.94-42.221-42.221-42.221zm0 68.409c-14.44 0-26.188-11.748-26.188-26.188s11.748-26.188 26.188-26.188 26.188 11.748 26.188 26.188-11.748 26.188-26.188 26.188z"/>
								<path d="m307.31 333.5c-23.281 0-42.221 18.941-42.221 42.221 0 23.281 18.941 42.221 42.221 42.221s42.221-18.941 42.221-42.221-18.941-42.221-42.221-42.221zm0 68.409c-14.44 0-26.188-11.748-26.188-26.188s11.748-26.188 26.188-26.188 26.188 11.748 26.188 26.188-11.748 26.188-26.188 26.188z"/>
						</svg>


						<div class="cats__item-name">
							Товары со скидкой
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>



		<!-- AD BLOCK -->
		<section class="p-y-layout" style="padding-top:0 !important;">
			<div class="container">
				<!-- <div class="ad">
					Рекламный блок
				</div> -->
				@widget("banner-widget")
				{{--<a href="https://www.instagram.com/p/BrU0SfwARIC/" target="_blank"><img src="/assets/img/osto.jpg" class="img-responsive center-block" alt=""></a>--}}
			</div>
		</section>

		<!-- RECOMENDED SHOPS -->
		<section class="p-y-layout">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<h3 class="title title--dark title--layout">РЕКОМЕНДОВАННЫЕ МАГАЗИНЫ</h3>
					</div>

					<div class="col-sm-6 text-sm-right">
						<a href="{{ route('recommended_shops') }}" class="layout-link">Больше рекомендованных магазинов</a>
					</div>
				</div>

				<!-- Carousel items -->
				<div class="item-carousel2">
					@foreach($recommended_shops as $shop)
						@include('partials.shop')
					@endforeach
				</div>
			</div>
		</section>

		<!-- REOMENDED PRODUCTS -->
		<section class="p-b-layout">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<h3 class="title title--dark title--layout">РЕКОМЕНДОВАННЫЕ ТОВАРЫ</h3>
					</div>

					<div class="col-sm-6 text-sm-right">
						<a href="{{ action('ProductController@getRecommended') }}" class="layout-link">Больше рекомендованных товаров</a>
					</div>
				</div>

				<!-- Carousel items -->
				<div class="item-carousel">
					@foreach($recommended_product as $product)
						@include('partials.product')
					@endforeach
				</div>
			</div>
		</section>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){

		$('.js-multiple-select').select2({
			tags: false,
			multiple: "multiple",
			width: '100%'

		}).val(null).trigger("change");

		$('.js-single-select').select2({
			minimumResultsForSearch: Infinity,
			width: '100%'
		}).val(null).trigger("change");

		var data = [
			@foreach(App\Models\Product::orderBy('title', 'asc')->pluck('id','title') as $key=>$value)
					{ value:  '{{ $key }}', data: '{{ $value }}' },
			@endforeach
		]

		var auto_complete = $('.js-header-autocomplete');

		auto_complete.autocomplete({
		    lookup: data,
		    appendTo: auto_complete.parent(),
		    forceFixPosition: true,
		    zIndex: 5,
		    maxHeight: 250,
		});

		$('.js-scroll-down').click(function(){
			$('html, body').animate({
				scrollTop: $('.directions').offset().top
			}, 800);
		});
	})
</script>

@stop




