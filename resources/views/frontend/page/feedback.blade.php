@extends('layouts.frontend')

@section('styles')
<style>
	.g-recaptcha{ display: inline-block; }
</style>
@stop

@section('content')

<section class="feedback p-t-nav-sticked">
	<div class="p-y-layout">
		<div class="container">
	
			<h1 class="title title--dark title--layout">Обратная связь</h1>
			<div class="row">
				<div class="col-sm-7">
					
					<div class="feedback__col bg-white">
					@if(Session::has('success'))
						<h3>
							Ваш запрос отправлен, спасибо!<br/>
							Мы скоро ответим Вам!
						</h3>
					@else
						<p class="p-b-20">Оставьте сообщение, чтобы связаться с администрацией сайта</p>
						{!! Form::open(['action' => 'PageController@postFeedback','method' => 'post'], ['class' => 'm-t-20']) !!}
							<div class="row">
								<div class="col-sm-4">
									<label for="" class="input-label p-y-15">Выберите тему</label>
								</div>
								<div class="col-sm-8">
									<div class="select-container form-group {{ ($errors->has('feedback_type')) ? 'form-group--error' : '' }}">
										<select class="input input--gray" name="feedback_type">
										<option value="">---------------</option>
										@foreach($types as $type)
											<option value="{{ $type->id }}">
											{{ $type->{'title_'.App::getLocale()} }}
											</option>
										@endforeach	
                                        </select>
                                        @if($errors->has('feedback_type'))
											<div class="form-group__label form-group__label--error">{{ $errors->get('feedback_type')[0] }}</div>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<label for="" class="input-label p-y-15">Текст сообщения:</label>
								</div>
								<div class="col-sm-8">
									<div class="form-group {{ ($errors->has('text')) ? 'form-group--error' : '' }}">
										<textarea name="text" class="input input--gray" rows="4" type="text" value=""></textarea>
										@if($errors->has('text'))
											<div class="form-group__label form-group__label--error">{{ $errors->get('text')[0] }}</div>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<label for="" class="input-label p-y-15">Ваш e-mail:</label>
								</div>
								<div class="col-sm-8">
									<div class="form-group {{ ($errors->has('email')) ? 'form-group--error' : '' }}">
										<input name="email" class="input input--gray" type="email">
										@if($errors->has('email'))
											<div class="form-group__label form-group__label--error">{{ $errors->get('email')[0] }}</div>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<label for="" class="input-label p-y-15">Номер телефона:</label>
								</div>
								<div class="col-sm-8">
									<div class="form-group {{ ($errors->has('phone')) ? 'form-group--error' : '' }}">
										<input name="phone" class="input input--gray" type="text">
										@if($errors->has('phone'))
											<div class="form-group__label form-group__label--error">{{ $errors->get('phone')[0] }}</div>
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 text-right">
									<div class="m-b-20 {{ ($errors->has('g-recaptcha-response')) ? 'form-group--error' : '' }}">
									{!! Recaptcha::render() !!}
									</div>
									@if($errors->has('g-recaptcha-response'))
											<div class="form-group__label form-group__label--error">{{ $errors->get('g-recaptcha-response')[0] }}</div>
										@endif
									<button class="btn btn--purple">Отправить</button>
								</div>
							</div>
						{!! Form::close() !!}
						@endif
					</div>
				</div>

				<div class="col-sm-5">
					<div class="feedback__col bg-purple cl-white text-center">
						<img src="{{ asset('assets/img/feedback-img.svg') }}" class="feedback__col-img" alt="feedback-letter">
						<div class="m-t-20">
							<strong class="font-bold">Ваше сообщение не останется	без внимания наших специалистов</strong>
						</div>
						<div class="m-t-20 m-b-20">
							Пожалуйста, выберите категорию сообщения и постарайтесь максимально точно описать свой вопрос. Наиболее полная информация поможет нам оперативно отреагировать на Ваше обращение.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!--/.p-layout-->
</section>

@stop

@section('scripts')

@stop