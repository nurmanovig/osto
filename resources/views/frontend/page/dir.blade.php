@extends('layouts.frontend')

@section('content')

<section class="p-t-nav-sticked">
	<div class="p-y-layout bg-white">
		<div class="container">
			<div class="row">
				@include("widgets.breadcrumbs", ["pages" => $pages])
				<div class="col-sm-3">
					<img src="{{ (Upload::hasFiles('malls', $dir->id)) ? Upload::getThumbFiles('malls', $dir->id,'400x370')[0] : '/assets/img/noimage-mall.png' }}" alt="{{ $dir->title }}" class="img-responsive default-radius">
				</div>
				<div class="col-sm-9">

					<h1 class="ln_title_dir title title--layout">
						{{ $dir->title }}
					</h1>
					<p>
						Колличество магазинов - <span class="font-bold">{{ $dir->shops()->count() }}</span>
					</p>
					<p class="m-t-20">
						{!! $dir->description !!}
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="p-y-layout">
		<div class="container">

			<div class="row p-b-layout">
				<div class="col-sm-12">
					{{ Form::open(['id' => 'search_form', 'method'=>'get']) }}
						<div class="big-o-search big-o-search--rounded search__big-o small-shade">
							
							<input type="text" name="q" form="search_form" placeholder="Введите название товара" value="{{ Request::get('q') }}" class="big-o-search__input input input--white js-search-autocomplete">
							
							<button href="#" class="search__btn big-o-search__btn icon-search-main" type="submit" form="search_form"></button>
						
						</div>
					{!! Form::close() !!}

				</div>
			</div>
			<div class="row">
				@foreach($shops as $shop)
				<div class="col-sm-4 col-lg-3 p-x-0 m-b-20">
					@include('partials.shop')
				</div>
				@endforeach
			</div>
			<div class="text-right">
				<?php $params = Request::all()?>
				{!! $shops->appends($params)->render() !!}
			</div>
		</div>
	</div><!--/.p-layout-->
</section>

@stop

@section('scripts')

@stop