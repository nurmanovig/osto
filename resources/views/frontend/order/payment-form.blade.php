<?php
use App\Models\Product;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
</div>
<div class="modal-body" style="padding:30px 45px;">
	
	<div class="check-content">
		<h2>Заказ </h2>
		<ol class="check-content-list-dotted check-content-list-numeral">
		@foreach($products as $product)
			<li>
				<div class="pull-left">{{$product['title']}} ( {{Product::getSizeInfoStatic($product['_size'])}} )</div>
				<div class="pull-right">{{$product['_price']}} {{ $currency }}</div>
			</li>
		@endforeach
		</ol>

		<hr>

		<ul class="check-content-list-dotted">
			<li><div class="pull-left"><strong>Сумма заказа</strong></div>  <div class="pull-right">{{$product_summ}} {{ $currency }}</div></li>
			<li><div class="pull-left"><strong>Скидка</strong></div> <div class="pull-right">{{ $sale_summ }} {{ $currency }}</div></li>
			<li><div class="pull-left"><strong>Стоимость доставки</strong></div> <div class="pull-right">{{ ($delivery_price > 0) ? $delivery_price ." UZS" : "Бесплатная доставка" }}</div></li>
			<li><div class="pull-left"><strong>Итого</strong></div> <div class="pull-right">{{ $summ }} {{ $currency }}</div></li>
		</ul>
{{ Form::open(array('url'=>'/checkout', 'id' => 'chekcout-last-form')) }}
		<div class="form-group">
			<textarea cols="30" rows="3" name="comments" placeholder="Комментарии к заказу ..."></textarea>
		</div>

		<p>
			<strong>Оплата:</strong>
		</p>

		<!-- <div class="payments">		
			<a href="#"  data-type="cash" class="pay_method fleft tdnone box br5 marginbott20 marginleft20 tcenter small lheight14  selected">
				<img src="/assets/img/payment-cash.png" alt="">
			</a>

			<a href="#"  data-type="click" class="pay_method fleft tdnone box br5 marginbott20 marginleft20 tcenter small lheight14  selected">
				<img src="/assets/img/click.jpg" alt="">
			</a>

			<a href="#"  data-type="payme" class="pay_method fleft tdnone box br5 marginbott20 marginleft20 tcenter small lheight14  selected">
				<img src="/assets/img/payme.png" alt="">
			</a>

			<a href="#"  data-type="paynet" class="pay_method fleft tdnone box br5 marginbott20 marginleft20 tcenter small lheight14 selected">
				<img src="/assets/img/paynet.jpg" alt="">
			</a>
		</div> -->

		<div class="form-group">
			<p>
				<label class="sk-checkbox">
					<input type="radio" name="pay_type" value="1">
					<span class="sk-checkbox-icon"></span>
					<span class="sk-checkbox-text">Оплата наличными</span>
				</label>
			</p>
			<!--<p>-->
			<!--	<label class="sk-checkbox">-->
			<!--		<input type="radio" name="pay_type" value="2">-->
			<!--		<span class="sk-checkbox-icon"></span>-->
			<!--		<span class="sk-checkbox-text">Оплата картой</span>-->
			<!--	</label>-->
			<!--</p>-->
		</div>

		<p><strong>Кому :</strong> {{$order_data['username']}}</p>
		<p><strong>Телефон :</strong> {{ $order_data['phone'] }}</p>
		<p><strong>Адрес :</strong> {{ $order_data['address'] }}</p> 

		
		<div class="text-center" style="margin-top: 30px;">
			<button id="checkout" class="btn btn-wide btn--purple">Оформить заказ</button>
		</div>
{{ Form::close() }}
	</div>

	
</div>	