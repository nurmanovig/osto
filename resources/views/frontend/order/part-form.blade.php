<?php
use App\Models\Product;
?>
<h1 class="cart-header">Корзина</h1>
@if(empty($products))
    <div class="inner-container">

        <div class="empty-bsk-block-wrap">
            <div class="empty-bsk-block">
                <div class="empty-bsk-img" >
                    <div style=" background-image: url(/assets/img/cart.png) "></div>
                </div>
                <p>Корзина пуста</p>
            </div>
        </div>
    </div>
@else
    <div class="table-responsive cart_info">
        <table class="table table-condensed" style="min-width: 1100px;">
            <thead>
                <tr class="cart_menu">
                    <td class="image">Товар</td>
                    <td class="description">Описание</td>
                    <td>Размер</td>
                    <td>Магазин</td>
                    
                    <td class="quantity">Количество</td>
                    <td class="quantity">Скидка ( UZS )</td>
                    <td class="total">Общая сумма ( UZS )</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                    <tr data-id="{{$product['id']}}">
                        <td class="cart_product">
                            <a href="/product/view/{{$product['alias']}}">
                                <img src="{{ (Upload::hasFiles('product',$product['id'])) ? Upload::getThumbMainProductFile('product',$product['id'],'250x375')[0] : '/assets/img/noimage.png'}}" alt="Shop image">
                            </a>
                               
                        </td>
                        <td class="cart_description">
                            <h4>
                                <a href="/product/view/{{$product['alias']}}">
                                    {{substr($product['title'], 0, 70)}} 
                                </a>
                                
                            </h4>
                        </td>
                        
                        <td><small class="size" data-size="{{$product['_size']}}">{{Product::getSizeInfoStatic($product['_size'])}}</small></td>
                        <td>
                        @if(!empty($product['shop']))
                            {{$product['shop']['title']}}
                        @else
                            Магазин  недоступен
                        @endif
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <a class="cart_quantity_up" href=""> - </a>
                                <input class="cart_quantity_input " type="text" disabled value="{{$product['_count']}}" autocomplete="off" size="2">
                                <a class="cart_quantity_down" href=""> + </a>
                            </div>
                        </td>

                        <td class="cart_price">
                            <p>{{$product['_saleprice']}}</p>
                        </td>
                        

                        <td class="cart_total">
                           
                            <p class="cart_total_price">{{ $product['_countprice'] }}</p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href="">x</a>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>

    <div class="form-group col-md-6 col-md-offset-3">
        <div style="margin-bottom: 40px;" class="promo-code">
            <input class="input input--gray code" type="text" placeholder="Промо код">
            <button class="btn btn--purple send-promo" type="submit">Подтвердить</button>
        </div>

        <div class="form-group">
            <p><strong>Итого без доставки:</strong></p>
            <p class="clearfix"><span class="pull-left">Общая сумма заказа</span> <span class="pull-right">{{$summ_str}}</span></p>
        </div>

        <div class="form-checkout">
            <div class="text-center col-md-6 col-md-offset-3">
                <input class="btn btn--purple checkout-open " type="submit" value="Оформить заказ">
            </div>
        </div>
    </div>
@endif