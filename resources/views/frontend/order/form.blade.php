@extends('layouts.frontend')

@section('content')
<section class="p-t-nav-sticked container cart-section">
    @include("frontend.order.part-form")
</section>



<div class="modal checkout fade" >
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="width: 650px;background-color:#f2f2f2;">
        </div>
    </div>
</div>
@stop