@extends('layouts.frontend')

@section('content')

    <link rel="stylesheet" href="/assets/leaflet/leaflet.css" />
    <script src="/assets/leaflet/leaflet-src.js"></script>

    <div class="lnodej_container">
    <div class="lnodej_all">
        <div class="lnodej_all_leftright">
            <?php $fullname = "/uploads/thumb/shop/200x200/".$shop->id."/".$shop->main_image;?>
            <img src="{{ Upload::hasImage($fullname) ? : '/assets/img/noimage-shop.png'}}" alt="Shop image" class="img-responsive default-radius small-shade ln_center_flex">

            <br/>
            <h1 class="title m-t-20">{{ $shop->title }}</h1>
            <p class="m-t-20">
                Колличество товаров - <span class="font-bold">{{ $shop->products->count() }}</span>
            </p>

            <div class="btn-contact small-shade default-radius clearfix m-t-20">
                <div class="btn-contact__phone js-phone">
                    {{ $shop->phone_format }}
                </div>

                <button class="btn-contact__phone-expander js-phone-expander">Показать</button>
            </div>
            <div class="m-t-20">
                <span class="font-bold">Адрес:</span><br/>
                {{ $shop->address }}
            </div>


            <!-- ========== MAP ========== -->
            <div class="p-y-15">
                <a href="" class="cl-purple">Посмотреть на карте <i class="icon-angle-down"></i></a>
            </div>
            <div id="map_shop" class="js-map default-radius small-shade shop__map" data-lat="{{ $shop->lat }}" data-lng="{{ $shop->lng }}"></div>
            <!-- ========== MAP ========== -->

            <div class="m-t-20">
                <span class="font-bold">Режим работы: {{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</span>

                <div class="row">
                    @if($shop->day1)
                        <div class="col-xs-6">понедельник</div>
                        <div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
                    @else
                        <div class="col-xs-6">понедельник</div>
                        <div class="col-xs-6">Выходной</div>
                    @endif
                </div>

                <div class="row">
                    @if($shop->day2)
                        <div class="col-xs-6">вторник</div>
                        <div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
                    @else
                        <div class="col-xs-6">вторник</div>
                        <div class="col-xs-6">Выходной</div>
                    @endif
                </div>

                <div class="row">
                    @if($shop->day3)
                        <div class="col-xs-6">среда</div>
                        <div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
                    @else
                        <div class="col-xs-6">среда</div>
                        <div class="col-xs-6">Выходной</div>
                    @endif
                </div>

                <div class="row">
                    @if($shop->day4)
                        <div class="col-xs-6">четверг</div>
                        <div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
                    @else
                        <div class="col-xs-6">четверг</div>
                        <div class="col-xs-6">Выходной</div>
                    @endif
                </div>

                <div class="row">
                    @if($shop->day5)
                        <div class="col-xs-6">пятница</div>
                        <div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
                    @else
                        <div class="col-xs-6">пятница</div>
                        <div class="col-xs-6">Выходной</div>
                    @endif
                </div>

                <div class="row">
                    @if($shop->day6)
                        <div class="col-xs-6">суббота</div>
                        <div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
                    @else
                        <div class="col-xs-6">суббота</div>
                        <div class="col-xs-6">Выходной</div>
                    @endif
                </div>

                <div class="row">
                    @if($shop->day7)
                        <div class="col-xs-6">воскресенье</div>
                        <div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
                    @else
                        <div class="col-xs-6">воскресенье</div>
                        <div class="col-xs-6">Выходной</div>
                    @endif
                </div>
            </div>
        </div>
        <div class="lnodej_all_filter">
            <div class="lnodej_all_filter_lefttt">
                <h4>Tavari</h4>
            </div>
            @include('partials.products_new')
        </div>
        <div class="lnodej_all_leftright lnodej_none">
            right
        </div>
    </div>
</div>
@stop



@section('scripts')
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
    <script src="{{ asset('assets/js/product-mode-changer.js') }}"></script>
    <script>
        var map_shop,
                marker = null;
        mapInit();
        function mapInit(options = {}){
            position = [
                $("#map_shop").data("lat"),
                $("#map_shop").data("lng"),
            ];

            console.log(position);

            accessToken = "pk.eyJ1IjoibnVybWFub3ZpZyIsImEiOiJjampoemg3YW4ycjVrM3ZudmQ1bWk0bjV5In0.ov5Y46Myp4-nugnYyHzHmw";
            map_shop = L.map('map_shop',{
                center: position,
                zoom: 15
            });

            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                // attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
                accessToken: accessToken
            }).addTo(map_shop);

            addMarker({coords: position});

        }

        function addMarker(info, clear = true){

            var options = {};


            if(info.options){
                if(info.icon){
                    var icon = L.icon({
                        iconUrl: info.icon,
                        iconSize: [38, 95], // size of the icon
                    });
                    options.icon = icon;
                }
            }

            var marker = L.marker(info.coords, options);

            if(info.popup){
                marker.bindPopup(info.popup);
            }

            marker.addTo(map_shop);

            map_shop.addLayer(marker);
        }


        $('.js-phone-expander').click(function(){
            var button = $(this);

            $.ajax({
                url : "/shop/number/" + {{ $shop->id }},
                type : 'GET',
                dataType: 'json',

                success: function(data){
                    if(data !== 0){
                        button.hide();
                        $('.js-phone').html('+ ' + data.phone);
                    }
                }
            });
        });

        // Product Mode changer
        var prod_mod_changer = new ProductModeChanger({
            url: '',
            data:  '',
            btnGrid: $('.js-btn-grid'),
            btnList: $('.js-btn-list'),
            resultContainer: $('.js-products-ajax-container'),
        });
    </script>

@stop