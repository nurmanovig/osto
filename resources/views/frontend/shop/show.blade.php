@extends('layouts.frontend')

@section('content')

<link rel="stylesheet" href="/assets/leaflet/leaflet.css" />
<script src="/assets/leaflet/leaflet-src.js"></script>


<section class="shop p-t-nav-sticked">
	<div class="p-y-layout">
		<div class="container">
			<div class="row">
				@include("widgets.breadcrumbs", ["pages" => $pages])
				<!-- Sidebar -->
				<div class="col-md-3">
					<?php $fullname = "/uploads/thumb/shop/200x200/".$shop->id."/".$shop->main_image;?>
						<img src="{{ Upload::hasImage($fullname) ? : '/assets/img/noimage-shop.png'}}" alt="Shop image" class="img-responsive default-radius small-shade ln_center_flex">
					
					<br/>
					<h1 class="title m-t-20">{{ $shop->title }}</h1>
					<p class="m-t-20">
						Колличество товаров - <span class="font-bold">{{ $shop->products->count() }}</span>
					</p>

					<div class="btn-contact small-shade default-radius clearfix m-t-20">
						<div class="btn-contact__phone js-phone">
							{{ $shop->phone_format }}
						</div>

						<button class="btn-contact__phone-expander js-phone-expander">Показать</button>
					</div>
					<div class="m-t-20">
						<span class="font-bold">Адрес:</span><br/>
						{{ $shop->address }}
					</div>
					
					
                    <!-- ========== MAP ========== -->
					<div class="p-y-15">
						<a href="" class="cl-purple">Посмотреть на карте <i class="icon-angle-down"></i></a>
					</div>
					<div id="map_shop" class="js-map default-radius small-shade shop__map" data-lat="{{ $shop->lat }}" data-lng="{{ $shop->lng }}"></div>
                    <!-- ========== MAP ========== -->

					<div class="m-t-20">
						<span class="font-bold">Режим работы: {{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</span>

						<div class="row">
							@if($shop->day1)
							<div class="col-xs-6">понедельник</div>
							<div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
							@else
							<div class="col-xs-6">понедельник</div>
							<div class="col-xs-6">Выходной</div>
							@endif
						</div>
						
						<div class="row">
							@if($shop->day2)
							<div class="col-xs-6">вторник</div>
							<div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
							@else
							<div class="col-xs-6">вторник</div>
							<div class="col-xs-6">Выходной</div>
							@endif
						</div>

						<div class="row">
							@if($shop->day3)
							<div class="col-xs-6">среда</div>
							<div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
							@else
							<div class="col-xs-6">среда</div>
							<div class="col-xs-6">Выходной</div>
							@endif
						</div>

						<div class="row">
							@if($shop->day4)
							<div class="col-xs-6">четверг</div>
							<div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
							@else
							<div class="col-xs-6">четверг</div>
							<div class="col-xs-6">Выходной</div>
							@endif
						</div>

						<div class="row">
							@if($shop->day5)
							<div class="col-xs-6">пятница</div>
							<div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
							@else
							<div class="col-xs-6">пятница</div>
							<div class="col-xs-6">Выходной</div>
							@endif
						</div>

						<div class="row">
							@if($shop->day6)
							<div class="col-xs-6">суббота</div>
							<div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
							@else
							<div class="col-xs-6">суббота</div>
							<div class="col-xs-6">Выходной</div>
							@endif
						</div>

						<div class="row">
							@if($shop->day7)
							<div class="col-xs-6">воскресенье</div>
							<div class="col-xs-6">{{ Carbon\Carbon::parse($shop->working_hour_start)->format('H:i') }} - {{ Carbon\Carbon::parse($shop->working_hour_end)->format('H:i')}}</div>
							@else
							<div class="col-xs-6">воскресенье</div>
							<div class="col-xs-6">Выходной</div>
							@endif
						</div>		   
					</div>
				</div>

				<!-- Right content main -->
				<div class="col-md-9">
					
					<div class="row">
						<div class="col-sm-6">
							<h1 class="title title--layout">Товары</h1>
						</div>
						{{-- <div class="col-sm-6 text-right">
							<button class="btn btn--purple btn-square icon-grid m-l-10 js-btn-grid" data-active="{{ (Session::get('product-view-mode')=='grid') ? 'true' : 'false'  }}"></button>
								
							<button class="btn btn--gray btn-square icon-list js-btn-list" data-active="{{ (Session::get('product-view-mode')=='list') ? 'true' : 'false'  }}"></button>
						</div> --}}
					</div>

					<!-- Results all products -->
					<div class="row js-products-ajax-container">
						@forelse($products as $product)
						<div class="col-xs-6 col-sm-4 col-md-3 m-b-5">
							<article class="card m-x-0 m-b-20">
								<div class="card__viewport">
									@if($product->sale_status)
							<span class="sale_product">{{ $product->sale_value }} %</span>
						@endif
									<a class="card__img-cont ln_card__img-cont_2" href="/product/view/{{ $product->alias }}">
										<div class="card__img-cover">
											<i class="icon-eye centered"></i>

											<button class="likes likes--white card__likes">
												<i class="icon-like"></i>
												<span class="likes__amount">{{ $product->likes()->count() }}</span>
											</button>
										</div>

										<img src="{{ (!Upload::hasFiles('product',$product->id)) ? '/assets/img/noimage.png' : Upload::getThumbMainProductFile('product',$product->id,'250x375')[0] }}" alt="Shop image">
									</a> 

									<div class="card__content">
										<a class="card__title" href="#">
											{{-- <strong>
												{{ $product->category->title }}, 
											</strong> --}}
											{{ $product->title }}
										</a>
										
										@if($product->sale_value )
							<div class="card__purpl actual_price">{{ $product->price_format }} {{ Config::get('site.currency')[$product->currency] }}</div>

							<div class="card__purple-text">{{ salePrice($product->price, $product->sale_value) }} {{ Config::get('site.currency')[$product->currency] }}</div>

						@else
							<div class="card__purple-text">{{ $product->price_format }} {{ Config::get('site.currency')[$product->currency] }}</div>

						@endif
									</div>

									</a>
								</div>
							</article>
						</div>
					@empty	
						<div class="col-sm-12">
							<div class="bg-white cl-gray p-y-layout text-center default-radius small-shade">
								<img src="{{ asset('assets/img/icon-bag.svg') }}" alt="" height="100" class="m-b-20"><br/>
								В этом магазине еще нет товаров
							</div>
						</div>
					</div>	
					@endforelse
				    <div class="clearfix"></div>
					<!-- PAGINATION -->
					<div class="text-right">
					    <?php $params = Request::all() ?>
						{{ $products->appends($params)->links() }}
					</div>

				</div>
			</div>
		</div>
	</div><!--/.p-layout-->
</section>
@stop

@section('scripts')
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="{{ asset('assets/js/product-mode-changer.js') }}"></script>
<script>
	var map_shop, 
		marker = null;
	mapInit();
	function mapInit(options = {}){
            position = [
                $("#map_shop").data("lat"),
                $("#map_shop").data("lng"),
            ];

            console.log(position);

            accessToken = "pk.eyJ1IjoibnVybWFub3ZpZyIsImEiOiJjampoemg3YW4ycjVrM3ZudmQ1bWk0bjV5In0.ov5Y46Myp4-nugnYyHzHmw";
            map_shop = L.map('map_shop',{
                center: position,
                zoom: 15
            });

            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            // attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
             accessToken: accessToken
            }).addTo(map_shop);

            addMarker({coords: position});

    }

	function addMarker(info, clear = true){

        var options = {};

        
        if(info.options){
            if(info.icon){
                var icon = L.icon({
                    iconUrl: info.icon,
                    iconSize: [38, 95], // size of the icon
                });
                options.icon = icon;
            }
        }

        var marker = L.marker(info.coords, options);
        
        if(info.popup){
            marker.bindPopup(info.popup);
        }
        
        marker.addTo(map_shop);
        
        map_shop.addLayer(marker);
    }


  	$('.js-phone-expander').click(function(){
		var button = $(this);
	
		$.ajax({
			url : "/shop/number/" + {{ $shop->id }},
			type : 'GET',
			dataType: 'json',

			success: function(data){
				if(data !== 0){
					button.hide();
					$('.js-phone').html('+ ' + data.phone);
				}
			}
		});
	});

  	// Product Mode changer
	var prod_mod_changer = new ProductModeChanger({
		url: '',
		data:  '',
		btnGrid: $('.js-btn-grid'),
		btnList: $('.js-btn-list'),
		resultContainer: $('.js-products-ajax-container'),
	});
</script>

@stop