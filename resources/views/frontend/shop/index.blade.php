@extends('layouts.frontend')

@section('content')

<section class="p-t-nav-sticked">
	<div class="cover" style="background-image: url({{ $page['cover'] }})">
		<div class="cover__text text-center">
			<h1>{{ $page['title'] }}</h1>
		</div>
	</div>
	<div class="p-y-layout">
		<div class="container">
			<div class="row p-b-layout">
				<div class="col-sm-12">
					{{ Form::open(['action' => 'ProductController@getIndex', 'id' => 'search_form', 'method'=>'get']) }}
						<div class="big-o-search big-o-search--rounded search__big-o small-shade">
							
							<input type="text" name="keyword" form="search_form" placeholder="Введите название товара" value="{{ Request::get('q') }}" class="big-o-search__input input input--white js-search-autocomplete">
							
							<button href="#" class="search__btn big-o-search__btn icon-search-main" type="submit" form="search_form"></button>
						
						</div>
					{!! Form::close() !!}
				</div>
			</div>

			<div class="row">
				@foreach($shops as $shop)
				<div class="col-xs-6 col-sm-3 col-lg-2 p-x-0 m-b-20">
					@include('partials.shop')
				</div>
				@endforeach
			</div>
			<div class="text-right">
				{!! $shops->render() !!}
			</div>
		</div>
	</div><!--/.p-layout-->
</section>

@stop

@section('scripts')
<script>
	var autocomplete_data = [
		@foreach(App\Models\Product::orderBy('title', 'asc')->pluck('id','title') as $key=>$value)
				{ value:  '{{ $key }}', data: '{{ $value }}' },
		@endforeach
	]

	var auto_complete = $('.js-search-autocomplete');

	auto_complete.autocomplete({
		lookup: autocomplete_data,
	    appendTo: auto_complete.parent(),
	    forceFixPosition: true,
	    zIndex: 5,
	    maxHeight: 250,
	});
</script>
@stop