@extends('layouts.frontend')

@section('content')
<div class="lnodej_container">
    <div class="lnodej_all">
        <div class="lnodej_all_leftright">

        </div>
        <div class="lnodej_all_filter">
            <div class="lnodej_all_filter_lefttt">
                <h4>{{ $title }}</h4>
                <div class="lnodej_toplinkc">
                    @include('widgets.breadcrumbs', ['pages' => $pages])
                </div>
            </div>
            <div class="lnodej_all_filter_rughtas">
                <h6>Сортировать по</h6>
                <div class="right_nl_select">

                    <select type="text" onchange="this.form.submit()" form="search_form" name="sort_by" class="input js-select " data-theme="white" data-search="0">
                        <?php $sort = getSortItems() ; ?>

                        @foreach($sort as $k=>$v)
                        <option value="{{$k}}" {{ (Request::get('sort_by') == $k ? "selected='selected'" : '') }}>{{$v}}</option>
                        @endforeach
                    </select>
                    <span class="right_nl_select_downicon"></span>
                </div>
            </div>
        </div>
        <div class="lnodej_all_leftright lnodej_none">
            right
        </div>
    </div>
</div>
<!-- main items -->
<div class="lnodej_container">
    <div class="lnodej_all">
        <div class="lnodej_all_leftright">
            {{ Form::open(['action' => 'ProductController@getIndex', 'id' => 'search_form', 'method'=>'get']) }}

            <input type="hidden" name="category" value="{{isset($category_alias) ? $category_alias: null}}">

            <input type="hidden" name="sale" value="{{(Request::get('sale') == 1) ? 1: 0}}">
            {{ Form::close() }}
            <div class="js-search-sidebar-parent">

                <form method="get" id="filterMainForm">
                    <div class="sk-filter">

                        @widget("category-list-search", [["category_id" => $category_id, 'all_category' => isset($all_category) ? $all_category : false, 'sale' => !isset($sale) ? : $sale ]])

                        <div class="form-group opened">
                            <div class="form-group-title">ЦЕНА <i class="dropdown__icon icon-angle-down"></i></div>
                            <div class="form-group-collapse">
                                <div id="filterPriceList" class="sk-checkbox-group">

                                    <p>
                                        <label class="sk-checkbox">
                                            <input type="radio" form="search_form"
                                                   name="price"
                                                   <?php if (isset($_GET['price']) && $_GET['price']  == '0-99000') echo "checked" ?>
                                                   value="0-99000">

                                            <span class="sk-checkbox-icon"></span>
                                            <span class="sk-checkbox-text">0 - 49 000 UZS</span>
                                        </label>
                                    </p>
                                    <p>
                                        <label class="sk-checkbox">
                                            <input type="radio" form="search_form"
                                                   name="price"
                                                   <?php if (isset($_GET['price']) && $_GET['price'] == '0-99000') echo "checked" ?>
                                                   value="0-99000">

                                            <span class="sk-checkbox-icon"></span>
                                            <span class="sk-checkbox-text">50 000 - 99 000 UZS</span>
                                        </label>
                                    </p>
                                    <p>
                                        <label class="sk-checkbox">
                                            <input type="radio" form="search_form"
                                                   name="price"
                                                   <?php if (isset($_GET['price']) && $_GET['price'] == '99000-149000') echo "checked" ?>
                                                   value="99000-149000">

                                            <span class="sk-checkbox-icon"></span>
                                            <span class="sk-checkbox-text">100 000 - 149 000 UZS</span>
                                        </label>
                                    </p>

                                    <p>
                                        <label class="sk-checkbox">
                                            <input type="radio" form="search_form"
                                                   name="price"
                                                   <?php if (isset($_GET['price']) && $_GET['price'] == '149000-199000') echo "checked" ?>
                                                   value="149000-199000">

                                            <span class="sk-checkbox-icon"></span>
                                            <span class="sk-checkbox-text">150 000 - 199 000 UZS</span>
                                        </label>
                                    </p>

                                    <p>
                                        <label class="sk-checkbox">
                                            <input type="radio" form="search_form"
                                                   name="price"
                                                   <?php if (isset($_GET['price']) && $_GET['price'] == '200000-299000') echo "checked" ?>
                                                   value="200000-299000">

                                            <span class="sk-checkbox-icon"></span>
                                            <span class="sk-checkbox-text">200 000 - 299 000 UZS</span>
                                        </label>
                                    </p>

                                    <p>
                                        <label class="sk-checkbox">
                                            <input type="radio" form="search_form"
                                                   name="price"
                                                   <?php if (isset($_GET['price']) && $_GET['price'] == '300000-499000') echo "checked" ?>
                                                   value="300000-499000">

                                            <span class="sk-checkbox-icon"></span>
                                            <span class="sk-checkbox-text">300 000 - 499 000 UZS</span>
                                        </label>
                                    </p>

                                    <p>
                                        <label class="sk-checkbox">
                                            <input type="radio" form="search_form"
                                                   name="price"
                                                   <?php if (isset($_GET['price']) && $_GET['price'] == '500000-999000') echo "checked" ?>
                                                   value="500000-999000">

                                            <span class="sk-checkbox-icon"></span>
                                            <span class="sk-checkbox-text">500 000 - 999 000 UZS</span>
                                        </label>
                                    </p>

                                    <p>
                                        <label class="sk-checkbox">
                                            <input type="radio" form="search_form"
                                                   name="price"
                                                   <?php if (isset($_GET['price']) && $_GET['price'] == '1000000-100000000') echo "checked" ?>
                                                   value="1000000-100000000">

                                            <span class="sk-checkbox-icon"></span>
                                            <span class="sk-checkbox-text">1 000 000 UZS +</span>
                                        </label>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group opened">
                            <div class="form-group-title">Цвет <i class="dropdown__icon icon-angle-down"></i></div>
                            <div class="form-group-collapse">
                                <div class="sk-checkbox-group two-columns">
                                    <?php $i = 0;
                                    $colors = isset($_GET['colors']) ? $_GET['colors'] : [];
                                    foreach (config('app.colors') as $color) { ?>
                                    <p>
                                        <label class="sk-checkbox">
                                            <input type="checkbox" form="search_form" <?= in_array($color, $colors) ? "checked" : "" ?> name="colors[<?=$i?>]" value="<?=$color?>">
                                            <span class="sk-checkbox-icon"></span>
                                            <span class="sk-checkbox-text"><?=$color?></span>
                                        </label>
                                    </p>
                                    <?php $i++; } ?>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn--green btn--block" form="search_form" type="submit">Применить фильтр</button>
                    </div>

                </form>
            </div>
        </div>
        <div class="lnodej_all_filter">
            <div class="lnodej_all_items">

                @include('partials.products_new')
            </div>
        </div>
        <div class="lnodej_all_leftright lnodej_none">
            right
        </div>
    </div>
</div>

@stop

@section('scripts')
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="{{ asset('assets/js/product-mode-changer.js') }}"></script>
<script type="text/javascript">

    $(document).ready(function() {
        var form = $('#filterMainForm');
        var url = window.location.pathname;
        // $(document).on('change', '#filterMainForm input', function(e) {
        //     var data = form.serialize();
        //
        //     $( ".col-sm-8.col-md-8.col-lg-9" ).load(url+"?"+data, function() {
        //         alert( "Load was performed.");
        //     });
        //
        // });
    });

</script>

@stop