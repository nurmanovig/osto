@extends('layouts.frontend2')

@section("style")


@endsection
@section('content')
    <link rel="stylesheet" href="/assets/leaflet/leaflet.css" />
    <script src="/assets/leaflet/leaflet-src.js"></script>

    <section class="product-inner p-t-nav-sticked">
        <div class="p-y-layout">
            <div class="container">
                @include('widgets.breadcrumbs', ['pages' => $pages])
                <div class="zoomklay_all_left_nl_all">


                    <div class="zoomklay_all_left">
                        <div id="ln_loader">
                            <img src="/assets/img/preloader.gif">
                        </div>
                        <?php
                        $images = Upload::getFiles('product', $product->id);
                        ?>
                        <div class="litleslider_zoom none_slider_imgs">
                            @forelse($images as $image)
                                <div class="litleslider_zoom_item">
                                    <div class="litleslider_zoom_itemin">
                                        <img src="{{ asset($image) }}">
                                    </div>
                                </div>
                            @empty
                                <div class="litleslider_zoom_item">
                                    <div class="litleslider_zoom_itemin">
                                        <img src="{{ asset('assets/img/noimage.png') }}">
                                    </div>
                                </div>

                            @endforelse
                        </div>

                        @if(Auth::check())
                            <a href="#" class="product-inner__favourite js-favourite" data-id="{{ $product->id }}" title="В избранные">
                                <i class="icon-star"></i>
                                <span class="js-favourite-text">{{ $favourite ? 'Убрать' : 'Добавить в избранное' }}</span>
                            </a>
                        @endif
                        <div class="bigzoom none_slider_imgs">

                            @forelse($images as $image)

                                <div>
                                    <a data-fancybox="gallery"  data-sub-html=".caption"   href="{{ asset($image) }}" class="portfolio_1item">
                                        <img src="{{ asset($image) }}">
                                    </a>
                                </div>
                            @empty
                                <div>
                                    <a data-fancybox="gallery"  data-sub-html=".caption"   href="{{ asset($image) }}" class="portfolio_1item">
                                        <img src="{{ asset('assets/img/noimage.png') }}">
                                    </a>
                                </div>

                            @endforelse

                        </div>
                    </div>


                    <div class="right_nl">
                        <div class="right_nl_top">
                            <h4 class="right_nl_top_h4">{{ $product->title }}</h4>
                            <h6 class="right_nl_top_h6">
                                @if($product->sale_value )
                                    <div class="sk-product-page-price ln_sale_one"> {{ $product->price_format }} {{ Config::get('site.currency')[$product->currency] }} </div>

                                    <div class="sk-product-page-price ln_price_remove_margin"> {{ salePrice($product->price, $product->sale_value) }}  {{ Config::get('site.currency')[$product->currency] }} </div>

                                @else
                                    <div class="sk-product-page-price ln_price_remove_margin">
                                       {{ $product->price_format }}  {{ Config::get('site.currency')[$product->currency] }}
                                    </div>

                                @endif</h6>
                            <p class="right_nl_top_p">Звездный бомбер, стильно сочетающийся как и с джинсами так и с платьями крохи </p>
                            <h5 class="right_nl_top_h5">color: <span>white</span></h5>
                            <h5 class="right_nl_top_h5">size: <span><a href="https://osto.uz/page/tablitsa-razmerov" target="_blank" class="link-underline">таблица размеров</a></span></h5>
                        </div>
                        <div class="right_nl_select_btn">
                            @if(!empty($product->size_product))
                                <div class="right_nl_select">
                                    <select class="child" id="size_select" style="border:1px solid #6b37a8; height:40px;padding-top: 8px;padding-bottom: 8px;font-size:14px;">
                                        <option value="0">Выберите размер</option>
                                        @foreach($product->size_product as $size)

                                            <option value="{{$size['id']}}">{{$size['key']}}</option>
                                        @endforeach
                                    </select>
                                    <span class="right_nl_select_downicon"></span>
                                </div>


                                <div class="modal modal-styled fade" id="size-modal">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content" style="width: 370px;">

                                            <div class="modal-body">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <div class="form-group">
                                                    <h2 class="text-center">
                                                        Вы не выбрали размер
                                                    </h2>
                                                    <div style="margin:30px 0;" class="text-center">
                                                        <label style="display:inline-block;margin-right:10px;">Размер</label>
                                                        <div class="select-container" style="display:inline-block;width:150px;">
                                                            <select class="input input--white child" style="border:2px solid #f3f3f3; height:40px;padding-top: 10px;padding-bottom: 10px;">
                                                                @foreach($product->size_product as $size)
                                                                    <option value="{{$size['id']}}">{{$size['key']}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-center">
                                                    <div>
                                                        <button class="btn_new btn--purple btn-lg" id="sk-buy-btn">
                                                            Добавить в корзину
                                                        </button>
                                                    </div>
                                                    <div style="margin-top:10px;">
                                                        <button class="btn_new btn--default btn-link close" data-dismiss="modal">
                                                            Продолжить покупку
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <a data-sizes="{{json_encode($product->size_product)}}" data-id="{!! $product->id!!}" class="right_nl_bntsa add-cart">Добавить в корзину</a>

                            <div class="right_nl_sot">
                                <script async src="https://usocial.pro/usocial/usocial.js?v=6.1.4" data-script="usocial" charset="utf-8"></script>
                                <div class="uSocial-Share" data-pid="ff216c4278c9e8c06c2883c93dd208e4" data-type="share" data-options="round,style1,default,absolute,horizontal,size48,counter0" data-social="telegram,fb" data-mobile="vi,wa,sms"></div>
                                <!-- /uSocial -->

                                <div class="likes {{ $user_like ? 'is-liked' : 'is-disliked' }}">
                                    @if(Auth::check())
                                        <a href="#" data-id="{{ $product->id }}" class="btn-outlinll m-r-5 js-like" title="Like">
                                            <svg viewBox="0 0 471.701 471.701" width="16" height="16">
                                                <path d="m433.6 67.001c-24.7-24.7-57.4-38.2-92.3-38.2s-67.7 13.6-92.4 38.3l-12.9 12.9-13.1-13.1c-24.7-24.7-57.6-38.4-92.5-38.4-34.8 0-67.6 13.6-92.2 38.2-24.7 24.7-38.3 57.5-38.2 92.4 0 34.9 13.7 67.6 38.4 92.3l187.8 187.8c2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-3.9l188.2-187.5c24.7-24.7 38.3-57.5 38.3-92.4 0.1-34.9-13.4-67.7-38.1-92.4zm-19.2 165.7l-178.7 178-178.3-178.3c-19.6-19.6-30.4-45.6-30.4-73.3s10.7-53.7 30.3-73.2c19.5-19.5 45.5-30.3 73.1-30.3 27.7 0 53.8 10.8 73.4 30.4l22.6 22.6c5.3 5.3 13.8 5.3 19.1 0l22.4-22.4c19.6-19.6 45.7-30.4 73.3-30.4s53.6 10.8 73.2 30.3c19.6 19.6 30.3 45.6 30.3 73.3 0.1 27.7-10.7 53.7-30.3 73.3z"/>
                                            </svg>
                                        </a>
                                    @else
                                        <a href="#" class="btn-outline btn-rounded btn-outline--purple btn-square m-r-5 js-user-btn">
                                            <svg viewBox="0 0 471.701 471.701" width="16" height="16">
                                                <path d="m433.6 67.001c-24.7-24.7-57.4-38.2-92.3-38.2s-67.7 13.6-92.4 38.3l-12.9 12.9-13.1-13.1c-24.7-24.7-57.6-38.4-92.5-38.4-34.8 0-67.6 13.6-92.2 38.2-24.7 24.7-38.3 57.5-38.2 92.4 0 34.9 13.7 67.6 38.4 92.3l187.8 187.8c2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-3.9l188.2-187.5c24.7-24.7 38.3-57.5 38.3-92.4 0.1-34.9-13.4-67.7-38.1-92.4zm-19.2 165.7l-178.7 178-178.3-178.3c-19.6-19.6-30.4-45.6-30.4-73.3s10.7-53.7 30.3-73.2c19.5-19.5 45.5-30.3 73.1-30.3 27.7 0 53.8 10.8 73.4 30.4l22.6 22.6c5.3 5.3 13.8 5.3 19.1 0l22.4-22.4c19.6-19.6 45.7-30.4 73.3-30.4s53.6 10.8 73.2 30.3c19.6 19.6 30.3 45.6 30.3 73.3 0.1 27.7-10.7 53.7-30.3 73.3z"/>
                                            </svg>
                                        </a>
                                    @endif

                                </div>

                                <span class="cl-gray like-count">{{ $product->likes()->count() }}</span>

                            </div>
                            <div class="right_nl_magazin">
                                <div class="right_nl_magazin_left">
                                    <h6 class="right_nl_magazin_h6">Магазин:</h6>
                                    <div class="right_nl_magazin_img">
                                        <?php $fullname = "/uploads/thumb/shop/200x200/".$product->shop->id."/".$product->shop->main_image;?>
                                        <img src="{{ Upload::hasImage($fullname) ? : '/assets/img/noimage-shop.png'}}" alt="Shop image" class="">

                                    </div>
                                </div>
                                <div class="right_nl_magazin_right">
                                    <h6 class="right_nl_magazin_h6">{{$product->shop->mall->title }}</h6>
                                    <p class="right_nl_magazin_h6pp">Режим работы: {{ Carbon\Carbon::parse($product->shop->working_hour_start)->format('H:i') }}-{{ Carbon\Carbon::parse($product->shop->working_hour_end)->format('H:i') }}</p>
                                    <p class="right_nl_magazin_h6pp">
                                        @if($product->shop->workingDays()['work'])
                                            {{ implode(',',$product->shop->workingDays()['work']) }}
                                        @endif
                                    </p>
                                    <div class="right_nl_magazin_tel">
                                        <div class="text-center">
                                            <div class="phone-button">
											<span class="js-phone">
												{{ $product->shop->phone_format }}
											</span>
                                                <small class="js-phone-expander link-underline">Показать</small>
                                            </div>
                                        </div></div>
                                    <a href="/shop/view/{{ $product->shop->alias }}" class="right_nl_magazin_tel">Перейти в магазин</a>
                                </div>

                            </div>

                        </div>
            </div>
                    </div>
                </div>
            <div class="p-y-layout p-t-0">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="product-inner__tabs">
                                <li class="product-inner__tab js-tab is-active" data-index="0">Условия доставки</li>
                                <li class="product-inner__tab js-tab" data-index="1">Карта магазина</li>
                                <li class="product-inner__tab js-tab product-inner__tab--last" data-index="2">Отзывы
                                    ({{ $product->comments->count() }})</li>
                            </ul>

                            <div class="product-inner__tab-contents">

                                <div class="product-inner__tab-content js-tab-content is-active">
                                    <div class="product-inner__description" style="max-width: 800px">
                                        Доставка будет занимать от 1 до 3 суток после подтверждения заказа. <br>
                                        Мы доставляем товар по городу Ташкент и в остальные регионы Узбекистана. Для получения заказанного товара, Вам необходимо ответить на 2 звонка. <br>
                                        1-звонок: В течении одного часа наш оператор свяжется с Вами от 9:00 – 18:00 или на следующий день если товар был заказан после 18:00, в выходные дни или в официальный нерабочий день. Мы сообщаем Вам дату и время доставки. <br>
                                        2-звонок: За час до доставки оператор повторно звонит вам чтобы уточнить адрес. В случае, если Вас не будет по указанному адресу во время доставки, мы меняем адрес во время разговора по телефону.
                                    </div>
                                </div>

                                <!-- Map -->
                                <div class="product-inner__tab-content js-tab-content">
                                    <input type="hidden" class="js-lat-input-shop" value="{{$product->shop->lat}}">
                                    <input type="hidden" class="js-lng-input-shop" value="{{$product->shop->lng}}">
                                    <div id="map_shop" class="product-inner__map small-shade">

                                    </div>
                                </div>

                                <!-- Reviews -->
                                <div class="product-inner__tab-content js-tab-content">
                                    @foreach($product->comments as $key=>$val)
                                        <div class="comment @if($key == $product->comments->count() - 1) comment--last @endif">
                                            <div class="comment__thumb" style="background-image: url({{ asset('assets/img/user-no-thumb.svg') }})"></div>

                                            <div class="comment__info">
                                                <div class="comment__title title">{{$val->user->name}}</div>
                                                <div class="comment__date">{{ $val->created_at }}</div>
                                                <div class="comment__text">{{$val->text}}</div>
                                            </div>
                                        </div>
                                    @endforeach

                                    <div class="title product-inner__review-title">ДОБАВИТЬ ОТЗЫВ</div>

                                    @if(Auth::check())

                                        {{ Form::open(['method' => 'post', 'action' => 'CommentController@postComment',
                                        'class' => 'js-comment-form']) }}

                                        <input type="hidden" name="product_id" value="{{ $product->id }}">

                                        <div class="form-group">
                                            <textarea class="product-inner__review-input input input--white small-shade" placeholder="Ваш отзыв" name="text"></textarea>
                                        </div>

                                        <div class="text-right">
                                            <button type="submit" class="btn btn--purple js-comment-btn">Оставить отзыв</button>
                                        </div>
                                </div>
                                {{ Form::close() }}
                                @else
                                    <p>Только авторизованные пользователи могут оставить отзыв. </p><br />
                                    <a class="btn btn--green js-user-btn">Авторизация</a>
                                @endif
                            </div><!--/.product-inner__tab-contents-->
                        </div><!--/.col-xs-9-->
                    </div><!--/.row-->
                </div><!--/.container-->
            </div><!--/.p-layout-->
        </div>

    </section>

    <section class="p-y-layout bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h3 class="title title--dark title--layout">ПОХОЖИЕ ТОВАРЫ</h3>
                </div>
                <div class="col-sm-6 text-sm-right">
                    <a href="/product/popular" class="layout-link">Больше популярных товаров</a>
                </div>
            </div>

            <!-- Carousel items -->
            <div class="item-carousel">
                @foreach($products as $product)
                    @include('partials.product')
                @endforeach
            </div>
        </div>
    </section>

@stop
@section('scripts')

    <script src="{{ asset('/assets/js/inner-product/bootstrap.min.js')}}"></script>
    {{--<script src="{{ asset('/assets/js/inner-product/slick.min.js')}}"></script>--}}
    <script src="{{ asset('/assets/js/inner-product/wow.min.js')}}"></script>
    <!-- <script> new WOW().init();</script> -->
    <script src="{{ asset('/assets/js/inner-product/jquery.fancybox.js')}}"></script>
    <script src="{{ asset('/assets/js/inner-product/common.js')}}"></script>
    <script type="text/javascript">
        setTimeout(function(){
            $(".none_slider_imgs").removeClass("none_slider_imgs");
            slider_product.init();
            $("#ln_loader").remove();
        }, 500);

        $('.js-phone-expander').click(function(){

            var button=$(this);

            $.ajax({
                url : "/shop/number/" + {{ $model->shop->id }},
                type : 'GET',
                dataType: 'json',

                success: function(data){
                    if(data !== 0){
                        button.hide();
                        $('.js-phone').html(data.phone);
                    }
                }
            });
        });

        (function(){
            /* LIKE SYSTEM */
            var count = $('.like-count');
            var like = $('.likes');
            var is_busy = false;

            $('.js-like').click(function(e) {
                e.preventDefault();

                if (!is_busy)
                {
                    is_busy = true;

                    $.ajax({
                        url : "/product/like/" + $(this).data('id'),
                        type: 'GET',
                        dataType: 'JSON',

                        success: function(data){
                            console.log(data);

                            is_busy = false;

                            if(data.error == 0){
                                like.addClass('is-liked');
                                var like_count = parseInt(count.html()) + 1;
                                count.html(like_count);

                            }
                            else if(data.error == 1){
                                like.removeClass('is-liked').addClass('is-disliked');
                                var like_count = parseInt(count.html()) - 1;
                                count.html(like_count);
                            }
                        }
                    });
                }
            });
        })();

        var map;
        var marker = null;

        var product_tabs = new App.Tab({
            tabs: $('.js-tab'),
            tabContents: $('.js-tab-content'),

            onChange: function(index){
                if (index == 1) {
                    mapInitByPosition("map_shop", [$(".js-lat-input-shop").val(), $(".js-lng-input-shop").val()], {}, true) ;
                }else{
                    destroyMap();
                }
            }
        });



        function destroyMap()
        {
            var parent = $('#map_shop').parent();
            $('#map_shop').remove();
            parent.append('<div id="map_shop" class="product-inner__map small-shade"></div>');

        }

        // Ajax comment

        var comment_form = $('.js-comment-form');
        var comment_btn = $('.js-comment-btn');
        var comment_input = comment_form.find('textarea');
        var comment_form_group = comment_input.parent();

        comment_input.keyup(function(){
            comment_form_group.removeClass('form-group--error');
            comment_form_group.find('.form-group__label--error').removeClass('form-group__label--error').remove();
        });

        comment_btn.on('click', function(e){
            e.preventDefault();
            console.log('Clicked');

            if (!comment_form_group.hasClass('form-group--error')) {
                var form_data = comment_form.serialize();

                comment_btn.attr('disabled', true).addClass('is-waiting');

                $.ajax({
                    url: '/comment/insert',
                    type: 'POST',
                    data: form_data,

                    success: function(data){
                        comment_btn.attr('disabled', false).removeClass('is-waiting');

                        if (data.success) {
                            comment_input.val('');

                            var alert = $(
                                    '<div class="alert alert--success form-group">' +
                                    '<button class="alert__dismisser"></button>' + data.success +
                                    '</div>'
                            ).hide();

                            $('.product-inner__review-title').after(alert);

                            alert.slideDown(200);

                            window.setTimeout(function(){
                                alert.slideUp(200, function(){
                                    alert.remove();
                                });
                            }, 5000);
                        }else{
                            if (!comment_form_group.hasClass('form-group--error')) {
                                $.each(data.errors.text, function(index, val){
                                    var error_text = $('<div class="form-group__label form-group__label--error">' + val + '</div>');

                                    comment_form_group.append(error_text).addClass('form-group--error');
                                });
                            }
                        }
                    },
                });
            }
        });

    </script>

    <script type="text/javascript" src="/assets/js/map.js"></script>

    <script>


    </script>
@stop