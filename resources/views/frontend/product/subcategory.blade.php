<div class="col-md-6 category_list" data-id="{{ $category_id }}">
	<div class="form-group">
		<label class="input-label">Выберите подкатегорию</label>
		<div class="select-container">
			<select class="input input--gray child " name="category_id">
				<option >
						Выберите подкатегорию
					</option>
				@foreach($category as $item)
					<option value="{{ $item->id }}">
						{{ $item->title_ru }}
					</option>
				@endforeach
				
			</select>
			
		</div>
	</div>
</div>