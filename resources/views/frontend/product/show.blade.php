@extends('layouts.frontend')

@section("style")
	<link rel="stylesheet" href="/assets/css/slick-lightbox.css">

	@endsection
@section('content')
<link rel="stylesheet" href="/assets/leaflet/leaflet.css" />
<script src="/assets/leaflet/leaflet-src.js"></script>
	<section class="product-inner p-t-nav-sticked">
		<div class="p-y-layout">
			<div class="container">
				<div class="row">
					<div class="sk-product-page">
					@include('widgets.breadcrumbs', ['pages' => $pages])
				
					<div class="row">
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-6">
									<?php
										$images = Upload::getFiles('product', $product->id);
									?>
									@if(Auth::check())
									<a href="#" class="product-inner__favourite js-favourite" data-id="{{ $product->id }}" title="В избранные">
										<i class="icon-star"></i>
										<span class="js-favourite-text">{{ $favourite ? 'Убрать' : 'Добавить в избранное' }}</span>
									</a>
									@endif
									<div class="product-inner__slider js-slider">
										@forelse($images as $key => $item)
											<div class="product-inner__slider-item">
												<a href="{{ asset($item) }}" data-fancybox="gallery" data-caption=""><img src="{{ asset($item) }}" alt="Product image" class="img-responsive"></a>
												@if($product->sale_status)
													<div class="sale_product">
														{{ $product->sale_value }} %
													</div>
												@endif
											</div>
										@empty
											<div class="product-inner__slider-item" style="background-image: url({{ asset('assets/img/noimage.png') }});"></div>
											<div class="product-inner__slider-item" style="background-image: url({{ asset('assets/img/noimage.png') }});"></div>
											<div class="product-inner__slider-item" style="background-image: url({{ asset('assets/img/noimage.png') }});"></div>
											<div class="product-inner__slider-item" style="background-image: url({{ asset('assets/img/noimage.png') }});"></div>
										@endforelse
									</div>
				
									<ul class="product-inner__slider-nav js-slider-nav">
										@forelse($images as $item)
											<li class="product-inner__slider-nav-item">
												<img src="{{ $item }}" alt="Product image" class="img-responsive">

											</li>
										@empty
											<li class="product-inner__slider-nav-item" style="background-image: url({{ asset('assets/img/noimage.png') }});"></li>
											<li class="product-inner__slider-nav-item" style="background-image: url({{ asset('assets/img/noimage.png') }});"></li>
											<li class="product-inner__slider-nav-item" style="background-image: url({{ asset('assets/img/noimage.png') }});"></li>
											<li class="product-inner__slider-nav-item" style="background-image: url({{ asset('assets/img/noimage.png') }});"></li>
										@endforelse
									</ul>
				
								</div>
								<div class="col-md-6">
									<div class="product-inner__info">
										<h3 class="title product-inner__info-title">
											{{ $product->title }}
										</h3>

										@if($product->sale_value )
											<div class="sk-product-page-price ln_sale_one"> {{ Config::get('site.currency')[$product->currency] }} {{ $product->price_format }}</div>

											<div class="sk-product-page-price ln_price_remove_margin"> {{ Config::get('site.currency')[$product->currency] }} {{ salePrice($product->price, $product->sale_value) }}</div>

										@else
											<div class="sk-product-page-price ln_price_remove_margin">
											{{ Config::get('site.currency')[$product->currency] }} 	{{ $product->price_format }} 
											</div>

										@endif

										<div class="sk-product-page-description ln_description_product">

											{!! $product->description !!}

										</div>


										
				
										{{-- <div class="likes {{ (!empty($user_like) && $user_like->status == '1') ? 'is-liked' : 'is-disliked' }} likes--default product-inner__info-likes" @if(Auth::check()) data-auth='true' @else data-auth='false' @endif>
											<span class="likes__item likes__item--first">
												@if(!Auth::check())
												<i class="icon-like js-user-btn"></i>
												@else
												<i class="icon-like"></i>
												@endif
												<span class="js-likes">{{ $product->likes->where('status','1')->count() }}</span>
				
											</span>
				
											<span class="likes__item">
												@if(!Auth::check())
												<i class="icon-dislike js-user-btn"></i>
												@else
												<i class="icon-dislike"></i>
												@endif
												<span class="js-dislikes">{{ $product->likes->where('status','0')->count() }}</span>
											</span>
										</div> --}}
				
										<div><span style="font-family: MullerMedium">Цвет:</span>  {{$model->color}} </div>

											@if(!empty($product->size_product))
												<div class="form-group m-t-20">
													<label class="input-label">Размеры:</label>

													<br>

													<div class="select-container" style="display:inline-block;width:232px;">
														<select class="input input--white child" id="size_select" style="border:1px solid #6b37a8; height:40px;padding-top: 8px;padding-bottom: 8px;font-size:14px;">
															<option value="0">Выберите размер</option>
															@foreach($product->size_product as $size)

															<option value="{{$size['id']}}">{{$size['key']}}</option>
															@endforeach
														</select>
													</div>

													<p style="margin:6px 0;">
														<small><a href="https://osto.uz/page/tablitsa-razmerov" target="_blank" class="link-underline">таблица размеров</a></small>
													</p>
												</div>

												<div class="modal modal-styled fade" id="size-modal">
													<div class="modal-dialog" role="document">
														<div class="modal-content" style="width: 370px;">

															<div class="modal-body">
																<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																<div class="form-group">
																	<h2 class="text-center">
																		Вы не выбрали размер
																	</h2>
																	<div style="margin:30px 0;" class="text-center">
																		<label style="display:inline-block;margin-right:10px;">Размер</label>
																		<div class="select-container" style="display:inline-block;width:150px;">
																			<select class="input input--white child" style="border:2px solid #f3f3f3; height:40px;padding-top: 10px;padding-bottom: 10px;">
																				@foreach($product->size_product as $size)
																				<option value="{{$size['id']}}">{{$size['key']}}</option>
																				@endforeach
																			</select>
																		</div>
																	</div>
																</div>
																<div class="text-center">
																	<div>
																		<button class="btn_new btn--purple btn-lg" id="sk-buy-btn">
																			Добавить в корзину
																		</button>
																	</div>
																	<div style="margin-top:10px;">
																		<button class="btn_new btn--default btn-link close" data-dismiss="modal">
																			Продолжить покупку
																		</button>
																	</div>
																</div>
															</div>




														</div>
													</div>
												</div>
											@endif


											<div class="text-center" style="float: left;margin-right: 10px;">
												<a data-sizes="{{json_encode($product->size_product)}}" data-id="{!! $product->id!!}" class="btn btn--purple add-cart">Добавить в корзину</a>
											</div>

											<div class="clearfix"></div>

											<div style="margin-top:15px;width:177px;text-align:center;">
												
												<!-- uSocial -->
											<script async src="https://usocial.pro/usocial/usocial.js?v=6.1.4" data-script="usocial" charset="utf-8"></script>
											<div class="uSocial-Share" data-pid="ff216c4278c9e8c06c2883c93dd208e4" data-type="share" data-options="round,style1,default,absolute,horizontal,size48,counter0" data-social="telegram,fb" data-mobile="vi,wa,sms"></div>
											<!-- /uSocial -->

											<div class="likes {{ $user_like ? 'is-liked' : 'is-disliked' }}">
												@if(Auth::check())
												<a href="#" data-id="{{ $product->id }}" class="btn-outline btn-rounded btn-outline--purple btn-rounded m-r-5 js-like" title="Like">
													<svg viewBox="0 0 471.701 471.701" width="16" height="16">
														<path d="m433.6 67.001c-24.7-24.7-57.4-38.2-92.3-38.2s-67.7 13.6-92.4 38.3l-12.9 12.9-13.1-13.1c-24.7-24.7-57.6-38.4-92.5-38.4-34.8 0-67.6 13.6-92.2 38.2-24.7 24.7-38.3 57.5-38.2 92.4 0 34.9 13.7 67.6 38.4 92.3l187.8 187.8c2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-3.9l188.2-187.5c24.7-24.7 38.3-57.5 38.3-92.4 0.1-34.9-13.4-67.7-38.1-92.4zm-19.2 165.7l-178.7 178-178.3-178.3c-19.6-19.6-30.4-45.6-30.4-73.3s10.7-53.7 30.3-73.2c19.5-19.5 45.5-30.3 73.1-30.3 27.7 0 53.8 10.8 73.4 30.4l22.6 22.6c5.3 5.3 13.8 5.3 19.1 0l22.4-22.4c19.6-19.6 45.7-30.4 73.3-30.4s53.6 10.8 73.2 30.3c19.6 19.6 30.3 45.6 30.3 73.3 0.1 27.7-10.7 53.7-30.3 73.3z"/>
													</svg>
												</a>
												@else
												  <a href="#" class="btn-outline btn-rounded btn-outline--purple btn-square m-r-5 js-user-btn">
												  	<svg viewBox="0 0 471.701 471.701" width="16" height="16">
														<path d="m433.6 67.001c-24.7-24.7-57.4-38.2-92.3-38.2s-67.7 13.6-92.4 38.3l-12.9 12.9-13.1-13.1c-24.7-24.7-57.6-38.4-92.5-38.4-34.8 0-67.6 13.6-92.2 38.2-24.7 24.7-38.3 57.5-38.2 92.4 0 34.9 13.7 67.6 38.4 92.3l187.8 187.8c2.6 2.6 6.1 4 9.5 4s6.9-1.3 9.5-3.9l188.2-187.5c24.7-24.7 38.3-57.5 38.3-92.4 0.1-34.9-13.4-67.7-38.1-92.4zm-19.2 165.7l-178.7 178-178.3-178.3c-19.6-19.6-30.4-45.6-30.4-73.3s10.7-53.7 30.3-73.2c19.5-19.5 45.5-30.3 73.1-30.3 27.7 0 53.8 10.8 73.4 30.4l22.6 22.6c5.3 5.3 13.8 5.3 19.1 0l22.4-22.4c19.6-19.6 45.7-30.4 73.3-30.4s53.6 10.8 73.2 30.3c19.6 19.6 30.3 45.6 30.3 73.3 0.1 27.7-10.7 53.7-30.3 73.3z"/>
													</svg>
												  </a>
												@endif

											</div>
				
											<span class="cl-gray like-count">{{ $product->likes()->count() }}</span>

											</div>


					
									</div><!--/.product-inner__info-->
								</div>
							</div>
						</div><!--/.col-md-9-->
						
						<div class="col-md-3">
							<div class="product-inner__sidebar">
								<div class="product-inner__shop-info small-shade default-radius clearfix">
									<?php $fullname = "/uploads/thumb/shop/200x200/".$product->shop->id."/".$product->shop->main_image;?>
									<img src="{{ Upload::hasImage($fullname) ? : '/assets/img/noimage-shop.png'}}" alt="Shop image" class="product-inner__shop-thumb img-responsive small-shade">
									
									
									<div class="product-inner__shop-title">
										{{ $product->shop->title }}<br>
										@if($product->shop->mall)
											<span class="font-light">{{$product->shop->mall->title }}</span>
										@else
				
										@endif
									</div>
									
									<div class="product-inner__shop-worktime">
										Режим работы: <span class="font-light">{{ Carbon\Carbon::parse($product->shop->working_hour_start)->format('H:i') }}-{{ Carbon\Carbon::parse($product->shop->working_hour_end)->format('H:i') }}</span> 
									</div>
				
									<div class="product-inner__shop-worktime-dates">
										<div class="p-b-5 font-light">
											@if($product->shop->workingDays()['work'])
											{{ implode(',',$product->shop->workingDays()['work']) }}
											@endif
										</div>
				
										<div>
											@if($product->shop->workingDays()['rest'])
											 Выходной:
											 <span class="font-light">{{ implode(',',$product->shop->workingDays()['rest']) }}</span>
											 @endif
										</div>
									</div>

									<div class="text-center">
										<div class="phone-button">
											<span class="js-phone">
												{{ $product->shop->phone_format }}
											</span>
											<small class="js-phone-expander link-underline">Показать</small>
										</div>
									</div>
									
									<div class="text-center">
										<a href="/shop/view/{{ $product->shop->alias }}" class="btn btn--purple">Перейти в магазин</a>
									</div>
									
								</div><!--/.product-inner__info-->
							</div>
						</div>
					</div><!--/.col-md-9-->
				</div>
				</div>
			</div>
		</div>
		
		<div class="p-y-layout p-t-0">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<ul class="product-inner__tabs">
							<li class="product-inner__tab js-tab is-active" data-index="0">Условия доставки</li>
							<li class="product-inner__tab js-tab" data-index="1">Карта магазина</li>
							<li class="product-inner__tab js-tab product-inner__tab--last" data-index="2">Отзывы
							({{ $product->comments->count() }})</li>
						</ul>

						<div class="product-inner__tab-contents">

							<div class="product-inner__tab-content js-tab-content is-active">
								<div class="product-inner__description" style="max-width: 800px">
                                    Доставка будет занимать от 1 до 3 суток после подтверждения заказа. <br>
                                    Мы доставляем товар по городу Ташкент и в остальные регионы Узбекистана. Для получения заказанного товара, Вам необходимо ответить на 2 звонка. <br>
                                    1-звонок: В течении одного часа наш оператор свяжется с Вами от 9:00 – 18:00 или на следующий день если товар был заказан после 18:00, в выходные дни или в официальный нерабочий день. Мы сообщаем Вам дату и время доставки. <br>
                                    2-звонок: За час до доставки оператор повторно звонит вам чтобы уточнить адрес. В случае, если Вас не будет по указанному адресу во время доставки, мы меняем адрес во время разговора по телефону.
                                </div>
							</div>

							<!-- Map -->
							<div class="product-inner__tab-content js-tab-content">
								<input type="hidden" class="js-lat-input-shop" value="{{$product->shop->lat}}">
								<input type="hidden" class="js-lng-input-shop" value="{{$product->shop->lng}}">
								<div id="map_shop" class="product-inner__map small-shade">
									
								</div>
							</div>
							
							<!-- Reviews -->
							<div class="product-inner__tab-content js-tab-content">
								@foreach($product->comments as $key=>$val)
									<div class="comment @if($key == $product->comments->count() - 1) comment--last @endif">
										<div class="comment__thumb" style="background-image: url({{ asset('assets/img/user-no-thumb.svg') }})"></div>

										<div class="comment__info">
											<div class="comment__title title">{{$val->user->name}}</div>
											<div class="comment__date">{{ $val->created_at }}</div>
											<div class="comment__text">{{$val->text}}</div>
										</div>
								</div>
								@endforeach

								<div class="title product-inner__review-title">ДОБАВИТЬ ОТЗЫВ</div>

								@if(Auth::check())

								{{ Form::open(['method' => 'post', 'action' => 'CommentController@postComment', 
								'class' => 'js-comment-form']) }}

								<input type="hidden" name="product_id" value="{{ $product->id }}">
								
								<div class="form-group">
									<textarea class="product-inner__review-input input input--white small-shade" placeholder="Ваш отзыв" name="text"></textarea>
								</div>
								
								<div class="text-right">
									<button type="submit" class="btn btn--purple js-comment-btn">Оставить отзыв</button>
								</div>
							</div>
							{{ Form::close() }}
							@else
							<p>Только авторизованные пользователи могут оставить отзыв. </p><br />
							<a class="btn btn--green js-user-btn">Авторизация</a>
							@endif
						</div><!--/.product-inner__tab-contents-->
					</div><!--/.col-xs-9-->
				</div><!--/.row-->
			</div><!--/.container-->
		</div><!--/.p-layout-->
	</section>

	<section class="p-y-layout bg-light-gray">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<h3 class="title title--dark title--layout">ПОХОЖИЕ ТОВАРЫ</h3>
				</div>
				<div class="col-sm-6 text-sm-right">
					<a href="/product/popular" class="layout-link">Больше популярных товаров</a>
				</div>
			</div>

			<!-- Carousel items -->
			<div class="item-carousel">
				@foreach($products as $product)
					@include('partials.product')
				@endforeach
			</div>
		</div>
	</section>
	
@stop
@section('scripts')
	<script type="text/javascript" src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/assets/js/slick-lightbox.min.js') }}"></script>
	<script type="text/javascript">
		


			$('[data-fancybox]').fancybox({

				baseTpl	: 
						'<div class="fancybox-container" role="dialog" tabindex="-1">' +
							'<div class="fancybox-bg"></div>' +
							'<button class="fancybox-dismisser js-fancybox-dismisser">&times;</button>' + 
							'<div class="fancybox-controls">' +
								'<div class="fancybox-infobar js-fancybox-infobar">' +
									'<button data-fancybox-previous class="fancybox-arrow fancybox-arrow--prev arrow arrow--light arrow--md arrow--prev" title="Previous"></button>' +
									'<button data-fancybox-next class="fancybox-arrow fancybox-arrow--next arrow arrow--light arrow--md arrow--next" title="Next"></button>' +
								'</div>' +
							'</div>' +
							'<div class="fancybox-slider-wrap">' +
								'<div class="fancybox-slider"></div>' +
							'</div>' +
							'<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' +
						'</div>',

				beforeMove: resizeControls,
				afterLoad: resizeControls,
				buttons: {} 
			});

			function resizeControls(a){
				var new_width = $(a.current.$image).width();
				$('.js-fancybox-infobar').animate({
					width: new_width + ($('.fancybox-arrow').width()) + 'px'
				}, 500);
			}		

			$('body').on('click', '.js-fancybox-dismisser', function(){
				$.fancybox.close( 'all' );
			});
		
			$('.js-phone-expander').click(function(){
				
				var button=$(this);
			
				$.ajax({
					url : "/shop/number/" + {{ $model->shop->id }},
					type : 'GET',
					dataType: 'json',

					success: function(data){
						if(data !== 0){
							button.hide();
							$('.js-phone').html(data.phone);
						}
					}
				});
			});

			(function(){
				/* LIKE SYSTEM */
				var count = $('.like-count');
				var like = $('.likes');
				var is_busy = false;

				$('.js-like').click(function(e) {
					e.preventDefault();

					if (!is_busy) 
					{
						is_busy = true;

						$.ajax({
							url : "/product/like/" + $(this).data('id'),
							type: 'GET',
							dataType: 'JSON',

							success: function(data){
								console.log(data);

								is_busy = false;

								if(data.error == 0){
									like.addClass('is-liked');
									var like_count = parseInt(count.html()) + 1;
									count.html(like_count);							
									
								}
								else if(data.error == 1){
									like.removeClass('is-liked').addClass('is-disliked');
									var like_count = parseInt(count.html()) - 1;
									count.html(like_count);
								}
							}
						});
					}
				});	
			})();
		
	</script>
	<script type="text/javascript">
		var sliderNav = $('.js-slider-nav');
		var slider = $('.js-slider');
		$('.js-slider-nav').slickLightbox({
			src: 'src',
			itemSelector: '.product-inner__slider-nav-item'
		});

		sliderNav.slick({
			slidesToShow: 3,
			arrows: true,
			prevArrow: '<button class="product-inner__slider-arrow product-inner__slider-arrow--prev"><i class="icon-prev"></i></button>',
			nextArrow: '<button class="product-inner__slider-arrow product-inner__slider-arrow--next"><i class="icon-next"></i></button>',
			asNavFor: '.js-slider',
			focusOnSelect: true,
            variableWidth: true,

		});

		slider.slick({
			slidesToShow: 1,
			arrows: false,
			asNavFor: '.js-slider-nav'
		});

		$(window).on('resize load', function(){
			if ($(this).width() > screen.md) {
				slider.height("auto");
			}
		});


		var map;
        var marker = null;

		var product_tabs = new App.Tab({
			tabs: $('.js-tab'),
			tabContents: $('.js-tab-content'),

			onChange: function(index){
				if (index == 1) {
					mapInitByPosition("map_shop", [$(".js-lat-input-shop").val(), $(".js-lng-input-shop").val()], {}, true) ;
				}else{
                    destroyMap();
                }
			}
		});	



        function destroyMap()
        {
        	var parent = $('#map_shop').parent();
            $('#map_shop').remove();
            parent.append('<div id="map_shop" class="product-inner__map small-shade"></div>');

        }

      	// Ajax comment

      	var comment_form = $('.js-comment-form');
      	var comment_btn = $('.js-comment-btn');
      	var comment_input = comment_form.find('textarea');
      	var comment_form_group = comment_input.parent();

      	comment_input.keyup(function(){
      		comment_form_group.removeClass('form-group--error');
      		comment_form_group.find('.form-group__label--error').removeClass('form-group__label--error').remove();
      	});

      	comment_btn.on('click', function(e){
      		e.preventDefault();
      		console.log('Clicked');

      		if (!comment_form_group.hasClass('form-group--error')) {
      			var form_data = comment_form.serialize();

      			comment_btn.attr('disabled', true).addClass('is-waiting');

      			$.ajax({
	      			url: '/comment/insert',
	      			type: 'POST',
	      			data: form_data,

	      			success: function(data){	      		
	      				comment_btn.attr('disabled', false).removeClass('is-waiting');

	      				if (data.success) {
	      					comment_input.val('');

	      					var alert = $(
	      						'<div class="alert alert--success form-group">' +
	      							'<button class="alert__dismisser"></button>' + data.success + 
	      						'</div>'
	      					).hide();

	      					$('.product-inner__review-title').after(alert);

	      					alert.slideDown(200);

	      					window.setTimeout(function(){
	      						alert.slideUp(200, function(){
	      							alert.remove();
	      						});
	      					}, 5000);
	      				}else{
	      					if (!comment_form_group.hasClass('form-group--error')) {
	      						$.each(data.errors.text, function(index, val){
		      						var error_text = $('<div class="form-group__label form-group__label--error">' + val + '</div>');

			      					comment_form_group.append(error_text).addClass('form-group--error');
		      					});
	      					}
	      				}
	      			},
	      		});
      		}
      	});

	</script>

	<script type="text/javascript" src="/assets/js/map.js"></script>
    <script>

        
    </script>
@stop