@extends('layouts.frontend')

@section('content')
	<section class="p-t-nav-sticked">
		<div class="container p-y-layout">
				
			<h1 class="title title--layout">{{ strtoupper($title)}}</h1>
			@include("widgets.breadcrumbs", ["pages" => $pages])
			<div class="grid">
				@foreach($categories as $category)
					<div class="grid__item">
						<article class="category">
							<!-- 274x110 Img dimensions -->
							<?php $fullname = "/uploads/thumb/category/274x110/".$category->id."/".$category->main_image;?>
							<img src="{{ Upload::hasImage($fullname) ? : '/assets/img/temp/category_img.jpg'}}" alt="Shop image">
							
							<div class="category__content">
							<a href="{{ action('PageController@getCategory', $category->alias) }}">
							<div class="category__title">{{ $category->title }}</div>
							</a>
								<ul class="category__list">
								   @foreach(App\Models\Category::where('parent_id',$category->id)->get() as $subcategory)
										<li class="category__list-item">
											<a class="category__list-link" href="{{ action('PageController@getCategory', $subcategory->alias) }}">{{
												$subcategory->title }}</a>
										</li>
									@endforeach 
								</ul>
							</div>
						</article>
					</div><!--/.grid__item-->
				@endforeach
			</div>

		</div>
	</section>

@stop

@section('scripts')

<!-- 	<script type="text/javascript" src="{{ asset('assets/js/masonry.js') }}"></script> -->
	<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
	<script type="text/javascript">

		$(document).ready(function() {
			$('.grid').masonry({
			  itemSelector: '.grid__item',
			});
		});

	</script>

@stop