@extends('layouts.backend')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
          <div class="col-xs-6">
              Обратная связь
          </div>

        </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>E-mail</th>
                      <th>Тип</th>
                      <th>Текст</th>
                      <th width="50">Действия</th>
                  </tr>
              </thead>
                  <tbody>

                      
                    </tbody>       
                </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('backend/js/datatables.min.js')}}"></script>

<script type="text/javascript">
     
         $("#data-table").dataTable({ 

             "aProcessing": true,
             "aServerSide": true,
             "ajax": "{{ action('Backend\FeedbackController@getData') }}",

        columns: [
            {data: 'id', name: 'id'},
            {data: 'email', name: 'email'},
            {data: 'type.title_ru', name: 'type.title_ru'},
            {data: 'text', name: 'text'},
            {data: 'id', name: 'id'}
        ],

        columnDefs: [
            { 
              targets: 4,
              data: null, 
              searchable:false, 
              
              render: function(row,type,val,meta){
                   return '<a href="#" name="delete_item" id="' + val.id +'" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
              }
          },

        ],
           
       });

  </script>
@include('partials.backend-delete', ['action' => 'Backend\FeedbackController@postDelete'])
@stop