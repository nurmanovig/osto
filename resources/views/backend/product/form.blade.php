@extends('layouts.backend')

@section('styles')
    <link rel="stylesheet" href="{{ asset('backend/css/redactor.css') }}">
@stop

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ empty($product) ? 'Добавить' : 'Редактировать' }}
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
            {!! Form::open(['files' => true]) !!}
                <div class="form-group">
                    {!! Form::label('title', 'Название') !!}
                    {!! Form::text('title', empty($product) ? null : $product->title, ["class" => "form-control"]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Описание') !!}
                    {!! Form::text('description', empty($product) ? null : $product->description, ['class' => 'redactor']) !!}
                </div>
        
                <div class="form-group">
                    {!! Form::label('price', 'Цена') !!}
                    {!! Form::text('price', empty($product) ? null : $product->price, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('currency', 'Валюта') !!}
                    {!! Form::text('currency', 'Сум', ['class' => 'form-control', 
                        'disabled' => 'true']) !!}
                </div>
                
                <div class="form-group">
                    {!! Form::label('category', 'Категория') !!}
                    {!! Form::select('category_id', $categories, empty($product) ? null : $product->category_id, ['class'=> 'form-control',
                                'id' => 'category']) !!}
                </div>
                

                <div class="form-group">
                    {!! Form::label('shop', 'Магазин') !!}
                    {!! Form::select('shop_id', $shop, empty($product) ? null : $product->shop_id, ['class' => 'form-control']) !!}
                </div>

                
                <div class="form-group">
                    {!! Form::label('is_recommended', 'Рекомендованный') !!}
                    {!! Form::select('recommended', ['0'=>'Нет','1'=>'Да'], empty($product->recommended) ? null : $product->recommended ,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('is_popular', 'Популярный') !!}
                    {!! Form::select('popular', ['0'=>'Нет','1'=>'Да'], empty($product->popular) ? null : $product->popular ,['class'=>'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('is_active', 'Активный') !!}
                    {!! Form::select('status', ['0'=>'Нет','1'=>'Да'], empty($product->status) ?  null : $product->status,['class'=>'form-control']) !!}
                </div>

                @if(!empty($product) && Upload::hasFile("product", $product->id))
                    <div class="form-group">
                        <div class="row">
                        @foreach(Upload::getFiles("product", $product->id) as $key=>$image)
                          <div class="col-xs-6 col-md-3" id="img_{{$key}}">
                            <div class="thumbnail">
                              <img src="{{$image}}" alt="..." />
                            </div>
                            <p><a href="#" data-key="{{$key}}" class="btn btn-danger" role="button"><i class="fa fa-trash"></i></a></p>
                          </div>
                        @endforeach
                        </div>
                    </div>
                {!! Form::hidden('images_for_delete', null) !!}
                @endif
                
                <div class="form-group">
                    {!! Form::label('images', 'Изображение') !!}
                    <input type="file" multiple="multiple" name="images[]" accept="image/jpeg">
                </div>
                    
                <div class="form-group">
                    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                    <a href="{{ action('Backend\ProductController@getIndex') }}" class="btn btn-default">Отменить</a>
                </div>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
</div>
@stop

@section('scripts')

    <script type="text/javascript">
    var trash = [];
      $('.btn.btn-danger').click(function(){
        trash.push($(this).data('key'));
        $('#img_'+$(this).data('key')).remove();
        $('input[name=images_for_delete]').val(trash.join(','));
        return false;
      });

        tinymce.init({
            selector: '.redactor',
            height: 200,
            menubar: false,
            plugins:[
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
                  ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: '//www.tinymce.com/css/codepen.min.css',
            file_browser_callback : function(field_name, url, type, win) { 

                    // from http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript
                    var w = window,
                    d = document,
                    e = d.documentElement,
                    g = d.getElementsByTagName('body')[0],
                    x = w.innerWidth || e.clientWidth || g.clientWidth,
                    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

                var cmsURL = '/fm/index.html?&field_name='+field_name;

                if(type == 'image') {           
                    cmsURL = cmsURL + "&type=images";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });         

            }
        });
        $('#category').on('change', function(e){
            console.log(e);
            
            var cat_id = e.target.value;

            // ajax 
            
            $.get('mypanel/product/form/ajax-subcat?cat_id='+cat_id, function(data){
                    // success data
                    $('#subcategory').empty();
                    $.each(data, function(index,subcatObj){
                        $('#subcategory').append(
                        '<option value="'+subcatObj.id+'">'+subcatObj.title_ru+'</option>')
                    });
            });
        });
    </script>
@stop

