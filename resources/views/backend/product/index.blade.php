@extends('layouts.backend')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        Поиск
    </div>
    
    <div class="panel-body">
      <div class="row">
          {!! Form::open(['method' => 'GET', action('Backend\ProductController@getIndex')]) !!}
          <div class="col-sm-9">
            <div class="form-group">
              <input class="form-control" placeholder="Название" id="search" name="query" type="text">
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <button type="submit" class="btn btn-success btn-block">Найти</button>
            </div>
          </div>
          {!! Form::close() !!}
      </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
          <div class="col-xs-6">
            Продукты
          </div>
            <div class="col-xs-6 text-right">
                <a class="btn btn-success" href="{{action('Backend\ProductController@getForm')}}"> + Добавить</a>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Название</th>
                      <th>Цена</th>
                      <th>Категория</th>
                      <th>Рекоменд.</th>
                      <th>Популярный</th>
                      <th>Активный</th>
                      <th width="100"></th>
                  </tr>
              </thead>
                  <tbody>
                      @foreach($products as $product) 
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ str_limit($product->title, 100) }}</td>
                            <td>{{ $product->price }} {{ Config::get('site.currency')[$product->currency] }}</td>
                            <td>{{ ($product->category) ?  $product->category->title_ru : null }}</td>
                            <td>
                              <button data-current="{{$product->recommended}}" id="recommended-{{$product->id}}" onclick="changeData({{$product->id}},'recommended')" 
                                class="{{($product->recommended == 1) ? "btn btn-success": "btn btn-danger"}}"
                                >Статус</button>
                            </td>
                            <td>
                              <button data-current="{{$product->popular}}" id="popular-{{$product->id}}" onclick="changeData({{$product->id}},'popular')" 
                                class="{{($product->popular == 1) ? "btn btn-success": "btn btn-danger"}}"
                                >Статус</button>
                            </td>
                            <td>
                              <button data-current="{{$product->status}}" id="status-{{$product->id}}" onclick="changeData({{$product->id}},'status')" 
                                class="{{($product->status == 1) ? "btn btn-success": "btn btn-danger"}}"
                                >Статус</button>
                            </td>
                            <td>                              
                                <a href="{{ action('Backend\ProductController@getForm', [$product->id]) }}" class="btn btn-primary">
                                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                <a href="#" name="delete_item" id="{{$product->id}}" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger" data-action="/mypanel/product/delete">
                                  <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                      @endforeach
                  </tbody>       
              </table>
        </div>
        {!! $products->links() !!}
    </div>
</div>
@endsection

@section('scripts')
  @include('partials.backend-delete', ['action' => 'Backend\ProductController@postDelete'])
  <script src="{{asset('backend/js/datatables.min.js')}}"></script>
  <script type="text/javascript">
      function changeData(id,type){
        var inProgress = false;
        $.ajax({ url: '/mypanel/product/change/' + id + "/" + type,
                type: 'GET',
                success: function(data){
                    var changeColor = $('button#'+type+"-"+id);
                         if(changeColor.data('current') == 0){
                            changeColor.removeClass('btn-danger').addClass('btn-success').data('current',1);
                         }else{
                            changeColor.removeClass('btn-success').addClass('btn-danger').data('current',0);
                         }
                }
            });
      }   
  </script>

@stop