@extends('layouts.backend')

@section('styles')
    <link rel="stylesheet" href="{{ asset('backend/css/redactor.css') }}">
@stop

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ empty($product) ? 'Добавить' : 'Редактировать' }}
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    {!! Form::open(['files' => true]) !!}

                    @foreach($sort_keys as $key)
                        <div class="col-sm-12">
                            <div class="form-group col-sm-4">
                                {!! Form::label('title', 'Название') !!}
                                {!! Form::text('', $key, ["class" => "form-control", 'disabled']) !!}
                            </div>

                            <div class="form-group col-sm-4">
                                {!! Form::label('description', 'Описание') !!}
                                {!! Form::text($key,!isset($sort_items[$key]) ? null : $sort_items[$key], ['class' => 'form-control']) !!}
                            </div>

                        </div>
                    @endforeach

                    <div class="form-group">
                        {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                         </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

