@extends('layouts.backend')

@section('content')
<div class="row">
	<div class="col-sm-4">
   		<div class="box">
            <div class="box-icon bg-primary">
                <i class="fa fa-shopping-cart"></i>
            </div>
            	<div class="box-title">ПРОДУКТЫ</div>
        	    <div class="box-content">{{ $count['product'] }}</div>
    	</div>
	</div>

	<div class="col-sm-4">
   		<div class="box">
            <div class="box-icon bg-primary">
                <i class="fa fa-tags"></i>
            </div>
            	<div class="box-title">КАТЕГОРИИ</div>
        	    <div class="box-content">{{ $count['category'] }}</div>
    	</div>
	</div>


	<div class="col-sm-4">
   		<div class="box">
            <div class="box-icon bg-primary">
                <i class="fa fa-shopping-bag"></i>
            </div>
            	<div class="box-title">МАГАЗИНЫ</div>
        	    <div class="box-content">{{ $count['shop'] }}</div>
    	  </div>
	</div>

	<div class="col-sm-4">
   		<div class="box">
            <div class="box-icon bg-primary">
                <i class="fa fa-user-o"></i>
            </div>
            	<div class="box-title">ПОЛЬЗОВАТЕЛИ</div>
        	    <div class="box-content">{{ $count['user'] }}</div>
    	  </div>
	</div>

	<div class="col-sm-4">
   		<div class="box">
            <div class="box-icon bg-primary">
                <i class="fa fa-user"></i>
            </div>
            	<div class="box-title">ПРОДАВЦЫ</div>
        	    <div class="box-content">{{ $count['seller'] }}</div>
    	  </div>
	</div>



	<div class="col-sm-4">
   		<div class="box">
            <div class="box-icon bg-primary">
                <i class="fa fa-comment"></i>
            </div>
            	<div class="box-title">КОММЕНТАРИИ</div>
        	    <div class="box-content">{{ $count['comment'] }}</div>
    	  </div>
	</div>

	<div class="col-sm-4">
   		<div class="box">
            <div class="box-icon bg-primary">
                <i class="fa fa-question"></i>
            </div>
            	<div class="box-title">ОБРАТНАЯ СВЯЗЬ</div>
        	    <div class="box-content">{{ $count['feedback'] }}</div>
    	  </div>
	</div>

</div>
@endsection