@extends('layouts.backend')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
          <div class="col-xs-6">
            Страницы
          </div>

            <div class="col-xs-6 text-right">
                <a class="btn btn-success"
                 href="{{ action('Backend\PageController@getForm') }}"> + Добавить</a>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead> 
                  <tr>
                      <th>ID</th>
                      <th>Заголовок</th>
                      <th>Ссылка</th>
                      <th>Текст</th>
                      <th width="100"></th>
                  </tr>
              </thead>
                  <tbody>
                  @foreach($pages as $page)
                    <tr>
                        <td>{{ $page->id }}</td>
                        <td>{{ $page->title_ru }}</td>
                        <td>{{ $page->alias }}</td>
                        <td>{!! str_limit(strip_tags($page->description_ru,100)) !!}</td>
                        <td>      
                          <a href="{{ action('Backend\PageController@getForm', [$page->id]) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a> 
                          
                          <a href="#" name="delete_item" id="{{ $page->id }}" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                        </td>         
                    </tr>
                  @endforeach
                    </tbody>       
                </table>
            {!! $pages->links() !!}
        </div>
    </div>
</div>
@endsection

@section('scripts')
@include('partials.backend-delete', ['action' => 'Backend\PageController@postDelete'])
@stop