@extends('layouts.backend')

@section('styles')
    
@stop

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ empty($page) ? 'Добавить' : 'Редактировать' }}
    </div>
    <div class="panel-body">
        <ul class="nav nav-tabs" role="tablist">
            @foreach(Config::get('app.supported_locales') as $i=>$v)
                <li role="presentation" @if($i==0)class="active" @endif><a href="#{{$v}}" aria-controls="{{$v}}"
                role="tab" data-toggle="tab"
                style="text-transform: uppercase">{{$v}}</a>
                </li>
            @endforeach    
        </ul>
        <br>
      <div class="row">
        <div class="col-sm-12">
            {!! Form::open(['files' => true]) !!}
            <div class="tab-content">
                @foreach(Config::get('app.supported_locales') as $i=>$v)
                    <div role="tabpanel" class="tab-pane fade in @if($i==0) active @endif" id="{{$v}}">
                        <div class="form-group">
                            {!! Form::label('title', 'Название') !!}
                            {!! Form::text('title_'.$v, empty($page) ? null : $page->{'title_'.$v}, ["class" => "form-control"]) !!}
                        </div>

                        <div class="form-group">
                        {!! Form::label('description', 'Описание') !!}
                        {!! Form::text('description_'.$v, empty($page) ? null : $page->{
                            'description_'.$v}, ['class' => 'redactor']) !!}
                        </div>

                        <div class="form-group">
                        {!! Form::label('meta_description', 'META - Описание') !!}
                        {!! Form::text('meta_description_'.$v, empty($page) ? null : $page->{
                            'meta_description_'.$v}, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                        {!! Form::label('meta_keywords', 'META - Заголовок') !!}
                        {!! Form::text('meta_keywords_'.$v, empty($page) ? null : $page->{
                            'meta_keywords_'.$v}, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                @endforeach    
            </div> 
                    
                <div class="form-group">
                    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                    <a href="{{ action('Backend\PageController@getIndex') }}" class="btn btn-default">Отменить</a>
                </div>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
</div>
@stop

@section('scripts')

    <script type="text/javascript">
        tinymce.init({
            selector: '.redactor',
            height: 200,
            menubar: false,
            plugins:[
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
                  ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: '//www.tinymce.com/css/codepen.min.css',
            file_browser_callback : function(field_name, url, type, win) { 

                    // from http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript
                    var w = window,
                    d = document,
                    e = d.documentElement,
                    g = d.getElementsByTagName('body')[0],
                    x = w.innerWidth || e.clientWidth || g.clientWidth,
                    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

                var cmsURL = '/fm/index.html?&field_name='+field_name;

                if(type == 'image') {           
                    cmsURL = cmsURL + "&type=images";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });         

            }
        });
    </script>
@stop
