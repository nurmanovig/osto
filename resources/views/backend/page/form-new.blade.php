@extends('layouts.backend')

@section('styles')
    
@stop

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ empty($page) ? 'Добавить' : 'Редактировать' }}
    </div>
    <div class="panel-body">
        <ul class="nav nav-tabs" role="tablist">
            @foreach(Config::get('app.supported_locales') as $i=>$v)
                <li role="presentation" @if($i==0)class="active" @endif><a href="#{{$v}}" aria-controls="{{$v}}"
                role="tab" data-toggle="tab"
                style="text-transform: uppercase">{{$v}}</a>
                </li>
            @endforeach    
        </ul>
        <br>
      <div class="row">
        <div class="col-sm-12">
            {!! Form::open(['files' => true]) !!}
            <div class="tab-content">
                @foreach(Config::get('app.supported_locales') as $i=>$v)
                    <div role="tabpanel" class="tab-pane fade in @if($i==0) active @endif" id="{{$v}}">
                        <div class="form-group">
                            {!! Form::label('title', 'Название') !!}
                            {!! Form::text('title_'.$v, empty($page) ? null : $page->{'title_'.$v}, ["class" => "form-control"]) !!}
                        </div>

                        <div class="form-group">
                        {!! Form::label('description', 'Описание') !!}
                        {!! Form::textarea('description_'.$v, empty($page) ? null : $page->{
                            'description_'.$v}, ['class' => 'redactor', 'id' => 'redactor_'.$v]) !!}
                       
                        </div>

                        <div class="form-group">
                        {!! Form::label('meta_description', 'META - Описание') !!}
                        {!! Form::text('meta_description_'.$v, empty($page) ? null : $page->{
                            'meta_description_'.$v}, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                        {!! Form::label('meta_keywords', 'META - Заголовок') !!}
                        {!! Form::text('meta_keywords_'.$v, empty($page) ? null : $page->{
                            'meta_keywords_'.$v}, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                @endforeach    
            </div> 
                    
                <div class="form-group">
                    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                    <a href="{{ action('Backend\PageController@getIndex') }}" class="btn btn-default">Отменить</a>
                </div>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
</div>
@stop

@section('scripts')
    <!-- <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script> -->
    <script src="{{ asset('/assets/library/ckeditor/ckeditor.js') }}" type="text/javascript" charset="utf-8" ></script>
    <script type="text/javascript">
    var options = {
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
      };
    var editor = CKEDITOR.replace( 'redactor_ru', options);
    var editor = CKEDITOR.replace( 'redactor_uz', options);
        // $(".redactor").ckeditor();

        // $("form").submit(function(e){
        //     e.preventDefault();
        //     var data = $(this).serialize();
        //     var action = $(this).attr("action");
        //     console.log(data);
            
        //     $.ajax({
        //         url: action,
        //         data: data,
        //         async: false,
        //         method: "POST",
                
        //         success: function(response){
        //             console.log(response);
        //         }
        //     })
        // });
    </script>
@stop
