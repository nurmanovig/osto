@extends('layouts.backend')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
          <div class="col-xs-6">
            Дочерние категории
          </div>

            <div class="col-xs-6 text-right">
                <a class="btn btn-success" href="{{action('Backend\CategoryController@addParent',[$id])}}"> + Добавить</a>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table">
              <thead>
                  <tr>
                      <th>№</th>
                      <th>Название</th>
                      <th>Действия</th>
                  </tr>
              </thead>
              <?php $count = 1; ?>
                  <tbody>
                  @foreach($parent as $item)

                    <tr>
                        <td>{{ $count++ }}</td>
                        <td>{{ $item->title_ru }}</td>
                        <td>      
                          <a href="{{ action('Backend\CategoryController@getForm', [$item->id]) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a> 
                          
                          <a href="#" name="delete_item" id="{{ $item->id }}" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger"><i class="fa fa-trash"></i></a>

                          <a href="/mypanel/category/parent/{{ $item->id }}" class="btn btn-info">Дочерние</a>
                        </td>
                        
                    </tr>

                  @endforeach
                      
                    </tbody>       
                </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@include('partials.backend-delete', ['action' => 'Backend\CategoryController@postDelete'])  
@endsection
