@extends('layouts.backend')
    <style >
        .cats__item_inner {
    display: block;
    color: #6b37a8;
    background: #fff;
    cursor: pointer;
}

.cats__item_inner i {
    font-size: 72px;
}
    </style>

@section('content')

<div class="panel panel-default">
    
    <!-- <div class="col-xs-6 ">
        <a class="btn btn-success" href="{{
          action('Backend\MallCategoryController@getForm',[0]) }}"> + Добавить</a>
    </div> -->
    <div class="panel-heading">
        Список слов
    </div>
    <div class="panel-body">

        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table">
             <thead>
                <tr>
                    <th>Ключ</th>
                    <th>Значение</th>
                    <th>Цвет</th>
                    <th width="50"></th>
                </tr>
             </thead>
            
             <tbody>
                @foreach($keys as $key)
                <tr>
                    <td><a class='cats__item_inner'><?php echo $key->key ?></a></td>
                    <td><a class='cats__item_inner'><?php echo $key->word ?></a></td>
                    <td><a class='cats__item_inner'><?php echo $key->class ?></a></td>
                    <td><a href="/mypanel/key/form/{{$key->id}}" class="btn btn-primary">
                          <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a></td>
                </tr>
                @endforeach
             </tbody>      
            </table>
        </div>
    </div>
</div>
@endsection

