@extends('layouts.backend')

@section('styles')
    <link rel="stylesheet" href="{{ asset('backend/css/redactor.css') }}">
@stop

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Редактировать
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
            {!! Form::open() !!}
                <div class="form-group">
                    <input class="form-control" type="text" disabled name="word" value="{{ $key ? $key->key : null }}">

                    @if(!empty($errors) && $errors->has('price_delivery'))
                    <span class="form-group__label form-group__label--error">
                        {{ $errors->get('phone')[0] }}
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <input class="form-control" type="text" name="word" value="{{ $key ? $key->word : null }}">
                    
                    @if(!empty($errors) && $errors->has('phone'))
                    <span class="form-group__label form-group__label--error">
                        {{ $errors->get('phone')[0] }}
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <select class="form-control" name="class">
                        <option>success</option>
                        <option>primary</option>
                        <option>purple</option>
                        <option>warning</option>
                        <option>danger</option>
                        <option>default</option>
                    </select>
                    
                    @if(!empty($errors) && $errors->has('phone'))
                    <span class="form-group__label form-group__label--error">
                        {{ $errors->get('phone')[0] }}
                    </span>
                    @endif
                </div>

                    
                <div class="form-group">
                    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                </div>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
</div>

@stop


