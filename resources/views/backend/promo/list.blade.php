@extends('layouts.backend')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
          <div class="col-xs-6">
            Продукты
          </div>
            <div class="col-xs-6 text-right">
                <a class="btn btn-success" href="{{action('Backend\PromoCodeController@getForm')}}"> + Добавить</a>
            </div>
        </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table" data-current="{{ URL::full()}}" >
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Код</th>
                      <th>Процент</th>
                      <th>Дата старта</th>
                      <th>Дата окончание</th>
                      <th></th>

                  </tr>
              </thead>
                  <tbody>
                      @foreach($promo_codes as $key => $promo) 
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $promo->code }}</td>
                            <td>{{ $promo->percent }}</td>
                            <td>{{ $promo->start_at }}</td>
                            <td>{{ $promo->finished_at }}</td>
                            <td>                              
                                <a href="{{ action('Backend\PromoCodeController@getForm', [$promo->id]) }}" class="btn btn-primary">
                                  <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                <a href="#" name="delete_item" id="{{$promo->id}}" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger" data-action="/mypanle/promo/delete">
                                  <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                      @endforeach
                  </tbody>       
              </table>
        </div>
        {!! $promo_codes->links() !!}
    </div>
</div>
@stop

@section("scripts")
  @include('partials.backend-delete', ['action' => 'Backend\PromoCodeController@postDelete'])
@stop