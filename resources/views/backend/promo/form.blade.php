@extends('layouts.backend')

@section('styles')
    <link rel="stylesheet" href="{{ asset('backend/css/redactor.css') }}">
@stop

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ empty($promo) ? 'Добавить' : 'Редактировать' }}
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
            {!! Form::open(['files' => true]) !!}
                <div class="form-group">
                    {!! Form::label('code', 'Код') !!}
                    {!! Form::text('code', empty($promo) ? null : $promo->code, ["class" => "form-control"]) !!}
                </div>

                
        
                <div class="form-group">
                    {!! Form::label('percent', 'Скидка') !!}
                    {!! Form::text('percent', empty($promo) ? null : $promo->percent, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('start_at', 'Начало') !!}
                    {!! Form::text('start_at', empty($promo) ? null : $promo->start_at, ['class' => 'form-control datepicker']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('finished_at', 'Окончание') !!}
                    {!! Form::text('finished_at', empty($promo) ? null : $promo->finished_at, ['class' => 'form-control datepicker' 
                        ]) !!}
                </div>


                    
                <div class="form-group">
                    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                    <a href="{{ action('Backend\PromoCodeController@getIndex') }}" class="btn btn-default">Отменить</a>
                </div>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
</div>

@stop
@section("script")

    <script type="text/javascript">
    $(function () {
    
    });
    </script>
@stop

