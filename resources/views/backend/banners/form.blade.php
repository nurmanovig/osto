@extends('layouts.backend')

@section('styles')
    <link rel="stylesheet" href="{{ asset('backend/css/redactor.css') }}">
@stop

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            {{ empty($item) ? 'Добавить' : 'Редактировать' }}
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    {!! Form::open(['files' => true]) !!}
                    <div class="form-group">
                        @if($item)
                            <img src="{{$item->img}}" alt="">
                        @endif
                        {!! Form::label('img', 'Фото') !!}
                        {!! Form::file('img',["class" => "form-control"]) !!}
                    </div>



                    <div class="form-group">
                        {!! Form::label('url', 'URL') !!}
                        {!! Form::text('url', empty($item) ? null : $item->url, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('actived_at', 'Активно до') !!}
                        {!! Form::text('actived_at', empty($item) ? null : $item->actived_at, ['class' => 'form-control datepicker']) !!}
                    </div>


                    <div class="form-group">
                        {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                        <a href="{{ action('Backend\BannerController@getIndex') }}" class="btn btn-default">Отменить</a>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop
@section("script")

    <script type="text/javascript">
        $(function () {

        });
    </script>
@stop

