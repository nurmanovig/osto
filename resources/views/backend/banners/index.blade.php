@extends('layouts.backend')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    Продукты
                </div>
                <div class="col-xs-6 text-right">
                    <a class="btn btn-success" href="{{action('Backend\BannerController@getForm')}}"> + Добавить</a>
                </div>
            </div>
        </div>

        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="data-table" data-current="{{ URL::full()}}" >
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Фото</th>
                        <th>Url</th>
                        <th>Дата окончание</th>
                        <th></th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td><img src="/banners/{{$item->img}}" alt="" width="100px" height="100px"></td>
                            <td>{{ $item->url }}</td>
                            <td>{{ $item->actived_at }}</td>
                            <td>
                                <a href="{{ action('Backend\BannerController@getForm', [$item->id]) }}" class="btn btn-primary">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                                <a href="#" name="delete_item" id="{{$item->id}}" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger" data-action="/mypanle/promo/delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {!! $items->links() !!}
        </div>
    </div>
@stop

@section("scripts")
    @include('partials.backend-delete', ['action' => 'Backend\BannerController@postDelete'])
@stop