@extends('layouts.backend')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
      <div class="row">
          <div class="col-xs-6">  
            Категории
          </div>
          <div class="col-xs-6 text-right">
              <a href="{{ action('Backend\CategoryController@getForm') }}" class="btn btn-success">+ Добавить категорию</a>
          </div>
      </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table">
            <thead>
                <tr>
                    <th width="50px">ID</th>
                    <th>Название (рус) </th>
                    <th width="200"></th>
                </tr>
            </thead>

            <tbody>

            </tbody>             
        </table>
        </div>
    </div>
</div>
@endsection		

@section('scripts')

    @include('partials.backend-delete', ['action' => 'Backend\CategoryController@postDelete'])
    
    <script src="{{asset('backend/js/datatables.min.js')}}"></script>
    <script type="text/javascript">
     
         $("#data-table").dataTable({ 

             "aProcessing": true,
             "aServerSide": true,
             "ajax": "{{ action('Backend\CategoryController@getData') }}",

        columns: [
            {data: 'id', name: 'id'},
            {data: 'title_ru', name: 'title_ru'},
        ],



        columnDefs: [
            { 
              targets: 2,
              data: null, 
              searchable:false, 
              
              render: function(row,type,val,meta){
                  console.log(val);
                   return '<a href="/mypanel/category/form/' + val.id + '" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> ' +
                           '<a href="#" name="delete_item" id="' + val.id +'" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger"><i class="fa fa-trash"></i></a>' 
                           + " " + '<a href="/mypanel/category/parent/' + val.id + '" class="btn btn-info">Дочерние</a>';
              },


          },

        ],
           

       });

  </script>
@stop