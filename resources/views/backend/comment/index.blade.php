@extends('layouts.backend')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
          <div class="col-xs-6">
              Комментарии
          </div>

        </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Пользователь</th>
                      <th>Текст комментария</th>
                      <th>Дата создания</th>
                      <th width="100">Действия</th>

                  </tr>
              </thead>
                  <tbody>

                      
                  </tbody>       
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('backend/js/datatables.min.js')}}"></script>

<script type="text/javascript">
     
         $("#data-table").dataTable({ 

             "aProcessing": true,
             "aServerSide": true,
             "ajax": "{{ action('Backend\CommentController@getData') }}",

        columns: [
            {data: 'id', name: 'id'},
            {data: 'user.name', name: 'user.name'},
            {data: 'text', name: 'text'},
            {data: 'created_at', name: 'created_at'},
            {data: 'id', name: 'id'}
        ],

        columnDefs: [
            { 
              targets: 4,
              data: null, 
              searchable:false, 
              
              render: function(row,type,val,meta){
                   return '<a href="#" name="delete_item" id="' + val.id +'" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
              }
          },

        ],
           
       });

  </script>


@include('partials.backend-delete', ['action' => 'Backend\CommentController@postDelete'])
@stop