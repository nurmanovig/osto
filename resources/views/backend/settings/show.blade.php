@extends('layouts.backend')

@section('content')
<div class="panel panel-default">
    
    <!-- <div class="col-xs-6 ">
        <a class="btn btn-success" href="{{
          action('Backend\MallCategoryController@getForm',[0]) }}"> + Добавить</a>
    </div> -->
    <div class="panel-heading">
        Услуги
    </div>
    <div class="panel-body">

        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table">
             <thead>
                <tr>
                    <th>Сумма доставки</th>
                    <th>Курс</th>
                    <th width="50"></th>
                </tr>
             </thead>
             
             <tbody>
                <tr>
                    <td>{{ $setting->price_delivery }}</td>
                    <td>{{ $setting->course }}</td>
                    <td>
                        <a href="{{ action('Backend\SettingController@getForm') }}" class="btn btn-primary">
                          <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>
                    </td>
                    
                </tr>
             </tbody>      
            </table>
        </div>
    </div>
</div>
@endsection
