@extends('layouts.backend')

@section('styles')
    <link rel="stylesheet" href="{{ asset('backend/css/redactor.css') }}">
@stop

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Редактировать
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
            {!! Form::open() !!}
                <div class="form-group">
                    {!! Form::label('price_delivery', 'Сумма доставки') !!}
                    {!! Form::text('price_delivery', empty($setting) ? null : $setting->price_delivery, ["class" => "form-control"]) !!}

                    @if(!empty($errors) && $errors->has('price_delivery'))
                    <span class="form-group__label form-group__label--error">
                        {{ $errors->get('phone')[0] }}
                    </span>
                    @endif
                </div>

                <div class="form-group">
                    {!! Form::label('course', 'Курс ') !!}
                    {!! Form::text('course', empty($setting) ? null : $setting->course, ['class' => 'form-control']) !!}
                    
                    @if(!empty($errors) && $errors->has('phone'))
                    <span class="form-group__label form-group__label--error">
                        {{ $errors->get('phone')[0] }}
                    </span>
                    @endif
                </div>

                    
                <div class="form-group">
                    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                </div>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
</div>

@stop
@section("script")

    <script type="text/javascript">
    $(function () {
    
    });
    </script>
@stop

