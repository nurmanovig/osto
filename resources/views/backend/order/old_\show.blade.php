<?php
use App\Models\OrderProduct;
use App\Models\Order;
?>
@extends('layouts.backend')

@section('content')
<link rel="stylesheet" href="/assets/leaflet/leaflet.css" />
<script src="/assets/leaflet/leaflet-src.js"></script>
<script src="/assets/leaflet/esri-leaflet.js"></script>
<script src="/assets/leaflet/esri-leaflet-geocoder.js"></script>
<div class="panel panel-default">

		<h2>№{{$order->id}}</h2>
</div>
<div class="panel panel-default order"  data-current="{{ URL::full()}}" data-id={{$order->id}}>
	<div class="panel-body">
		
		<h4>Сумма: {{ $order->summ }} сум</h4>
		<h4>Скидка: {{ $order->sale }} сум</h4>
		<h4>Статус: 

		<?php $status = Order::status($order->status);?>
		<span class="text text-{{$status['class']}}">{{$status['text']}}</span>

		</h4>
		<h4>Вид оплаты: 

			<?php $pstatus = Order::paytype($order->pay_type);?>
			<span class="text text-{{$pstatus['class']}}">{{$pstatus['text']}}</span>

		</h4>                  
		<h4>Статус оплаты: 

			<?php $pstatus = Order::paystatus($order->pay_status);?>
			<span class="text text-{{$pstatus['class']}}">{{$pstatus['text']}}</span>

		</h4>
		<h4>Время: {{ $order->created_at }}</h4>
		<h4>Обновлен: {{ $order->updated_at }}</h4>
		<h3>Товары. </h3>
				
		<div class="table-responsive">
			<table class="table table-bordered table-hover" id="data-table">
				<thead>
					<tr>
						<th></th>
						<th></th>
						<th>Название</th>
						<th>Магазин</th>
						
						<th>Количество</th>
						<th>Скидка</th>
						<th>Сумма</th>
						<th>Статус</th>

					</tr>
				</thead>
				<tbody>
						@foreach($order->products as $key => $or_products) 
						<?php $product = $or_products->product; ?>
						@if(!$product)
							<h2>Некоторые товары из базы были удалены</h2>
						@else

							<tr>
								<td>{{ $key+1 }}</td>
								<td><img style="width: 150px;" src="{{ (Upload::hasFiles('product',$product['id'])) ? Upload::getThumbMainProductFile('product',$product['id'],'208x200')[0] : '/assets/img/noimage.png'}}" alt="Shop image"></td>
								<td>{{ $product->title }} <small>( {{ $product->getSizeInfo($or_products->size_product)}} )</small></td>
								<td>{{ $product->shop->title }}</td>
								<td>{{ $or_products->count }}</td>
								<td>{{ $or_products->count * $or_products->sale }} сум</td>
								<td>{{ $or_products->price * $or_products->count }} сум</td>
								
								<td>
										<?php $status = OrderProduct::status($or_products->status);?>
										<span class="text text-{{$status['class']}}">{{$status['text']}}</span>
								</td>
							</tr>
						@endif
						@endforeach
				</tbody>       
			</table>
		</div>
	</div>
	<div class="col-sm-6">
	@if($order->status == Order::STATUS_INACTIVE)
			<button class="btn btn-danger status" data-status="{{ Order::STATUS_STOPPED}}">Остановить</button>
			<button class="btn btn-info status" data-status="{{ Order::STATUS_STARTED}}">Подготовка товара</button>
	@elseif($order->status == Order::STATUS_STARTED)
			<button class="btn btn-danger status" data-status="{{ Order::STATUS_STOPPED}}">Остановить</button>
			<button class="btn btn-info status" data-status="{{ Order::STATUS_FINISHED}}">Доставлен</button>
	@elseif($order->status == Order::STATUS_FAIL)
			<button class="btn btn-danger status" data-status="{{ Order::STATUS_STOPPED}}">Остановить</button>
			<button class="btn btn-info status" data-status="{{ Order::STATUS_INACTIVE}}">Восстановить</button>
	@endif
	</div>
	<div class="col=sm-6 col-sm-offset-6">
		<?php
		
		 $events = Order::payEvents($order->pay_status);
		
		?>
		@if(!empty($events))
			@foreach($events as $event)
				<span class="{{ $event['class'] }}" data-status="{{$event['status']}}">{{ $event['text'] }}</span>
			@endforeach
		@endif
	</div>
	<div class="clearfix"></div>
</div>

<div class="panel panel-default">
		<h4>Имя: {{$order->username}}</h4>
		<h4>Телефон: {{$order->phone}}</h4>
		<h4>Адрес: {{$order->address}}</h4>
		<h4>Комментария: {{$order->comments}}</h4>
</div>


    {!! Form::hidden('lat', empty($order) ? null : $order->lat, ["class" => "form-control js-lat-input"]) !!}



    {!! Form::hidden('lng', empty($order) ? null : $order->lng, ["class" => "form-control js-lng-input"]) !!}



<div id="order_map" class="js-shop-map only_marker w-100" style="height: 300px; background-color: rgb(204, 204, 204); position: relative; overflow: hidden;">

@stop
@section("scripts")
<script type="text/javascript" src="/assets/js/map.js"></script>
<script>
        var options = {
            addMarker: true,
            // event: {
            //     elem: {
            //         address: $(".address_input"),
            //         lat: $(".js-lat-input"),
            //         lng: $(".js-lng-input"),
            //     },

            // },
            // markers: [
            //             {
            //                 popup: "aaaadasd",
            //                 coords: {
            //                     lat: 41.32661731708665,
            //                     lng: 69.23296451568604
            //                 }
            //             }
            //         ]

        };
        
        

        getSubjects();
        
        function getSubjects(){
        	var id = $(".order").data("id");

        	$.ajax({
        		url: "/mypanel/order/infomap",
        		data: {id: id},
        		success: function(response){
        			markers = [];
        			for(x in response){
        				var marker = {};
        				var popup = "";

        				popup = "<h4>"+response[x].title+"<h4>";
						popup += "<h5>" + response[x].address + "<h5>";
    					popup += "<p>" + response[x].phone + "<p>";

    					if(response[x].type == "user"){
    						marker.icon = 
        						{
        							src: "/assets/img/marker-person.png",
        							width: 55,
        							height: 60
        						};
    					}

        				if(response[x].type == "shop"){
        					
        					marker.icon = 
        						{
        							src: "/assets/img/marker-general.png",
        							width: 55,
        							height: 60
        						};
        					popup += "<p>" + response[x].work_time + "<p>";
        					popup += "<p>" + response[x].work_days + "<p>";
        				}

        				marker.coords = response[x].coords;
        				marker.address = response[x].address;
        				marker.popup = "<div class='popup-map'>" + popup + "</div>";
        				markers.push(marker);

        			}



        			options.markers = markers;

        			console.log(options.markers);
        			if($(".js-lat-input").val() && $(".js-lng-input")){
			            mapInitByPosition("order_map", [$(".js-lat-input").val(), $(".js-lng-input").val()], options) ;
			        }else{
			            mapInitByNavigator("order_map", options);
			        }
        		}
        	})
        }
    </script>
@stop