<?php
use App\Models\Order;
use Illuminate\Support\Facades\URL;

?>
@extends('layouts.backend')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Поиск
    </div>
    
    <div class="panel-body">
      <div class="row">
          {!! Form::open(['method' => 'GET', action('Backend\OrderController@getIndex')]) !!}
          <div class="col-sm-9">
            <div class="form-group">
              <input class="form-control" placeholder="ID заказа" id="search" name="id" type="text">
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <button type="submit" class="btn btn-success btn-block">Найти</button>
            </div>
          </div>
          {!! Form::close() !!}
      </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
          <div class="col-xs-12">
            Заказы
          </div>

        <a class="btn bnt-default" href="{{ URL::to('/mypanel/orders') }}">Все</a> 
        @foreach(Order::status() as $key => $item)
            <a class="btn btn-{{$item['class']}}" href="{{ URL::action('Backend\OrderController@getIndex',['status' => $key]) }}">{{$item['text']}}</a> 
        @endforeach

        <h3>Количество заказов: {{$info->count}}</h3>
        <h3>Общая сумма: {{$info->summ ? : 0}} {{$currency}}</h3>

        </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table"  data-current="{{ URL::full()}}">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>№ ЗАКАЗА</th>
                      <th>Цена {{$currency}}</th>
                      <th>Время</th>
                      <th>Статус</th>
                      <th>Вид оплаты</th>
                      <th>Статус оплаты</th>

                      <th width="100"></th>
                  </tr>
              </thead>
                  <tbody>
                      @foreach($orders as $key => $order) 
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td><a href="/mypanel/order/show/{{$order->id}}">№ {{ $order->id }}</a></td>
                            <td>{{ $order->summ }}</td>
                            <td>{{ $order->created_at }}</td>
                            
                            <td>
                            <?php $order_status = Order::status($order->status);?>
                            <span class="text text-{{$order_status['class']}}">{{$order_status['text']}}</span>
                            
                            </td>
                            <td>
                            <?php $pstatus = Order::paytype($order->pay_type);?>
                            <span class="text text-{{$pstatus['class']}}">{{$pstatus['text']}}</span>
                            
                            </td>
                            <td>
                            <?php $pstatus = Order::paystatus($order->pay_status);?>
                            <span class="text text-{{$pstatus['class']}}">{{$pstatus['text']}}</span>
                            
                            </td>
                            

                            <td>                              
                               
                                 <a  name="delete_item" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger" id="{{$order->id}}"  class="btn btn-danger delete_item">
                                  <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                      @endforeach
                  </tbody>       
              </table>
        </div>
        {!! $orders->appends($status !== null ? ['status' => $status] : [])->links() !!}

    </div>
</div>

@stop

@section('scripts')
  @include('partials.backend-delete', ['action' => 'Backend\OrderController@postDelete'])
@stop