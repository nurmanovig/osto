@extends('layouts.backend')
    <style >
        .cats__item_inner {
    display: block;
    color: #6b37a8;
    background: #fff;
    cursor: pointer;
}

.cats__item_inner i {
    font-size: 72px;
}
    </style>

@section('content')
<div class="panel panel-default">
       <div class="panel-body">
        {!! Form::open(['files' => true, 'action' => 'Backend\IconController@postForm']) !!}
            <div class="form-group col-sm-4">
                {!! Form::text('icon', null, ["class" => "form-control", 'placeholder' => 'Значение']) !!}
            </div>

           
            <div class="form-group col-sm-2">
                {!! Form::submit('Добавить', array('class' => 'btn btn-primary')) !!}
            </div>
        {!! Form::close() !!}
    </div>
</div>
<div class="panel panel-default">
    
    <!-- <div class="col-xs-6 ">
        <a class="btn btn-success" href="{{
          action('Backend\MallCategoryController@getForm',[0]) }}"> + Добавить</a>
    </div> -->
    <div class="panel-heading">
        Список иконок
    </div>
    <div class="panel-body">

        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table">
             <thead>
                <tr>
                    <th>№</th>
                    <th>Значение</th>
                    <th width="50"></th>
                </tr>
             </thead>
            
             <tbody>
                @foreach($icons as $icon)
                <tr>
                    <td>{{ $icon->id }}</td>
                    <td><a class='cats__item_inner'><?php echo $icon->icon ?></a></td>
                    <td>
                        <a href="#" name="delete_item" id="{{$icon->id}}" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger" data-action="/mypanel/size-product/delete">
                          <i class="fa fa-trash"></i>
                        </a>
                    </td>
                    
                </tr>
                @endforeach
             </tbody>      
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  @include('partials.backend-delete', ['action' => 'Backend\IconController@postDelete'])
@stop
