@extends('layouts.backend')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        Ползователи
    </div>
    <div class="panel-body">

        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Телефон</th>
                    <th>E-mail</th>
                    <th width="100"></th>
                </tr>
             </thead>
             <tbody>
               
                @foreach($users as $key => $user)
                  <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->lastname }}</td>
                    <td>{{ $user->phone }}</td>
                    <td>{{ $user->email }}</td>
                    <td width="100">
                    <a href="/mypanel/user/form/{{$user->id}}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                    <a href="#" name="delete_item" id="{{$user->id}}" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger"><i class="fa fa-trash"></i></a
                    ></td>
                </tr>
                @endforeach
                 
             
              </tbody>       
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')

  @include('partials.backend-delete', ['action' => 'Backend\UserController@postDelete'])
    

@stop