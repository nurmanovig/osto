@extends('layouts.backend')

@section('styles')
    <link rel="stylesheet" href="{{ asset('backend/css/redactor.css') }}">
@stop

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ 'Редактировать' }}
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
            {!! Form::open(['files' => true]) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Имя') !!}
                    {!! Form::text('name', empty($user) ? null : $user->name, ["class" => "form-control"]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('lastname', 'Фамилия') !!}
                    {!! Form::text('lastname', empty($user) ? null : $user->lastname, ["class" => "form-control"]) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('phone', 'Телефон') !!}
                    {!! Form::text('phone', empty($user) ? null : $user->phone, ["class" => "form-control"]) !!}
                </div>

                <div class="form-group">
                {!! Form::label('email', 'E-mail') !!}
                {!! Form::text('email', empty($user) ? null : $user->email,['class' => 'form-control']) !!}
                </div>
                
                <div class="form-group">
                    <label>Новый пароль</label>
                    <input name="password" type="password" class="form-control">
                </div>
                
                <div class="form-group">
                    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                    <a href="{{ action('Backend\UserController@getIndex') }}" class="btn btn-default">Отменить</a>
                </div>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
</div>
@stop

@section('scripts')

    <script type="text/javascript">
        tinymce.init({
            selector: '.redactor',
            height: 200,
            menubar: false,
            plugins:[
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
                  ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: '//www.tinymce.com/css/codepen.min.css',
            file_browser_callback : function(field_name, url, type, win) { 

                    // from http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript
                    var w = window,
                    d = document,
                    e = d.documentElement,
                    g = d.getElementsByTagName('body')[0],
                    x = w.innerWidth || e.clientWidth || g.clientWidth,
                    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

                var cmsURL = '/fm/index.html?&field_name='+field_name;

                if(type == 'image') {           
                    cmsURL = cmsURL + "&type=images";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });         

            }
        });
    </script>

@stop
