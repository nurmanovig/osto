<?php
use App\Models\Order;
use Illuminate\Support\Facades\URL;

?>
@extends('layouts.backend')

@section('content')
<div class="panel panel-default">

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
          <div class="col-xs-12">
            Магазин № {{$shop->id}}
          </div>

        

        </div>
    </div>

    <div class="panel-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table"  data-current="{{ URL::full()}}">

                    <tr>
                        <td>Название</td>
                        <td>{{$shop->title}}</td>
                    </tr>
                    <tr>
                        <td>Адрес</td>
                        <td>{{$shop->address}}</td>
                    </tr>
                    <tr>
                        <td>Телефон</td>
                        <td>{{$shop->phone}}</td>
                    </tr>
                   
              </tbody>       
          </table>
        </div>

    </div>
</div>

@stop