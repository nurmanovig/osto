@extends('layouts.backend')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-xs-6">
            Магазины
            </div>    

              <div class="col-xs-6 text-right">
                  <a class="btn btn-success" href="{{action('Backend\ShopController@getForm')}}"> + Добавить</a>
              </div>  
        </div> 
    </div>

    <div class="panel-body" data-current="{{ URL::full()}}">
        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table">
              <thead>
                <tr>
                      <th>ID</th>
                      <th>Название</th>
                      <th>Направления</th>
                      <th>Адрес</th>
                      <th width="100"></th>
                      <th width="100"></th>
                  </tr>
              </thead>
              <tbody>
                @foreach($shops as $shop)
                  <tr>
                    <td>{{$shop->id}}</td>
                    <td>{{$shop->title}}</td>
                    <td>{{$shop->direction->title}}</td>
                    <td>{{$shop->address}}</td>
                    <td>
                      
                      <?php $statuses = $shop->getStatuses(); ?>

                      @foreach($statuses as $status)
                        <span class="{{ $status['class'] }}" data-id="{{$shop->id}}" data-type="{{ $status['type'] }}">{{ $status['text'] }}</span>
                        <div class="clearfix"></div>
                      @endforeach
                      
                    </td>
                    <td>
                      <a href="/mypanel/shops/form/{{ $shop->id }}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                      <a href="#" name="delete_item" id="{{ $shop->id }}" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
          </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  <!-- <script src="{{asset('backend/js/datatables.min.js')}}"></script> -->
<!--   <script type="text/javascript">
     
         $("#data-table").dataTable({ 
             "aProcessing": true,
             "aServerSide": true,
             "ajax": "{{ action('Backend\ShopController@getData') }}",

        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'direction.title_ru', name: 'direction.title_ru'},
            {data: 'address', name: 'address'},
            {data: 'id', name: 'id'}
        ],

        columnDefs: [
            { 
              targets: 4, 
              data: null, 
              searchable:false, 
              
              render: function(row,type,val,meta){
                   return '<a href="/mypanel/shop/' + val.id + '" class="btn btn-info"><i class="fa" aria-hidden="true"></i></a> <a href="/mypanel/shops/form/' + val.id + '" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a href="#" name="delete_item" id="' + val.id +'" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
              }
          },

        ],
           
       });

  </script> -->
  @include('partials.backend-delete', ['action' => 'Backend\ShopController@postDelete'])

@stop