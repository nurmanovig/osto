@extends('layouts.backend')

@section('styles')
    <link rel="stylesheet" href="{{ asset('backend/css/redactor.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/bootstrap-timepicker.min.css') }}">
@stop

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        {{ empty($shop) ? 'Добавить' : 'Редактировать' }}
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
            {!! Form::open(['files' => true]) !!}
            
                <div class="form-group">
                    {!! Form::label('title', 'Название') !!}
                    {!! Form::text('title', empty($shop) ? null : $shop->title, ["class" => "form-control"]) !!}
                </div>

                <div class="form-group">
                {!! Form::label('address', 'Адрес') !!}
                {!! Form::text('address', empty($shop) ? null : $shop->address, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('phone', 'Контактный телефон') !!}
                    {!! Form::text('phone', empty($shop) ? null : $shop->phone, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('lat', 'Latitude') !!}
                    {!! Form::text('lat', empty($shop) ? null : $shop->lat, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('lng', 'Longitude') !!}
                    {!! Form::text('lng', empty($shop) ? null : $shop->lng, ['class'=> 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('seller', 'Продавец') !!}
                    {!! Form::select('user_id', $seller, empty($shop) ? null : $shop->user_id, ['class'=> 'form-control']) !!}
                </div>     

                <div class="form-group">{!! Form::label('workdays', 'Рабочие дни') !!}</div>

                <div class="form-group well">
                    {!! Form::label('monday', 'Пн') !!}
                    {!! Form::checkbox('day1', 1, isset($shop->day1) ? $shop->day1 : null) !!} 

                    {!! Form::label('tuesday', 'Вт') !!}
                    {!! Form::checkbox('day2', 1, isset($shop->day2) ? $shop->day2 : null) !!} 

                    {!! Form::label('wednesday', 'Ср') !!}
                    {!! Form::checkbox('day3', 1, isset($shop->day3) ? $shop->day3 : null) !!} 

                    {!! Form::label('thursday', 'Пт') !!}
                    {!! Form::checkbox('day4', 1, isset($shop->day4) ? $shop->day4 : null) !!} 

                    {!! Form::label('thursday', 'Пт') !!}
                    {!! Form::checkbox('day5', 1, isset($shop->day5) ? $shop->day5 : null) !!} 

                    {!! Form::label('saturday', 'Сб') !!}
                    {!! Form::checkbox('day6', 1, isset($shop->day6) ? $shop->day6 : null) !!} 

                    {!! Form::label('sunday', 'Вс') !!}
                    {!! Form::checkbox('day7', 1, isset($shop->day7) ? $shop->day7 : null) !!} 
                </div>
                
                <div class="form-group">
                    {!! Form::label('working_hour_start', 'Открытие в') !!}
                        <div class="input-group bootstrap-timepicker timepicker">
                            {!! Form::text('working_hour_start', empty($shop) ? null : $shop->working_hour_start, ['class' => 'form-control input-small', 'id' => 'workstart'])!!}
                            <span class="input-group-addon">
                                <i class="glyphicon glyphicon-time"></i>
                            </span>
                        </div>

                    {!! Form::label('working_hour_end', 'Закрытие в') !!}
                        <div class="input-group bootstrap-timepicker timepicker">
                             {!! Form::text('working_hour_end', empty($shop) ? null : $shop->working_hour_end, ['class' => "form-control input-small", 'id' => 'workend']) !!}
                             <span class="input-group-addon">
                                <i class="glyphicon glyphicon-time"></i>
                             </span>
                        </div>
                </div>
                <h3>Payme конфигурация</h3>
                <div class="form-group">
                    {!! Form::label('phone', 'Мерчант') !!}
                    {!! Form::text('merchant', empty($shop->shopPaymeConfig) ? null : $shop->shopPaymeConfig->merchant, ['class'=> 'form-control']) !!}
                </div>


                @if(!empty($shop))
                    <div class="form-group">
                        @if(Upload::hasFile('shop', $shop->id))
                            <div class="row">
                                <div class="col-sm-6">
                                    <img src="{!! Upload::getFile('shop', $shop->id) !!}" width="100%">
                                </div>
                            </div>
                        @endif
                    </div>
                @endif

                <div class="form-group">
                    {!! Form::label('image', 'Изображение') !!}
                    <input type="file" multiple="multiple" name="image" accept="image/jpeg/png">
                </div>   
                    
                <div class="form-group">
                    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                    <a href="{{ action('Backend\ShopController@getIndex') }}" class="btn btn-default">Отменить</a>
                </div>
            {!! Form::close() !!}
        </div>
      </div>
    </div>
</div>
@stop

@section('scripts')
    <script>    
    $(document).ready(function(){
        $(document).on('change', '#direction', function(e){
             
            var direction_id = $(this).val();
            var parent = $(this).parent().parent();
            var option = " ";
            // console.log(direction_id);
            $.ajax({
                type:'GET',
                url:'{!! action("Backend\ShopController@getFind") !!}', 
                data: {'id':direction_id},
                success:function(data){
                    // console.log(data);

                 if(data.length <= 0){
                    $("#woops").hide();
                 }   
                 else{
                    $("#woops").show();
                 }

                    for(var i=0; i < data.length; i++){
                        option+='<option value="'+data[i].id+'">'+data[i].title_ru+'</option>'; 
                    }

                    parent.find('#child').html(" ");
                    parent.find('#child').append(option);

                },
                error:function(data){
                    console.log('error');
                }      
            });
            
        });
    });
    </script>

    <script src="{{asset('backend/js/bootstrap-timepicker.min.js') }}"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: '.redactor',
            height: 200,
            menubar: false,
            plugins:[
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
                  ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: '//www.tinymce.com/css/codepen.min.css',
            file_browser_callback : function(field_name, url, type, win) { 

                    // from http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript
                    var w = window,
                    d = document,
                    e = d.documentElement,
                    g = d.getElementsByTagName('body')[0],
                    x = w.innerWidth || e.clientWidth || g.clientWidth,
                    y = w.innerHeight|| e.clientHeight|| g.clientHeight;

                var cmsURL = '/fm/index.html?&field_name='+field_name;

                if(type == 'image') {           
                    cmsURL = cmsURL + "&type=images";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });         

            }
        });

        $(function () {
            $('#workstart').timepicker({
                minuteStep: 1,
                template: 'modal',
                appendWidgetTo: 'body',
                showMeridian: false,
                defaultTime: false
            });

            $('#workend').timepicker({
                minuteStep: 1,
                template: 'modal',
                appendWidgetTo: 'body',
                showMeridian: false,
                defaultTime: false
            });
        });
    </script>

@stop
