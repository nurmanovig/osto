@extends('layouts.backend')

@section('content')
<div class="panel panel-default">
       <div class="panel-body">
        {!! Form::open(['files' => true, 'action' => 'Backend\SizeProductController@postForm']) !!}
            <div class="form-group col-sm-4">
                {!! Form::text('key', null, ["class" => "form-control", 'placeholder' => 'Значение']) !!}
            </div>

            <div class="form-group col-sm-6">
                {!! Form::text('value', null, ['class' => 'form-control', 'placeholder' => 'Описание']) !!}
            </div>
                
            <div class="form-group col-sm-2">
                {!! Form::submit('Добавить', array('class' => 'btn btn-primary')) !!}
            </div>
        {!! Form::close() !!}
    </div>
</div>
<div class="panel panel-default">
    
    <!-- <div class="col-xs-6 ">
        <a class="btn btn-success" href="{{
          action('Backend\MallCategoryController@getForm',[0]) }}"> + Добавить</a>
    </div> -->
    <div class="panel-heading">
        Список размер товаров
    </div>
    <div class="panel-body">

        <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data-table">
             <thead>
                <tr>
                    <th>№</th>
                    <th>Значение</th>
                    <th>Описание</th>
                    <th width="50"></th>
                </tr>
             </thead>
            
             <tbody>
                @foreach($sizes as $size)
                <tr>
                    <td>{{ $size->id }}</td>
                    <td>{{ $size->key }}</td>
                    <td>{{ $size->value }}</td>
                    <td>
                        <a href="#" name="delete_item" id="{{$size->id}}" data-toggle="modal" data-target="#deleteModel" class="btn btn-danger" data-action="/mypanel/size-product/delete">
                          <i class="fa fa-trash"></i>
                        </a>
                    </td>
                    
                </tr>
                @endforeach
             </tbody>      
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  @include('partials.backend-delete', ['action' => 'Backend\SizeProductController@postDelete'])
@stop
