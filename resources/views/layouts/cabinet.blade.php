<?php
use Illuminate\Support\Facades\Auth;


?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="width=device-width, initial-scale=0.8, maximum-scale=0.8 user-scalable=no">
		<meta name="author" content="Sanjar Mirakhmedov | #DarkC0der |">
		<title>{{ (isset($meta)) ? $meta['title'] : Lang::get('main.seo.main_page.title') }}</title>

		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=cyrillic" rel="stylesheet">
		<link href="{{ asset('assets/css/styles.min.css?v=1.0') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/cart.css?v=1.1') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/sk.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/ln_style.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/15.01.19.css') }}" rel="stylesheet">
		<!--LiveInternet counter--><script type="text/javascript">
        new Image().src = "//counter.yadro.ru/hit?r"+
        escape(document.referrer)+((typeof(screen)=="undefined")?"":
        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
        screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
        ";h"+escape(document.title.substring(0,150))+
        ";"+Math.random();</script><!--/LiveInternet-->

	</head>
	<body class="body @if(!Request::is('/')) nav-page @endif">

		<div class="preloader" style="display: none;">
			<div class="preloader__circle"></div>
		</div>

		<!-- Big menu -->
		@include('partials.menu')

		<!-- Navbar -->
		@include('partials.navbar') 

		<!-- Main Content -->

		<div class="cabinet min-height p-t-nav-sticked">
			<div class="container p-y-layout">
				<div class="row">
					<div class="col-sm-4 col-md-3">
						<div class="cabinet-title" style="margin-bottom: 50px;margin-top: -30px;"><strong>Персональный кабинет</strong></div>

						@if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('salesman'))
                            <div class="cabinet-menu">
                                
                                <?php $shops = Auth::user()->shops()->get();
                                    $shop = null;
                                    if (isset($shops[0])) {
                                        $shop = $shops[0];
                                    }

                                if ($shop) { ?>

                                <div class="cabinet-menu-avatar">
                                    	<?php $fullname = "/uploads/thumb/shop/200x200/".$shop->id."/".$shop->main_image;?>
                                    <div style="width: 120px;height:120px;    background-position: 50%;background-color:#fff;display: inline-block; background-size: cover; background-image: url({{ Upload::hasImage($fullname) ? : '/assets/img/noimage-shop.png'}})"></div>
                                </div>
                                <div class="cabinet-menu-header">
                                    <p>
                                        <strong><?=$shop->title?></strong>
                                    </p>
                                </div>
                                <hr>

                                <?php } ?>


                                <div class="cabinet-menu-nav">
                                    <a href="{{ action('CabinetController@getIndex') }}" class="cabinet__panel-nav-item @if(Request::is('cabinet/product*')) is-active @endif">Товары</a>

                                    <a href="{{ action('ShopController@getForm') }}" class="cabinet__panel-nav-item @if(Request::is('cabinet/shop*')) is-active @endif">Магазин</a>

                                    <a href="{{ action('CabinetController@getOrders') }}" class="cabinet__panel-nav-item {{ active_class(if_action('App\Http\Controllers\CabinetController@getOrders'), 'is-active') }}">Заказы<span class="badge">{{\App\Models\OrderProduct::countOrderProduct(Auth::user()->id)}}</span></a>

                                    <a href="{{ action('CabinetController@getUserReviews') }}" class="cabinet__panel-nav-item {{ active_class(if_action('App\Http\Controllers\CabinetController@getUserReviews'), 'is-active') }}">Отзывы</a>
                                   
                                    <a href="{{ action('CabinetController@getFavorite') }}" class="cabinet__panel-nav-item {{ active_class(if_action('App\Http\Controllers\CabinetController@getFavorite'), 'is-active') }}">Избранные товары</a>


                                    <a href="{{ action('CabinetController@getProfile') }}" class="cabinet__panel-nav-item {{ active_class(if_action('App\Http\Controllers\CabinetController@getProfile'), 'is-active') }}">Настройки</a>
                                </div>
                            </div>

						    <div class="cabinet__sidebar">
                                <div class="cabinet__sidebar-top">
                                    <div class="cabinet__sidebar-name">
                                        <div class="text-center" style="font-size:22px;margin-bottom: 10px;">Статистика</div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="counter-list">
                                        <div class="list-icons">
                                            <ul>
                                                <li>
                                                    <img class="pull-left" src="/assets/img/icons/cart.png" alt=""> <strong> {{Auth::user()->product()->count()}} </strong>
                                                    <div>
                                                        <small>Мои товары</small>
                                                    </div>
                                                </li>
                                                <li>
                                                    <img class="pull-left" src="/assets/img/icons/delivery.png" alt=""> <strong> {{ Auth::user()->productOrders()->count() }} </strong>
                                                    <div>
                                                        <small>Товаров заказали</small>
                                                    </div>
                                                </li>
                                            </ul>

                                            <ul>
                                                <li>
                                                    <img class="pull-left" src="/assets/img/icons/like.png" alt=""> <strong> {{ Auth::user()->productFavourites()->count() }} </strong>
                                                    <div>
                                                        <small>Добавили в избранное</small>
                                                    </div>
                                                </li>
                                                <li>
                                                    <img class="pull-left" src="/assets/img/icons/message.png" alt=""> <strong> {{ Auth::user()->productComments()->count() }} </strong>
                                                    <div>
                                                        <small>Отзывы покупателей</small>
                                                    </div>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li>
                                                    <img class="pull-left" src="/assets/img/icons/shared.png" alt=""> <strong> {{ Auth::user()->productComments()->count() }} </strong>
                                                    <div>
                                                        <small>Поделились товаром</small>
                                                    </div>
                                                </li>
                                                <li>
                                                    <img class="pull-left" src="/assets/img/icons/phone.png" alt=""> <strong> {{ Auth::user()->shops()->sum('phone_view_amount') }} </strong>
                                                    <div>
                                                        <small>Посмотрели номер</small>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <p class="text-center" style="margin-bottom:10px;margin-top:20px;">
                                            <strong>Заказы</strong>
                                        </p>
                                        @foreach(Auth::user()->countOrderByType() as $status)
                                        <div class="counter-list__item">
                                            <span class="counter-list__key">
                                               {{$status[1]}}
                                            </span>

                                             <span class="counter-list__value">
                                                {{$status[0]}}
                                            </span>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
						@else
                            <div class="cabinet-menu">
                                <div class="cabinet-menu-header"><br>
                                    <p>
                                        <strong><?=Auth::user()->name?></strong>
                                    </p>
                                    <p>
                                        <small><?=Auth::user()->lastname?></small>
                                    </p>
                                </div>
                                <hr>
                                <div class="cabinet-menu-nav">
                                    <a href="{{ action('CabinetController@getFavorite') }}" class="cabinet__panel-nav-item {{ active_class(if_action('App\Http\Controllers\CabinetController@getFavorite'), 'is-active') }}">Избранные товары</a>

                                    <a href="{{ action('CabinetController@getReviews') }}" class="cabinet__panel-nav-item {{ active_class(if_action('App\Http\Controllers\CabinetController@getReviews'), 'is-active') }}">Мои отзывы</a>

                                    <a href="{{ action('CabinetController@getProfile') }}" class="cabinet__panel-nav-item {{ active_class(if_action('App\Http\Controllers\CabinetController@getProfile'), 'is-active') }}">Настройки</a>

                                    <a href="{{ action('CabinetController@getMyOrders') }}" class="cabinet__panel-nav-item {{ active_class(if_action('App\Http\Controllers\CabinetController@getMyOrders'), 'is-active') }}">Мои заказы</a>
                                </div>
                            </div>

                            <div class="cabinet-questions cabinet-block">
                                <strong>Если я не знаю ...</strong>
                                <div style="padding-left:30px;">
                                    <p>
                                        <a href="https://osto.uz/page/oformlenie-zakaza"><i>Как оформить заказ?</i></a>
                                    </p>
                                    <p>
                                        <a href="https://osto.uz/page/kak-vernut-tovar"><i>Как вернуть товар?</i></a>
                                    </p>
                                    <p>
                                        <a href="https://osto.uz/page/tablitsa-razmerov"><i>Как определить размер?</i></a>
                                    </p>
                                </div>
                            </div>
						@endif
					</div>

					<div class="col-sm-8 col-md-9">
						<div class="cabinet-title text-right" style="margin-bottom: 20px;">
							<small>ID: {{ Auth::user()->id}}</small>
							@if(Auth::user()->hasRole('admin'))
                                <span style="margin-left:10px;">
                                    <a href="{{ action('Backend\PageController@getDashboard') }}" class="btn btn--green cabinet__panel-logout" style="padding:3px 10px;">
                                        Админка
                                    </a>
                                </span>
                            @endif
						</div>

						<section class="cabinet__panel">
							<div class="cabinet__panel-content">
								@yield('content')
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>

		<!-- Footer -->
		@include('partials.footer')

		<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
		<script src="{{ asset('assets/js/jquery.mask.js') }}"></script>
		<script src="{{ asset('assets/js/slick.min.js') }}"></script>
		<script src="{{ asset('assets/js/select2.full.js') }}"></script>
		<script src="{{ asset('assets/js/sweetalert2.js') }}"></script>
		<!-- <script src="{{ asset('assets/js/sweetalert.min.js') }}"></script> -->
		<script src="{{ asset('assets/js/helpers.js') }}"></script>
		<script src="{{ asset('assets/js/jquery.autocomplete.min.js') }}"></script>
		<script src="{{ asset('assets/js/app.js') }}"></script>
		<script src="{{ asset('assets/js/cart.js') }}"></script>
		@yield('scripts')
	</body>
</html> 
<!-- S.Mirakhmedov - numerotech - v.1.1 -->
