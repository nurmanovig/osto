<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=0.8, maximum-scale=0.8 user-scalable=no">
		<meta name="author" content="Sanjar Mirakhmedov | #DarkC0der |">

		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&amp;subset=cyrillic" rel="stylesheet">

		<link href="{{ asset('assets/css/styles.min.css?v=1.1') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/cart.css?v=1.1') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/sk.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/ln_style.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/css/ln_style_new_items.css') }}" rel="stylesheet">

		<link href="{{ asset('/assets/css/inner-product/bootstrap.min.css') }}" />
		<link href="/assets/css/inner-productlibs/font-awesome-4.7.0/css/font-awesome.min.css" />
		<link href="{{ asset('/assets/css/inner-product/slick.css') }}" />
		<link href="{{ asset('/assets/css/inner-product/animate.css') }}">
		<link href="{{ asset('/assets/css/inner-product/hover.css') }}">
		<link href="{{ asset('/assets/css/inner-product/jquery.fancybox.css') }}" />
		<link href="/assets/css/innser-product/main.css" />
		<link href="{{ asset('/assets/css/inner-product/media.css') }}" />

		
		<meta name="description" content="{{ (isset($meta)) ? $meta['description'] : Lang::get('main.seo.main_page.description') }}">
		<title>{{ (isset($meta)) ? $meta['title'] : Lang::get('main.seo.main_page.title') }}</title>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="/{{ asset('assets/imgapple-touch-icon.png') }}" />
		<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/img/apple-touch-icon-57x57.png') }}" />
		<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/img/apple-touch-icon-72x72.png') }}" />
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-touch-icon-76x76.png') }}" />
		<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/img/apple-touch-icon-114x114.png') }}" />
		<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/img/apple-touch-icon-120x120.png') }}" />
		<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/img/apple-touch-icon-144x144.png') }}" />
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/img/apple-touch-icon-152x152.png') }}" />
		<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/apple-touch-icon-180x180.png') }}" />
		<!--LiveInternet counter--><script type="text/javascript">
        new Image().src = "//counter.yadro.ru/hit?r"+
        escape(document.referrer)+((typeof(screen)=="undefined")?"":
        ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
        screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
        ";h"+escape(document.title.substring(0,150))+
        ";"+Math.random();</script><!--/LiveInternet-->


		@yield('styles')
		<link href="{{ asset('assets/css/15.01.19.css') }}" rel="stylesheet">

	</head>
	<body class="body {{ active_class(!if_route('home'), 'nav-page') }}"> 
	
		<div id="app">
		<div class="preloader" style="display: none;">
			<div class="preloader__circle"></div>
		</div>
		
		@include('partials.menu')

		<div class="wrapper">
			@include('partials.navbar')
			
			@yield('content')	
			
			@include('partials.footer')
		</div>

		</div>
		
		<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
		<script src="{{ asset('assets/js/slick.min.js') }}"></script>
		<script src="{{ asset('assets/js/select2.full.js') }}"></script>

		<script src="{{ asset('assets/js/sweetalert2.js') }}"></script>
		<script src="{{ asset('assets/js/helpers.js') }}"></script>
		<script src="{{ asset('assets/js/jquery.autocomplete.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/jquery.matchHeight.js') }}"></script>
		<script src="{{ asset('assets/js/app.js') }}"></script>
		<script src="{{ asset('assets/js/favorite.js') }}"></script>
		<script src="{{ asset('assets/js/cart.js') }}"></script>

		<script type="text/javascript" src="{{ asset('assets/js/jquery.mask.js') }}"></script>

		@if(Request::has('login'))
		<script type="text/javascript"> 
				App.openLogin();
		</script>
		@endif
		@yield('scripts')
	</body>
</html> 
<!-- S.Mirakhmedov - numerotech - v.1.1 -->
