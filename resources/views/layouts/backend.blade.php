<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>OSTO.UZ :: ADMIN PANEL</title>
        <link href="{{ asset('backend/less/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/styles.min.css') }}" rel="stylesheet">
        <link href="{{ asset('backend/less/styles.css') }}" rel="stylesheet">
        <link href="{{ asset('backend/css/bootstrap-datetimepicker.min.css')  }}">
        <link href="{{ asset('backend/css/select2.min.css')  }}">
        <link href="{{ asset('backend/css/style.css')  }}">
<link href="{{ asset('assets/css/ln_style_new_items.css') }}" rel="stylesheet">
        
    </head>
    <body class="">

        <div class="overlay"></div>
        
        <div class="wrapper">
            <nav class="navbar">
                <a href="{{ url('/') }}" class="navbar__brand" style="background-image: url({{ asset('assets/img/logo-osto-1.png')}}); background-size:auto 40px"></a>

                <button class="navbar__toggle">
                    <span></span>
                </button>

                <ul class="navbar__nav">
                    
                    <!-- Logout button -->
                    <li class="navbar__list navbar__list--end">
                        <a href="{{ url('/logout') }}" class="navbar__link navbar__logout">
                            <i class="fa fa-sign-out"></i>
                        </a>
                    </li>
                </ul>
            </nav>

            <aside class="sidebar">
                <p class="sidebar__label">MAIN</p>

                <ul class="sidebar__nav">

                    <!-- List item -->
                    <!-- <li class="sidebar__list sidebar__list--drop-toggle">
                        <a href="#" class="sidebar__link">
                            <i class="fa fa-anchor sidebar__icon"></i>
                            <span class="sidebar__text">Link 1</span>
                            <i class="fa fa-angle-right sidebar__caret"></i>
                            <span class="tag tag-purple sidebar__tag">8</span>
                        </a>
                        

                        <ul class="sidebar__dropdown">
                            
                            <li class="sidebar__mobile-text">Link 1</li>

                            <li class="sidebar__list">
                                <a href="#" class="sidebar__link sidebar__link--dropdown">Drop 1</a>
                            </li>

                            <li class="sidebar__list">
                                <a href="#" class="sidebar__link sidebar__link--dropdown">Drop 2</a>
                            </li>

                            <li class="sidebar__list">
                                <a href="#" class="sidebar__link sidebar__link--dropdown">Drop 3</a>
                            </li>
                        </ul>
                    </li> -->
                    
                    <!-- List item -->
                    <li class="sidebar__list @if(Request::is('mypanel')) sidebar__list--active @endif">
                        <a href="{{ url('mypanel') }}" class="sidebar__link">
                            <i class="fa fa-dashboard sidebar__icon"></i>
                            <span class="sidebar__text">Dashboard</span>
                        </a>
                    </li>

                    <li class="sidebar__list @if(Request::is('mypanel/page*')) sidebar__list--active @endif">
                        <a href="{{ url('mypanel/page') }}" class="sidebar__link">
                            <i class="fa fa-file sidebar__icon"></i>
                            <span class="sidebar__text">Страницы</span>
                        </a>
                    </li>

                    <li class="sidebar__list @if(Request::is('mypanel/order*')) sidebar__list--active @endif">
                        <a href="{{ url('mypanel/orders') }}" class="sidebar__link">
                            <i class="fa fa-file sidebar__icon"></i>
                            <span class="sidebar__text">Заказы <span class="badge">{{ \App\Models\Order::countPaid()}}</span></span>
                        </a>
                    </li>


                    <li class="sidebar__list sidebar__list--drop-toggle">
                        <a href="#" class="sidebar__link">
                            <i class="fa fa-shopping-cart  sidebar__icon"></i>
                            <span class="sidebar__text">Продукты</span>
                            <i class="fa fa-angle-right sidebar__caret"></i>
                        </a>
                        
                        <ul class="sidebar__dropdown">
                            <li class="sidebar__list  @if(Request::is('mypanel/product*')) sidebar__list--active @endif">
                                <a href="{{ url('mypanel/product') }}" class="sidebar__link sidebar__link--dropdown"> Список продуктов</a>
                            </li>

                            <li class="sidebar__list @if(Request::is('mypanel/category*')) sidebar__list--active @endif">
                                <a href="{{ url('mypanel/category') }}" class="sidebar__link sidebar__link--dropdown"> Категории</a>
                            </li>
                            <li class="sidebar__list @if(Request::is('mypanel/icon*')) sidebar__list--active @endif">
                                <a href="{{ url('mypanel/icon') }}" class="sidebar__link sidebar__link--dropdown"> Иконки</a>
                            </li>

                            <li class="sidebar__list @if(Request::is('mypanel/size-product*')) sidebar__list--active @endif">
                                <a href="{{ url('mypanel/size-product') }}" class="sidebar__link sidebar__link--dropdown"> Размеры товаров</a>
                            </li>

                            <li class="sidebar__list @if(Request::is('mypanel/products/sort-setting*')) sidebar__list--active @endif">
                                <a href="{{ url('mypanel/products/sort-setting') }}" class="sidebar__link sidebar__link--dropdown"> Настройка сортировки</a>
                            </li>
                        </ul>
                    </li>

                    <li class="sidebar__list @if(Request::is('mypanel/user*')) sidebar__list--active @endif">
                        <a href="{{ action('Backend\UserController@getIndex') }}" class="sidebar__link">
                            <i class="fa fa-user-circle-o sidebar__icon"></i>
                            <span class="sidebar__text">Пользователи</span>
                        </a>
                    </li>

                    <li class="sidebar__list @if(Request::is('mypanel/salesman*')) sidebar__list--active @endif">
                        <a href="{{ action('Backend\SalesmanController@getIndex') }}" class="sidebar__link">
                            <i class="fa fa-user-o sidebar__icon"></i>
                            <span class="sidebar__text">Продавцы</span>
                        </a>
                    </li>    

                     <li class="sidebar__list @if(Request::is('mypanel/shops*')) sidebar__list--active @endif">
                        <a href="{{ action('Backend\ShopController@getIndex') }}" class="sidebar__link">
                            <i class="fa fa-shopping-bag sidebar__icon"></i>
                            <span class="sidebar__text">Магазины</span>
                        </a>
                    </li>

                    <li class="sidebar__list @if(Request::is('mypanel/service*')) sidebar__list--active @endif">
                        <a href="{{ action('Backend\SettingController@getShow') }}" class="sidebar__link">
                            <i class="fa fa-file sidebar__icon"></i>
                            <span class="sidebar__text">Услуги</span>
                        </a>
                    </li>
                    
                    <li class="sidebar__list @if(Request::is('mypanel/comment*')) sidebar__list--active @endif">
                        <a href="{{ action('Backend\CommentController@getIndex') }}" class="sidebar__link">
                            <i class="fa fa-comment  sidebar__icon"></i>
                            <span class="sidebar__text">Комментарии</span>
                        </a>
                    </li>

                    <li class="sidebar__list @if(Request::is('mypanel/feedback*')) sidebar__list--active @endif">
                        <a href="{{ action('Backend\FeedbackController@getIndex') }}" class="sidebar__link">
                            <i class="fa fa-question sidebar__icon"></i>
                            <span class="sidebar__text">Обратная связь</span>
                        </a>
                    </li>





                    <li class="sidebar__list sidebar__list--drop-toggle">
                        <a href="#" class="sidebar__link">
                            <i class="fa fa-shopping-cart  sidebar__icon"></i>
                            <span class="sidebar__text">Доп. Функции</span>
                            <i class="fa fa-angle-right sidebar__caret"></i>
                        </a>

                        <ul class="sidebar__dropdown">
                            <li class="sidebar__list @if(Request::is('mypanel/promo*')) sidebar__list--active @endif">
                                <a href="{{ action('Backend\PromoCodeController@getIndex') }}" class="sidebar__link">
                                    <i class="fa fa-file sidebar__icon"></i>
                                    <span class="sidebar__text">Промо коды</span>
                                </a>
                            </li>
                            <li class="sidebar__list @if(Request::is('mypanel/banner*')) sidebar__list--active @endif">
                                <a href="{{ action('Backend\BannerController@getIndex') }}" class="sidebar__link">
                                    <i class="fa fa-file sidebar__icon"></i>
                                    <span class="sidebar__text">Баннеры</span>
                                </a>
                            </li>
                            <li class="sidebar__list @if(Request::is('mypanel/sliders*')) sidebar__list--active @endif">
                                <a href="{{ action('Backend\SliderController@getIndex') }}" class="sidebar__link">
                                    <i class="fa fa-file sidebar__icon"></i>
                                    <span class="sidebar__text">Слайдер</span>
                                </a>
                            </li>
                            <li class="sidebar__list @if(Request::is('mypanel/information*')) sidebar__list--active @endif">
                                <a href="{{ action('Backend\InformationController@getIndex') }}" class="sidebar__link">
                                    <i class="fa fa-file sidebar__icon"></i>
                                    <span class="sidebar__text">Информационный раздел</span>
                                </a>
                            </li>
                            <li class="sidebar__list @if(Request::is('mypanel/filemanager*'))sidebar__list--active @endif">
                                <a href="{{ url('mypanel/filemanager') }}" class="sidebar__link">
                                    <i class="fa fa-file-image-o sidebar__icon"></i>
                                    <span class="sidebar__text">Файловый менеджер</span>
                                </a>
                            </li>

                            <li class="sidebar__list @if(Request::is('mypanel/key*'))sidebar__list--active @endif">
                                <a href="{{ url('mypanel/key') }}" class="sidebar__link">
                                    <i class="fa fa-file-image-o sidebar__icon"></i>
                                    <span class="sidebar__text">Ключивые слова</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </aside>

            <main class="content">
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissable">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <p>{!! Session::get('success') !!}</p>
                    </div>
                @endif

                @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissable">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <p>{!! Session::get('error') !!}</p>
                    </div>
                @endif

                @foreach($errors->all() as $message)
                    <div class="alert alert-danger alert-dismissable">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <p>{{$message}}</p>
                    </div>
                @endforeach
                @yield('content')
            </main> 
        </div>

        <script src="{{ asset('backend/js/jquery.min.js') }}"></script>
        <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('backend/js/tinymce.min.js') }}"></script>
        <script src="{{ asset('backend/js/jquery.tinymce.min.js') }}"></script>
        <script src="{{ asset('backend/js/jquery.cookie.js') }}"></script>
        <script src="{{ asset('backend/js/select2.min.js') }}"></script>
        <script src="{{ asset('backend/js/admin.js') }}"></script>
        <script src="{{ asset('backend/js/bootstrap-datetimepicker.min.js') }}"></script>
        <script src="{{ asset('backend/js/cart.js') }}"></script>

        @yield('scripts')

    </body>
</html>