

<div class="lnallm_container">
    <div class="lnallm_top_h4">
        <h4>Все магазины</h4>
    </div>
    <div class="lnallm_all">
        @foreach($shops as $item)
        <div class="lnallm_all_1">
            <div class="lnallm_all_1_img">
                <?php $fullname = "/uploads/thumb/shop/200x200/".$item->id."/".$item->main_image;?>
                <img src="{{ Upload::hasImage($fullname) ? : '/assets/img/temp/category_img.jpg'}}" alt="Shop image">
            </div>
            <div class="lnallm_all_1_content">
                <h6>{{ $item->title }}</h6>
                <p>{{$item->address}}</p>
                <p>Режим работы: <span>{{$item->working_start}} - {{$item->working_end}}</span></p>
                <p>
                    {{$item->day1 ? "Пн, " : null}}
                    {{$item->day2 ? "Вт, " : null}}
                    {{$item->day3 ? "Ср, " : null}}
                    {{$item->day4 ? "Чт, " : null}}
                    {{$item->day5 ? "Пт, " : null}}
                    {{$item->day6 ? "Сб, " : null}}
                    {{$item->day7 ? "Вс, " : null}}
                </p>
                <a href="{{ action('ShopController@show', $item->alias) }}">Перейти в магазин</a>
            </div>
        </div>
        @endforeach
    </div>
</div>