<article class="card card--expand @if(!Auth::check()) card--hasNoFavButton @endif">
	<div class="card__viewport">
		@if($product->sale_status)
							<span class="sale_product">{{ $product->sale_value }} %</span>
						@endif
		<a class="card__img-cont ln_img_cont_article" href="/product/view/{{ $product->alias }}">
			<div class="card__img-cover">
				<i class="icon-eye centered"></i>

				<button class="likes likes--white card__likes">
					<i class="icon-like"></i>
					<span class="likes__amount">{{ $product->likes()->count() }}</span>
				</button>
			</div>

			<?php $fullname = "/uploads/thumb/product/250x375/".$product->id."/".$product->main_image;?>
						<img src="{{ Upload::hasImage($fullname) ? : '/assets/img/noimage.png'}}" alt="Shop image">
		</a>

		<div class="card__content">
			<a class="card__title" href="/product/view/{{ $product->alias }}">
                            {{ $product->title }}
            <?php if($product->category):?>, 
			<strong>

			    {{ $product->category->title }}

			</strong>
			<?php endif;?>
			</a>
			@if($product->sale_value )
				<div class="card__purpl actual_price">{{ $product->price_format }} {{ Config::get('site.currency')[$product->currency] }}</div>

				<div class="card__purple-text">{{ salePrice($product->price, $product->sale_value) }} {{ Config::get('site.currency')[$product->currency] }}</div>

			@else
				<div class="card__purple-text">{{ $product->price_format }} {{ Config::get('site.currency')[$product->currency] }}</div>

			@endif
			

		</div>
			@if(Auth::check())
				<?php
					$favorite = null;
			        if(Auth::check()){
			          $favorite = App\Models\Favourite::where('product_id', $product->id)->where('user_id', Auth::user()->id)
			                ->first();
			        } 
				?>
				<!-- <a class="card__btn-bottom  add-cart" data-id="{{ $product->id }}" title="Добавить в корзину" >
					<span class="js-favourite-text">Добавить в корзину</span></a> -->
				
			@endif
	</div>
</article>