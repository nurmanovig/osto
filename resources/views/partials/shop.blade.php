@if($shop->mall_id!=0)
<article class="card card--default">
	<div class="card__viewport">
		<a class="card__img-cont" href="{{ action('ShopController@show', $shop->alias) }}">
			<div class="card__img-cover">
				<i class="icon-eye centered"></i>

				<button class="likes likes--white card__likes">
					<i class="icon-like"></i>
					<span class="likes__amount">0</span>
				</button>
			</div>
			<?php $fullname = "/uploads/thumb/shop/200x200/".$shop->id."/".$shop->main_image;?>
		
						<img src="{{ Upload::hasImage($fullname) ? : '/assets/img/noimage-shop.png'}}" alt="Shop image" class="">
		</a>

		<div class="card__content ln_shop_content_main">
			<a class="card__title" href="{{ action('ShopController@show', $shop->alias) }}"><strong>{{ $shop->title }}</strong>, {{ $shop->mall->title }}</a>
			<div class="card__purple-text nowrap">{{ $shop->products->count() }} товаров</div>		
		</div>
	</div>
</article>
@endif