@foreach($products as $product)
	<div class="card-horizontal">

		<!-- Rasm razmeri 208x200 -->
		<a href="/product/view/{{ $product->alias }}" style="background-image: url('{{ (!Upload::hasFiles('product',$product->id)) ? '/assets/img/noimage.png' : Upload::getThumbFiles('product',$product->id,'208x200')[0] }}');" class="card-horizontal__img" data-mh="list_mh"></a>

		<div class="card-horizontal__content" data-mh="list_mh">
			<a class="card-horizontal__title" href="/product/view/{{ $product->alias }}">
				{{ $product->title }}
			</a>

			{{-- <div class="card-horizontal__category">
				{{$product->category->title }}
			</div> --}}

			<div class="card-horizontal__price">
				{{ $product->price_format }} {{ Config::get('site.currency')[$product->currency] }}
			</div>

			<div class="card-horizontal__text">{{ str_limit($product->short, 120) }}</div>

			<span class="card-horizontal__divider"></span>

			<a href="/product/view/{{ $product->alias }}" class="btn btn--purple m-r-5">Перейти к товару</a>
			<a href="#" data-id="{{ $product->id }}" class="btn-outline btn-outline--purple btn-square m-r-5 add-cart" title="Добавить в корзину">
				<i class="icon-cart"></i>
			</a>
			@if(Auth::check())
				<?php
					$favorite = null;
			        if(Auth::check()){
			          $favorite = App\Models\Favourite::where('product_id', $product->id)->where('user_id', Auth::user()->id)
			                ->first();
			        } 
				?>
				
				<a data-id="{{ $product->id }}" class="btn-outline btn-outline--purple btn-square m-r-5 js-favourite" title="Добавить в избранное">+</a>

				
			@endif	
			
			
			<span class="card-horizontal__like-amount">
				<i class="icon-like"></i>
				{{ $product->likes()->count() }}
			</span>
		</div>
	</div>
@endforeach


