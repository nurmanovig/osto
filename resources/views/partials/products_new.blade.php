<div class="lnodej_all_items">
    @foreach($products as $product)

        <a href="/product/view/{{ $product->alias }}" class="lnodej_all_items_pod">
            <div class="lnodej_all_items_pod_imgs">
                <?php $fullname = "/uploads/thumb/product/250x375/".$product->id."/".$product->main_image;?>
                <img src="{{ Upload::hasImage($fullname) ? : '/assets/img/noimage.png'}}" alt="Shop image">

            </div>
            <div class="lnodej_all_items_contern">
                <p>{{$product->title}}</p>
                <div class="lnodej_all_items_contern_slae">
                    @if($product->sale_value )
                        <h6 class="lnodej_all_items_contern_slae11">{{ $product->price_format }} {{ Config::get('site.currency')[$product->currency] }}</h6>

                        <h6 class="lnodej_all_items_contern_slae22">{{ salePrice($product->price, $product->sale_value) }} {{ Config::get('site.currency')[$product->currency] }}</h6>

                    @else
                        <h6 class="lnodej_all_items_contern_slae22">{{ $product->price_format }} {{ Config::get('site.currency')[$product->currency] }}</h6>

                    @endif
                </div>
            </div>
        </a>
    @endforeach
    <div class="text-right">
        <?php $params = Request::all() ?>
        {{ $products->appends($params)->links() }}
    </div>
</div>