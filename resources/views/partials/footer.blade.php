<style>

    h4 {
        margin: 25px 0 10px;
    }

    .footer__info {
        margin-bottom: 25px;
    }
    
    .footer .container .row .col-sm-3 ul li a {
        line-height: 25px;
    }
    
    .footer__socials {
        padding: 0;
        float: none;
    }
    
    .footer__info .socials {
        margin-left: -5px;
        margin-top: 10px;
    }
    
    .socials__item {
        margin: 0 10px;
    }
    
    .socials__item img {
        width: 32px;
        height: 32px;
    }
    
</style>

<footer class="footer">
	<div class="container">
		<div class="row footer__info">
			<div class="col-sm-3">
			    <h4>О компании</h4>
			    <ul>
    				<li><a href="/page/o-nas" target="blanc_">О нас</a></li>
    				<li><a href="/page/vakansii" target="blanc_">Вакансии</a></li>
    				<li><a href="/feedback" target="blanc_">Обратная связь</a></li>
    				<li><a href="/page/polzovatelskoe-soglashenie" target="blanc_">Пользовательское соглашение</a></li>
				</ul>	
			</div>
			<div class="col-sm-3">
			    <h4>Помощь</h4>
			    <ul>
    			    <li><a href="/page/tablitsa-razmerov" target="blanc_">Как определить размер?</a></li>
    				<li><a href="/page/oformlenie-zakaza" target="blanc_">Как оформить заказ?</a></li>
    				<li><a href="/page/kak-vernut-tovar" target="blanc">Как вернуть товар?</a></li>
				</ul>
			</div>
			<div class="col-sm-3">
			    <h4>Контакты:</h4>
			    <ul>
    			    <li><a href="#">(+998) 71 231 09 11</a></li>
    				<li><a href="#">(+998) 99 897 95 57</a></li>
    				<li><a href="#">Режим работы: Пн-Пт 9:00-18:00</a></li>
    				<li><a href="#">e-mail: info@osto.uz</a></li>
				</ul>
			</div>
			<div class="col-sm-3">
				<div class="footer__socials">
					<h4>Следите за нами</h4>
				    <ul class="socials socials--white row">
						<li class="socials__item">
							<a href="https://www.facebook.com/osto.uz/" class="socials__link" target="blanc_"><img src="/assets/img/footer-icons/facebook.png"></a>
							<a href="https://t.me/osto.uz" class="socials__link" target="blanc_"><img src="/assets/img/footer-icons/telegram.png"></a>
						</li>
						<li class="socials__item">
							<a href="https://www.instagram.com/osto.uz/" class="socials__link" target="blanc_"><img src="/assets/img/footer-icons/instagram.png"></a>
							<a href="#" class="socials__link" target="blanc"><img src="/assets/img/footer-icons/youtube.png"></a>
    					</li>
    					<li>
   
					
    					</li>
					</ul>

				</div>
			</div>
			
		</div>
		
		
	</div>
	<div class="footer__copy p-y-15">
		<div class="container">
			<div class="row">
				<div class="col-sm-10">
					<span class="footer__copy-text">&copy; 2018 Osto.uz Все права защищены.<br/>
					Полное или частичное копирование материалов Сайта разрешено только с письменного разрешения владельца Сайта</span>
				</div>
                <div class="col-sm-2">
				<!--LiveInternet logo--><a href="//www.liveinternet.ru/click"
                    target="_blank"><img src="//counter.yadro.ru/logo?18.6"
                    title="LiveInternet: показано число просмотров за 24 часа, посетителей за 24 часа и за сегодня"
                    alt="" border="0" width="88" height="31"/></a><!--/LiveInternet-->
                </div>
			</div>
		</div>
	</div>
</footer>

<div id="w3lssbmincart">
	
</div>
