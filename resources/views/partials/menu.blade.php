<div class="overlay" style="display: none;"></div>

<div class="login" style="display: none;">
	<button class="login__close js-login-close">
		&times;
	</button>

	<div class="login__content centered">

		<div class="text-center">
			<h3 class="title title--light login__title">АВТОРИЗАЦИЯ</h3>
		</div>
		
		{!! Form::open(['action' => 'UserController@postAjaxLogin', 'method' => 'POST', 'class' => 'js-auth-form']) !!}
			<div class="form-group">

				<label class="login__input-label">Тел.номер или Эл. почта</label>
				<input type="text" name="login" class="input small-shade tr-border login__input">
			</div>

			<div class="form-group">
				<label class="login__input-label">Пароль</label>
				<input type="password" name="password" class="input small-shade tr-border login__input">
			</div>
			<div class="form-group">
				<div class="form-group__label form-group__label--error js-auth-message">

				</div>
			</div>

			<div class="row">
				<div class="col-sm-6 form-group">
					<button class="btn btn--green btn--block" type="submit">Войти</button>
				</div>


				<div class="col-sm-6 form-group">
					<a class="btn btn--white btn--block" href="/register">Регистрация</a>
				</div>
			</div>
		{{ Form::close() }}
	</div>
</div>

<div class="menu" style="display: none;">
	<div class="menu-primary shops">
		<div class="menu-col-main">
			<ul class="menu__nav centered-y">
				<li class="menu__item"></li>
			</ul>
		</div>
	</div>
</div>