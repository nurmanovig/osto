<!-- Results all products -->
<div class="row">
	@foreach($products as $product)
		<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 m-b-5">
			<article class="card m-x-0 m-b-20">
				<div class="card__viewport">

					<a class="card__img-cont ln_card__img-cont " href="/product/view/{{ $product->alias }}">

						@if($product->sale_status)
							<span class="sale_product">{{ $product->sale_value }}%</span>
						@endif
						<div class="card__img-cover">
							<i class="icon-eye centered"></i>

							<button class="likes likes--white card__likes">
								<i class="icon-like"></i>
								<span class="likes__amount">{{ $product->likes()->count() }}</span>
							</button>
						</div>
						<?php $fullname = "/uploads/thumb/product/250x375/".$product->id."/".$product->main_image;?>
						<img src="{{ Upload::hasImage($fullname) ? : '/assets/img/noimage.png'}}" alt="Shop image">
					</a> 
					
					
					<div class="card__content">
						<a class="card__title" href="#">
							{{-- <strong>
								{{ $product->category->title }}, 
							</strong> --}}
							{{ $product->title }}
						</a>
						@if($product->sale_value )
							<div class="card__purpl actual_price">{{ $product->price_format }} {{ Config::get('site.currency')[$product->currency] }}</div>

							<div class="card__purple-text">{{ salePrice($product->price, $product->sale_value) }} {{ Config::get('site.currency')[$product->currency] }}</div>

						@else
							<div class="card__purple-text">{{ $product->price_format }} {{ Config::get('site.currency')[$product->currency] }}</div>

						@endif
					</div>

					<!-- <a class="card__btn-bottom  add-cart" data-id="{{ $product->id }}" title="Добавить в корзину" >
					<span class="js-favourite-text">Добавить в корзину</span></a> -->
				</div>
			</article>
		</div>
	@endforeach
</div>	