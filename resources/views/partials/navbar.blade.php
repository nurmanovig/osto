<?php
use Illuminate\Support\Facades\Session;
?>
<style>

</style>
<!-- HAMBURGER !!! MENU -->
<button class="hamburger js-menu-hamburger">
	<span></span>
	<span></span>
	<span></span>
</button>

<!-- Main top navigation -->
<nav class="navbar nav-zindex">	
	<div class="col-xs-6 col-xs-push-3 p-x-0 text-center">
		<a href="/" class="navbar__logo icon-osto_logo"></a>
	</div>
<!-- <div class="col-xs-1 col-xs-push-1 location">
	<button class="btn_new btn-default location_btn location_address" title="{{ SESSION::has("location.address") ? SESSION::get("location.address")[0] : "Указать свой адрес"}}">{{ SESSION::has("location.address") ? SESSION::get("location.address")[0] : "Указать свой адрес"}}</button>
	
</div> -->
	<div class="col-xs-6 col-sm-4 col-xs-push-1 col-sm-push-2 p-x-0 text-right">
		
		
		<div class="navbar__right" style="float:right">

			@if(!Auth::check())
				<a href="#" class="js-user-btn navbar__sign-in">
					<i class="icon-sign-in"></i> <span class="hidden-sm">Войти</span>
				</a>
			@else
				@if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('salesman'))
				<a href="{{ action('CabinetController@getIndex') }}" class="navbar__username m-r-5">{{ substrName(Auth::user()->name) }}</a>
				@else
				<a href="{{ action('CabinetController@getFavorite') }}">{{ Auth::user()->name }}</a>
				@endif

				|

				<a class="navbar__logout m-l-5" href="/logout">
					<span class="hidden-sm">Выйти</span>
					<i class="icon-sign-in"></i>
				</a>
			@endif
		</div>

		<div class="navbar__right" style="float:right">
		
			<div class="cart cart box_1"> 

				<button class="w3view-cart">

					<svg viewBox="0 0 483.1 483.1">
	<path d="m434.55 418.7l-27.8-313.3c-0.5-6.2-5.7-10.9-12-10.9h-58.6c-0.1-52.1-42.5-94.5-94.6-94.5s-94.5 42.4-94.6 94.5h-58.6c-6.2 0-11.4 4.7-12 10.9l-27.8 313.3v1.1c0 34.9 32.1 63.3 71.5 63.3h243c39.4 0 71.5-28.4 71.5-63.3v-1.1zm-193-394.7c38.9 0 70.5 31.6 70.6 70.5h-141.2c0.1-38.9 31.7-70.5 70.6-70.5zm121.5 435h-243c-26 0-47.2-17.3-47.5-38.8l26.8-301.7h47.6v42.1c0 6.6 5.4 12 12 12s12-5.4 12-12v-42.1h141.2v42.1c0 6.6 5.4 12 12 12s12-5.4 12-12v-42.1h47.6l26.8 301.8c-0.3 21.4-21.5 38.7-47.5 38.7z"/>
</svg>




					<span class="badge">0</span>
				</button>

			</div>

		</div>

	</div>
</nav>