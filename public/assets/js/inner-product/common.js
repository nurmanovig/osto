var slider_product = {

  init : function(){
    $(function () {
      $('.bigzoom').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.litleslider_zoom'
      });
      $('.litleslider_zoom').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.bigzoom',
        // centerMode: true,
        focusOnSelect: true,
        vertical: true,
        arrows: false,
        draggable: false,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              infinite: true,
            }
          },
          // {
          //   breakpoint: 601,
          //   settings: {
          //     slidesToShow: 3,
          //     slidesToScroll: 1,
          //     // vertical: false,
          //   }
          // },
          // {
          //   breakpoint: 480,
          //   settings: {
          //     slidesToShow: 2,
          //     slidesToScroll: 1,
          //     // vertical: false,
          //   }
          // }
        ]
      });
    })

  }
}