$(function(){
	getCount();
	$(".w3view-cart").click(function(e){
		e.preventDefault();
		getCart(true);		
	})

	$("body").on("click", "#w3lssbmincart .sbmincart-closer", function(e){
		e.preventDefault();
		$("#w3lssbmincart").removeClass("active");
	});

	// $("body").on("click", ".size_product", function(e){
	// 	$(".size_product").removeClass("active");
	// 	$(this).addClass("active");
	// })


    //Вы не выбрали
    $("body").on("click", ".add-cart", function(e){
		e.preventDefault();
		var id = $(this).data("id");
		var size = parseInt($("#size_select").val());
        var sizes = $(this).data("sizes");

        if(!size){

			$("#size-modal").addClass("active");

		} else {
			addCart(id, size);
		}
	});

	$('#sk-buy-btn').click(function(e){
		e.preventDefault();
		
		var sk_size = $("#size-modal").find('select').val(),
			id = $('.add-cart').data("id");

		$("#size-modal").removeClass("active");

		addCart(id, sk_size);
	})



	/* CHANGE item in cart*/
	$("body").on("click", ".sbmincart-minus, .sbmincart-plus", function(e){
		e.preventDefault();
		var this_elem = $(this);
		var id = this_elem.parent().parent().parent().data("id");
		var size = this_elem.parent().parent().parent().find("li.size").data("size");
		var count = parseInt(this_elem.parent().find(".sbmincart-quantity").val());

		if(this_elem.hasClass("sbmincart-plus")){
			count++;
		}else if(count > 1){
			count--;
		}else{
			count = 1;
		}

		////console.log(size);

		changeCart(id, count, size);

		if($(".cart_info").html()){
			cartPart();
		}
	});

	$("body").on("click", ".cart_quantity_up, .cart_quantity_down", function(e){
		e.preventDefault();
		var this_elem = $(this);
		var id = this_elem.parent().parent().parent().data("id");
		var count = parseInt(this_elem.parent().find(".cart_quantity_input").val());
		var size = this_elem.parent().parent().parent().find("small.size").data("size");
		
		if(this_elem.hasClass("cart_quantity_down")){
			count++;
		}else if(count > 1){
			count--;
		}else{
			count = 1;
		}

		changeCart(id, count, size);
		cartPart();
	});


	/* CHANGE item*/

	/* DELETE item by cart*/
	$("body").on("click", ".sbmincart-remove", function(e){
		e.preventDefault();
		var id = $(this).parent().parent().data("id");
		var size = $(this).parent().parent().find("small.size").data("size");

		deleteItem(id, size);
		if($(".cart_info").html()){
			cartPart();
		}
	});

	$("body").on("click", ".cart_quantity_delete", function(e){
		e.preventDefault();
		var id = $(this).parent().parent().data("id");
		var size = $(this).parent().parent().find("small.size").data("size");
		////console.log(size);
		deleteItem(id, size);
		cartPart();
	});
	/* DELETE item*/

	/* SEND PROMO CODE*/
	$("body").on("click", ".send-promo", function(e){
		var code = $(".code").val();
		$.ajax({
			url: "/checkpromo",
			method: "POST",
			data: {code: code},
			beforeSend: function (request) {
        	 	request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
    		},
    		success: function(response){
    		  //  console.log(response);
    			if(response.errors){
    				swal({
    				    text: response.errors,
    					icon: "warning"
    				});
    				$(".code").empty();
    			}else{
    				$(".promo-code").remove();

    				swal(response.success);
    			}
    		}
		})
		
	})
	/* SEND PROMO CODE*/

	/* */
	$("body").on("click", ".checkout-open", function(e){
		var this_elem  = $(this);
		var btn_text = this_elem.text();
		$.ajax({
			url: "/checkout-form",
			method: "GET",
			beforeSend: function(){

			},
			success: function(response){
				$(".form-checkout").html(response);
				
    $("#phone").mask('+ (999) 99 999-9999', {'translation': {0: {pattern: /[0-9*]/}}});
			}
		})
	});

	$("body").on("click", ".checkout-btn", function(e){
		e.preventDefault();
		$(".form-checkout input").next("span").empty();

		$("#chekcout-form").submit();
	})

	$("body").on("submit", "#chekcout-form", function(e){
   // e.preventDefault();
   		
   		$(".checkout-btn").attr("disabled", "disabled");
	    var action = $(this).attr("action");
	    var data = $(this).serialize();
	    
	            	
	    $.ajax({
	        url: action,
	        method: "POST",
	        data: data,
	        beforeSend: function(request){
	        	request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
	        },
	        success: function(response){
	        	////console.log(response);
	        	if(response.notice){

	        		swal({
						html: response.notice, 
						type:"warning",
					});
	        	}else if(response.errors){
	            	var errors = response.errors;
	            	for(var x in errors){
	            		$(".form-checkout input[name='"+x+"']").next().html(errors[x]);
	 	           	}
	    			        	
	            }else{
	            	
	            	$(".modal.checkout .modal-content").html(response);
	            	$(".modal.checkout").addClass("active");
	            	$('body').addClass('no-scroll');
	            }

	            if($(".checkout-btn")){
	            	$(".checkout-btn").removeAttr("disabled");

	            }

	        }
	    });
	    return false;
	});
	/* */

	/**/
	$("body").on("click", ".modal.checkout .close", function(e){
		closePaymentModal();

	});

	$('body').on('click', '#size-modal .close', function(e){
		$('#size-modal').removeClass('active');
	})

	
	/**/

	$("body").on("click", ".order-section .status", function(e){
		e.preventDefault();
		var status = $(this).data("status");
		var product_id = $(this).parent().data("product_id");
		var order_id = $(this).parent().data("order_id");
		var size_product = $(this).parent().parent().find(".size").data("size");
		var url = $(".order-section").data("current");
		////console.log(status + " +++ " + order_id + "--" + product_id);
		$.ajax({
			url: "/cabinet/chage-status",
			method: "POST",
			data: {status: status, product_id: product_id, order_id:order_id, size_product, size_product},
			beforeSend: function (request) {
        	 	request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
    		},
    		success: function(response){
    			////console.log(response);
    			$(location).attr("href", url);
    			// $(".order-section").html(response);
    		}
		});
	});

	$(".list_product").click(function(){
		var id = $(this).data("id");
		$.ajax({
			url: "/cabinet/my-order",
			data: {id: id},
			success: function(response){
				swal({
					html: response,
				  confirmButtonText: "ОК", 
				});
			}
		})
	});


	$("body").on("click", ".location_btn", function(e){
		$.ajax({
			url: "/map",
			beforeSend: function(){
				$("#w3lssbmincart").addClass("modal-map");
				$("#w3lssbmincart").html("<div class='loading'></div>");
			},
			success: function(response){
				$("#w3lssbmincart").append(response);
				$("#w3lssbmincart").find(".loading").remove();


			}
		})
	});

	$("body").on("click", ".address_save", function(){
		////console.log("111");
    	var elem = $(this);
    	var address = $('.address-info').val();
    	var lat = $(".lat").val();
    	var lng = $(".lng").val();
    	
    	elem.attr("disabled", "disabled");

    	$.ajax({
    		url: "/map-init",
    		data: {address:address, lat: lat, lng: lng},
    		method: "POST",
    		success: function(response){
    			if(response.success){
    				$(".close_map").click();
    				$(".location_address").html(address);
    			}else{
    				elem.removeAttr("disabled");
    			}
    		}
    	});
    });

    $("body").on("click", ".close_map", function(e){
		$(".modal-map").removeClass("modal-map");
	});


    $("body").on("click", "#checkout", function(e){
    	e.preventDefault();
    	
    
    	$("#chekcout-last-form").submit();

    });

    $("body").on("submit", "#chekcout-last-form", function(e){
    	e.preventDefault();
        	
    	var data = $(this).serialize();
    	////console.log(data);
    	$.ajax({
    		url: "/payment-action",
    		data: data,
    		method: "POST",
    		beforeSend: function (request) {
	        	return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
	    	},
    		success: function(response){
        //        console.log("checkout");
        //        console.log(response);
    			if(response.hasOwnProperty('notice') ){
					swal({
						text: response.notice,
						type: "warning"
					});
				}else if(response.success){
					//$("body").on("click", ".modal.checkout .close");
					closePaymentModal();
					cartPart();
					swal({
						text: response.success,
						type: "success"
					});

				}else if(response.url){
					closePaymentModal();
					cartPart();
					$(location).attr("target", "_blank").attr("href", response.url);
					
				}else if(response){
					cartPart();
					$(".modal-content").html(response);
				}
    		}
    	});
    });

    $("body").on("click", ".pay_method", function(e){
        

     	e.preventDefault();
    	var type = $(this).data("type");
        var this_elem = $(this);
    	$.ajax({
    		url: "/online-payment",
    		data: {type: type},
    		method: "POST",
    		beforeSend: function (request) {
	        	return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
	    	},
	    	success: function(response){
                checkoutValidate(this_elem, response);
                return;
	    		
	    	}
    	});
    	
    })

    function checkoutValidate(this_elem, response){
//        console.log(response);
    	if(response.notice){
			swal({
				text: response.notice,
				type: "warning"
			});
		
    	    return;
    	}
		
		if(!response.success){
		    return;
		}
		
		if(response.url){
			setTimeout(function(){

				$(location).attr("href", response.url);
			}
			, 1400);
		}
		
		
		if(this_elem.attr("form")){

		    $("#"+this_elem.attr("form")).submit();
		}
		
		closePaymentModal();
		swal({
			text: response.success,
			type: "success"
		});
			
		
    }


    /* get categories*/
// ".menu-secondary.categories"
	$("body").on("click", ".js-menu-hamburger", function(e){
		if(!$(this).hasClass("category-open")) {

			$(this).addClass("category-open");
			$.ajax({
				url: "/menu-shops",
				success: function (response) {
					////console.log(111);
					$(".menu-primary.shops").html(response);
				}
			})
		}
	});

	$("#sale_status").click(function(e){
		////console.log($(this).prop("checked"));
		if($(this).prop("checked")){
			$("label[for='sale_status']").html("Вкл");
			$(".status_input").removeAttr("disabled").removeClass("active");
		}else{
			$("label[for='sale_status']").html("Выкл");
			$(".status_input").attr("disabled", "disabled").addClass("active");

		}
	})
});

function getCart(active = false){

	if($("#w3lssbmincart").hasClass("active") || active){
		////console.log("getcart");
		$.ajax({
			url: "/get-cart",
			success: function(response){
				////console.log(response);
				$("#w3lssbmincart").removeClass("modal-map");
				$("#w3lssbmincart")
						.html(response)
						.addClass("active");
			}
		})
	}
	
}

function getPartCart(id){
	$.ajax({
		url: "/get-cart",
		success: function(response){
			$("#w3lssbmincart").removeClass("modal-map");
			$("#w3lssbmincart")
					.html(response)
					.addClass("active");
		}
	})
}

function addCart(id, size, count = 1, activeclass=true){

	$.ajax({
		url: "/add-cart",
		data: {id: id, size: size, count: count},
		method:"POST",
		dataType: "json",
		beforeSend: function (request) {
        	return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
    	},
		success: function(response){

			if(response.notice){

				swal({
					html: response.notice, 
					type:"warning",
				});
			}else{

				if(activeclass){
					$("#w3lssbmincart").addClass("active");
				}
				
				$(".w3view-cart .badge").html(response);
				if($("#w3lssbmincart").hasClass("active")){
					getCart();
				}
			}
			
		}
	})
}

function getCount(){
	$.ajax({
		url: "/get-cart-count",
		success: function(response){
			$(".w3view-cart .badge").html(response);
		}
	})
}

function changeCart(id, count, size){
	$.ajax({
		url: "/change-cart",
		data: {id: id, count: count, size:size},
		method:"POST",
		dataType: "json",
		
    	success: function(response){
    		if(response.notice){

				swal({
					html: response.notice, 
					type:"warning",
				});
			}else{
				$(".w3view-cart .badge").html(response);
				if($("#w3lssbmincart").hasClass("active")){
					
				}
			}

			getCart();
			getCount();
    	}
	});

}

function deleteItem(id, size){
	////console.log({id: id,size: size});
	$.ajax({
		url: "/delete-cart-item",
		data: {id: id, size:size},
		method:"POST",
		async: false,
// 		dataType: "json",
		beforeSend: function (request) {
        	request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
    	}, 
    	success: function(response){
    	  //  console.log(response);
    		getCart();
			getCount();
    	}
	});

	
}

function closePaymentModal(){
	$(".modal.checkout").removeClass("active")
							.find(".modal-content")
							.empty();
	$('body').removeClass('no-scroll');
}



function cartPart(){
	
	$.ajax({
		url: "/full-cart-part",
		success: function(response){
			$(".cart-section").html(response);
		}
	})
}

function modalMap(){
	$("#w3lssbmincart").addClass("modal-map");
}

function evalJSFromHtml(html) {
  var newElement = document.createElement('div');
  newElement.innerHTML = html;

  var scripts = newElement.getElementsByTagName("script");
  for (var i = 0; i < scripts.length; ++i) {
    var script = scripts[i];
    eval(script.innerHTML);
  }
}



/* MAP*/

function postData( data, elem){
 //  console.log(data);
    
    // $("body").on("submit", "#click_form");
}
