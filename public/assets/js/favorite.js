$('.js-favourite').click(function(e) {
	e.preventDefault();
	var _this = $(this);
	$.ajax({
		url : "/product/add-favourite/" + $(this).data('id'),
		type: 'GET',
		dataType: 'JSON',

		success: function(data){
			if(data.error == 0){
				_this.find('.js-favourite-text').html(data.text);
			}
		}
	});
});