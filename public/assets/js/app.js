var _body = $("body"),
    _window = $(window),
    window_width = _window.width(),
    window_height = _window.height(),
    scrolled = _window.scrollTop(),
    lastScrollPos = scrolled,
    isScrolling = !1,
    screen = {
        md: 992
    },
    VHSupport;
$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    }
});
var App = {
    wrapper: $(".wrapper"),
    overlay: $(".overlay"),
    overlaySpeed: 600,
    header: $(".header"),
    navbar: $(".navbar"),
    isOverlayAnimating: !1,
    preloader: $(".preloader"),
    login: $(".login"),
    userBtn: $(".js-user-btn"),
    authForm: $(".js-auth-form"),
    authMessage: $(".js-auth-message"),
    navbar: $(".navbar"),
    navbarStickedHeight: 90,
    hamburger: $(".js-menu-hamburger"),
    menu: $(".menu"),
    menuItems: $(".menu__item"),
    menuItemsDelay: 100,
    menuTotal: null,
    item_carousel_settings: {
        slidesToShow: 5,
        autoplay: !0,
        speed: 300,
        autoplaySpeed: 3e3,
        arrows: !0,
        cssEase: "ease-in-out",
        prevArrow: '<button class="carousel-arrow carousel-arrow--prev"><i class="icon icon-prev"></i></button>',
        nextArrow: '<button class="carousel-arrow carousel-arrow--next"><i class="icon icon-next"></i></button>',
        responsive: [{
            breakpoint: 1600,
            settings: {
                slidesToShow: 5
            }
        }, {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                arrows: !1
            }
        }]
    },
    item_carousel2_settings: {
        slidesToShow: 5,
        autoplay: !0,
        speed: 300,
        autoplaySpeed: 3e3,
        arrows: !0,
        cssEase: "ease-in-out",
        prevArrow: '<button class="carousel-arrow carousel-arrow--prev"><i class="icon icon-prev"></i></button>',
        nextArrow: '<button class="carousel-arrow carousel-arrow--next"><i class="icon icon-next"></i></button>',
        responsive: [{
            breakpoint: 1600,
            settings: {
                slidesToShow: 5
            }
        }, {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                arrows: !1
            }
        }]
    },
    showPreloader: function() {
        this.preloader.show(), _body.addClass("o-hidden")
    },
    hidePreloader: function() {
        this.preloader.hide(), _body.removeClass("o-hidden")
    },
    init: function() {
        VHSupport = App.header.height() == window_height, App.initMenu(), App.navSticky(), App.initFilter(), App.initItemCarousels(), App.initLogin(), App.initSelect(), App.initAlerts(), App.initTips()
    },
    initTips: function() {
        $(".tip__btn").click(function(e) {
            e.preventDefault();
            var n = $(this).parent();
            n.hasClass("is-active") ? n.removeClass("is-active") : (n.addClass("is-active"), window.setTimeout(function() {
                _body.one("click", function() {
                    n.removeClass("is-active")
                })
            }, 10))
        })
    },
    initAlerts: function() {
        _body.on("click", ".alert__dismisser", function() {
            var e = $(this).parent();
            e.slideUp(200, function() {
                e.remove()
            })
        })
    },
    initSelect: function() {
        var e = $(".js-select");
        0 != e.length && e.each(function() {
            var e = $(this);
            e.select2({
                minimumResultsForSearch: 1 / 0,
                theme: e.data("theme"),
                width: "100%"
            })
        })
    },
    getOverlayOpenedSize: function() {
        return 3 * (window_height > window_width ? window_height : window_width)
    },
    navSticky: function() {
        var e = !0;
        _body.hasClass("nav-page") && (e = !1, _body.addClass("nav-sticked"));
        Math.abs(lastScrollPos - scrolled) < 20 || (scrolled > lastScrollPos && scrolled > App.navbarStickedHeight ? _body.addClass("nav-hidden") : (scrolled + _window.height() < $(document).height() && _body.removeClass("nav-hidden").addClass("nav-sticked"), e && scrolled <= App.navbarStickedHeight && _body.removeClass("nav-sticked")), lastScrollPos = scrolled)
    },
    initItemCarousels: function() {
        $(".item-carousel").each(function(e, n) {
            var i = $(n);
            i.on("init", function() {
                i.css("visibility", "visible")
            }), i.slick(App.item_carousel_settings)
        })
        $(".item-carousel2").each(function(e, n) {
            var i = $(n);
            i.on("init", function() {
                i.css("visibility", "visible")
            }), i.slick(App.item_carousel2_settings)
        })
    },
    initFilter: function() {
        var e = this.header.find(".header__filter"),
            n = !1;
        $(".header__filter-btn").click(function() {
            n || (n = !0, _body.hasClass("filter-extended") ? (_body.removeClass("filter-extended"), e.fadeOut(200, function() {
                n = !1
            })) : (_body.addClass("filter-extended"), e.fadeIn(200, function() {
                n = !1
            })))
        })
    },
    animateMenuItems: function(e, n) {
        var i = 0,
            t = 0;
        this.menuItems.each(function(o, a) {
            window.setTimeout(function() {
                $(a).animate(e, function() {
                    ++t >= App.menuTotal && n && n()
                })
            }, i), i += App.menuItemsDelay
        })
    },
    initMenu: function() {
        var e = !1;
        this.menuTotal = this.menuItems.length, this.hamburger.click(function() {
            if (!e) {
                e = !0;
                var n = App.hamburger.width(),
                    i = App.hamburger.position(),
                    t = {
                        width: n + "px",
                        height: n + "px",
                        left: i.left + "px",
                        top: i.top + "px"
                    };
                if (_body.hasClass("menu-opened")) _body.removeClass("menu-opened"), App.animateMenuItems({
                    opacity: 0,
                    left: "-50px"
                }, function() {
                    App.menu.hide(), App.overlay.animate(t, .7 * App.overlaySpeed, function() {
                        App.overlay.hide().attr("style", null), _body.removeClass("o-hidden"), e = !1
                    })
                });
                else {
                    _body.addClass("menu-opened o-hidden");
                    var o = App.getOverlayOpenedSize(),
                        a = .5 * o;
                    App.overlay.show(), App.overlay.css(t), App.overlay.animate({
                        width: o + "px",
                        height: o + "px",
                        left: -a + "px",
                        top: -a + "px"
                    }, App.overlaySpeed, function() {
                        App.menu.show(), App.animateMenuItems({
                            opacity: 1,
                            left: "0"
                        }, function() {
                            e = !1
                        })
                    })
                }
            }
        })
    },
    openLogin: function(e) {
        e.preventDefault();
        App.login.find(".login__content");
        var n = App.login.find(".js-login-close");
        _body.addClass("login-opened o-hidden");
        var i = App.userBtn.position(),
            t = App.userBtn.offset(),
            o = {
                right: _window.width() - (t.left + App.userBtn.width() / 2) + "px",
                top: i.top + App.userBtn.height() / 2 + "px",
                width: "0px",
                height: "0px"
            },
            a = App.getOverlayOpenedSize(),
            s = .5 * a;
        App.overlay.show().css(o), App.hamburger.fadeOut(250), App.overlay.animate({
            right: -s + "px",
            top: -s + "px",
            width: a + "px",
            height: a + "px"
        }, App.overlaySpeed, function() {
            App.login.fadeIn(250), App.isOverlayAnimating = !1
        }), n.one("click", function() {
            App.isOverlayAnimating || App.login.fadeOut(250, function() {
                App.overlay.animate(o, .7 * App.overlaySpeed, function() {
                    App.hamburger.fadeIn(250), _body.removeClass("login-opened o-hidden"), App.isOverlayAnimating = !1
                })
            })
        })
    },
    auth: function(e) {
        e.preventDefault();
        var n = $(this),
            i = n.find("[type=submit]");
        App.authMessage.hide(100), i.attr("disabled", !0).addClass("is-waiting"), $.ajax({
            type: "POST",
            url: n.attr("action"),
            data: n.serialize(),
            dataType: "JSON",
            success: function(e) {
                i.attr("disabled", !1).removeClass("is-waiting"), 1 == e.status ? window.location.href = e.url : App.authMessage.addClass("m-b-20").fadeIn(500).html(e.msg)
            }
        })
    },
    initLogin: function() {
        App.userBtn.click(App.openLogin), App.authForm.submit(App.auth)
    }
};
App.Tab = function(e) {
    this.tabs = e.tabs, this.tabContents = e.tabContents, this.onChange = e.onChange, this.init()
}, App.Tab.prototype.init = function() {
    var e = this;
    e.tabs.click(function(n) {
        n.preventDefault();
        var i = $(this);
        if (!i.hasClass("is-active")) {
            var t = parseInt(i.data("index"));
            e.tabs.removeClass("is-active"), e.tabContents.removeClass("is-active"), e.tabContents.eq(t).addClass("is-active"), i.addClass("is-active"), e.onChange && e.onChange(t)
        }
    })
}, $(document).ready(App.init);
var Modal = function() {
        var e = function(e) {
                var n = this;
                n.modal = e.modal, n.openBtn = e.openBtn, n.closeBtn = e.closeBtn, n.modal_content = n.modal.find(".modal__content"), n.isOpen = !1, n.isAnimating = !1, n.fadeDuration = e.fadeDuration || 400, n.init()
            },
            n = e.prototype;
        return n.init = function() {
            var e = this;
            e.openBtn.click(function(n) {
                n.preventDefault(), e.isOpen || e.isAnimating || e.openModal()
            }), e.closeBtn.click(function(n) {
                n.preventDefault(), e.isOpen && !e.isAnimating && e.closeModal()
            }), e.modal.click(function(n) {
                e.isOpen && !e.isAnimating && $(n.target).is(e.modal) && e.closeModal()
            })
        }, n.openModal = function() {
            var e = this;
            e.isAnimating = !0, _body.addClass("o-hidden"), e.modal.fadeIn(e.fadeDuration), window.setTimeout(function() {
                e.modal_content.addClass("modal__content--in").one(transitionEnd, function() {
                    e.isAnimating = !1, e.isOpen = !0
                })
            }, e.fadeDuration / 1.5)
        }, n.closeModal = function() {
            var e = this;
            e.isAnimating = !0, e.modal_content.removeClass("modal__content--in").one(transitionEnd, function() {
                e.modal.fadeOut(e.fadeDuration, function() {
                    e.isAnimating = !1, e.isOpen = !1, _body.removeClass("o-hidden")
                })
            })
        }, e
    }(),
    phoneMask = $(".js-phone-mask");
phoneMask.length > 0 && phoneMask.mask("000000000000", {
        placeholder: "+998(__)___-__-__"
    }),
    function() {
        var e = !1,
            n = !1;
        $(".dropdown__label").click(function() {
            var i = $(this).parent(),
                t = i.find(".dropdown__content");
            e ? n || (i.removeClass("is-active"), t.slideUp(200, function() {
                n = !1, e = !1
            })) : n || (i.addClass("is-active"), t.slideDown(200, function() {
                n = !1, e = !0
            }))
        })
    }(), _window.resize(function() {
        window_width = _window.width()
    }), _window.scroll(function() {
        scrolled = _window.scrollTop(), isScrolling = !0, window.setInterval(function() {
            isScrolling && (App.navSticky(), isScrolling = !1)
        }, 250)
    });


$(document).ready(function(){

	var $filter = $('.sk-filter'),
		$groups = $filter.find('.form-group'),
		$categories = $filter.find('.categories-list li.has-child > a');

	// $categories.click(function(e){
	// 	e.preventDefault();
	// 	$(this).parent().toggleClass('active');
	// })

	$groups.each(function(){
		var $this = $(this),
			$title = $this.find('.form-group-title'),
			$collapse = $this.find('.form-group-collapse');

		$title.click(function(e){
			e.preventDefault();

			$this.toggleClass('opened');
		})
	})
})