var markers;

function mapInitByPosition(mapTag, position, options = {}, addMarkerForPos = false){
	accessToken = "pk.eyJ1IjoibnVybWFub3ZpZyIsImEiOiJjampoemg3YW4ycjVrM3ZudmQ1bWk0bjV5In0.ov5Y46Myp4-nugnYyHzHmw";
	myMap = L.map(mapTag,{
			center: position,
			zoom: 15
	});

	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
	// attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
	 	accessToken: accessToken
	}).addTo(myMap);

	if(addMarkerForPos){
		addMarker({coords: position});
	}

	if(options.hasOwnProperty('markers')){
		for(x in options.markers){
			addMarker(options.markers[x], false);

		}
	}

	if(options.hasOwnProperty('event')){
		eventListener(options.event);
	}
}

function mapInitByNavigator(mapTag, options = {}, addMarkerForPos = false){
	navigator.geolocation.getCurrentPosition(function(pos) 
		{
				position = [
						pos.coords.latitude,
						 pos.coords.longitude
				];

				mapInitByPosition(mapTag, position, options, addMarkerForPos);
				// addMarker();
		});	
}


function eventListener(marker = {}){
	myMap.on("click", function(event){
		console.log(event.latlng);

		marker.coords = [event.latlng.lat, event.latlng.lng];
		console.log(marker);
		addMarker(marker);
		
		if(marker.elem)
			geocodeByLatLng(event.latlng, marker.elem);
	})

}



function addMarker(info, clear = true){

	var options = {};

	if(clear){
			clearMarkers();
	}
	
		if(info.icon){
			var icon = L.icon({
					iconUrl: info.icon.src,
					iconSize: [info.icon.width, info.icon.height], // size of the icon
			});
			options.icon = icon;
		}

	var marker = L.marker(info.coords, options);
	
	if(info.popup){
			marker.bindPopup(info.popup);
	}
	
	marker.addTo(myMap);
	
	myMap.addLayer(marker);
	markers.push(marker);
}

function addMultiMarker(informations){
	for(x in informations){
			addMarker(informations[x]);
	}   
}

function clearMarkers(){
		console.log(markers);
		for(x in markers){
				myMap.removeLayer(markers[x]);

		}
		markers = [];
}


function geocodeByLatLng(coords, elem){
	var address;
	if(elem.lat){
		elem.lat.val(coords.lat);
	}

	if(elem.lng){
		elem.lng.val(coords.lng);
	}

	if(elem.address){
		var geocodeService = L.esri.Geocoding.geocodeService();
		geocodeService.reverse().latlng(coords).run(function(error, result)  {
				console.log(result.address.Match_addr);
				console.log(elem.addres);
				elem.address.val(result.address.Match_addr);
		});
	}

	console.log(address);
}
