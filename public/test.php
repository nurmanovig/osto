<?php
function img_resize($src, $dest, $width, $height, $rgb = 0xFFFFFF, $quality = 100) {  
        if (!file_exists($src)) {  
                return false;  
        }  
        $size = getimagesize($src);  
 
        if ($size === false) {  
                return false;  
        }  
 
        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));  
        $icfunc = 'imagecreatefrom'.$format;  
        if (!function_exists($icfunc)) {  
                return false;  
        }  
 
        $x_ratio = $width  / $size[0];  
        $y_ratio = $height / $size[1];  
 
        if ($height == 0) {  
 
                $y_ratio = $x_ratio;  
                $height  = $y_ratio * $size[1];  
 
        } elseif ($width == 0) {  
 
                $x_ratio = $y_ratio;  
                $width   = $x_ratio * $size[0];  
 
        }  
 
        $ratio       = min($x_ratio, $y_ratio);  
        $use_x_ratio = ($x_ratio == $ratio);  
 
        $new_width   = $use_x_ratio  ? $width  : floor($size[0] * $ratio);  
        $new_height  = !$use_x_ratio ? $height : floor($size[1] * $ratio);  
        $new_left    = $use_x_ratio  ? 0 : floor(($width - $new_width)   / 2);  
        $new_top     = !$use_x_ratio ? 0 : floor(($height - $new_height) / 2);  
 
        $isrc  = $icfunc($src);  
        $idest = imagecreatetruecolor($width, $height);  
        
        $dir = pathinfo($dest, PATHINFO_DIRNAME);
        $dir_arr = explode("/", $dir);
        $str_dir = "";

        foreach ($dir_arr as $key => $value) {
            $str_dir.= $value."/";
            if(!is_dir($str_dir)){
                
                mkdir($str_dir, 0777);
            }
        }

        imagefill($idest, 0, 0, $rgb);  
 
        imagecopyresampled($idest, $isrc, $new_left, $new_top, 0, 0, $new_width, $new_height, $size[0], $size[1]);  
        imagejpeg($idest, $dest, $quality);
 
        imagedestroy($isrc);  
        imagedestroy($idest);  
 
        return true;  
}

function clearDir($dir){
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if($file == "." || $file == "..")
                        continue;
                    chmod($dir."/", 0777);
                    echo $dir."/".$file;
                    unlink($dir."/".$file);
                }
                closedir($dh);
            }
        }
    }  

function createDir($dir){
        $arr = explode("/", $dir);
        $str = "";
        foreach ($arr as $key => $item) {
            $str .= $item;
            if(!is_dir($str)){
                mkdir($str);
            }
        }

    } 

    clearDir("uploads/product");
// $src = "1.png";
// $dest = "2.JPG";
// $width = 208;
// $height = 200;

// echo img_resize($src, $dest, $width, $height,  0xFFFFFF,100);
?>