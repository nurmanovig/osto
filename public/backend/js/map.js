var myMap;
var lat_input = $('.js-lat-input');
var lng_input = $('.js-lng-input');
var marker = null;
var infoWindow;
var lat_val = lat_input.val();
var lng_val = lng_input.val();

var markers = [];
function initMap() {
    if (navigator.geolocation && !lat_val  && !lng_val) 
    {
        navigator.geolocation.getCurrentPosition(function(pos) 
        {
            position = {
                lat: pos.coords.latitude,
                lng: pos.coords.longitude
            };

            createMap();
        });
    }else{
        position = {
            lat: lat_val *1,
            lng: lng_val*1
        };

        createMap();
    }
}

function createMap()
{

    myMap = new google.maps.Map($('.js-shop-map')[0], {
        center: position,
        scrollwheel: true, 
        zoom: 14
    });
    if(!$(".js-shop-map").hasClass("only_marker")){
        google.maps.event.addListener(myMap, 'click', function(event) {
           placeMarker(event.latLng, myMap);
        });
        placeMarker({lat: position.lat, lng: position.lng}, myMap);
    }else{
        onePlaceMarker({lat: position.lat, lng: position.lng}, myMap);
    }

    
}



function placeMarker(pos, myMap) {

    deleteMarkers();
    var marker = new google.maps.Marker({
        position: pos,
        map: myMap
    });


    $('.js-lat-input').val(pos.lat);
    $('.js-lng-input').val(pos.lng);
    
    geocodeLatLng(myMap,pos);
    markers.push(marker);

}


function onePlaceMarker(pos, myMap) {

    var marker = new google.maps.Marker({
        position: pos,
        map: myMap
    });


}




function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
}

  // Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}

  // Shows any markers currently in the array.
function showMarkers() {
    setMapOnAll(map);
}

  // Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    clearMarkers();
    markers = [];
}

function geocodeLatLng(myMap, coordinates) {
    var geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow;
    console.log(coordinates);

    geocoder.geocode({'location': coordinates}, function(results, status) {
      if (status === 'OK') {
        if (results[0]) {
          // map.setZoom(14);
          // var marker = new google.maps.Marker({
          //   position: coordinates,
          //   map: myMap
          // });
          console.log(results[0].formatted_address);
          // codeAddress(myMap, results[0].formatted_address);

          $(".address_input").val(results[0].formatted_address);
          // infowindow.setContent(results[0].formatted_address);
          // infowindow.open(map, marker);
        } else {
         /// window.alert('No results found');
        }
      } else {
        //window.alert('Geocoder failed due to: ' + status);
      }
    });
}
