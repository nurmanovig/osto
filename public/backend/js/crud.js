  function Crud(options){
    var _ = this, _btn, _type;
    _.options = options;
    _.currentItem = null;
    _.datatable = null;
    _editData = [];

    _.prepareDatatable = function(){
      return {
        pageLength: _.options.list.datatable.pageLength || 10,
        order: _.options.list.datatable.order || [[0, 'desc']],
        processing: true,
        serverSide: true,
        ajax: _.options.list.url,
        columns: _.options.list.datatable.columns,
        columnDefs: _.options.list.datatable.columnDefs
      }
    }
    /**
     * Make a button
     * @param  obj data
     * @param  string classes
     * @param  string inner  
     * @param  Array dataArr   
     * @return string
     */
    _.makeButton = function(data, classes, inner, dataArr) {
      var dataArr = typeof dataArr !== 'undefined' ?  dataArr : [],
          dataHtml = 'data-id="' + data.id + '"';

      $.each(dataArr, function(i, val) {
        if(Array.isArray(val)) {
          dataHtml += ' data-' + val[0] + '="' + val[1] + '"';
        }else {
          dataHtml += ' data-' + val + '="' + data[val] + '"';
        }

      });

      return '<button class="btn '+ classes + '"'
        + dataHtml
        + '>'
        + inner
        + '</button> ';
    }

    _.cleanError = function() {
      $('.help-block').remove();
      $('.has-error').removeClass('has-error');
    }

    _.onClickAddBtn = function() {
      $('body').on('click', '.btn-add', function(e){
        $('input[type=text], input[type=hidden], input[type=password], textarea').val('');
        $('input[type=checkbox], input[type=radio]').prop('checked', false);
        $('select').find('option:eq(0)').prop('selected', true);
        _.cleanError();
      });
    }

    _.onClickEditBtn = function() {
      $('body').on('click', '.btn-edit', function(e){
        _btn = $(this);
        // Data
        _.editData = _.datatable.row(_btn.closest("tr")).data();
        // Set Value
        $('#form').find(':input').each(function(i, elem) {
          _type = this.type || this.tagName.toLowerCase();
          if((_type == 'checkbox' || _type == 'radio')) {
            console.log($(this).val());
            if(_.editData[$(this).attr('name')] == $(this).val()) {
              $(this).prop('checked', true);
            }else{
              $(this).prop('checked', false);
            }
          } else {
            $(this).val(_.editData[$(this).attr('name')] );
          }
        });
        _.cleanError();
      });
    }

    _.onClickFormBtn = function() {
      $('body').on('click', '.ajax-form', function (e) {
        e.preventDefault();
        _btn = $(this);
        _btn.prepend('<i class="fa fa-spinner fa-spin"></i> ');
        _btn.prop('disabled', true);
        $.ajax({
          type: "POST",
          url: _.options.form.url,
          data: $('#form').serialize(),
          async : true,
          success: function(response) {
            if(response.status == 'success') {
              _.datatable.draw();
              $('#formModal').modal('hide');
            } else if(response.status == 'error') {
              _.cleanError();
              $.each(response.errors, function(elem, item){
                $.each(item, function(i, error) {
                  $('[name='+elem+']').after('<span class="help-block">' + error + '</span>').parent().addClass('has-error');
                })
              });
            }else{
              alert('Пожалуйста, обновите страницу');
              $('#formModal').modal('hide');
            }
            _btn.prop('disabled', false);
            _btn.find('i').remove();
          },
          error: function(){
            _.cleanError();
            _btn.prop('disabled', false);
            _btn.find('i').remove();
            $('#formModal').modal('hide');
            alert('Ошибка!');
          }
        });
      });
    }

    _.onClickRemoveBtn = function() {
      $('body').on('click', '[data-target="#removeModal"]', function(e){
        $('#delete_id').val($(this).data('id'));
      });
    }

    _.onClickAjaxRemoveBtn = function() {
      $('body').on('click', '.ajax-remove', function (e) {
        e.preventDefault();
        _btn = $(this);
        _btn.prepend('<i class="fa fa-spinner fa-spin"></i> ');
        _btn.prop('disabled', true);
        $.ajax({
          type: "POST",
          url: _.options.remove.url,
          data: $('#delete_form').serialize(),
          async : true,
          success: function(response) {
            if(response.status == 'success') {
              _.datatable.draw();
            } else if(response.status == 'error') {
              alert(response.message);
            }else{
              alert('Пожалуйста, обновите страницу');
            }
            _btn.prop('disabled', false);
            _btn.find('i').remove();
            $('#removeModal').modal('hide');
          }
        });
      });
    }

    /**
     * Intialize events
     * @return void
     */
    _.init = function() {
      _.onClickAddBtn();
      _.onClickFormBtn();
      _.onClickRemoveBtn();
      _.onClickAjaxRemoveBtn();
      _.onClickEditBtn();
      _.datatable = $("#datatable").DataTable(_.prepareDatatable());
    }

    _.init();
    return _;
  }