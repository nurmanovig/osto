var body = $('body'),
	_window = $(window);

const preloader = $('.preloader');

// Sidebar links

var asideLinks = $('.sidebar__list--drop-toggle > a');

asideLinks.click(function(){
	let _this = $(this),
		listItem = _this.parent(),
		dropdown = listItem.find('.sidebar__dropdown');
		
	if (listItem.hasClass('sidebar__list--active')) {
		listItem.removeClass('sidebar__list--active');
		dropdown.slideUp(300);
	}else{
		asideLinks.parent()
			      .removeClass('sidebar__list--active')
		          .find('.sidebar__dropdown')
		          .slideUp(300);

		listItem.addClass('sidebar__list--active');
		dropdown.slideDown(300);
	}
});

// Open menu if it has active inside

asideLinks.each(function(index, item){
	let _this = $(item),
		listItem = _this.parent();

	if (listItem.find('.sidebar__list--active').length > 0) {
		listItem.addClass('sidebar__list--active');
		listItem.find('.sidebar__dropdown').slideDown(300);
	}
});

// Make sidebar contract when overlay is clicked
$('.overlay').click(function(){
	SIDEBAR.contract();
});

var SIDEBAR = {
	toggle: $('.navbar__toggle'),

	isCompact: function(){
		return body.hasClass('compact-sidebar');
	},

	expand: function(){
		body.removeClass('compact-sidebar');
		$.cookie('isNavbarExpanded', 'true');
	},

	contract: function(){
		body.addClass('compact-sidebar');
		$.cookie('isNavbarExpanded', 'false');
	},

	init: function(){
		this.toggle.click(() => {
			if(SIDEBAR.isCompact()){
				SIDEBAR.expand();
			} else{
				SIDEBAR.contract();
			}
		});
	}
}

// Initialize sidebar toggle

SIDEBAR.init();

// On document load if window is less than > 768
// than contract the sidebar.

if (_window.width() <= 768 && !SIDEBAR.isCompact()) {
	SIDEBAR.contract();
}

if ($.cookie('isNavbarExpanded') === 'false') {
	SIDEBAR.contract();
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(window).on('load', function(){
	preloader.hide();
});




