$(function(){
    $(".order .status").click(function(){
        var status = $(this).data("status");
        var id = $(".order").data("id");
        var url = $(".order").data("current");
        console.log(id + "--" + url + "==" + status);
        $.ajax({
            url: "/mypanel/order/status",
            method: "POST",
            data: {id: id, status:status},
            beforeSend: function (request) {
                request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function(response){
                console.log(response);
                $(location).attr("href", url);
            }
        })
    });

    $(".delete_item").click(function(e){
        e.preventDefault();
        var id = $(this).data("id");
        var action = $(this).data("url");
        var url = $("#data-table").data("current");
        $.ajax({
            url: action,
            method: "POST",
            data: {delete: id},
            beforeSend: function (request) {
                request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function(response){
                $(location).attr("href", url);
            }
        })
    });

    $('.pay_status').click(function(e){
        e.preventDefault();
        var id = $(".order").data("id");
        var url = $(".order").data("current");
        console.log(id);
        $.ajax({
            url: "/mypanel/order/paystatus",
            method: "POST",
            data: {id, id},
            beforeSend: function (request) {
                request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function(response){
                $(location).attr("href", url);
            }
        })
    });

    $(".status_shop").click(function(e){
        var id = $(this).data("id");
        var type = $(this).data("type");
        var url = $(".panel-body").data("current");
        console.log({id: id, type: type});

        $.ajax({
            url: "/mypanel/shop/status",
            method: "POST",
            data: {id: id, type: type},
            beforeSend: function (request) {
                request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function(response){

                $(location).attr("href", url);
                // $(".order-section").html(response);
            }
        });

        
        
    });

    $('.datepicker').datetimepicker({
       
    });
})