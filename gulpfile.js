/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

var gulp = require('gulp');
var uglify = require('gulp-uglify');

gulp.task('default', ['watch']);

gulp.task('watch', function(){
    gulp.src(['resources/assets/js/*.js'])
        .pipe(uglify().on('error', function(e){
        	console.log(e);
        }))
        .pipe(gulp.dest('public/assets/js'));
});

gulp.watch('resources/assets/js/*.js', ['watch']);

