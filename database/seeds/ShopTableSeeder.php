<?php

use Illuminate\Database\Seeder;
use App\Models\Shop;

class ShopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Shop::insert([

        	[
        		'title' => 'Магазин1',
        		'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
        		'phone' => '9993934939934',
        		'user_id' => '1',
                'alias' => 'magazin1',
        	],

        	[
        		'title' => 'Магазин1',
        		'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
        		'phone' => '9993934939934',
        		'user_id' => '1',
                'alias' => 'magazin2',
        	],

        	[
        		'title' => 'Магазин1',
        		'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
        		'phone' => '9993934939934',
        		'user_id' => '1',
                'alias' => 'magazin3',
        	],


        	[
        		'title' => 'Магазин1',
        		'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
        		'phone' => '9993934939934',
        		'user_id' => '1',
                'alias' => 'magazin4',
        	],

        	[
        		'title' => 'Магазин1',
        		'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
        		'phone' => '9993934939934',
        		'user_id' => '1',
                'alias' => 'magazin5',
        	],


        	[
        		'title' => 'Магазин1',
        		'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
        		'phone' => '9993934939934',
        		'user_id' => '1',
                'alias' => 'magazin6',
        	],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin7',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin8',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin9',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin10',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin11',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin12',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin13',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin14',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin15',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin16',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin17',
            ],


            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin18',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin19',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin20',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin21',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin22',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin23',
            ],

            [
                'title' => 'Магазин1',
                'address' => 'Beshagach 12',
                'mall_type' => '1',
                'mall_id' => '1',
                'floor' => '10',
                'shop_number' => '12',
                'phone' => '9993934939934',
                'user_id' => '1',
                'alias' => 'magazin24',
            ],


        ]);
    }
}
