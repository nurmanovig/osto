<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([

        	[

        	"parent_id" => '0',
        	'title_ru' => 'Женская одежда',
        	'title_uz' => 'Женская одежда',
            'icon'  => 'icon-womens-clothing',
            'alias' => 'jenskoe-odejda',

        	],

        	[

        	"parent_id" => '0',
        	'title_ru' => 'Мужская одежда',
        	'title_uz' => 'Мужская одежда',
            'icon'  => 'icon-mens-clothing',
            'alias'  => 'mujskoe-odejda',        

        	],


        	[

        	"parent_id" => '0',
        	'title_ru' => 'Детская одежда',
        	'title_uz' => 'Детская одежда',
            'icon'  => 'icon-mens-clothing',
            'alias'  => 'detskoe-odejda',  

        	],


        	[

        	"parent_id" => '0',
        	'title_ru' => 'Сумки и обувь',
        	'title_uz' => 'Сумки и обувь',
            'icon'  => 'icon-mens-clothing',
            'alias'  => 'sumky-obuv',      
        	],


        	[

        	"parent_id" => '0',
        	'title_ru' => 'Часы и бижутерия',
        	'title_uz' => 'Часы и бижутерия',
            'icon'  => 'icon-mens-clothing',
            'alias'  => 'chasy-bijuteri',      
        	],

        ]);
    }
}
