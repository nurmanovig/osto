<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        for($i=0; $i<100; $i++)

        DB::table('products')->insert([
        	[
        	 'title' => $this->getRandomWord()." ". $this->getRandomWord(),
        	 'description' => 'Английская локализация учитывает множественное число, но прямое соответствие важнее. Если у вас в ключевике или названии есть «kid», то и при поиске «kids» вы найдетесь, но при прочих равных ваш конкурент с ключевиком «kids» в выдаче будет выше. Тут есть какие-то исключения — иногда это правило не срабатывало, не смог докопаться, почему. ',

             'category_id' => '1',
        	 'price' => rand(50,2000000),
             'alias' => $this->getRandomWord(),
             'view_amount' => rand(5,15),
             'status' => '0',
             'like_amount' => rand(1,100),
             'shop_id' => '1',
             'user_id' => '1',
        	]
        ]);
        	
    }


    private function getRandomWord($len = 10) {
        $word = array_merge(range('a', 'z'), range('A', 'Z'));
             shuffle($word);
        return substr(implode($word), 0, $len);
    }
}
