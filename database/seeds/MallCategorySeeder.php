<?php

use Illuminate\Database\Seeder;

class MallCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mall_categories')->insert([
        	[	
        	'parent_id' => 0,
        	'title_ru'  => 'БАЗАРЫ',
        	'title_uz'	=> 'БАЗАРЫ',
        	'description_ru' => 'Lorem ipsum dolor sit amet, consecteturadipisici',
        	'description_uz' => 'Lorem ipsum dolor sit amet, consecteturadipisici',
        	],
        	
        	[
        	'parent_id' => 0,
        	'title_ru'  => 'ТОРГОВЫЕ ЦЕНТРЫ',
        	'title_uz'	=> 'ТОРГОВЫЕ ЦЕНТРЫ',
        	'description_ru' => 'Lorem ipsum dolor sit amet, consecteturadipisici',
        	'description_uz' => 'Lorem ipsum dolor sit amet, consecteturadipisici',	
        	],

        	[
        	'parent_id' => 0,
        	'title_ru'  => 'БРЕНДОВЫЕ МАГАЗИНЫ',
        	'title_uz'	=> 'БРЕНДОВЫЕ МАГАЗИНЫ',
        	'description_ru' => 'Lorem ipsum dolor sit amet, consecteturadipisici',
        	'description_uz' => 'Lorem ipsum dolor sit amet, consecteturadipisici',	
        	],

        	[
        	'parent_id' => 0,
        	'title_ru'  => 'ЧАСТНЫЕ ТОРГОВЦЫ',
        	'title_uz'	=> 'ЧАСТНЫЕ ТОРГОВЦЫ',
        	'description_ru' => 'Lorem ipsum dolor sit amet, consecteturadipisici',
        	'description_uz' => 'Lorem ipsum dolor sit amet, consecteturadipisici',	
        	]
        ]);
    }
}
