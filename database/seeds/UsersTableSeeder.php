<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('roles')->truncate();
        // DB::table('users')->truncate();
        // DB::table('role_user')->truncate();

            DB::table('users')->insert([
                [
                    'name' => 'admin',
                    'lastname' => 'admin',
                    'email' => 'admin',
                    'phone' => '+998971234567',
                    'password' => bcrypt('123123')
                ],

                [
                    'name' => 'salesman',
                    'lastname' => 'salesman',
                    'email' => 'salesman@mail.ru',
                    'phone' => '+998901234567',
                    'password' => bcrypt('salesman')
                ],

                [
                    'name' => 'user',
                    'lastname' => 'user',
                    'email' => 'user@mail.ru',
                    'phone' => '+998931234567',
                    'password' => bcrypt('userpassword')
                ]
            ]);

            DB::table('roles')->insert([
                [
                	'name' => 'admin',
                	'display_name' => 'Project owner',
                	'description' => 'lorem ipsum dolor sit amet'
                ],

                [
                    'name' => 'salesman',
                    'display_name' => 'Salesman',
                    'description    ' => 'lorem ipsum dolor sit amet'
                ],

                [
                    'name' => 'user',
                    'display_name' => 'User',
                    'description' => 'lorem ipsum dolor'
                ]
            ]);

            DB::table('role_user')->insert([
                [
                    'user_id' => 1,
                    'role_id' => 1
                ],

                [
                    'user_id' => 2,
                    'role_id' => 2
                ],

                [
                    'user_id' => 3,
                    'role_id' => 3
                ]
            ]);
    }
}
