<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('alias');
            $table->text('short');
            $table->text('description');
            $table->integer('category_id');
            $table->integer('price');
            $table->enum('currency', ['sum','usd'])->default('sum');
            $table->enum('sex',['male','female','general']);
            $table->enum('is_recommended',[0,1]);
            $table->enum('is_popular',[0,1]);
            $table->integer('shop_id');
            $table->integer('user_id')->default(0);
            $table->integer('like_amount')->default(0);
            $table->integer('dislike_amount')->default(0);
            $table->integer('view_amount')->default(0);
            $table->enum('is_active',[0,1]);
            // $table->
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
