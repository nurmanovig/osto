<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('paycom_transaction_id');
            $table->string('paycom_time');
            $table->datetime('paycom_time_datetime');
            $table->datetime('create_time');
            $table->datetime('perform_time');
            $table->datetime('cancel_time');
            $table->integer('amount');
            $table->tinyInteger('state');
            $table->tinyInteger('reason');
            $table->text('receivers');
            $table->integer('order_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
