<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymeUz extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('payme_uz', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction', 50);
            $table->string('code', 25);
            $table->string('state', 25);
            $table->string('owner_id', 25);
            $table->string('amount', 25);
            $table->string('reason', 25);
            $table->string('payme_time', 25);
            $table->string('cancel_time', 25);
            $table->string('create_time', 25);
            $table->string('perform_time', 25);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
