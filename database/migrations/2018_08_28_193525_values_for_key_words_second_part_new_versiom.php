<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ValuesForKeyWordsSecondPartNewVersiom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::table('key_words')->insert([
                [
                    'key' => 'ORDER',
                    'word' => 'Остановлен',
                    'class' => 'primary',
                    'status' => -1,
                ],
                [
                    'key' => 'ORDER',
                    'word' => 'Ошибка',
                    'class' => 'primary',
                    'status' => 0,
                ],
                [
                    'key' => 'ORDER',
                    'word' => 'Без действие',
                    'class' => 'primary',
                    'status' => 1,
                ],
                [
                    'key' => 'ORDER',
                    'word' => 'Подготовка товара',
                    'class' => 'primary',
                    'status' => 2,
                ],
                [
                    'key' => 'ORDER',
                    'word' => 'Доставлен',
                    'class' => 'primary',
                    'status' => 3,
                ],
                [
                    'key' => 'ORDER_PRODUCT',
                    'word' => 'Остановлен',
                    'class' => 'primary',
                     'status' => 0,
                ],
                [
                    'key' => 'ORDER_PRODUCT',
                    'word' => 'Без действие',
                    'class' => 'primary',
                     'status' => 1,
                ],
                [
                    'key' => 'ORDER_PRODUCT',
                    'word' => 'В ожидание',
                    'class' => 'primary',
                     'status' => 2,
                ],
                [
                    'key' => 'ORDER_PRODUCT',
                    'word' => 'Готово',
                    'class' => 'primary',
                     'status' => 3,
                ],
                [
                    'key' => 'ACTION_STOPPED',
                    'word' => 'Остановлен',
                    'class' => 'primary',
                     'status' => 0,
                ],
                [
                    'key' => 'ACTION_INACTION',
                    'word' => 'Получена заявка',
                    'class' => 'primary',
                     'status' => 1,
                ],
                [
                    'key' => 'ACTION_WAIT',
                    'word' => 'Готово',
                    'class' => 'primary',
                     'status' => 1,
                ],
                [
                    'key' => 'ACTION_READY',
                    'word' => 'Готово',
                    'class' => 'success',
                     'status' => 1,
                ],
                
                

        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
