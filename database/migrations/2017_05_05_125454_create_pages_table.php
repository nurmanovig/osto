<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_ru',255);
            $table->string('title_uz',255);
            $table->text('description_ru');
            $table->text('description_uz');
            $table->text('meta_description_uz');
            $table->text('meta_description_ru');
            $table->text('meta_keywords_ru');
            $table->text('meta_keywords_uz');
            $table->string('alias',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
