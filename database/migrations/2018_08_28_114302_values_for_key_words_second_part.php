<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ValuesForKeyWordsSecondPart extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         DB::table('key_words')->insert([
                [
                    'key' => 'PRODUCT_STATUS_STOPPED',
                    'word' => 'Остановлен'
                ],
                [
                    'key' => 'PRODUCT_STATUS_READY',
                    'word' => 'Без действие'
                ],
                [
                    'key' => 'PRODUCT_STATUS_INACTION',
                    'word' => 'В ожидание'
                ],
                [
                    'key' => 'PRODUCT_STATUS_READY',
                    'word' => 'Готово'
                ],
                

        ]);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
