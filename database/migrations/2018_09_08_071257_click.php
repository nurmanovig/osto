<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Click extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('click_uz', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('click_trans_id');
            $table->string('account');
            $table->integer('amount');
            $table->integer('date');
            $table->string('status');
            $table->string('error');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
