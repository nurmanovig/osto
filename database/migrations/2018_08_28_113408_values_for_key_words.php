<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ValuesForKeyWords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        DB::table('key_words')->insert([
                [
                    'key' => 'STATUS_STOPPED',
                    'word' => 'Остановлен'
                ],
                [
                    'key' => 'STATUS_FAIL',
                    'word' => 'Ошибка'
                ],
                [
                    'key' => 'STATUS_INACTIVE',
                    'word' => 'Без действие'
                ],
                [
                    'key' => 'STATUS_STARTED',
                    'word' => 'Подготовка товара'
                ],
                [
                    'key' => 'STATUS_FINISHED',
                    'word' => 'Доставлен'
                ],
                

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
