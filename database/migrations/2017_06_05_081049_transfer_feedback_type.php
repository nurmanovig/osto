<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransferFeedbackType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('feedback_type')->insert([
                [
                    'id' => '1',
                    'title_ru' => 'Вопросы модерации объявлений',
                    'title_uz' => 'Вопросы модерации объявлений'
                ],
                [
                    'id' => '2',
                    'title_ru' => 'Платные услуги',
                    'title_uz' => 'Платные услуги'
                ],
                [
                    'id' => '3',
                    'title_ru' => 'Вопросы по использованию сайта',
                    'title_uz' => 'Вопросы по использованию сайта'
                ],
                
                [
                    'id' => '4',
                    'title_ru' => 'Мошенничество, ложные объявления',
                    'title_uz' => 'Мошенничество, ложные объявления'
                ],

                [
                    'id' => '5',
                    'title_ru' => 'Заблокирована учетная запись',
                    'title_uz' => 'Заблокирована учетная запись'
                ],

                [
                    'id' => '6',
                    'title_ru' => 'Предложения, партнерство',
                    'title_uz' => 'Предложения, партнерство'
                ],

                [
                    'id' => '7',
                    'title_ru' => 'Другое',
                    'title_uz' => 'Другое'
                ],

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
