<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnInShops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE shops MODIFY COLUMN title_uz VARCHAR(255) NULL');
        DB::statement('ALTER TABLE shops MODIFY COLUMN address_uz TEXT NULL');
        DB::statement('ALTER TABLE shops MODIFY COLUMN phone VARCHAR(255) NULL');

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
