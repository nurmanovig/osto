<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeShopsTableColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shops', function(Blueprint $table){
            $table->dropColumn('title_ru');
            $table->dropColumn('title_uz');
            $table->dropColumn('address_ru');
            $table->dropColumn('address_uz');
            $table->string('title',255)->after('id');
            $table->text('address')->after('mall_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
