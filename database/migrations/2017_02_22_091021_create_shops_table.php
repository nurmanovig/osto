<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_ru');
            $table->string('title_uz');
            $table->integer('mall_type')->default(0);
            $table->integer('mall_id')->default(0);
            $table->integer('floor')->default(0);
            $table->integer('shop_number')->default(0);
            $table->string('contact_person');
            $table->text('address_ru');
            $table->text('address_uz');
            $table->string('phone');    
            $table->integer('user_id')->unsigned()->deault(0);
            $table->decimal('lat', 18, 14)->default(0);
            $table->decimal('lng', 18, 14)->default(0);
            $table->enum('is_recommended',[0,1]);
            $table->integer('view_amount')->default(0);
            $table->integer('like_amount')->default(0);
            $table->integer('dislike_amount')->default(0);
            $table->integer('product_amount')->default(0);
            $table->integer('phone_view_amount')->default(0);
            $table->boolean('day1')->default(0);
            $table->boolean('day2')->default(0);
            $table->boolean('day3')->default(0);
            $table->boolean('day4')->default(0);
            $table->boolean('day5')->default(0);
            $table->boolean('day6')->default(0);
            $table->boolean('day7')->default(0);
            $table->time('working_hour_start')->default(0);
            $table->time('working_hour_end')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shops');
    }
}
