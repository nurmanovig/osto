<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameOrdersIspayStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement("ALTER TABLE `orders` CHANGE `is_pay` `status` TINYINT(1) NOT NULL DEFAULT '0'");
        // Schema::table('orders', function (Blueprint $table) {
        //    $table->renameColumn('is_pay', 'status');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
