<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductsTableColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function(Blueprint $table){
            $table->dropColumn('is_recommended');
            $table->dropColumn('is_popular');
            $table->boolean('recommended')->default(0)->after('sex');
            $table->boolean('popular')->default(0)->after('sex');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table){
            $table->enum('is_recommended');
            $table->enum('is_popular');
        });
    }
}
