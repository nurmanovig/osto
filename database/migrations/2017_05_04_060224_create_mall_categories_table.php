<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMallCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mall_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned();
            $table->string('title_ru');
            $table->string('title_uz');
            $table->text('description_ru');
            $table->text('description_uz');
            $table->text('address_ru')->nullable();
            $table->text('address_uz')->nullable();
            $table->string('phone')->nullable();
            $table->decimal('lat', 18, 14)->default(0);
            $table->decimal('lng', 18, 14)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mall_categories');
    }
}
