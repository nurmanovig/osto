<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('summ');
            $table->string('username');
            $table->string('phone');
            $table->boolean('promo_code')->default(0);
            $table->text('address');
            $table->string('lat');
            $table->string('lng');
            $table->boolean('with_delivery')->default(0);
            $table->boolean('is_pay')->default(0);
            $table->enum('pay_type', ['payme', 'click', 'paynet']);
            $table->enum('currency', ['sum', 'usd'])->default("sum");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("orders");
    }
}
